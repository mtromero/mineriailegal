library(foreign)
library(rgdal)
library(rgeos)
library(maptools)
library(sp)
library(raster)
library(landsat)
library(RColorBrewer)
library(ggmap)
library(plotrix)
library(classInt)
setwd("H:/Copy/MineriaIlegal/")
municipios.sf<-readShapePoly("RawData\\shapefilefilesmunicipios\\municipios")
RoyaltiesUse=read.dta("CreatedData/RoyaltiesUse.dta")
colnames(RoyaltiesUse)[1]="CODANE2"

colors <- brewer.pal(4, "YlOrRd")

RoyaltiesUse$PropTransf= RoyaltiesUse$PropTransf*100
brks<-classIntervals(RoyaltiesUse$PropTrans, n=4, style="quantile")
brks<- brks$brk


pdf("Results/Graphs/SeveralYrs.pdf")
par(mfrow=c(2,3))
for(ano in c(2008,2009,2010,2012,2013,2014)){
municipios.plot=merge(municipios.sf,RoyaltiesUse[RoyaltiesUse$ano==ano,c("CODANE2","PropTransf")],all.y=F,all.x=T)
#set breaks for the 9 colors 
plot(municipios.plot, col=colors[findInterval(municipios.plot$PropTransf, brks,all.inside=TRUE)], axes=F, pch = 19,main=paste("Year",ano))
legend("bottomleft", legend=leglabs(round(brks)), fill=colors, bty="n",x.intersp = .5, y.intersp = .5)
}
dev.off()

for(ano in c(2008,2009,2010,2012,2013,2014)){
pdf(paste("Results/Graphs/Distr",ano,".pdf",sep=""))
municipios.plot=merge(municipios.sf,RoyaltiesUse[RoyaltiesUse$ano==ano,c("CODANE2","PropTransf")],all.y=F,all.x=T)
#set breaks for the 9 colors 
brks<-classIntervals(municipios.plot$PropTransf, n=9, style="quantile")
brks<- brks$brk
plot(municipios.plot, col=colors[findInterval(municipios.plot$PropTransf, brks,all.inside=TRUE)], axes=F, pch = 19,main=paste("Year",ano))
legend("bottomleft", legend=leglabs(round(brks)), fill=colors, bty="n",x.intersp = .5, y.intersp = .5)
 dev.off()
}


