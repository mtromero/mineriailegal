
if(Sys.info()["user"]=="mauri") {
  setwd("C:/Users/mauri/Dropbox/MineriaIlegal/")
  load("C:/Users/mauri/Dropbox/PYP_Birth/CreatedData/PanelMunicipalEEVV.RData")
  PanelMunicipalEEVV=data.frame(PanelMunicipalEEVV)
  PanelMunicipalEEVV=PanelMunicipalEEVV[which(PanelMunicipalEEVV$ANO<=2013),]
  PanelMunicipalEEVV$Date=PanelMunicipalEEVV$ANO+(PanelMunicipalEEVV$Quarter-1)/4
  PanelMunicipalEEVV=PanelMunicipalEEVV[sort.int(PanelMunicipalEEVV$Date,index.return=T)$ix,]
  CuantosMuni= aggregate(cbind(!is.na(PanelMunicipalEEVV$Produccion),!is.na(PanelMunicipalEEVV$AreaMinadaKm2)),by=list(PanelMunicipalEEVV$Date),FUN=sum)
  MuniProd= aggregate(cbind(PanelMunicipalEEVV$Produccion>0,PanelMunicipalEEVV$AreaMinadaKm2>0),by=list(PanelMunicipalEEVV$Date),FUN=sum)
  MuniProd[,c(2,3)]=MuniProd[,c(2,3)]/CuantosMuni[,c(2,3)]
  ProdArea= aggregate(cbind(PanelMunicipalEEVV$Produccion,PanelMunicipalEEVV$AreaMinadaKm2),by=list(PanelMunicipalEEVV$Date),FUN=sum)
  ProdArea$Prod=ProdArea$V2/ProdArea$V1
}
ProdArea=ProdArea[complete.cases(ProdArea),]
plot(ProdArea[,1],ProdArea$Prod,ylab="grs/m2",xlab="ano")

library(foreign)
library(gplots)
library(Hmisc)
library(rgdal)
library(raster)
library(rgeos)
library(sp)
library(scales)
library(maptools)
library(GISTools)
library(RColorBrewer)
library(classInt)
#library(xlsx)
library(data.table)
library(dismo)
library(xtable)
library(readstata13)
library(ggplot2)
library(reshape2)
library(grid)
library(gridExtra)
library(gtable)
library(emoa)
library(plyr)
library(stringr)
library(pBrackets)
library(metafor)




cm_bd_final=read.dta("RawData/CensoMinero/cm_bd_final.dta",convert.factors=F)

cm_bd_final=cm_bd_final[cm_bd_final$at_pyr_03_01_07==1,]

cm_bd_final$ag_ue_06
