﻿library(raster)
library(sp)
library(maptools)
library(rgdal)
library(maptools)
library(rgeos)
library(doParallel)
library(foreach)
projectionAll="+proj=eqdc +lat_0=-32 +lon_0=-60 +lat_1=-5 +lat_2=-42 +x_0=0 +y_0=0 +ellps=aust_SA +units=m +no_defs"
#setwd("/media/mauricio/TeraHDD1/Copy/MineriaIlegal/")
#setwd("C:/Users/Mauricio/Copy/MineriaIlegal/")
setwd("/media/mauricio/TeraHDD1/Dropbox/MineriaIlegal")

###########################
##The municipality data
###########################
load(file='RawData/Municipios_GIS.RData')


###########################
##The muinin titles data
###########################
Info_Titulos=read.csv("RawData/Tierra_Minada/Info_Titulos.csv")
Info_Titulos$FECHA.INSCRIPCION=as.Date(as.character(Info_Titulos$FECHA.INSCRIPCION),format="%m/%d/%Y")
Info_Titulos$FECHA.TERMINACION=as.Date(as.character(Info_Titulos$FECHA.TERMINACION),format="%m/%d/%Y")
colnames(Info_Titulos)[3]="CODIGO_RMN"

TITULOS_PAIS <- readOGR("RawData/Tierra_Minada","TITULOS_PAIS")
TITULOS_PAIS <- spTransform(TITULOS_PAIS, CRS(projectionAll))

DataKeep=TITULOS_PAIS@data
TITULOS_PAIS=gBuffer(TITULOS_PAIS,byid=T,width=0)
TITULOS_PAIS=SpatialPolygonsDataFrame(TITULOS_PAIS, DataKeep, match.ID = TRUE)


Info_Titulos$CODIGO_RMN=as.character(Info_Titulos$CODIGO_RMN)
TITULOS_PAIS$CODIGO_RMN=as.character(TITULOS_PAIS$CODIGO_RMN)

TITULOS_PAIS=TITULOS_PAIS[!duplicated(TITULOS_PAIS$CODIGO_RMN),]
Info_Titulos=Info_Titulos[!duplicated(Info_Titulos$CODIGO_RMN),]

TITULOS_PAIS@data=merge(TITULOS_PAIS@data,Info_Titulos,by="CODIGO_RMN",all.x=T,all.y=F)

TITULOS_PAIS<-TITULOS_PAIS[!(TITULOS_PAIS@data$MODALIDAD=="LICENCIA DE EXPLORACION"),]
##which permits are "gold" related
INDEX_ORO=which(grepl("ORO",as.character(TITULOS_PAIS$MINERALES.x)))
MINAS_ORO=TITULOS_PAIS[INDEX_ORO,]


###########################
##DIVIPOLA (ADMING BOUNDRIES COLOMBIA)
###########################
DIVIPOLA_20130930=read.csv('RawData/DIVIPOLA_20130930.csv',fileEncoding="latin1")
DIVIPOLA_20130930=DIVIPOLA_20130930[which(DIVIPOLA_20130930$Tipo=="CM" | DIVIPOLA_20130930$Tipo=="CD"),]
DIVIPOLA_20130930$Nombre.Municipio=as.character(DIVIPOLA_20130930$Nombre.Municipio)
DIVIPOLA_20130930$Nombre.Municipio=chartr('áéíóúüÁÉÍÓÚÜñÑ','aeiouuAEIOUUnN',DIVIPOLA_20130930$Nombre.Municipio)
DIVIPOLA_20130930$Nombre.Departamento=as.character(DIVIPOLA_20130930$Nombre.Departamento)
DIVIPOLA_20130930$Nombre.Departamento=chartr('áéíóúüÁÉÍÓÚÜñÑ','aeiouuAEIOUUnN',DIVIPOLA_20130930$Nombre.Departamento)
DIVIPOLA_20130930$Municipio=DIVIPOLA_20130930$Nombre.Municipio
DIVIPOLA_20130930$Departamento=DIVIPOLA_20130930$Nombre.Departamento
DIVIPOLA_20130930$CodigoDane=DIVIPOLA_20130930$Codigo.Municipio
DIVIPOLA_20130930=DIVIPOLA_20130930[,c("Departamento","Municipio","CodigoDane")]
DIVIPOLA_20130930$Municipio=tolower(DIVIPOLA_20130930$Municipio)
DIVIPOLA_20130930$Departamento=tolower(DIVIPOLA_20130930$Departamento)
save(DIVIPOLA_20130930,file='CreatedData/DIVIPOLA.RData')
write.csv(DIVIPOLA_20130930,file='CreatedData/DIVIPOLA.csv')

###########################
## GOLD production data
###########################

goldProduction=read.csv('RawData/ProduccionOro_2015.csv',fileEncoding="latin1")
goldProduction$Municipio=as.character(goldProduction$Municipio)
goldProduction$Municipio=sub(" ","",goldProduction$Municipio)
goldProduction$Departamento=sub(" ","",goldProduction$Departamento)
goldProduction$Municipio=chartr('áéíóúüÁÉÍÓÚÜñÑ','aeiouuAEIOUUnN',goldProduction$Municipio)   
goldProduction$Departamento=as.character(goldProduction$Departamento)
goldProduction$Departamento=chartr('áéíóúüÁÉÍÓÚÜñÑ','aeiouuAEIOUUnN',goldProduction$Departamento)  
goldProduction$Municipio=tolower(goldProduction$Municipio)
goldProduction$Departamento=tolower(goldProduction$Departamento)
MunicipiosMerge=DIVIPOLA_20130930[,c("Departamento","Municipio","CodigoDane")]
ProduccionMunicipal=merge(goldProduction, MunicipiosMerge, by = c('Departamento', 'Municipio'),all=T)
ProduccionMunicipal=ProduccionMunicipal[!is.na(ProduccionMunicipal$CodigoDane),] #Borrar todo lo que no tenga codigo dane
ProduccionMunicipal=data.frame(ProduccionMunicipal)
ProduccionMunicipal[is.na(ProduccionMunicipal)]=0
ProduccionMunicipal=aggregate(ProduccionMunicipal[,setdiff(colnames(ProduccionMunicipal),c("Departamento","Municipio","CodigoDane"))],by=list(ProduccionMunicipal$CodigoDane),FUN=sum)
colnames(ProduccionMunicipal)[1]=c("CodigoDane")

PanelProduccion=reshape(ProduccionMunicipal, varying = list(setdiff(colnames(ProduccionMunicipal),c("CodigoDane"))),direction="long",idvar="CodigoDane")
PanelProduccion$Year=rep(2004:2015, each=length(unique(PanelProduccion$CodigoDane))*4)[1:dim(PanelProduccion)[1]]
PanelProduccion$Quarter=rep(1:4, each=length(unique(PanelProduccion$CodigoDane)),length.out=dim(PanelProduccion)[1])
PanelProduccion$Produccion=PanelProduccion$X2004.Marzo
PanelProduccion$CodigoDane=as.numeric(as.character(PanelProduccion$CodigoDane))
PanelProduccion=subset(PanelProduccion,  select=c(CodigoDane,Year,Quarter,Produccion))


#################
#################

MinasPedacitos=NULL
for(k in 1:dim(MINAS_ORO)[1]){
    for (i in 1:dim(Municipios)[1]){
    Intersection=gIntersection(Municipios[i, ], MINAS_ORO[k,])
    if(!is.null(Intersection)){
     row.names(Intersection)=paste(i,k)
     datos=data.frame(cbind(Municipios@data[i,"CODANE2"],MINAS_ORO@data[k,c("CODIGO_RMN","FECHA.TERMINACION","FECHA.INSCRIPCION")]))
     colnames(datos)=c("CODANE2","CODIGO_RMN","FECHA.TERMINACION","FECHA.INSCRIPCION")
     datos$CODIGO_RMN=as.character(datos$CODIGO_RMN)
     row.names(datos)=paste(i,k)
     Intersection=SpatialPolygonsDataFrame(Intersection,datos)
     if(!is.null(MinasPedacitos)) MinasPedacitos=spRbind(MinasPedacitos, Intersection)
     if(is.null(MinasPedacitos)) MinasPedacitos=Intersection
    }
}
}

PanelProduccion$YearQ <- as.yearqtr(paste0(PanelProduccion$Year,"-",PanelProduccion$Quarter), format = "%Y-%q")
PanelProduccion$Area=0  
for(i in 1:dim(PanelProduccion)[1]){
if( sum(MinasPedacitos$CODANE2==PanelProduccion$CodigoDane[i] &  as.yearqtr(MinasPedacitos$FECHA.INSCRIPCION, format = "%Y-%m-%d")<PanelProduccion$YearQ[i] &  as.yearqtr(MinasPedacitos$FECHA.TERMINACION, format = "%Y-%m-%d")>PanelProduccion$YearQ[i],na.rm=T)>0) PanelProduccion$Area[i]=1
}

PanelProduccion$ProduccionD=(PanelProduccion$Produccion>0)
PanelProduccion$MisReport=(PanelProduccion$Produccion>0 &  PanelProduccion$Area==0 )


plot(aggregate(PanelProduccion$MisReport,by=list(PanelProduccion$YearQ),FUN=mean),pch=19,ylab="Proportion of municipalities with prod>0 and area=0",xlab="quarter")

plot(aggregate(PanelProduccion$MisReport[PanelProduccion$Produccion>0],by=list(PanelProduccion$YearQ[PanelProduccion$Produccion>0]),FUN=mean),pch=19,ylab="Proportion of municipalities with prod>0, for which area=0",xlab="quarter")



