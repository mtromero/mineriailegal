library(rvest)

mainPath<-"C:/Users/santi/Dropbox/MineriaIlegal/CreatedData"

# Import lista cedulas
dataowners=read.csv(file=paste0(mainPath,"/gold_sellers_cedulas.csv"))
dataowners$depto_vota=""
dataowners$muni_vota=""
dataowners$puesto_vota=""
dataowners$dirpuesto_vota=""
dataowners$fechainscri_vota=""
dataowners$mesa_vota=""
# Ciclo sobre las cedulas

#for (i in nrow(dataowners):1) {
  for (i in 45353:1) {
  
  html <- read_html(paste0("https://wsp.registraduria.gov.co/censo/_censoResultado.php?nCedula=",dataowners$CEDULAONITVENDEDOR[i],"&nCedulaH=&x=77&y=6"))
  # Note the td td weird thing for html_nodes were obtained by visual inspection of the registraduria webpage
  # using selector gadget http://selectorgadget.com/
  #Click on the box you want to extract the text from and the tool display the name of the "cell"
  
  todo <- html_nodes(html,"td+ td")
  
  #Need to check the person alive and registered to vote
  if (length(todo)>0) {
    dataowners$depto_vota[i]<- html_text(todo[1])
    dataowners$muni_vota[i] <- html_text(todo[2])
    dataowners$puesto_vota[i] <- html_text(todo[3])
    dataowners$dirpuesto_vota[i] <- html_text(todo[4])
    dataowners$fechainscri_vota[i] <- html_text(todo[5])
    dataowners$mesa_vota[i] <- html_text(todo[6])
  } else {
    #Check if person is dead
    todo <- html_nodes(html,"strong")
    if (length(todo)==3) {
      dataowners$puesto_vota[i] <- html_text(todo[2])
      todo <- html_nodes(html,"li")
      dataowners$fechainscri_vota[i] <- substr(html_text(todo),regexpr("No.",html_text(todo)),9+regexpr("a�o",html_text(todo)))
    } else {
      dataowners$puesto_vota[i] <- "No registrado"
      dataowners$fechainscri_vota[i] <- "9Mar2017"
    }
  }
  
} # End for owners

write.csv(dataowners,file=paste0(mainPath,"/gold_sellers_scrapvota_Mar9_2017.csv"))