load("CreatedData/InputsRepartidor.RData")
FactorFDR=merge(merge(merge(FactorFDR,NBI_Departamental),PoblacionDepartamental),NombresDeps)

#FDR#
pdf("Results/Graphs/FDR_NBI.pdf")
plot(FactorFDR$NBI,FactorFDR$FactorFDR,type="p",xlab="NBI",ylab="Proportion of FDR",pch=19)
text(FactorFDR$NBI,FactorFDR$FactorFDR,labels=FactorFDR$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FDR_Pop.pdf")
FactorFDR$PoblacionRelativa=FactorFDR$Poblacion/sum(FactorFDR$Poblacion)
plot(FactorFDR$PoblacionRelativa,FactorFDR$FactorFDR,type="p",xlab="Pop. share",ylab="Proportion of FDR",pch=19)
text(FactorFDR$PoblacionRelativa,FactorFDR$FactorFDR,labels=FactorFDR$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()


FactorFCR=FunFactorFCR(NBI_Departamental,NBI_Municipal,PoblacionDepartamental,DesempleoDepartamental,DemepleoNacional,NBI_Pais,PoblacionPais)

FactorFCR=merge(merge(merge(merge(FactorFCR,NBI_Departamental),PoblacionDepartamental),NombresDeps),DesempleoDepartamental)

FactorTotal=merge(FactorFCR,FactorFDR)
#Of non direct assignments (POT FUNDS), 60% goes to FCR and 40% goes to FDR.
#Of the 60% that goes to FCR, 60% is determined by povverty. 50% by PobrezaDep and 50% by pobreza muno
FactorTotal$FactorTotal=FactorTotal$FactorFDR*0.4+0.6*(0.6*(0.5*FactorTotal$FactorPobrezaDepartamental+0.5*FactorTotal$FactorPobrezaMunicipal)+0.3*0+0.1*0)

#FCR - pobreza muni
pdf("Results/Graphs/FDR_NBI_PM.pdf")
plot(FactorFCR$NBI,FactorFCR$FactorPobrezaMunicipal,type="p",pch=19,xlab="NBI",ylab="Proportion of FCR - by Pobreza Municipal")
text(FactorFCR$NBI,FactorFCR$FactorPobrezaMunicipal,labels=FactorFCR$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FDR_Pop_PM.pdf")
FactorFCR$PoblacionRelativa=FactorFCR$Poblacion/sum(FactorFCR$Poblacion)
plot(FactorFCR$PoblacionRelativa,FactorFCR$FactorPobrezaMunicipal,type="p",pch=19,xlab="Pop. share",ylab="Proportion of FCR - by Pobreza Municipal")
text(FactorFCR$PoblacionRelativa,FactorFCR$FactorPobrezaMunicipal,labels=FactorFCR$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FDR_Desmp_PM.pdf")
plot(FactorFCR$TD,FactorFCR$FactorPobrezaMunicipal,type="p",pch=19,xlab="Unemployment Rate",ylab="Proportion of FCR - by Pobreza Municipal")
text(FactorFCR$TD,FactorFCR$FactorPobrezaMunicipal,labels=FactorFCR$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

#FCR - pobreza Dep
pdf("Results/Graphs/FDR_NBI_PD.pdf",width=12)
plot(FactorFCR$NBI,FactorFCR$FactorPobrezaDepartamental,type="p",pch=19,xlab="NBI",ylab="Proportion of FCR - by Pobreza Departamental")
text(FactorFCR$NBI,FactorFCR$FactorPobrezaDepartamental,labels=FactorFCR$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()


 pdf("Results/Graphs/FDR_NBI_PD_2.pdf",width=12)
plot(FactorFCR$NBI,FactorFCR$FactorPobrezaDepartamental,type="p",xlab="NBI",ylab="Proportion of FCR - by Pobreza Departamental", cex=FactorFCR$PoblacionRelativa*100)
text(FactorFCR$NBI,FactorFCR$FactorPobrezaDepartamental,labels=FactorFCR$Nombre, cex= 0.5, adj = c(0,-1))
abline(v=30)
dev.off()

pdf("Results/Graphs/FDR_Pop_PD.pdf")
FactorFCR$PoblacionRelativa=FactorFCR$Poblacion/sum(FactorFCR$Poblacion)
plot(FactorFCR$PoblacionRelativa,FactorFCR$FactorPobrezaDepartamental,type="p",pch=19,xlab="Pop. share",ylab="Proportion of FCR - by Pobreza Departamental")
text(FactorFCR$PoblacionRelativa,FactorFCR$FactorPobrezaDepartamental,labels=FactorFCR$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FDR_Desmp_PD.pdf")
plot(FactorFCR$TD,FactorFCR$FactorPobrezaDepartamental,type="p",pch=19,xlab="Unemployment Rate",ylab="Proportion of FCR - by Pobreza Departamental")
text(FactorFCR$TD,FactorFCR$FactorPobrezaDepartamental,labels=FactorFCR$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

#Total
pdf("Results/Graphs/FactorTotal_NBI.pdf")
plot(FactorTotal$NBI,FactorTotal$FactorTotal,type="p",pch=19,xlab="NBI",ylab="Proportion of common funds")
text(FactorTotal$NBI,FactorTotal$FactorTotal,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FactorTotal_Pop.pdf")
FactorTotal$PoblacionRelativa=FactorTotal$Poblacion/sum(FactorTotal$Poblacion)
plot(FactorTotal$PoblacionRelativa,FactorTotal$FactorTotal,type="p",pch=19,xlab="Pop. share",ylab="Proportion of common funds")
text(FactorTotal$PoblacionRelativa,FactorTotal$FactorTotal,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FactorTotal_Desmp.pdf")
plot(FactorTotal$TD,FactorTotal$FactorTotal,type="p",pch=19,xlab="Unemployment Rate",ylab="Proportion of common funds")
text(FactorTotal$TD,FactorTotal$FactorTotal,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()