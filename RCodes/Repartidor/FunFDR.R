FunFactorFDR=function(NBI_Departamental,PoblacionDepartamental,NBI_Pais,PoblacionPais){
#Articulo 33, Ley 1530 de 2012
NBI_Departamental[,3]=NBI_Departamental[,2]/NBI_Pais
PoblacionDepartamental[,3]=PoblacionDepartamental[,2]/PoblacionPais

Factor=(NBI_Departamental[,3]^(0.4))*(PoblacionDepartamental[,3]^(0.6))

Factor=Factor/sum(Factor)

FactorFDR=data.frame(NBI_Departamental[,1],Factor)
colnames(FactorFDR)=c("DP","FactorFDR")
return(FactorFDR)
}