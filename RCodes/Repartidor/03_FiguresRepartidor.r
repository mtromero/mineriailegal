load("CreatedData/InputsRepartidor.RData")
FactorFDR=merge(merge(merge(FactorFDR,NBI_Departamental),PoblacionDepartamental),NombresDeps)

#FDR#
pdf("Results/Graphs/FDR_NBI.pdf")
plot(FactorFDR$NBI,FactorFDR$FactorFDR,type="p",xlab="NBI",ylab="Proportion of FDR",pch=19)
text(FactorFDR$NBI,FactorFDR$FactorFDR,labels=FactorFDR$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FDR_Pop.pdf")
FactorFDR$PoblacionRelativa=FactorFDR$Poblacion/sum(FactorFDR$Poblacion)
plot(FactorFDR$PoblacionRelativa,FactorFDR$FactorFDR,type="p",xlab="Pop. share",ylab="Proportion of FDR",pch=19)
text(FactorFDR$PoblacionRelativa,FactorFDR$FactorFDR,labels=FactorFDR$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()



FactorDptoFCR=merge(merge(merge(merge(FactorDptoFCR,NBI_Departamental),PoblacionDepartamental),NombresDeps),DesempleoDepartamental)

FactorTotal=merge(FactorFDR,FactorDptoFCR)


#FCR - pobreza muni
pdf("Results/Graphs/FDR_NBI_PM.pdf")
plot(FactorTotal$NBI,FactorTotal$FactorPobrezaMunicipal,type="p",pch=19,xlab="NBI",ylab="Proportion of FCR - by Pobreza Municipal")
text(FactorTotal$NBI,FactorTotal$FactorPobrezaMunicipal,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,0))
dev.off()

pdf("Results/Graphs/FDR_Pop_PM.pdf")
FactorTotal$PoblacionRelativa=FactorTotal$Poblacion/sum(FactorTotal$Poblacion)
plot(FactorTotal$PoblacionRelativa,FactorTotal$FactorPobrezaMunicipal,type="p",pch=19,xlab="Pop. share",ylab="Proportion of FCR - by Pobreza Municipal")
text(FactorTotal$PoblacionRelativa,FactorTotal$FactorPobrezaMunicipal,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FDR_Desmp_PM.pdf")
plot(FactorTotal$TD,FactorTotal$FactorPobrezaMunicipal,type="p",pch=19,xlab="Unemployment Rate",ylab="Proportion of FCR - by Pobreza Municipal")
text(FactorTotal$TD,FactorTotal$FactorPobrezaMunicipal,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

#FCR - pobreza Dep
pdf("Results/Graphs/FDR_NBI_PD.pdf",width=12)
plot(FactorTotal$NBI,FactorTotal$FactorPobrezaDepartamental,type="p",pch=19,xlab="NBI",ylab="Proportion of FCR - by Pobreza Departamental")
text(FactorTotal$NBI,FactorTotal$FactorPobrezaDepartamental,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()


 pdf("Results/Graphs/FDR_NBI_PD_2.pdf",width=12)
plot(FactorTotal$NBI,FactorTotal$FactorPobrezaDepartamental,type="p",xlab="NBI",ylab="Proportion of FCR - by Pobreza Departamental", cex=FactorTotal$PoblacionRelativa*100)
text(FactorTotal$NBI,FactorTotal$FactorPobrezaDepartamental,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
abline(v=30)
dev.off()

pdf("Results/Graphs/FDR_Pop_PD.pdf")
FactorTotal$PoblacionRelativa=FactorTotal$Poblacion/sum(FactorTotal$Poblacion)
plot(FactorTotal$PoblacionRelativa,FactorTotal$FactorPobrezaDepartamental,type="p",pch=19,xlab="Pop. share",ylab="Proportion of FCR - by Pobreza Departamental")
text(FactorTotal$PoblacionRelativa,FactorTotal$FactorPobrezaDepartamental,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FDR_Desmp_PD.pdf")
plot(FactorTotal$TD,FactorTotal$FactorPobrezaDepartamental,type="p",pch=19,xlab="Unemployment Rate",ylab="Proportion of FCR - by Pobreza Departamental")
text(FactorTotal$TD,FactorTotal$FactorPobrezaDepartamental,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

#Total
pdf("Results/Graphs/FactorTotal_NBI.pdf")
plot(FactorTotal$NBI,FactorTotal$FactorDptoFCR,type="p",pch=19,xlab="NBI",ylab="Proportion of common funds")
text(FactorTotal$NBI,FactorTotal$FactorDptoFCR,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FactorTotal_Pop.pdf")
FactorTotal$PoblacionRelativa=FactorTotal$Poblacion/sum(FactorTotal$Poblacion)
plot(FactorTotal$PoblacionRelativa,FactorTotal$FactorTotal,type="p",pch=19,xlab="Pop. share",ylab="Proportion of common funds")
text(FactorTotal$PoblacionRelativa,FactorTotal$FactorTotal,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()

pdf("Results/Graphs/FactorTotal_Desmp.pdf")
plot(FactorTotal$TD,FactorTotal$FactorTotal,type="p",pch=19,xlab="Unemployment Rate",ylab="Proportion of common funds")
text(FactorTotal$TD,FactorTotal$FactorTotal,labels=FactorTotal$Nombre, cex= 0.5, adj = c(0,-1))
dev.off()