load("CreatedData/InputsRepartidor.RData")
load("RawData/Municipios_GIS.RData")
Municipios$MunicipioDirecto=NA
Municipios$MunicipioIndirecto=NA
Municipios$Municipios=NA

source("/media/mauricio/TeraHDD1/git/mineriailegal/RCodes/Repartidor/FunPesoMunicipio.r")

for(i in 1:dim(Municipios)[1]){
  Reparticion=CuantoMunicipio(1,Municipios$CodigoDane[i],as.numeric(as.character(Municipios$DPTO_DPTO_[i])),FactorFDR,FactorDptoFCR,FactorMunicipal,PoblacionMunicipal,PoblacionDepartamental)
  Municipios$MunicipioDirecto[i]=Reparticion$MunicipioDirecto
  Municipios$MunicipioIndirecto[i]=Reparticion$MunicipioIndirecto
}
Municipios$TransferenciaTotal=Municipios$MunicipioDirecto+Municipios$MunicipioIndirecto
Municipios=Municipios[sort.int(Municipios$MunicipioDirecto,index.return=T)$ix,]

colors <- brewer.pal(4, "YlOrRd")
brks<-classIntervals(Municipios$TransferenciaTotal, n=5, style="quantile")
brks<- brks$brk
plot(Municipios, col=colors[findInterval(Municipios$TransferenciaTotal, brks,all.inside=TRUE)], axes=F, pch = 19)
legend("bottomleft", legend=leglabs(round(brks,4)), fill=colors, bty="n",x.intersp = .5, y.intersp = .5)


hist(Municipios$TransferenciaTotal)
