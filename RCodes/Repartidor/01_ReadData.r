setwd("C:/Users/Mauricio/Copy/MineriaIlegal/")
source("C:/Users/Mauricio/Documents/git//mineriailegal/RCodes/Repartidor/FunFDR.R")
source("C:/Users/Mauricio/Documents/git//mineriailegal/RCodes/Repartidor/FunFCR.R")


#setwd("/media/mauricio/TeraHDD1/Dropbox/MineriaIlegal")
#source("/media/mauricio/TeraHDD1/git/mineriailegal/RCodes/Repartidor/FunFDR.R")
#source("/media/mauricio/TeraHDD1/git/mineriailegal/RCodes/Repartidor/FunFCR.R")

library(RODBC)
library(gdata)
library(stringr)
library(raster)
library(sp)
library(rgdal)
library(maptools)
library(rgeos)
library(RColorBrewer)
library(classInt)
library(foreign)


trim <- function (x) gsub("^\\s+|\\s+$", "", x)

PoblacionDepartamental <- read.csv("RawData/ProyeccionesPoblacion.csv",header = TRUE)
PoblacionDepartamental=PoblacionDepartamental[,1:38]
PoblacionDepartamental=data.frame(PoblacionDepartamental)
PoblacionDepartamental=data.frame(PoblacionDepartamental$DP,PoblacionDepartamental$X2011)
colnames(PoblacionDepartamental)=c("DP","Poblacion")
PoblacionDepartamental$Poblacion=as.numeric(gsub(",","",as.character(PoblacionDepartamental$Poblacion)))
PoblacionDepartamental=PoblacionDepartamental[which(!is.na(PoblacionDepartamental$Poblacion)),]
PoblacionPais=PoblacionDepartamental$Poblacion[which(PoblacionDepartamental$DP==0)]
PoblacionDepartamental=PoblacionDepartamental[-which(PoblacionDepartamental$DP==0),]

PoblacionMunicipal <- read.csv("RawData/PoblacionMunicipal.csv",header = TRUE)
PoblacionMunicipal=data.frame(PoblacionMunicipal)
PoblacionMunicipal=data.frame(PoblacionMunicipal$DPMP,PoblacionMunicipal$X2011)
colnames(PoblacionMunicipal)=c("codmpio","Poblacion")


CategoriasMunicipios <- read.csv("RawData/CategoriasMunicipios.csv",header = TRUE)
CategoriasMunicipios=data.frame(CategoriasMunicipios)
colnames(CategoriasMunicipios)=c("codmpio","Cat")


NBI_Departamental=read.csv("RawData/NBI_Departamental.csv",header = TRUE)
# channel <- odbcConnectExcel2007("RawData/NBI_Municipal.xls")
# NBI_Departamental=sqlFetch(channel, "Departamentos",stringsAsFactors=F) # get the lot_
# close(channel)
NombresDeps=NBI_Departamental[-1,c(1,2)]
colnames(NombresDeps)=c("DP","Nombre")
NombresDeps$Nombre=gsub("\\(.*","",NombresDeps$Nombre)
NombresDeps$Nombre=gsub("","",NombresDeps$Nombre)
NombresDeps$Nombre=trim(NombresDeps$Nombre)
save(NombresDeps,file="CreatedData/DepNombres.RData")


NBI_Departamental=NBI_Departamental[,c(1,5)]
colnames(NBI_Departamental)=c("DP","NBI")
NBI_Departamental$NBI=as.numeric(as.character(NBI_Departamental$NBI))
NBI_Departamental=NBI_Departamental[which(!is.na(NBI_Departamental$NBI)),]

NBI_Pais=NBI_Departamental$NBI[which(NBI_Departamental$DP==0)]
NBI_Departamental=NBI_Departamental[-which(NBI_Departamental$DP==0),]


# channel <- odbcConnectExcel2007("RawData/NBI_Municipal.xls")
# NBI_Municipal=sqlFetch(channel, "Municipios",stringsAsFactors=F) # get the lot_
# close(channel)
NBI_Municipal=read.csv("RawData/NBI_Municipal.csv",header = TRUE)
NBI_Municipal=NBI_Municipal[,c(1,3,7)]
NBI_Municipal=data.frame(paste0(str_pad(NBI_Municipal[,1], 2, pad = "0") ,str_pad(NBI_Municipal[,2], 3, pad = "0")),NBI_Municipal[,3])
colnames(NBI_Municipal)=c("codmpio","NBI")
NBI_Municipal$NBI=as.numeric(as.character(NBI_Municipal$NBI))
NBI_Municipal=NBI_Municipal[which(!is.na(NBI_Municipal$NBI)),]


# channel <- odbcConnectExcel2007("RawData/DesempleoDepartamental2012.xls")
# DesempleoDepartamental=sqlFetch(channel, "Departamentos anual",stringsAsFactors=F) # get the lot_
# close(channel)
DesempleoDepartamental=read.csv("RawData/DesempleoDepartamental2012.csv")
DesempleoDepartamental=DesempleoDepartamental[,c(1,12)]
DesempleoDepartamental=cbind(NombresDeps[1:24,],DesempleoDepartamental[which(DesempleoDepartamental[,1]=="TD"),2])
colnames(DesempleoDepartamental)=c("DP","NAME","TD")
DesempleoDepartamental$TD=as.numeric(as.character(DesempleoDepartamental$TD))

DesempleoDepartamentalNuevos=read.csv("RawData/DesempleoDepartamental_Nuevos2012.csv")
# DesempleoDepartamental=DesempleoDepartamental[,c(1,12)]
# channel <- odbcConnectExcel2007("RawData/DesempleoDepartamental_Nuevos2012.xls")
# DesempleoDepartamentalNuevos=sqlFetch(channel, "Nuevos Dptos Total Nuevos dptos",stringsAsFactors=F) # get the lot_
# close(channel)
DesempleoDepartamentalNuevos=DesempleoDepartamentalNuevos[,c(1,2)]
TD=DesempleoDepartamentalNuevos[which(DesempleoDepartamentalNuevos[,1]=="TD"),2]
DesempleoDepartamentalNuevos=cbind(NombresDeps[25:33,],c(TD[2:4],8,TD[5:9]))
colnames(DesempleoDepartamentalNuevos)=c("DP","NAME","TD")
DesempleoDepartamental=rbind(DesempleoDepartamental,DesempleoDepartamentalNuevos)
DesempleoDepartamental=DesempleoDepartamental[,c(1,3)]

DemepleoNacional=10.4


FactorFDR=FunFactorFDR(NBI_Departamental,PoblacionDepartamental,NBI_Pais,PoblacionPais)
FactorFCR=FunFactorFCR(NBI_Departamental,NBI_Municipal,PoblacionDepartamental,PoblacionMunicipal,CategoriasMunicipios,DesempleoDepartamental,DemepleoNacional,NBI_Pais,PoblacionPais)

FactorDptoFCR=FactorFCR$FactorDptoFCR
FactorMpioFCR=FactorFCR$FactorMpioFCR

save(NombresDeps,PoblacionDepartamental,PoblacionMunicipal,NBI_Departamental,NBI_Pais,NBI_Municipal,DesempleoDepartamental,DemepleoNacional,PoblacionPais,CategoriasMunicipios,FactorFDR,FactorDptoFCR,FactorMpioFCR,file="CreatedData/InputsRepartidor.RData")



Precios=read.csv("RawData/SIMCO/Precios_minerales_1990_2012.csv")