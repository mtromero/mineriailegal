##First lets read data
setwd("C:/Users/Mauricio/Dropbox/MineriaIlegal")
source("C:/Users/Mauricio/Documents/git/mineriailegal/RCodes/Repartidor/FunPesoMunicipio.r")
source("C:/Users/Mauricio/Documents/git/mineriailegal/RCodes/Repartidor/FunPortuarios.R")


library(foreign)
library(stringr)
library(data.table)
ProduccionOro_Anual=read.dta("CreatedData/ProduccionOro_Anual.dta")
ProduccionCarbon_Anual=read.dta("CreatedData/ProduccionCarbon_Anual.dta")
ProduccionPlata_Anual=read.dta("CreatedData/ProduccionPlata_Anual.dta")
ProduccionSal_Anual=read.dta("CreatedData/ProduccionSal_Anual.dta")
ProduccionPlatino_Anual=read.dta("CreatedData/ProduccionPlatino_Anual.dta")
ProduccionGasPetroleo_Anual=read.dta("CreatedData/ProduccionGasPetroleo_Anual.dta")
load("CreatedData/InputsRepartidor.RData") 


#Primero hago el average de los anos antes
ProduccionOro_Anual=setDT(ProduccionOro_Anual)
ProduccionOro_Anual=ProduccionOro_Anual[ano<=2010,list(prod_oro=mean(prod_oro,na.rm=T)),by="codmpio"]


ProduccionCarbon_Anual=setDT(ProduccionCarbon_Anual)
ProduccionCarbon_Anual=ProduccionCarbon_Anual[ano<=2010,list(prod_carbon=mean(prod_carbon,na.rm=T)),by="codmpio"]

ProduccionPlata_Anual=setDT(ProduccionPlata_Anual)
ProduccionPlata_Anual=ProduccionPlata_Anual[ano<=2010,list(prod_plata=mean(prod_plata,na.rm=T)),by="codmpio"]

ProduccionSal_Anual=setDT(ProduccionSal_Anual)
ProduccionSal_Anual=ProduccionSal_Anual[ano<=2010,list(prod_sal=mean(prod_sal,na.rm=T)),by="codmpio"]

ProduccionPlatino_Anual=setDT(ProduccionPlatino_Anual)
ProduccionPlatino_Anual=ProduccionPlatino_Anual[ano<=2010,list(prod_platino=mean(prod_platino,na.rm=T)),by="codmpio"]

ProduccionGasPetroleo_Anual=setDT(ProduccionGasPetroleo_Anual)
ProduccionGasPetroleo_Anual=ProduccionGasPetroleo_Anual[ano<=2010,list(prod_petroleo=mean(prod_petroleo,na.rm=T),prod_gas=mean(prod_gas,na.rm=T)),by="codmpio"]


##Listo ahora multiplicamos por los precios. Voy a usar el de 2010 y en USD... en q moneda esta no deberia ser importante pues la final todo lo vamos a medir con los mismos precios y en la misma moenda!

#PreciosMetales=read.csv("RawData/BanRep/PreciosMetales_Banrep.csv")
#PreciosMetales=setDT(PreciosMetales)
#PreciosMetales[,Fecha:=as.Date(Fecha,format="%d/%m/%Y")]
#PreciosMetales=PreciosMetales[Fecha>=as.Date("01/01/2010",format="%d/%m/%Y") & Fecha<=as.Date("31/12/2010",format="%d/%m/%Y"),list(Platino=mean(Platino),Plata=mean(Plata),Oro=mean(Oro))]

ProduccionOro_Anual[,valor_oro:=prod_oro*1224.39]
ProduccionPlata_Anual[,valor_plata:=prod_plata*20.15]
ProduccionPlatino_Anual[,valor_platino:=prod_platino*1583.92]
ProduccionGasPetroleo_Anual[,valor_petroleo:=prod_petroleo*79.48]
ProduccionCarbon_Anual[,valor_carbon:=prod_carbon*291.44]

##CUANTO SE VA EN REGALIAS

ProduccionOro_Anual[,regalias_oro:=valor_oro*0.04] #se asume q nada es oro aluvion...
ProduccionPlata_Anual[,regalias_plata:=valor_plata*0.04]
ProduccionPlatino_Anual[,regalias_platino:=valor_platino*0.05]

ProduccionCarbon_Anual[prod_carbon>3,regalias_carbon:=valor_carbon*0.1]
ProduccionCarbon_Anual[prod_carbon<=3,regalias_carbon:=valor_carbon*0.05]    

ProduccionGasPetroleo_Anual[prod_petroleo<=5000,regalias_petroleo:=valor_petroleo*0.08]
ProduccionGasPetroleo_Anual[prod_petroleo>5000 & prod_petroleo<=125000,regalias_petroleo:=valor_petroleo*(0.08+(prod_petroleo-5000)*0.1)]
ProduccionGasPetroleo_Anual[prod_petroleo>125000 & prod_petroleo<=400000,regalias_petroleo:=valor_petroleo*0.2]
ProduccionGasPetroleo_Anual[prod_petroleo>400000 & prod_petroleo<=600000,regalias_petroleo:=valor_petroleo*(0.2+(prod_petroleo-400000)*0.025)]
ProduccionGasPetroleo_Anual[prod_petroleo>600000 ,regalias_petroleo:=valor_petroleo*0.25]

ProduccionGasPetroleo_Anual=subset(ProduccionGasPetroleo_Anual,select=-prod_gas)

##AHORA VAMOS A DECIR EN QUE DEPARTAMENTO SE HACE



Produccion=merge(ProduccionOro_Anual,ProduccionPlata_Anual,by="codmpio",all=T)
Produccion=merge(Produccion,ProduccionPlatino_Anual,by="codmpio",all=T)
Produccion=merge(Produccion,ProduccionGasPetroleo_Anual,by="codmpio",all=T)
Produccion=merge(Produccion,ProduccionCarbon_Anual,by="codmpio",all=T)
Produccion=merge(Produccion,data.frame(codmpio=PoblacionMunicipal[,"codmpio"]),by="codmpio",all=T)

Produccion[is.na(Produccion)]=0

Produccion[,coddpto:=substr(str_pad(codmpio,width=5,pad=0),1,2)]
ProduccionPre=Produccion
##AHORA VAMOS CON LO DE ANTES
##VAMOS A CREAR 4 variables, lo q va al muni, lo que va al dpto, lo q va al FNR, lo q va a los municipios portuarios, y lo q va a la car
 
Produccion[prod_petroleo<=10000,regalias_petroleo_muni:=regalias_petroleo*0.32]
Produccion[prod_petroleo<=10000,regalias_petroleo_dpto:=regalias_petroleo*0.52]
Produccion[prod_petroleo<=10000,regalias_petroleo_fnr:=regalias_petroleo*0.08]
Produccion[prod_petroleo<=10000,regalias_petroleo_portuario:=regalias_petroleo*0.08]
Produccion[prod_petroleo<=10000,regalias_petroleo_car:=regalias_petroleo*0]

Produccion[prod_petroleo>10000 & prod_petroleo<=20000,regalias_petroleo_muni:=(1-(prod_petroleo-10000)/prod_petroleo)*regalias_petroleo*0.32+((prod_petroleo-10000)/prod_petroleo)*regalias_petroleo*0.25]
Produccion[prod_petroleo>10000 & prod_petroleo<=20000,regalias_petroleo_dpto:=(1-(prod_petroleo-10000)/prod_petroleo)*regalias_petroleo*0.52+((prod_petroleo-10000)/prod_petroleo)*regalias_petroleo*0.475]
Produccion[prod_petroleo>10000 & prod_petroleo<=20000,regalias_petroleo_fnr:=(1-(prod_petroleo-10000)/prod_petroleo)*regalias_petroleo*0.08+((prod_petroleo-10000)/prod_petroleo)*regalias_petroleo*0.195]
Produccion[prod_petroleo>10000 & prod_petroleo<=20000,regalias_petroleo_portuario:=(1-(prod_petroleo-10000)/prod_petroleo)*regalias_petroleo*0.08+((prod_petroleo-10000)/prod_petroleo)*regalias_petroleo*0.08]
Produccion[prod_petroleo>10000 & prod_petroleo<=20000,regalias_petroleo_car:=regalias_petroleo*0]
 
Produccion[prod_petroleo>20000 & prod_petroleo<=50000,regalias_petroleo_muni:=0.5*(1-(prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.32+0.5*(1-(prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.25+((prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.125]
Produccion[prod_petroleo>20000 & prod_petroleo<=50000,regalias_petroleo_dpto:=0.5*(1-(prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.52+0.5*(1-(prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.475+((prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.475]
Produccion[prod_petroleo>20000 & prod_petroleo<=50000,regalias_petroleo_fnr:=0.5*(1-(prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.08+0.5*(1-(prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.195+((prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.32]
Produccion[prod_petroleo>20000 & prod_petroleo<=50000,regalias_petroleo_portuario:=0.5*((prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.08+0.5*(1-(prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.08+((prod_petroleo-20000)/prod_petroleo)*regalias_petroleo*0.08]
Produccion[prod_petroleo>20000 & prod_petroleo<=50000,regalias_petroleo_car:=regalias_petroleo*0]

Produccion[prod_petroleo>50000,regalias_petroleo_muni:=regalias_petroleo*0.125]
Produccion[prod_petroleo>50000,regalias_petroleo_dpto:=regalias_petroleo*0.475]
Produccion[prod_petroleo>50000,regalias_petroleo_fnr:=regalias_petroleo*0.32]
Produccion[prod_petroleo>50000,regalias_petroleo_portuario:=regalias_petroleo*0.08]
Produccion[prod_petroleo>50000,regalias_petroleo_car:=regalias_petroleo*0]


Produccion[,regalias_oro_muni:=regalias_oro*0.87]
Produccion[,regalias_oro_dpto:=regalias_oro*0.1]
Produccion[,regalias_oro_fnr:=regalias_oro*0.03]
Produccion[,regalias_oro_portuario:=regalias_oro*0]
Produccion[,regalias_oro_car:=regalias_oro*0]

Produccion[,regalias_plata_muni:=regalias_plata*0.87]
Produccion[,regalias_plata_dpto:=regalias_plata*0.1]
Produccion[,regalias_plata_fnr:=regalias_plata*0.03]
Produccion[,regalias_plata_portuario:=regalias_plata*0]
Produccion[,regalias_plata_car:=regalias_plata*0]

Produccion[,regalias_platino_muni:=regalias_platino*0.87]
Produccion[,regalias_platino_dpto:=regalias_platino*0.1]
Produccion[,regalias_platino_fnr:=regalias_platino*0.03]
Produccion[,regalias_platino_portuario:=regalias_platino*0]
Produccion[,regalias_platino_car:=regalias_platino*0]


Produccion[prod_carbon>3,regalias_carbon_muni:=regalias_carbon*0.32]
Produccion[prod_carbon>3,regalias_carbon_dpto:=regalias_carbon*0.42]
Produccion[prod_carbon>3,regalias_carbon_fnr:=regalias_carbon*0.16]
Produccion[prod_carbon>3,regalias_carbon_portuario:=regalias_carbon*0.1]
Produccion[prod_carbon>3,regalias_carbon_car:=regalias_carbon*0]


Produccion[prod_carbon<=3,regalias_carbon_muni:=regalias_carbon*0.45]
Produccion[prod_carbon<=3,regalias_carbon_dpto:=regalias_carbon*0.45]
Produccion[prod_carbon<=3,regalias_carbon_fnr:=regalias_carbon*0]
Produccion[prod_carbon<=3,regalias_carbon_portuario:=regalias_carbon*0.1]
Produccion[prod_carbon<=3,regalias_carbon_car:=regalias_carbon*0]

REGALIAS_dpto=Produccion[,list(regalias_carbon_dpto=sum(regalias_carbon_dpto),
regalias_platino_dpto=sum(regalias_platino_dpto),
regalias_plata_dpto=sum(regalias_plata_dpto),
regalias_oro_dpto=sum(regalias_oro_dpto),
regalias_petroleo_dpto=sum(regalias_petroleo_dpto)),by="coddpto"]

REGALIAS_FNR=Produccion[,list(regalias_carbon_fnr=sum(regalias_carbon_fnr),
regalias_platino_fnr=sum(regalias_platino_fnr),
regalias_plata_fnr=sum(regalias_plata_fnr),
regalias_oro_fnr=sum(regalias_oro_fnr),
regalias_petroleo_fnr=sum(regalias_petroleo_fnr))]

REGALIAS_portuario=Produccion[,list(regalias_carbon_portuario=sum(regalias_carbon_portuario),
regalias_platino_portuario=sum(regalias_platino_portuario),
regalias_plata_portuario=sum(regalias_plata_portuario),
regalias_oro_portuario=sum(regalias_oro_portuario),
regalias_petroleo_portuario=sum(regalias_petroleo_portuario))]

REGALIAS_muni=subset(Produccion,select=c(codmpio,grep("muni",colnames(Produccion))))


REGALIAS_portuario=FunPortuario(COD_DANES=as.character(NBI_Municipal[,1]),NBI_Municipal,PoblacionMunicipal,TotalDestinado=sum(REGALIAS_portuario))
REGALIAS_portuario=subset(REGALIAS_portuario,select=c(codmpio,DINERO))
colnames(REGALIAS_portuario)=c("codmpio","regalias_puerto_muni")
REGALIAS_portuario$codmpio=as.numeric(as.character(REGALIAS_portuario$codmpio))
REGALIAS_muni=merge(REGALIAS_muni,REGALIAS_portuario,by="codmpio")
REGALIAS_muni_pre <- copy(REGALIAS_muni) 


for(i in 1:dim( REGALIAS_muni)[1]){

VectorIndirectas=subset(REGALIAS_dpto,subset=REGALIAS_dpto$coddpto==substr(str_pad(REGALIAS_muni$codmpio[i],width=5,pad=0),1,2),select=c("regalias_carbon_dpto","regalias_platino_dpto","regalias_plata_dpto","regalias_oro_dpto","regalias_petroleo_dpto"))*(PoblacionMunicipal$Poblacion[PoblacionMunicipal$codmpio==REGALIAS_muni$codmpio[i]]/PoblacionDepartamental$Poblacion[PoblacionDepartamental$DP==as.numeric(substr(str_pad(REGALIAS_muni$codmpio[i],width=5,pad=0),1,2))])



REGALIAS_muni_pre[i,regalias_carbon_dpto:=VectorIndirectas$regalias_carbon_dpto]
REGALIAS_muni_pre[i,regalias_platino_dpto:=VectorIndirectas$regalias_platino_dpto]
REGALIAS_muni_pre[i,regalias_plata_dpto:=VectorIndirectas$regalias_plata_dpto]
REGALIAS_muni_pre[i,regalias_oro_dpto:=VectorIndirectas$regalias_oro_dpto]
REGALIAS_muni_pre[i,regalias_petroleo_dpto:=VectorIndirectas$regalias_petroleo_dpto]
 }


 
 ## VAMOS POR EL AFTER
 ##en estos momentos se asume que las directas van todas al municipio...
 #Yo creo que se reparten como antes... mas o menos... todavia no tengo claro por ejemplo de donde sale la palta para los portuarios, o si es q los departamentos
 #ahora no tienen asignaciones directas...
 #tambien en estos momentos lo que devuelve de directas es las "directas" mas lo que le toca al muni por el FCR por ser pobre.
 #Yo creo que toca cambiar lo de directas para que basicamente se le haga lo que se hizo al pre...
Produccion=ProduccionPre
REGALIAS_muni_post=data.table(Produccion$codmpio)



for(i in 1:dim(Produccion)[1]){
Temp=CuantoMunicipio(SGR=Produccion$regalias_oro[i],Produccion$codmpio[i],Produccion$coddpto[i],FactorFDR,FactorDptoFCR,FactorMpioFCR,PoblacionMunicipal,PoblacionDepartamental)
REGALIAS_muni_post[i,regalias_oro_muni:=Temp$MunicipioDirecto]
REGALIAS_muni_post[i,FondoCompensacionRegional_oro:=Temp$FondoCompensacionRegional]
REGALIAS_muni_post[i,FondoDesarrolloRegional_oro:=Temp$FondoDesarrolloRegional]

Temp=CuantoMunicipio(SGR=Produccion$regalias_plata[i],Produccion$codmpio[i],Produccion$coddpto[i],FactorFDR,FactorDptoFCR,FactorMpioFCR,PoblacionMunicipal,PoblacionDepartamental)
REGALIAS_muni_post[i,regalias_plata_muni:=Temp$MunicipioDirecto]
#REGALIAS_muni_post[i,regalias_plata_dpto:=Temp$MunicipioIndirecto]
REGALIAS_muni_post[i,FondoCompensacionRegional_plata:=Temp$FondoCompensacionRegional]
REGALIAS_muni_post[i,FondoDesarrolloRegional_plata:=Temp$FondoDesarrolloRegional]

Temp=CuantoMunicipio(SGR=Produccion$regalias_platino[i],Produccion$codmpio[i],Produccion$coddpto[i],FactorFDR,FactorDptoFCR,FactorMpioFCR,PoblacionMunicipal,PoblacionDepartamental)
REGALIAS_muni_post[i,regalias_platino_muni:=Temp$MunicipioDirecto]
#REGALIAS_muni_post[i,regalias_platino_dpto:=Temp$MunicipioIndirecto]
REGALIAS_muni_post[i,FondoCompensacionRegional_platino:=Temp$FondoCompensacionRegional]
REGALIAS_muni_post[i,FondoDesarrolloRegional_platino:=Temp$FondoDesarrolloRegional]

Temp=CuantoMunicipio(SGR=Produccion$regalias_petroleo[i],Produccion$codmpio[i],Produccion$coddpto[i],FactorFDR,FactorDptoFCR,FactorMpioFCR,PoblacionMunicipal,PoblacionDepartamental)
REGALIAS_muni_post[i,regalias_petroleo_muni:=Temp$MunicipioDirecto]
#REGALIAS_muni_post[i,regalias_petroleo_dpto:=Temp$MunicipioIndirecto]
REGALIAS_muni_post[i,FondoCompensacionRegional_petroleo:=Temp$FondoCompensacionRegional]
REGALIAS_muni_post[i,FondoDesarrolloRegional_petroleo:=Temp$FondoDesarrolloRegional]

Temp=CuantoMunicipio(SGR=Produccion$regalias_carbon[i],Produccion$codmpio[i],Produccion$coddpto[i],FactorFDR,FactorDptoFCR,FactorMpioFCR,PoblacionMunicipal,PoblacionDepartamental)
REGALIAS_muni_post[i,regalias_carbon_muni:=Temp$MunicipioDirecto]
#REGALIAS_muni_post[i,regalias_carbon_dpto:=Temp$MunicipioIndirecto]
REGALIAS_muni_post[i,FondoCompensacionRegional_carbon:=Temp$FondoCompensacionRegional]
REGALIAS_muni_post[i,FondoDesarrolloRegional_carbon:=Temp$FondoDesarrolloRegional]
}

FondoCompensacionRegional=REGALIAS_muni_post[, lapply(.SD, sum, na.rm=TRUE), by=substr(str_pad(V1,width=5,pad=0),1,2) , .SDcols= colnames(REGALIAS_muni_post)[grep("FondoCompensacionRegional",colnames(REGALIAS_muni_post))]]

FondoDesarrolloRegional=REGALIAS_muni_post[, lapply(.SD, sum, na.rm=TRUE), by=substr(str_pad(V1,width=5,pad=0),1,2) , .SDcols= colnames(REGALIAS_muni_post)[grep("FondoDesarrolloRegional",colnames(REGALIAS_muni_post))]]
#
#total_post=sum(subset(FondoCompensacionRegional,select=-1))+sum(subset(FondoDesarrolloRegional,select=-1))+sum(subset(REGALIAS_muni_post,select=c(regalias_oro_muni, regalias_plata_muni, regalias_platino_muni, regalias_petroleo_muni, regalias_carbon_muni)))
#
 sum(Produccion$regalias_carbon)+sum(Produccion$regalias_oro)+sum(Produccion$regalias_plata)+sum(Produccion$regalias_platino)+sum(Produccion$regalias_petroleo)
#subset(FondoCompensacionRegional,select=-1)/subset(FondoDesarrolloRegional,select=-1)
 print(sum(subset(FondoCompensacionRegional,select=-1))+sum(subset(FondoDesarrolloRegional,select=-1)))
# 
for(i in 1:dim( REGALIAS_muni_post)[1]){
coddpto=substr(str_pad(REGALIAS_muni_post$V1[i],width=5,pad=0),1,2)


MunicipiosDirecto=subset(FondoCompensacionRegional,subset=substr==coddpto,select=colnames(FondoCompensacionRegional)[grep("FondoCompensacionRegional",colnames(FondoCompensacionRegional))])*(FactorMpioFCR$FactorMpioPobrezaFCR[FactorMpioFCR$codmpio==REGALIAS_muni_post$V1[i]]+FactorMpioFCR$FactorMpioRicosFCR[FactorMpioFCR$codmpio==REGALIAS_muni_post$V1[i]])

REGALIAS_muni_post[i,regalias_carbon_muni:=regalias_carbon_muni+MunicipiosDirecto$FondoCompensacionRegional_carbon]
REGALIAS_muni_post[i,regalias_platino_muni:=regalias_platino_muni+MunicipiosDirecto$FondoCompensacionRegional_platino]
REGALIAS_muni_post[i,regalias_plata_muni:=regalias_plata_muni+MunicipiosDirecto$FondoCompensacionRegional_plata]
REGALIAS_muni_post[i,regalias_oro_muni:=regalias_oro_muni+MunicipiosDirecto$FondoCompensacionRegional_oro]
REGALIAS_muni_post[i,regalias_petroleo_muni:=regalias_petroleo_muni+MunicipiosDirecto$FondoCompensacionRegional_petroleo]


MunicipioIndirecto=(subset(FondoDesarrolloRegional,subset=substr==coddpto,select=colnames(FondoDesarrolloRegional)[grep("FondoDesarrolloRegional",colnames(FondoDesarrolloRegional))])*FactorFDR$FactorFDR[FactorFDR$DP==as.numeric(coddpto)]+ 
subset(FondoCompensacionRegional,subset=substr==coddpto,select=colnames(FondoCompensacionRegional)[grep("FondoCompensacionRegional",colnames(FondoCompensacionRegional))])*FactorDptoFCR$FactorDptoFCR[FactorDptoFCR$DP==as.numeric(coddpto)])*
(PoblacionMunicipal$Poblacion[PoblacionMunicipal$codmpio==REGALIAS_muni_post$V1[i]]/PoblacionDepartamental$Poblacion[PoblacionDepartamental$DP==as.numeric(coddpto)])

REGALIAS_muni_post[i,regalias_carbon_dpto:=MunicipioIndirecto$FondoDesarrolloRegional_carbon]
REGALIAS_muni_post[i,regalias_platino_dpto:=MunicipioIndirecto$FondoDesarrolloRegional_platino]
REGALIAS_muni_post[i,regalias_plata_dpto:=MunicipioIndirecto$FondoDesarrolloRegional_plata]
REGALIAS_muni_post[i,regalias_oro_dpto:=MunicipioIndirecto$FondoDesarrolloRegional_oro]
REGALIAS_muni_post[i,regalias_petroleo_dpto:=MunicipioIndirecto$FondoDesarrolloRegional_petroleo]
 }


colnames(REGALIAS_muni_post)[1]="codmpio"
REGALIAS_muni_post=subset(REGALIAS_muni_post,select=c("codmpio",colnames(REGALIAS_muni_post)[grep("regalias",colnames(REGALIAS_muni_post))]))



save(REGALIAS_muni_post,file="CreatedData/REGALIAS_muni_post.RData")
save(REGALIAS_muni_pre,file="CreatedData/REGALIAS_muni_pre.RData")

 
write.dta(REGALIAS_muni_post,file="CreatedData/REGALIAS_muni_post.dta")
write.dta(REGALIAS_muni_pre,file="CreatedData/REGALIAS_muni_pre.dta")



