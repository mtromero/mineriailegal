CuantoMunicipio=function(SGR,codmpio,coddpto,FactorFDR,FactorDptoFCR,FactorMpioFCR,PoblacionMunicipal,PoblacionDepartamental){
  if(coddpto!=substr(str_pad(codmpio,width=5,pad=0),1,2)) stop("codmpio doest not match coddpto")
  coddpto=as.numeric(coddpto)
  ##SGR es cuanta plata exploto el municipio y dio en regalias...antes esto seria todo para el municipio/departamento
  ##Esta funcion va post 2014... es decir es largo plazo...ignore excepciones de primeros anos

  ##Primero va la parte que se destina a exploracion. Eso se quita del SGR
  Exploracion=0.02*SGR  #Acto Legislativo 5 de 2011, #articulo 2
  Funcionamiento=0.02*SGR  #Ley 1530 de 2012, #articulo 11
  SMSCE=0.01*SGR  #Acto Legislativo 5 de 2011, #articulo 2
  MagdalenaDique=0.005*SGR  #Ley 1530 de 2012, #articulo 11
  SGR_residual=(1-0.02-0.02-0.01-0.005)*SGR #Acto Legislativo 5 de 2011, #articulo 2
      
  ##Despues lo que va a cada fondo. Es 10% ciencia y tecnologia, 10% entidades terrritoriales, 30% fondo de estabilizacion
  ##50% restante va a asignaciones directas y fondos regionales
  ##20% del 50% restante va a asignaciones directas (excepto en 2012, 2013 y 2014), 80% va a los fondos. Del 80% el 60%  va al fondo de compensacion y el 40% al fondo de desarrollo.

  
  FondoCienciaTecnologia=0.1*SGR_residual  #Acto Legislativo 5 de 2011, #articulo 2
  FondoPensionesTerritoriales=0.1*SGR_residual  #Acto Legislativo 5 de 2011,#articulo 2
  PorcentajeAhorro=0.3
  FondoAhorroEstabilizacion=PorcentajeAhorro*SGR_residual  #Acto Legislativo 5 de 2011,#articulo 2
  PorcentajeDirectas=0.2      #Acto Legislativo 5 de 2011,#articulo 2
  AssignacionesDirectas=PorcentajeDirectas*(1-0.1-0.1-PorcentajeAhorro)*SGR_residual  #Acto Legislativo 5 de 2011
  FondoCompensacionRegional=0.6*(1-PorcentajeDirectas)*(1-0.1-0.1-PorcentajeAhorro)*SGR_residual  #Acto Legislativo 5 de 2011
  FondoDesarrolloRegional=0.4*(1-PorcentajeDirectas)*(1-0.1-0.1-PorcentajeAhorro)*SGR_residual  #Acto Legislativo 5 de 2011
  
  MunicipioDirecto=AssignacionesDirectas

 #FondoCompensacionRegional*(FactorMpioFCR$FactorMpioPobrezaFCR[FactorMpioFCR$codmpio==codmpio]+FactorMpioFCR$FactorMpioRicosFCR[FactorMpioFCR$codmpio==codmpio])
  
  MunicipioIndirecto=(FondoDesarrolloRegional*FactorFDR$FactorFDR[FactorFDR$DP==coddpto]+FondoCompensacionRegional*FactorDptoFCR$FactorDptoFCR[FactorDptoFCR$DP==coddpto])*(PoblacionMunicipal$Poblacion[PoblacionMunicipal$codmpio==codmpio]/PoblacionDepartamental$Poblacion[PoblacionDepartamental$DP==coddpto])
  
  return(list(MunicipioDirecto=MunicipioDirecto,MunicipioIndirecto=MunicipioIndirecto,FondoCompensacionRegional=FondoCompensacionRegional,FondoDesarrolloRegional=FondoDesarrolloRegional))
  
}
  
  

