setwd("/media/mauricio/TeraHDD1/Dropbox/MineriaIlegal/")

#install.packages('osmar')
# library(osmar)  
# src <- osmsource_file("RawData/colombia-latest.osm")
# bb <- corner_bbox(-79.365, -4.478, -66.797, 12.597)
# MinasColombia <- get_osm(bb, source = src)
# 
# Minas <- find(MinasColombia, way(tags(k == "landuse" & v == "quarry")))
# Minas <- find_down(MinasColombia, way(Minas))
# MinasLayer <- subset(MinasColombia, ids = Minas)
# spMinas=as_sp(MinasLayer,what="polygons")


library(sp)
library(rgdal)
setwd("/media/mauricio/TeraHDD1/Dropbox/MineriaIlegal/")
landuse=readOGR("RawData/colombia-latest.shp/","landuse") 
MinasOpenStreet=landuse[landuse$type=="quarry",]
MinasOpenStreet <- spTransform(MinasOpenStreet, CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"))
saveRDS(MinasOpenStreet,file="CreatedData/YESOpenStreet.Rda")


# points=readOGR("RawData/colombia-latest.shp/","points") 
# spMinasPoints=points[points$type=="mineshaft" ,]

