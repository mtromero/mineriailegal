library(raster)
library(sp)
library(rgdal)
library(maptools)
library(rgeos) 
getCoordinates<-function(coords, cSystem, spatial=FALSE){
  library(sp)
  points<-SpatialPoints(coords, proj4string=CRS("+proj=longlat +datum=WGS84"))
  points<-spTransform(points, CRS=cSystem)
  if (spatial) return(points)
  points@coords
}

getCircles<-function(coords, pad, CRS){
  library(raster)
  library(rgeos)
  
  print(paste0("getting circles"))
  coords<-as.matrix(coords)
  nCoords<-getCoordinates(coords, CRS, spatial=TRUE)
  
  circles<-lapply(1:nrow(nCoords@coords), FUN=function(i){
  c<-nCoords[i]
  r<-pad[i]
  gBuffer(c, width=r)
  })
  circles
}



setwd("/media/mauricio/TeraHDD1/Copy/MiningAndBirth")

# dirDat donde se encuentra el archivo procesado del censo, en la ws es 'C:/DataNubes/dat'
censo <- readRDS(file.path("RawData/GIS/", 'censoFilteredMaster2010.Rda'))
censo$pad <- sqrt(censo$area/pi)
coords <- censo[, c("lon", "lat")]
# la proyeccion
# coordinateSystem <- '+proj'
projectionAll="+proj=eqdc +lat_0=-32 +lon_0=-60 +lat_1=-5 +lat_2=-42 +x_0=0 +y_0=0 +ellps=aust_SA +units=m +no_defs"
circles<-getCircles(coords, censo$pad, CRS(projectionAll))
circles=circles[which(sapply(circles,length)==1)]
circles=sapply(circles, spTransform,CRSobj=CRS("+init=epsg:4326"))
circles[["makeUniqueIDs"]] = TRUE
A=do.call(rbind.SpatialPolygons, circles)
A=gConvexHull(A)
A=gIntersection(A,COL_30pts)
A=SpatialPolygonsDataFrame(A,data.frame(c(x=1),row.names=c(1)))
writeOGR(A, "RawData/GIS", "Censo_Convex", driver="ESRI Shapefile", overwrite_layer=TRUE)



COL_30pts = readOGR("RawData/GIS/COL_30pts", "COL_30pts")
COL_30pts <- spTransform(COL_30pts, CRS("+init=epsg:4326"))
writeOGR(COL_30pts, "RawData/GIS", "COL_30pts_BP", driver="ESRI Shapefile", overwrite_layer=TRUE)
TMC_Boundary=gConvexHull(TMC)
TMC_Boundary=SpatialPolygonsDataFrame(TMC_Boundary,data.frame(c(x=1),row.names=c(1)))
writeOGR(TMC_Boundary, "RawData/GIS", "TMC_ConvexHull", driver="ESRI Shapefile", overwrite_layer=TRUE)


TMC = readOGR("RawData/GIS", "TMC_Jul2012")
TMC <- spTransform(TMC, CRS("+init=epsg:4326"))
gIsValid(TMC, reason = T)
TMC=gBuffer(TMC,byid=T,width=0)
DataKeep=TMC@data
writeOGR(TMC, "RawData/GIS", "TMC_Proj", driver="ESRI Shapefile", overwrite_layer=TRUE)
TMC2=TMC[1:2,]
TMC2=gUnionCascaded(TMC2)
TMC2=SpatialPolygonsDataFrame(TMC2,data.frame(c(x=1),row.names=c(1)))
writeOGR(TMC2, "RawData/GIS", "TMC_Proj_single", driver="ESRI Shapefile", overwrite_layer=TRUE)


TMC2=gUnionCascaded(TMC)
TMC2=SpatialPointsDataFrame(TMC2, DataKeep, match.ID = TRUE)
TMC2=gBuffer(TMC2,byid=T,width=1)
TMC2=SpatialPolygonsDataFrame(TMC2,data.frame(c(x=1),row.names=c(1)))
writeOGR(TMC2, "RawData/GIS", "TMC2_Proj", driver="ESRI Shapefile", overwrite_layer=TRUE)

TMC3=gUnaryUnion(TMC)
TMC3=SpatialPointsDataFrame(TMC3, DataKeep, match.ID = TRUE)
TMC3=gBuffer(TMC3,byid=T,width=1)
TMC3=SpatialPolygonsDataFrame(TMC3,data.frame(c(x=1),row.names=c(1)))
writeOGR(TMC3, "RawData/GIS", "TMC3_Proj", driver="ESRI Shapefile", overwrite_layer=TRUE)


bb <- bbox(TMC)
cs <- c(0.1,0.1)  # cell size
cc <- bb[, 1] + (cs/2)  # cell offset
cd <- ceiling(diff(t(bb))/cs)  # number of cells per direction
grd <- GridTopology(cellcentre.offset=cc, cellsize=cs, cells.dim=cd)


sp_grd <- SpatialGridDataFrame(grd,
                           data=data.frame(id=1:prod(cd)))