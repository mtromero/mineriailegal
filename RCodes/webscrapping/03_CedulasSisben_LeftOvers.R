rm(list=ls())
require(RSelenium)
RSelenium::startServer()
RSelenium::checkForServer()

INFO=NULL
setwd("C:/Users/Mauricio/Dropbox/MineriaIlegal/SISBEN")
ListaCC=read.csv("CedulasBarequeros.csv")
load("SIBEN_BAREQUEROS_LO.RData")
INFO=INFO[!duplicated(INFO),]
ListaCC=ListaCC[which(!(ListaCC[,1] %in% as.numeric(INFO[,4]))),]

appURL <- 'https://wssisbenconsulta.sisben.gov.co/dnp_sisbenconsulta/dnp_sisben_consulta.aspx'
remDr <- remoteDriver(browserName = "phantomjs")
remDr$open()
  remDr$setImplicitWaitTimeout(100*10000)
  remDr$setTimeout(type = "page load", milliseconds = 100*10000)
  remDr$setAsyncScriptTimeout(milliseconds = 100*10000)
remDr$navigate(appURL)


for(cc in ListaCC){

Sys.sleep(2) # give the binary a moment
remDr$refresh()
remDr$findElement("css", "select[name='ddlTipoDocumento'] option:nth-child(2)")$clickElement()
remDr$findElement("css", "input[name='tboxNumeroDocumento']")$sendKeysToElement(list(as.character(cc)))
remDr$findElement("id", "btnConsultar")$clickElement()
TablaInfo=readHTMLTable(remDr$getPageSource()[[1]],header=F)[[3]]
TablaInfo=as.character(TablaInfo[,2])
if(length(TablaInfo)>3) {
                        INFO=rbind(INFO,TablaInfo)
                        print(TablaInfo[4])
                        }

}

save(INFO,file="SIBEN_BAREQUEROS_LO.RData")
write.csv(INFO,file="SIBEN_BAREQUEROS_LO.csv")



