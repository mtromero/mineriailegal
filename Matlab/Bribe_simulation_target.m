
%Price of gold
p=1;

%Quantity extracted
q=@(K) 200*(K/2)^0.5;

%Maximum capital
Kmin=1;
Kmax=100;

%Number of budgets after
NR1=50;

%Royalties paid by firm
alpha=0.05;

%Cost of extraction
C=@(x) x*0.5;

%Mining title
T=10;

piL=@(K) max(p*q(K)*(1-alpha)-C(q(K))-T,0);

%Probability of detection
Omega=@(x,K) K/600;

V=0;

%Muni budget
R=100;
%Royalties to mining muni
alpham=0.55;
%If f linear then change in surplus from mining is independet of affi
f=@(x) x;
S=@(K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);

theta=0.9;

b=@(K) (S(K)>0)*max(p*q(K)-C(q(K))-Omega(alpham,K)-piL(K)-theta*S(K),0);

alpham1=0.1;

%Post-royalties transfer
Kvec=[Kmin:Kmax];

S0vec=arrayfun(S,Kvec);
b0vec=arrayfun(b,Kvec);

alpham=0.1;
S=@(K) T+f(R)-f(p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
b=@(K) (S(K)>0)*max(p*q(K)-C(q(K))-Omega(alpham,K)-piL(K)-theta*S(K),0);


S1vec=arrayfun(S,Kvec);
b1vec=arrayfun(b,Kvec);

plot(Kvec,[S0vec;b0vec;S1vec;b1vec])



%% Before reform linear
%Muni budget
R0=100;
%Royalties to mining muni
alpham=0.55;
%If f linear then change in surplus from mining is independet of affi
f=@(x) 0.25*x;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
S=@(K) SRK(R0,K);

Kstar=fsolve(S,50);
prbeflinear=ones(1,NR1)*max(0,min(Kstar/Kmax,1));

%% After reform linear
%Muni budget
R1vec=(1:NR1);
praftlinear=zeros(0,NR1);
%Royalties to mining muni
alpham=0.1;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
for iR=1:NR1
S=@(K) SRK(R1vec(iR),K);
Kstar=fsolve(S,50);
praftlinear(iR)=max(0,min(Kstar/Kmax,1));
end

%% Before reform targeting

%Royalties to mining muni
alpham0=0.55;
alpham=0.55;
lambda=0.25;
ftgt=@(R,K) p*q(K)*alpha*alpham+R+lambda*(p*q(K)*alpha*alpham0>(p*q(K)*alpha*alpham+R))*(p*q(K)*alpha*alpham+R-p*q(K)*alpha*alpham0);

SRK=@(R,K) T-0.25*ftgt(0,K)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
S=@(K) SRK(R0,K);

Kstar=fsolve(S,50);
prbeftarget=ones(1,NR1)*max(0,min(Kstar/Kmax,1));

%% After reform targeting
%Muni budget
R1vec=(1:NR1);
prafttarget=zeros(0,NR1);
%Royalties to mining muni
alpham=0.1;
ftgt=@(R,K) p*q(K)*alpha*alpham+R+lambda*(p*q(K)*alpha*alpham0>p*q(K)*alpha*alpham+R)*(p*q(K)*alpha*alpham+R-p*q(K)*alpha*alpham0);

SRK=@(R,K) T+0.25*(R-ftgt(R,K))+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
for iR=1:NR1
S=@(K) SRK(R1vec(iR),K);
Kstar=fsolve(S,50);
prafttarget(iR)=max(0,min(Kstar/Kmax,1));
end

q=1000;
affivec=100*(p*q*alpha*alpham1+R1vec-p*q*alpha*alpham0)/(p*q*alpha*alpham0+R);


% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[0.32 0.38]);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(affivec,[prbeflinear;praftlinear;prbeftarget;prafttarget],'Parent',axes1,'LineWidth',2);
set(plot1(1),'DisplayName','Linear- before the reform');
set(plot1(2),'LineStyle','--','Color',[0 0 1],...
    'DisplayName','Linear- after the reform');
set(plot1(3),'Marker','.','LineStyle','none',...
    'DisplayName','Ref Dep- before the reform');
set(plot1(4),'Marker','.','LineStyle','--','Color',[1 0 0],...
    'DisplayName','Ref Dep- after the reform');

% Create xlabel
xlabel('Change in royalties as percentage of budget');

% Create ylabel
ylabel('Percentage of illegal mines');

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.51229861256362 0.354954538449682 0.392571428571428 0.188888888888889]);
print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\042015_Paper_royalties_illegal\Prob_illegal_target'))


