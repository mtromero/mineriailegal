clear
latexslides='C:\Users\santi\Documents\mineriailegal\Latex\201611_SEEPAC\Matlab\';
latexpaper='C:\Users\santiago.saavedrap\Documents\mineriailegal\Latex\201605_Paper_royalties_illegal\';
%Price of gold
p=1.5;

%Quantity extracted
q=@(K) 20*(K/2)^0.5;

%Maximum capital
Kmin=1;
Kmax=100;
% If NK<15000 de graph doesn't look smooth
NK=15000;
Kvec=linspace(Kmin,Kmax,NK);


%Royalties paid by firm
alpha=0.05;

%Cost of extraction
C=@(x) x*0.6;

%Mining title
T=10;

piL=@(K) max(p*q(K)*(1-alpha)-C(q(K))-T,0);


V=0;

%Penalty if detected
Pen=@(K,V)  V+85;

%Muni budget
scale_budget=1.4;

R=100*scale_budget;
R0=100*scale_budget;
%Number of budgets after
NR1=70;
R1vec=(1:NR1)*scale_budget;
alpham0=0.1;
alpham1=0.55;

%If f linear then change in surplus from mining is independet of affi
f=@(x) 0.261*x/scale_budget;

Na=21;
avec=alpham0+(alpham1-alpham0)*([1:Na]-1)/(Na-1);

%%% Constant prob detection
Omega=@(x,K) 0;

pr=zeros(0,Na);
parea=zeros(0,Na);

for ia=1:Na
%Royalties to mining muni
alpham=alpham0+(alpham1-alpham0)*(ia-1)/(Na-1);

SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*Pen(K,V);
S=@(K) SRK(R0,K);

if (S(Kmin)>0) 
    if (S(Kmax)<0)
Kstar=fsolve(S,50);
    else
        Kstar=Kmax;
    end
else 
    Kstar=Kmin;
end
pr(ia)=sum(Kvec<Kstar)/NK;
%Calculate pctg of area below cutoff
parea(ia)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);

end

%Increasing prob detection
Omega=@(x,K) K/600;

prlp=zeros(0,Na);
parealp=zeros(0,Na);

for ia=1:Na
%Royalties to mining muni
alpham=alpham0+(alpham1-alpham0)*(ia-1)/(Na-1);

SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*Pen(K,V);
S=@(K) SRK(R0,K);

if (S(Kmin)>0) 
    if (S(Kmax)<0)
Kstar=fsolve(S,50);
    else
        Kstar=Kmax;
    end
else 
    Kstar=Kmin;
end
prlp(ia)=sum(Kvec<Kstar)/NK;
%Calculate pctg of area below cutoff
parealp(ia)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);

end



%%% Change to constant prob detection
Pen=@(K,V)  V+K;

prlpkp=zeros(0,Na);
parealpkp=zeros(0,Na);

for ia=1:Na
%Royalties to mining muni
alpham=alpham0+(alpham1-alpham0)*(ia-1)/(Na-1);

SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*Pen(K,V);
S=@(K) SRK(R0,K);

if (S(Kmin)>0) 
    if (S(Kmax)<0)
Kstar=fsolve(S,50);
    else
        Kstar=Kmax;
    end
else 
    Kstar=Kmin;
end
prlpkp(ia)=sum(Kvec<Kstar)/NK;
%Calculate pctg of area below cutoff
parealpkp(ia)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);

end

% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1,'YTickLabel',{'0'},'YTick',4,...
    'XTick',[-10 0 10],...
    'FontSize',18);
% Uncomment the following line to preserve the Y-limits of the axes
%xlim(axes1,[-25 25]);
ylim(axes1,[0.7 1.3]);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(avec,[parea;parealp;parealpkp],'Parent',axes1,'LineWidth',2);
set(plot1(1),'Marker','.','Color',[1 0 0],...
    'DisplayName','Constant Pr()');
set(plot1(2),'MarkerSize',3,'Marker','square','LineStyle','none',...
    'Color',[0 0.498039215803146 0],...
    'DisplayName','Increasing Pr()');
set(plot1(3),'LineStyle','--','Color',[0 0 1],...
    'DisplayName','Increasing Pr(), Pen()');




% Create xlabel
xlabel('Share royalties for local municiaplity','FontSize',18);

% Create ylabel
ylabel('Percentage illegal area','FontSize',18);

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.139140178414752 0.715327696433473 0.46071427469807 0.204761899014314],...
    'EdgeColor',[1 1 1]);

print('-dpng',strcat(latexpaper,'IncreasingPrPen'))
