
%Price of gold
p=0.75;

%Quantity extracted
q=@(K) 20*(K/2)^0.5;

%Maximum capital
Kmin=1;
Kmax=100;
Kvec=[Kmin:Kmax];
vec0=0*Kvec;

%Number of budgets after
NR1=50;

%Royalties paid by firm
alpha=0.05;

%Cost of extraction
C=@(x) x*0.6;

%Mining title
T=5;



%Probability of detection
Omega=@(K) K^0.5/60;

V=0;
theta=0.9;
%Muni budget
R=100;


%If f linear then change in surplus from mining is independet of affi
f=@(x) x;

weffort=0.9;
piL=@(K) max(p*q(K)*(1-alpha)-C(q(K))-T,0);
piLvec=arrayfun(piL,Kvec);

effortvec=(0:20)/20;
alphamvec=(0:20)/20;

%%% Zero cost of monitoring
Estarvec=zeros(1,length(alphamvec));
Kstarvec=zeros(1,length(alphamvec));

Ce=@(e) 0*e^1.5;
for j=1:length(alphamvec)
%Royalties to mining muni
alpham=alphamvec(j);
S=@(K) T+p*q(K)*alpha+f(R)-f(R+p*q(K)*alpha*alpham)-Omega(K)*(K+V)*weffort;

b=@(K) (S(K)>0)*max(p*q(K)-C(q(K))-Omega(K)*K*weffort-piL(K)-theta*S(K),0);
Eevec=zeros(1,length(effortvec));

for i=1:length(effortvec) 
effort=effortvec(i);
piI=@(K) max((p*q(K)-C(q(K))-(effort+weffort*(1-effort))*Omega(K)*K-b(K)*effort)*(b(K)>0),0);

piIvec=arrayfun(piI,Kvec);
Svec=arrayfun(S,Kvec).*(piIvec>0);

Eb=sum(arrayfun(b,Kvec).*Kvec.*(Svec>0))/sum(Kvec.*(Svec>0));

Eevec(i)= Eb*effort-Ce(effort);
end

[basura,Etemp]=max(Eevec);
Estarvec(j)=effortvec(Etemp);
effort=Estarvec(j);


piI=@(K) max((p*q(K)-C(q(K))-(effort+weffort*(1-effort))*Omega(K)*K-b(K)*effort)*(b(K)>0),0);
piIvec=arrayfun(piI,Kvec);

Kstarvec(j)=max(Kvec.*((piIvec-piLvec)>0));
end
E0starvec=Estarvec;
K0starvec=Kstarvec;

%%% Zero cost of monitoring
Estarvec=zeros(1,length(alphamvec));
Kstarvec=zeros(1,length(alphamvec));

Ce=@(e) 2*e^1.5;
for j=1:length(alphamvec)
%Royalties to mining muni
alpham=alphamvec(j);
S=@(K) T+p*q(K)*alpha+f(R)-f(R+p*q(K)*alpha*alpham)-Omega(K)*(K+V)*weffort;

b=@(K) (S(K)>0)*max(p*q(K)-C(q(K))-Omega(K)*K*weffort-piL(K)-theta*S(K),0);
Eevec=zeros(1,length(effortvec));

for i=1:length(effortvec) 
effort=effortvec(i);
piI=@(K) max((p*q(K)-C(q(K))-(1+effort*(weffort-1))*Omega(K)*K-b(K)*effort)*(b(K)>0),0);

piIvec=arrayfun(piI,Kvec);
Svec=arrayfun(S,Kvec).*(piIvec>0);

Eb=sum(arrayfun(b,Kvec).*Kvec.*(Svec>0))/sum(Kvec.*(Svec>0));

Eevec(i)= Eb*effort-Ce(effort);
end

[basura,Etemp]=max(Eevec);
Estarvec(j)=effortvec(Etemp);
effort=Estarvec(j);


piI=@(K) max((p*q(K)-C(q(K))-(1+effort*(weffort-1))*Omega(K)*K-b(K)*effort)*(b(K)>0),0);
piIvec=arrayfun(piI,Kvec);

Kstarvec(j)=max(Kvec.*((piIvec-piLvec)>0));
end

figure1 = figure('Color',[1 1 1]);
axes1 = axes('Parent',figure1,'YTickLabel',{'0'},'YTick',0,'XTickLabel',{},'XTick',zeros(1,0),'FontSize',18);
box(axes1,'on');
hold(axes1,'all');
plot1 = plot(alphamvec,[Kstarvec;K0starvec],'Parent',axes1,'LineWidth',1);
set(plot1(2),'Marker','.','Color',[0 0.5 0],'MarkerSize',10,'DisplayName','No detection cost');
set(plot1(1),'Marker','*','Color',[0 0 1],'MarkerSize',10,'DisplayName','Costly detection');

title({'Surplus illegal mining for different mine sizes'});
xlabel({'Royalties share'});
ylabel({'Illegal mining'});

legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.507738095238093 0.694841269841272 0.380357142857143 0.152380952380952]);

print('-dpng',strcat('C:\Users\Santi\Documents\mineriailegal\Latex\201610_PublicSeminar\Matlab\K_Effort'))

figure1 = figure('Color',[1 1 1]);
axes1 = axes('Parent',figure1,'YTickLabel',{'0'},'YTick',0,'XTickLabel',{},'XTick',zeros(1,0),'FontSize',18);
ylim(axes1,[0 1.1]);
box(axes1,'on');
hold(axes1,'all');
plot1 = plot(alphamvec,[E0starvec;Estarvec],'Parent',axes1,'LineWidth',1);
set(plot1(1),'Marker','.','Color',[0 0.5 0],'MarkerSize',10,'DisplayName','No detection cost');
set(plot1(2),'Marker','*','Color',[0 0 1],'MarkerSize',10,'DisplayName','Costly detection');

title({'Surplus illegal mining for different mine sizes'});
xlabel({'Royalties share'});
ylabel({'Local authority optimal effort'});

legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.507738095238093 0.694841269841272 0.380357142857143 0.152380952380952]);

print('-dpng',strcat('C:\Users\Santi\Documents\mineriailegal\Latex\201610_PublicSeminar\Matlab\Effort'))
