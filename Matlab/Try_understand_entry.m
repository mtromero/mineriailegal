
%Price of gold
p=1.5;

%Quantity extracted
q=@(K) 20*(K/2)^0.5;

%Maximum capital
Kmin=1;
Kmax=100;
Kvec=[Kmin:Kmax];

%Number of budgets after
NR1=50;

%Royalties paid by firm
alpha=0.05;

%Cost of extraction
C=@(x) x*0.6;

%Mining title
T=10;



%Probability of detection
Omega=@(x,K) K/600;

V=0;
theta=0.9;
%Muni budget
R=100;
%Royalties to mining muni
alpham=0.55;
%If f linear then change in surplus from mining is independet of affi
f=@(x) 0.261*x;
S=@(K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);



piL=@(K) max(p*q(K)*(1-alpha)-C(q(K))-T,0);
b=@(K) (S(K)>0)*max(p*q(K)-C(q(K))-Omega(alpham,K)*K-piL(K)-theta*S(K),0);
piI=@(K) max(p*q(K)-C(q(K))-Omega(alpham,K)*K-b(K),0);



piLvec=arrayfun(piL,Kvec);
piIvec=arrayfun(piI,Kvec);


% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(Kvec,[piLvec;piIvec],'Parent',axes1,'LineWidth',1,'LineStyle','none');
set(plot1(1),'Marker','.','DisplayName','Legal');
set(plot1(2),'Marker','*','DisplayName','Illegal');

% Create title
title({'Profits Legal/Illegal different Capitals'});

% Create xlabel
xlabel({'Capital of the firm'});

% Create ylabel
ylabel({'Profits'});

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.732464878337864 0.401182226182226 0.0666178623718887 0.0873180873180873]);

print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\201602_DevoTea\ProfitsLvsI'))

%Price of gold
p=0.85;

S=@(K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);



piL=@(K) max(p*q(K)*(1-alpha)-C(q(K))-T,0);
b=@(K) (S(K)>0)*max(p*q(K)-C(q(K))-Omega(alpham,K)*K-piL(K)-theta*S(K),0);
piI=@(K) max((p*q(K)-C(q(K))-Omega(alpham,K)*K-b(K))*(b(K)>0),0);



piLvec=arrayfun(piL,Kvec);
piIvec=arrayfun(piI,Kvec);


% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(Kvec,[piLvec;piIvec],'Parent',axes1,'LineWidth',1,'LineStyle','none');
set(plot1(1),'Marker','.','DisplayName','Legal');
set(plot1(2),'Marker','*','DisplayName','Illegal');

% Create title
title({'Profits Legal/Illegal different Capitals'});

% Create xlabel
xlabel({'Capital of the firm'});

% Create ylabel
ylabel({'Profits'});

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.732464878337864 0.401182226182226 0.0666178623718887 0.0873180873180873]);

print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\201602_DevoTea\ProfitsLvsI_lowp'))
