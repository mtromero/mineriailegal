clear
%Price of gold
p=1.5;

%Quantity extracted
q=@(K) 20*(K/2)^0.5;

%Maximum capital
Kmin=1;
Kmax=100;
NK=10000;
Kvec=linspace(Kmin,Kmax,NK);
%Number of budgets after
NR1=50;

%Royalties paid by firm
alpha=0.05;

%Cost of extraction
C=@(x) x*0.6;

%Mining title
T=10;

piL=@(K) max(p*q(K)*(1-alpha)-C(q(K))-T,0);

%Probability of detection
Omega=@(x,K) K/600;
V=0;

%Muni budget
R=100;
R0=100;
%If f 
f=@(x) 0.085*x^1.2;
theta=0.9;

%% Before reform NO armed groups
%Royalties to mining muni
alpham=0.55;

SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
S=@(K) SRK(R0,K);
Kstar=fsolve(S,50);
prbefNag=ones(1,NR1)*sum(Kvec<Kstar)/NK;
pareabefNag=ones(1,NR1)*sum(Kvec.*(Kvec<Kstar))/sum(Kvec);


%% After the reform
%Muni budget
R1vec=(1:NR1);
praftNag=zeros(0,NR1);
pareaaftNag=zeros(0,NR1);
%Royalties to mining muni
alpham=0.1;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
for iR=1:NR1
S=@(K) SRK(R1vec(iR),K);
Kstar=fsolve(S,50);
praftNag(iR)=sum(Kvec<Kstar)/NK;
pareaaftNag(iR)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);
end



%% Armed groups with higher f

%Probability of detection
Omega=@(x,K) K/600;
theta=0.9;
f=@(x) 0.097*x^1.2;

%% Before reform 
%Royalties to mining muni
alpham=0.55;

SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
S=@(K) SRK(R0,K);
Kstar=fsolve(S,50);
prbefAG=ones(1,NR1)*sum(Kvec<Kstar)/NK;
pareabefAG=ones(1,NR1)*sum(Kvec.*(Kvec<Kstar))/sum(Kvec);


%% After the reform
%Muni budget
R1vec=(1:NR1);
praftAG=zeros(0,NR1);
pareaaftAG=zeros(0,NR1);
%Royalties to mining muni
alpham=0.1;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
for iR=1:NR1
S=@(K) SRK(R1vec(iR),K);
Kstar=fsolve(S,50);
praftAG(iR)=sum(Kvec<Kstar)/NK;
pareaaftAG(iR)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);
end


%% Illegal as percentage of area mined
alpham0=0.55;
alpham1=0.1;
q0=1000;
affivec=100*(p*q0*alpha*alpham1+R1vec-p*q0*alpha*alpham0)/(p*q0*alpha*alpham0+R);

% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[0.69 0.82]);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(affivec,[pareaaftAG;pareaaftNag;pareabefAG;pareabefNag],'Parent',axes1,'LineWidth',2);
set(plot1(1),'Marker','.','LineStyle','--','Color',[1 0 0],...
    'DisplayName','AG- after the reform');
set(plot1(2),'LineStyle','--','Color',[0 0 1],...
    'DisplayName','nAG- after the reform');

set(plot1(3),'Color',[1 0 0],...
    'DisplayName','AG- before the reform');

set(plot1(4),'Color',[0 0 1],'DisplayName','nAG- before the reform');


% Create xlabel
xlabel('Change in royalties as percentage of budget');

% Create ylabel
ylabel('Percentage of area mined illegally');

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.51229861256362 0.354954538449682 0.392571428571428 0.188888888888889]);
print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\201605_Paper_royalties_illegal\Prob_areaillegal_AGf'))
print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\201606_AppLunch\Prob_areaillegal_AGf'))



%% Armed groups with lower pr detection

%Probability of detection
Omega=@(x,K) K/609;
theta=0.9;
f=@(x) 0.085*x^1.2;

%% Before reform 
%Royalties to mining muni
alpham=0.55;

SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
S=@(K) SRK(R0,K);
Kstar=fsolve(S,50);
prbefAG=ones(1,NR1)*sum(Kvec<Kstar)/NK;
pareabefAG=ones(1,NR1)*sum(Kvec.*(Kvec<Kstar))/sum(Kvec);


%% After the reform
%Muni budget
R1vec=(1:NR1);
praftAG=zeros(0,NR1);
pareaaftAG=zeros(0,NR1);
%Royalties to mining muni
alpham=0.1;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
for iR=1:NR1
S=@(K) SRK(R1vec(iR),K);
Kstar=fsolve(S,50);
praftAG(iR)=sum(Kvec<Kstar)/NK;
pareaaftAG(iR)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);
end


%% Illegal as percentage of area mined
alpham0=0.55;
alpham1=0.1;
q0=1000;
affivec=100*(p*q0*alpha*alpham1+R1vec-p*q0*alpha*alpham0)/(p*q0*alpha*alpham0+R);

% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[0.69 0.82]);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(affivec,[pareaaftAG;pareaaftNag;pareabefAG;pareabefNag],'Parent',axes1,'LineWidth',2);
set(plot1(1),'Marker','.','LineStyle','--','Color',[1 0 0],...
    'DisplayName','AG- after the reform');
set(plot1(2),'LineStyle','--','Color',[0 0 1],...
    'DisplayName','nAG- after the reform');

set(plot1(3),'Color',[1 0 0],...
    'DisplayName','AG- before the reform');

set(plot1(4),'Color',[0 0 1],'DisplayName','nAG- before the reform');


% Create xlabel
xlabel('Change in royalties as percentage of budget');

% Create ylabel
ylabel('Percentage of area mined illegally');

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.51229861256362 0.354954538449682 0.392571428571428 0.188888888888889]);
print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\201605_Paper_royalties_illegal\Prob_areaillegal_AGPr'))
print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\201606_AppLunch\Prob_areaillegal_AGPr'))



