%%% This code is like Bribe_simulation_convex except that draws the
%%% distribution of capitals of the firms from an extreme value (ev)
%%% distribution instead of uniform

rngseed=2016;
rng(rngseed);
%Price of gold
p=1;

%Quantity extracted
q=@(K) 200*(K/2)^0.5;

%Maximum capital
Kmin=1;
Kmax=100;
NK=10000;

%Number of budgets after
NR1=50;

%Royalties paid by firm
alpha=0.05;
alpham0=0.55;
alpham1=0.1;

%Cost of extraction
C=@(x) x*0.5;

%Mining title
T=10;

piL=@(K) max(p*q(K)*(1-alpha)-C(q(K))-T,0);

%Probability of detection
Omega=@(x,K) K/600;

V=0;

%Muni budget
R=100;


%Distribution of firms
Kvec=random('ev',60,15,1,NK);

%Make sure is positive
Kvec=max(0.1,Kvec);



%% Before reform linear
%Muni budget
R0=100;
%Royalties to mining muni
alpham=0.55;
%If f linear then change in surplus from mining is independet of affi
f=@(x) 0.25*x;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
S=@(K) SRK(R0,K);

Kstar=fsolve(S,50);
%Calculate pctg of mines below cutoff
prbeflinear=ones(1,NR1)*sum(Kvec<Kstar)/NK;
%Calculate pctg of area below cutoff
pareabeflinear=ones(1,NR1)*sum(Kvec.*(Kvec<Kstar))/sum(Kvec);


%% After reform linear
%Muni budget
R1vec=(1:NR1);
praftlinear=zeros(0,NR1);
pareaaftlinear=zeros(0,NR1);
%Royalties to mining muni
alpham=0.1;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
for iR=1:NR1
S=@(K) SRK(R1vec(iR),K);
Kstar=fsolve(S,50);
praftlinear(iR)=sum(Kvec<Kstar)/NK;
pareaaftlinear(iR)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);
end

%% Before reform convex

%Royalties to mining muni
alpham0=0.55;
alpham=0.55;
%If f convex then change in surplus from mining 
f=@(x) 0.085*x^1.2;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
S=@(K) SRK(R0,K);

Kstar=fsolve(S,50);
prbefconvex=ones(1,NR1)*sum(Kvec<Kstar)/NK;
pareabefconvex=ones(1,NR1)*sum(Kvec.*(Kvec<Kstar))/sum(Kvec);
%% After reform convex
%Muni budget
R1vec=(1:NR1);
praftconvex=zeros(0,NR1);
pareaaftconvex=zeros(0,NR1);
%Royalties to mining muni
alpham=0.1;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
for iR=1:NR1
S=@(K) SRK(R1vec(iR),K);
Kstar=fsolve(S,50);
praftconvex(iR)=sum(Kvec<Kstar)/NK;
pareaaftconvex(iR)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);
end

%% Before reform concave

%Royalties to mining muni
alpham0=0.55;
alpham=0.55;
%If f convex then change in surplus from mining 
f=@(x) 0.82*x^0.8;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
S=@(K) SRK(R0,K);

Kstar=fsolve(S,50);
prbefconcave=ones(1,NR1)*sum(Kvec<Kstar)/NK;
pareabefconcave=ones(1,NR1)*sum(Kvec.*(Kvec<Kstar))/sum(Kvec);
%% After reform concave
%Muni budget
R1vec=(1:NR1);
praftconcave=zeros(0,NR1);
pareaaftconcave=zeros(0,NR1);
%Royalties to mining muni
alpham=0.1;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
for iR=1:NR1
S=@(K) SRK(R1vec(iR),K);
Kstar=fsolve(S,50);
praftconcave(iR)=sum(Kvec<Kstar)/NK;
pareaaftconcave(iR)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);
end

q=1000;
affivec=100*(p*q*alpha*alpham1+R1vec-p*q*alpha*alpham0)/(p*q*alpha*alpham0+R);

%% Plot pctg mines
% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[0.15 0.2]);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(affivec,[praftconvex;praftlinear;praftconcave;prbefconvex;prbeflinear;prbefconcave],'Parent',axes1,'LineWidth',2);
set(plot1(1),'Marker','.','LineStyle','--','Color',[1 0 0],...
    'DisplayName','Convex- after the reform');
set(plot1(2),'LineStyle','--','Color',[0 0 1],...
    'DisplayName','Linear- after the reform');
set(plot1(3),'MarkerSize',3,'Marker','square','LineStyle','none',...
    'Color',[0 0.498039215803146 0],...
    'DisplayName','Concave- after reform');
set(plot1(4),'Color',[1 0 0],...
    'DisplayName','Convex- before the reform');

set(plot1(5),'Color',[0 0 1],'DisplayName','Linear- before the reform');
set(plot1(6), 'Color',[0 0.498039215803146 0],...
    'DisplayName','Concave- before reform');

% Create xlabel
xlabel('Change in royalties as percentage of budget');

% Create ylabel
ylabel('Percentage of illegal mines');

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.51229861256362 0.354954538449682 0.392571428571428 0.188888888888889]);
print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\042015_Paper_royalties_illegal\Prob_illegal_convex_evrs'))
print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\201602_DevoTea\Prob_illegal_convex_evrs'))


%% Plot pctg area
% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[0.05 0.09]);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(affivec,[pareaaftconvex;pareaaftlinear;pareaaftconcave;pareabefconvex;pareabeflinear;pareabefconcave],'Parent',axes1,'LineWidth',2);
set(plot1(1),'Marker','.','LineStyle','--','Color',[1 0 0],...
    'DisplayName','Convex- after the reform');
set(plot1(2),'LineStyle','--','Color',[0 0 1],...
    'DisplayName','Linear- after the reform');
set(plot1(3),'MarkerSize',3,'Marker','square','LineStyle','none',...
    'Color',[0 0.498039215803146 0],...
    'DisplayName','Concave- after reform');
set(plot1(4),'Color',[1 0 0],...
    'DisplayName','Convex- before the reform');

set(plot1(5),'Color',[0 0 1],'DisplayName','Linear- before the reform');
set(plot1(6), 'Color',[0 0.498039215803146 0],...
    'DisplayName','Concave- before reform');

% Create xlabel
xlabel('Change in royalties as percentage of budget');

% Create ylabel
ylabel('Percentage of area mined illegaly');

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.51229861256362 0.354954538449682 0.392571428571428 0.188888888888889]);
print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\042015_Paper_royalties_illegal\Parea_illegal_convex_evrs'))
print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\201602_DevoTea\Parea_illegal_convex_evrs'))

