clear
latexslides='C:\Users\santi\Documents\mineriailegal\Latex\201607_PracticeJM1\Matlab\';
latexpaper='C:\Users\santi\Documents\mineriailegal\Latex\201605_Paper_royalties_illegal\';
%Price of gold
p=1.5;

%Quantity extracted
q=@(K) 20*(K/2)^0.5;

%Maximum capital
Kmin=1;
Kmax=100;
% If NK<15000 de graph doesn't look smooth
NK=15000;
Kvec=linspace(Kmin,Kmax,NK);


%Royalties paid by firm
alpha=0.05;

%Cost of extraction
C=@(x) x*0.6;

%Mining title
T=10;

piL=@(K) max(p*q(K)*(1-alpha)-C(q(K))-T,0);

%Probability of detection
Omega=@(x,K) K/600;
V=0;

%Muni budget
scale_budget=1.4;

R=100*scale_budget;
R0=100*scale_budget;
%Number of budgets after
NR1=70;
R1vec=(1:NR1)*scale_budget;
alpham0=0.55;
alpham1=0.1;



%% Before reform linear

%Royalties to mining muni
alpham=alpham0;
%If f linear then change in surplus from mining is independet of affi
f=@(x) 0.261*x/scale_budget;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
S=@(K) SRK(R0,K);

Kstar=fsolve(S,50);
prbeflinear=ones(1,NR1)*sum(Kvec<Kstar)/NK;
%Calculate pctg of area below cutoff
pareabeflinear=ones(1,NR1)*sum(Kvec.*(Kvec<Kstar))/sum(Kvec);

%% After reform linear
%Muni budget

praftlinear=zeros(0,NR1);
pareaaftlinear=zeros(0,NR1);
%Royalties to mining muni
alpham=alpham1;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
for iR=1:NR1
S=@(K) SRK(R1vec(iR),K);
Kstar=fsolve(S,50);
praftlinear(iR)=sum(Kvec<Kstar)/NK;
pareaaftlinear(iR)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);
end



%% Before reform target

%Royalties to mining muni

alpham=alpham0;

lambda=0.25;
f=@(x) (1-lambda*(x<R0))*0.261*x/scale_budget;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
S=@(K) SRK(R0,K);

Kstar=fsolve(S,50);

pareabeftarget=ones(1,NR1)*sum(Kvec.*(Kvec<Kstar))/sum(Kvec);

%I expand NK to smooth the graph but I need to adjust so same mass of firms
q0=sum(arrayfun(q,Kvec).*(Kvec>Kstar))/(NK/Kmax);
%% After reform target

pareaafttarget=zeros(0,NR1);
%Royalties to mining muni

alpham=alpham1;
SRK=@(R,K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);
for iR=1:NR1
S=@(K) SRK(R1vec(iR),K);
Kstar=fsolve(S,50);
pareaafttarget(iR)=sum(Kvec.*(Kvec<Kstar))/sum(Kvec);
end



affivec=100*(p*q0*alpha*alpham1+R1vec-p*q0*alpha*alpham0)/(p*q0*alpha*alpham0+R);




%% Graph to match the empirics looking only at change
changeil_target=100*(pareaafttarget-pareabeftarget);
changeil_linear=100*(pareaaftlinear-pareabeflinear);

% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1,'YTickLabel',{'0'},'YTick',4,...
    'XTick',[-10 0 10],...
    'FontSize',18);
% Uncomment the following line to preserve the Y-limits of the axes
xlim(axes1,[-25 25]);
%ylim(axes1,[4 6]);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(affivec,[changeil_target;changeil_linear],'Parent',axes1,'LineWidth',2);
set(plot1(1),'Marker','.','LineStyle','--','Color',[1 0 0],...
    'DisplayName','Target');
set(plot1(2),'LineStyle','--','Color',[0 0 1],...
    'DisplayName','Linear');



% Create xlabel
xlabel('Change in royalties as percentage of budget','FontSize',18);

% Create ylabel
ylabel('Change in percentage of area mined illegally','FontSize',18);

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.632083594784913 0.221653693745328 0.110541727672035 0.182586094866795]);
print('-dpng',strcat(latexpaper,'theo_pred_target'))
print('-dpng',strcat(latexslides,'theo_pred_target'))