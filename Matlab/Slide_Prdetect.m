
%Price of gold
p=0.75;

%Quantity extracted
q=@(K) 20*(K/2)^0.5;

%Maximum capital
Kmin=1;
Kmax=100;
Kvec=[Kmin:Kmax];
vec0=0*Kvec;

%Number of budgets after
NR1=50;

%Royalties paid by firm
alpha=0.05;

%Cost of extraction
C=@(x) x*0.6;

%Mining title
T=5;



%Probability of detection
OmegaH=@(K) K^0.5/80;
Omega=@(K) K^0.5/60;

V=0;
theta=0.9;
%Muni budget
R=100;
%Royalties to mining muni
alpham=0.55;
alphamL=0.1;
%If f linear then change in surplus from mining is independet of affi
f=@(x) x;
S=@(K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(q(K))*(K+V);
SamL=@(K) T+f(R)-f(R+p*q(K)*alpha*alphamL)+p*q(K)*alpha-Omega(q(K))*(K+V);
SprH=@(K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-OmegaH(q(K))*(K+V);
SamLprH=@(K) T+f(R)-f(R+p*q(K)*alpha*alphamL)+p*q(K)*alpha-OmegaH(q(K))*(K+V);

piL=@(K) max(p*q(K)*(1-alpha)-C(q(K))-T,0);
b=@(K) (S(K)>0)*max(p*q(K)-C(q(K))-Omega(K)*K-piL(K)-theta*S(K),0);
piI=@(K) max((p*q(K)-C(q(K))-Omega(K)*K-b(K))*(b(K)>0),0);
bamL=@(K) (SamL(K)>0)*max(p*q(K)-C(q(K))-Omega(K)*K-piL(K)-theta*SamL(K),0);
piIamL=@(K) max((p*q(K)-C(q(K))-Omega(K)*K-bamL(K))*(bamL(K)>0),0);
bH=@(K) (SprH(K)>0)*max(p*q(K)-C(q(K))-OmegaH(K)*K-piL(K)-theta*SprH(K),0);
piIH=@(K) max((p*q(K)-C(q(K))-OmegaH(K)*K-bH(K))*(bH(K)>0),0);
bamLH=@(K) (SamLprH(K)>0)*max(p*q(K)-C(q(K))-OmegaH(K)*K-piL(K)-theta*SamLprH(K),0);
piIamLH=@(K) max((p*q(K)-C(q(K))-OmegaH(K)*K-bamLH(K))*(bamLH(K)>0),0);

piLvec=arrayfun(piL,Kvec);
piIvec=arrayfun(piI,Kvec);
piIamLvec=arrayfun(piIamL,Kvec);
piIHvec=arrayfun(piIH,Kvec);
piIamLHvec=arrayfun(piIamLH,Kvec);

Svec=arrayfun(S,Kvec).*(piIvec>0);
SamLvec=arrayfun(SamL,Kvec).*(piIamLvec>0);
SprHvec=arrayfun(SprH,Kvec).*(piIHvec>0);
SamLHvec=arrayfun(SamLprH,Kvec).*(piIamLHvec>0);

figure1 = figure('Color',[1 1 1]);
axes1 = axes('Parent',figure1,'YTickLabel',{'0'},'YTick',0,'XTickLabel',{},'XTick',zeros(1,0),'FontSize',18);
box(axes1,'on');
hold(axes1,'all');
plot1 = plot(Kvec,[Svec;SamLvec],'Parent',axes1,'LineWidth',1,'LineStyle','none');
set(plot1(1),'Marker','.','MarkerSize',16,'DisplayName','Before Reform');
set(plot1(2),'Marker','^','MarkerSize',4,'DisplayName','After Reform');

title({'Surplus illegal mining for different mine sizes'});
xlabel({'Capital of the firm'});
ylabel({'Surplus'});

legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.507738095238093 0.694841269841272 0.380357142857143 0.152380952380952]);

print('-dpng',strcat('C:\Users\Santi\Documents\mineriailegal\Latex\201610_PublicSeminar\Matlab\Surplusbeta'))

figure1 = figure('Color',[1 1 1]);
axes1 = axes('Parent',figure1,'YTickLabel',{'0'},'YTick',0,'XTickLabel',{},'XTick',zeros(1,0),'FontSize',18);
box(axes1,'on');
hold(axes1,'all');
plot1 = plot(Kvec,[Svec;SamLvec;SprHvec;SamLHvec],'Parent',axes1,'LineWidth',1,'LineStyle','none');
set(plot1(1),'Marker','.','MarkerSize',16,'DisplayName','High Pr-Before');
set(plot1(2),'Marker','.','MarkerSize',12,'DisplayName','High Pr-After');
set(plot1(3),'Color','r','Marker','<','MarkerSize',5,'DisplayName','Low Pr-Before');
set(plot1(4),'Color','m','Marker','>','MarkerSize',5,'DisplayName','Low Pr-After');

title({'Surplus illegal and probability of detection'});
xlabel({'Capital of the firm'});
ylabel({'Surplus'});

legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.516666666666664 0.607539682539684 0.3875 0.293650793650794]);

print('-dpng',strcat('C:\Users\Santi\Documents\mineriailegal\Latex\201610_PublicSeminar\Matlab\SurplusPr'))

break
% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1,'YTickLabel',{},'YTick',zeros(1,0),'XTickLabel',{},'XTick',zeros(1,0),'FontSize',18);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(Kvec,[piLvec;piIvec;piIHvec],'Parent',axes1,'LineWidth',1,'LineStyle','none');
set(plot1(1),'Marker','.','DisplayName','Legal');
set(plot1(2),'Marker','p','MarkerSize',4,'DisplayName','Illegal-Low Pr');
set(plot1(3),'Marker','^','MarkerSize',4,'DisplayName','Illegal-High Pr');

% Create title
title({'Profits Legal/Illegal different Capitals'});

% Create xlabel
xlabel({'Capital of the firm'});

% Create ylabel
ylabel({'Profits'});

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.732464878337864 0.401182226182226 0.0666178623718887 0.0873180873180873]);

print('-dpng',strcat('C:\Users\Santi\Documents\mineriailegal\Latex\201610_PublicSeminar\Matlab\ProfitsLvsI'))

%Price of gold
p=0.85;

S=@(K) T+f(R)-f(R+p*q(K)*alpha*alpham)+p*q(K)*alpha-Omega(alpham,q(K))*(K+V);



piL=@(K) max(p*q(K)*(1-alpha)-C(q(K))-T,0);
b=@(K) (S(K)>0)*max(p*q(K)-C(q(K))-Omega(alpham,K)*K-piL(K)-theta*S(K),0);
piI=@(K) max((p*q(K)-C(q(K))-Omega(alpham,K)*K-b(K))*(b(K)>0),0);



piLvec=arrayfun(piL,Kvec);
piIvec=arrayfun(piI,Kvec);


% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(Kvec,[piLvec;piIvec],'Parent',axes1,'LineWidth',1,'LineStyle','none');
set(plot1(1),'Marker','.','DisplayName','Legal');
set(plot1(2),'Marker','*','DisplayName','Illegal');

% Create title
title({'Profits Legal/Illegal different Capitals'});

% Create xlabel
xlabel({'Capital of the firm'});

% Create ylabel
ylabel({'Profits'});

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.732464878337864 0.401182226182226 0.0666178623718887 0.0873180873180873]);

print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\201602_DevoTea\ProfitsLvsI_lowp'))
