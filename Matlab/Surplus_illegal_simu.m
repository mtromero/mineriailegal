
%Non royalties budget
R=100;

%Price of gold
p=1;

%Quantity extracted
q=1000;

%Royalties paid by firm
alpha=0.05;

%Royalties to mining muni
alpham0=0.55;
alpham1=0.1;

%Post-royalties transfer
R1vec=[1:100];

%If f linear then change in surplus from mining is independet of affi
f1=@(x) x;
%If f convex then change in surplus from mining is higher for negatively
%affected
%f=@(x) x^1.1;
%If f convex then change in surplus from mining is higher for positively
%affected
f2=@(x) 20*x^0.5;
%If f has reference dependence
lambda=0.25;
f3=@(x) x+lambda*(p*q*alpha*alpham0>x)*(x-p*q*alpha*alpham0);

dSdRef1=@(x) f1(R+x)-f1(p*q*alpha*alpham1+R+x)-(f1(R)-f1(p*q*alpha*alpham0+R));
dSdRef2=@(x) f2(R+x)-f2(p*q*alpha*alpham1+R+x)-(f2(R)-f2(p*q*alpha*alpham0+R));
dSdRef3=@(x) R+x-(R+f3(p*q*alpha*alpham1+x))-(R-(f3(p*q*alpha*alpham0)+R));

affivec=100*(p*q*alpha*alpham1+R1vec-p*q*alpha*alpham0)/(p*q*alpha*alpham0+R);
dSdRef1vec=arrayfun(dSdRef1,R1vec);
dSdRef2vec=arrayfun(dSdRef2,R1vec);
dSdRef3vec=arrayfun(dSdRef3,R1vec);

% Create figure
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[20 30]);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to plot
plot1 = plot(affivec,[dSdRef1vec;dSdRef2vec;dSdRef3vec],'Parent',axes1,'LineWidth',2);
set(plot1(1),'DisplayName','Linear');
set(plot1(2),'LineStyle','--','DisplayName','Concave');
set(plot1(3),'LineStyle',':','DisplayName','Ref Dependence');

% Create xlabel
xlabel('Change in royalties as percentage of budget');

% Create ylabel
ylabel('Change in illegal mining surplus');

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.684724255734505 0.359546925566341 0.112005856515373 0.117799352750809]);
print('-dpng',strcat('C:\Users\santi\Documents\mineriailegal\Latex\042015_Paper_royalties_illegal\Surplus_illegal.pdf'))

