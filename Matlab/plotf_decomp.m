%Weight given to budget use in muni projects
theta=0.5;
% Possible budgets
Bmin=1;
Bmax=100;
NB=100;
Bvec=linspace(Bmin,Bmax,NB);
%Fraction stolen from muni budget
alpha=@(B) 0.1*B^1.1;
%Valuation includes stolen plus political value of projects
f=@(B) alpha(B)*B+theta*(1-alpha(B))*B;
fBvec=arrayfun(f,Bvec);
plot(Bvec,fBvec);