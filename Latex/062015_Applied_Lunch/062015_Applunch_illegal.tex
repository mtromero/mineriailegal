\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}  
\usepackage{rotating}

\usepackage{nameref}
\usepackage{float}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{latexsym}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}

\usepackage{apacite}
\mode<presentation>{}
\usetheme{Warsaw}
\usepackage{graphicx}



\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Redistribuiting Royalties]{Optimal Royalties' Redistribution Policy}

\author{Mauricio Romero (UCSD) \\ Santiago Saavedra }

\normalsize

\date{June 5th 2015}




\begin{document}

\frame{\titlepage} 

\begin{frame}[label=roy_ineq]
\frametitle{Inequality in royalties distribution}
\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{roy_ineq.pdf} 
\label{Circle size proportional to population of the municipality}
\end{center}
\end{figure}
 \hyperlink{transf_ineq}{\beamergotobutton{Total transfers}}  \hyperlink{budget_ineq}{\beamergotobutton{Budget}}
\end{frame}

\begin{frame}

\frametitle{Motivation}
\begin{itemize}
\item Standard problem of optimal taxation with evasion
\begin{itemize}
\item Revenue vs efficiency
\end{itemize}
\item Law reform in Colombia in 2012
\begin{itemize}
\item Reduces direct royalties transfers to mining municipalities from 55\% to around 10\% 
\item Distributes resources nationally according to poverty indicators 
\item Increases oversight by the National Government
\end{itemize}
\item 63\% of the mines in Colombia are illegal (at least 51,826 miners, which is 50\% of mining employment)
\begin{itemize}
\item Less likely to pay royalties
\end{itemize}

\end{itemize}
\end{frame}


\begin{frame}

\frametitle{Research question}

What is the optimal royalties redistribution policy?
\begin{itemize}
\item What is the elasticity of evasion? Proxied by:
\begin{itemize}
\item Area mined without a permit
\item Reported production of legal mines
\end{itemize}
\item What is the marginal benefit of a royalties' dollar spent on
\begin{itemize}
\item Infant mortality rates
\end{itemize}

\end{itemize}
\end{frame}


\begin{frame}

\frametitle{Table of contents}

\begin{enumerate}
\item Details of the reform
\item Constructing the illegal mining time series and other data
\item Identification strategy
\item Preliminary results

\end{enumerate}
\end{frame}

\begin{frame}

\frametitle{Related literature}

\begin{itemize}
\item Optimal taxation and evasion
\begin{itemize}
\item Gorodnichenko et al (2009)
\end{itemize}
\item Decentralization and environmental impacts
\begin{itemize}
\item Burgess etal (2012), Lipscomb-Mobarak(2014), Sigman(2005)
\end{itemize}
\item Using satellites to study economic outcomes
\begin{itemize}
\item Henderson, Storeygard, and Weil (2011), Jayachandran (2009), Foster, Gutierrez,and Kumar (2009)
\end{itemize}
\item Mining 
\begin{itemize}
\item Idrobo et al (2012), Sanchez de la Sierra(2015)
\end{itemize}

\end{itemize}
\end{frame}




\begin{frame}[label=bef_reform]
\frametitle{Before the reform}
\begin{figure}[H]
\begin{center}
\includegraphics[width=1\textwidth]{bef_reform.pdf} 
%\caption{\tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
%\label{fig:evol_mineria}
\end{center}
\end{figure}
 \hyperlink{roybymin}{\beamergotobutton{Pctg by mineral}}
\end{frame}

\begin{frame}

\frametitle{After the reform}
\begin{figure}[H]
\begin{center}
\includegraphics[width=1.1\textwidth]{aft_reform.pdf} 
%\caption{\tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
%\label{fig:evol_mineria}
\end{center}
Poverty defined with index of unmet needs ($NBI$)  \hyperlink{nbi}{\beamergotobutton{NBI}}
\end{figure}

\end{frame}

\begin{frame}

\frametitle{Further details of the reform}

\begin{itemize}
\item Transition period of decreasing direct royalties share
\item Guarantee to mining municipalities that  royalties won't fall below 50\% of 2007-2010 average
\item The regional \textbf{development} fund resources are distributed among states according to:
\[ w_i=\left( \frac{Population_i}{Population_{COL}} \right)^{0.6} \left( \frac{NBI_{i}}{NBI_{COL}} \right)^{0.4} \]
\item The regional \textbf{compensation} fund resources are distributed among poor states ($NBI>30$) according to:
\[ w_i=\left( \frac{Population_i}{Population_{COL}} \right)^{0.4} \left( \frac{NBI_{i}}{NBI_{COL}} \right)^{0.5} \left( \frac{Unemployment_{i}}{Unemployment_{COL}} \right)^{0.1} \]

\end{itemize}
\end{frame}

\begin{frame}[label=thfram]

\frametitle{Theoretical framework}
\begin{itemize}
\item The reform reduced the percentage of direct transfers to mining municipalities (``increased the tax''). 
\begin{itemize}
\item[$\Rightarrow$] This increased the incentive of local authorities to collude with mine owners
\begin{itemize}
\item[-] New mines: obtaining a permit  \hyperlink{thmod}{\beamergotobutton{}}
\item[-] Existing mines: accurately reporting production
\end{itemize}
\end{itemize}
\item The reform increased transfers to poor municipalities. 
\begin{itemize}
\item[+] Improve allocation of resources to high marginal health improvements areas
\end{itemize}
\item The reform allocated funds to other purposes. 
\begin{itemize}
\item[-] Potentially reduced total health expenditure
\end{itemize}
\end{itemize}
\end{frame}




\begin{frame}

\frametitle{Data sources}

\begin{itemize}
\item Area mined illegally (without a permit)
\begin{itemize}
\item Satellite imagery from Landsat 7
\item Mining census 2010-2011 for training
\end{itemize}
\item Royalties by municipality 2004-2014 (National Planning Department)
\item Infant mortality rate (DANE)
\item Socio-economic characteristics of municipalities CEDE

\end{itemize}
\end{frame}






\frame{\frametitle{}
\includegraphics[scale=0.4]{AllLayers.png} 
}

\begin{frame}
%Split this in two and more colorful
\frametitle{Identifying Illegal Mines}
\begin{enumerate}
\item The 2010 Census identified legal and illegal mines in 23 states
\begin{itemize}
\item Separate 80\% of mines for training, 20\% testing
\end{itemize}
\item Remove clouds from each scene (Goslee, 2011)
\begin{itemize}
\item More than 1 billion pixels every 2 weeks initially for the country
\end{itemize}
\item Yearly cloudless composite
\begin{itemize}
\item Eight color bands (30x30mts) with values from 1 to 255 according to reflected energy
\end{itemize}
\item Subbaging: Random forest on several balanced subsamples (Paleologo et. al. 2010)
\item Cross-validate with remaining 20\%
\item ``Subtract'' legal mines
\item Predict for all years

\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Sample zoom in on predicted pixel}
\includegraphics[scale=0.48]{ZoomPixel.png} 
\end{frame}

\begin{frame}[label=roc]
\frametitle{Receiver Operating Characteristic (ROC) curve}
\includegraphics[scale=0.48]{ROCconCoordenadas.png} 

\hyperlink{rocstate}{\beamergotobutton{ROC One State}} \hyperlink{confmat}{\beamergotobutton{Confusion Matrix}}
\end{frame}



\begin{frame}[label=sumstat]
% Include sumstats for the munis in the illegal subsample. 
\frametitle{Summary statistics}
{\tiny \input{sumstats}}
{\tiny . \\An observation is a municipality-year. The royalties per capita are approximately 12U\$ per person per year. We are missing fiscal information for 2013-2014. The poverty index is updated after every census (2005) and in 2011 specifically for the reform. }
\end{frame}

\begin{frame}

\frametitle{Effect of the reform on municipality budget}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.87\textwidth]{Muni_minero_affectedTotal.pdf} 


\end{center}
\end{figure}

\end{frame}

\begin{frame}

\frametitle{Effect of the reform selected sample}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.87\textwidth]{Muni_minero_affectedTotal_selec.pdf} 


\end{center}
\end{figure}

\end{frame}

\begin{frame}

\frametitle{Identification strategy}

\begin{itemize}
\item Diff-diff strategy
\begin{itemize}
\item Before-after the reform
\item Municipalities differentially affected
\end{itemize}
\item Exploit RD threshold of poverty at 30\%
\item Concerns:
\begin{itemize}
\item The reform had been discussed in Congress since 2010
\item The National Government didn't accept new applications for permits from the end of 2011 to July 2013
\item Other National Government transfer rules constant
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}

\frametitle{Evolution of predicted illegal mining in selected sample}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.87\textwidth]{evolv_illegal.pdf} 
\end{center}
\end{figure}

\end{frame}

\begin{frame}

\frametitle{Empirical specification: }

{\footnotesize
\[\Delta \% Illegal_{mt}= \alpha+ \beta_+ \Delta \% Royalties_{mt}+ \beta_- \Delta \% Royalties_{mt}     + \gamma \Delta \% PriceIndex_{mt} + \varepsilon_{mt}\]
}
\begin{itemize}
\item $\Delta \% Illegal_{mt}$ is the increase of illegal mining in municipality $m$ at time $t$
\item $\Delta \% Royalties_{mt}$ is the change in royalties per capita allocated
\item $\Delta \% PriceIndex_{mt}$ is a set of time-varying controls which will include tax revenue and the price index of minerals extracted

\end{itemize}
\end{frame}

\begin{frame}
%Analogous graph for illegal mining
\frametitle{Infant mortality rate}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\textwidth]{tmi.pdf} 

\end{center}
\end{figure}

\end{frame}

\begin{frame}

\frametitle{Empirical specification: }

{\footnotesize
\[Y_{mt}= \sum_{i \in \Omega} \beta_i Royalties_{mt}  \times Before^{\Omega(i)}_t \times Mining^{\Omega(i)}_{m} \times Poor^{\Omega(i)}_{m} +X_{mt} \alpha+\gamma_m+ \gamma_{t}+\varepsilon_{mt}\]
}
\begin{itemize}
\item $Y_{mt}$ is the infant mortality in municipality $m$ at time $t$
\item $Royalties_{mt}$ is the amount of royalties per capita allocated
\item $X_{mt}$ is a set of time-varying controls which will include tax revenue
\item $\gamma_m$ municipality fixed effects
\item $\gamma_t$ time fixed effects
\end{itemize}
\end{frame}

%%Put together Basic_reg and bef_reg in a single slide
\begin{frame}[label=tmi_reg]
\frametitle{Royalties' effect on the infant mortality rate}

\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{3}{c}}
\toprule          
                  \multicolumn{3}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}  &  \multicolumn{1}{c}{(2)}   \\
\midrule

\input{Basic_reg} 
Time FE & Yes & Yes \\
Municipality FE & Yes & Yes\\
\bottomrule
\multicolumn{3}{c}{\scriptsize Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{3}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
The mean of the independent variable is 0.03 or U\$ 12 per capita. That is, at the mean, the TMI is reduced by 1/1000 \hyperlink{Bin_reg}{\beamergotobutton{Heterogeneous}}
\end{frame}






\begin{frame}

\frametitle{Future work}

\begin{itemize}

\item Get all the data
\item $Royalties_{mt}$ could include money from state level funds. Its value is affected by institutional capacity, as is illegal mining. 
\begin{itemize}
\item Instrument the amount of royalties with the law's hypothetical royalties
\[ {Royalties}_{mt}=\alpha {LawRoyalties}_{mt} \]
\end{itemize}
\item RD with funds cutoff
\item Mineral heterogeneity 
\begin{itemize}


\item Using satellites we mostly detect illegal mining of precious metals and construction materials
\item There is also illegal mining of coal, but mostly underground
\item Oil and gas are hard to extract illegally

\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[plain, noframenumbering]
\frametitle{Thank you}
\begin{itemize}
\item Gracias
\item Asante Sana
\item Merci
\item Obrigado
\item Grazie
\end{itemize}

\end{frame}


%\begin{frame}
%
%\frametitle{Discontinuity on FCR fund}
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.75\textwidth]{FDR_NBI_PD.pdf} 
%%\caption{\tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
%%\label{fig:evol_mineria}
%\end{center}
%\end{figure}
%
%\end{frame}


\begin{frame}[label=transf_ineq, noframenumbering]
\frametitle{National government transfers inequality}
\begin{figure}
\begin{center}
\includegraphics[width=0.9\textwidth]{transf_ineq.pdf} 

\end{center}
\end{figure}

\hyperlink{roy_ineq}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[label=budget_ineq, noframenumbering]
\frametitle{Municipality budgets inequality}
\begin{figure}
\begin{center}
\includegraphics[width=0.9\textwidth]{budget_ineq.pdf} 

\end{center}
\end{figure}

\hyperlink{roy_ineq}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[label=roybymin, noframenumbering]
\frametitle{Royalties by mineral}

\begin{itemize}

\item Hydrocarbons 8-25\% $^*$
\item Coal 5-10\% $^*$
\item Nickel 12\%
\item Alluvial gold 6\%
\item Copper, iron 5\%
\item Gold, silver 4\%
\item Gems 3\%
\item Construction materials 1\%
\end{itemize}
* Depends on volume extracted
\hyperlink{bef_reform}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[label=nbi, noframenumbering]
\frametitle{How the NBI poverty index is constructed}
Percentage of population with at least one unmet need:
\begin{itemize}

\item Inadequate housing (floor, walls)
\item Lack of toilet, sewage or water
\item More than 3 people per room
\item $6-12$ years old kid not attending school
\item Dependency ratio $>3$ or household head 2 years of education
\end{itemize}

\hyperlink{aft_reform}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[label=sgp, noframenumbering]
\frametitle{Basic services provision in Colombia}

\begin{itemize}
\item Inequality is high in Colombia
\begin{itemize}
\item Gini 57\%
\item  Infant Mortality Rates between  $2/1000$ to $90/1000$
\end{itemize}
\item Royalties resources should be prioritized for basic services like health, water and education
\begin{itemize}
\item Target of Infant Mortality Rate $16.5/1000$
\end{itemize}
\item States and municipalities are responsible for the provision of education, health, drinking water and sanitation
\item 63\% of municiaplities revenue SGP (transfers from Central Government) earmarked for school feeding, health and education
\item Local taxes 13\%
\item Royalties 5\%
\item By law at least 75\% of royalties invested in basic services

\end{itemize}
\hyperlink{aft_reform}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}
\frametitle{Royalties expending}

\begin{itemize}

\item Royalties resources should be prioritized for basic services like health, water and education
\begin{itemize}
\item Target of Infant Mortality Rate $16.5/1000$
\item Target of Net Enrollment in Basic Education: 100\%
\item Target of poor population with subsidized health insurance: 100\%
\item Target of population with access to drinking water 70\%
\item Target of population with access to sewage 70\%
\end{itemize}
\item States and municipalities are responsible for the provision of education, health, drinking water and sanitation
\item 63\% of municipalities revenue SGP (transfers from Central Government) earmarked for school feeding, health and education
\item Local taxes 13\%
\item Royalties 5\%

\end{itemize}

\end{frame}

\begin{frame}[label=sumstat, noframenumbering]
% Include sumstats for the munis in the illegal subsample. 
\frametitle{Summary statistics comparing to the selected sample}
{\tiny \input{sumstats_selec}}
{\tiny An observation is a municipality-year. The royalties per capita are approximately 12U\$ per person per year. We are missing fiscal information for 2013-2014. The poverty index is updated after every census (2005), and a special adjustment was made in 2011 for the reform. }
\end{frame}

\begin{frame}[label=thmod, noframenumbering]
\frametitle{Theoretical framework}
Consider now the profits if the firm is legal/illegal
\[ \pi_L= pq(1-\alpha)-C(q)-T\]
\[ \pi_I= pq-C(q)- \Omega(\alpha_m,q_I) K-w\]
Consider also the local government payouts in each case:
\[G_L=f(pq\alpha \alpha_m +R) - \gamma_L q \]
\[G_I=f(R) - \gamma_I q - \Omega(\alpha_m,q_I) V +w \]

\begin{itemize}

\item Other government revenue $R$
\item $p$ is international price of the mineral
\item $\alpha$ is the royalties rate of the mineral
\item $\alpha_m$ is the share of royalties for the mining municipality
\item $q_L$, $q_I$ are the quantities extracted legally and illegaly
\item $\gamma_L$, $\gamma_I$ are the local (pollution) costs of mining
\item $ \Omega(\alpha_m,q_I)$ the probability of illegal mining being detected
\item $V$ is the penalty associated with illegal mining

\item T, the annualized cost of the mining title process
\item The cost function $C(q)$
\item $K$ is the capital (machinery) that is destroyed if caught
\end{itemize}
\end{frame}

\begin{frame}[noframenumbering]
\frametitle{Theoretical model}
The ``surplus'' of illegal mining is $S=\pi_L-\pi_I+G_I-G_L$, and the firm gets $\theta S$ assuming Nash bargaining

\begin{itemize}
\item If national government doesn't change enforcement ($\frac{\partial  \Omega(\alpha_m,q_I) }{\partial \alpha_m}=0$) then as $\alpha_m \downarrow, R \uparrow \Rightarrow (G_I-G_L) \uparrow \Rightarrow S \uparrow$ so is illegal mining more likely?
\item But if the government increases enforcement after the reform the effect is undetermined
\end{itemize}
\hyperlink{thfram}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[label=rocstate, noframenumbering]
\frametitle{ROC curve for one state}
\includegraphics[scale=0.4]{ROC.png} 
\hyperlink{roc}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[label=confmat, noframenumbering]
\frametitle{Confusion Matrix}
\includegraphics[scale=0.8]{ConfMatrPorc.png} 
\hyperlink{roc}{\beamergotobutton{Return}}
\end{frame}



\begin{frame}
\frametitle{Differential effects}
\[ \sum^4_{i=1} \beta_i Royalties_{mt}  \times Before_t \times Mining_{m}  \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Min_bef_reg1} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Differential effects}
\[ \sum^4_{i=1} \beta_i Royalties_{mt} \times Before_t \times Mining_{m}   \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Min_bef_reg2} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}[plain]
\frametitle{}
\[  \sum^8_{i=1} \beta_i Royalties_{mt} \times Before_t \times Mining_{m} \times Poor_{m}  \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Min_bef_poorreg1} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}[plain]
\frametitle{}
\[  \sum^8_{i=1} \beta_i Royalties_{mt} \times Before_t \times Mining_{m} \times Poor_{m} \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Min_bef_poorreg2} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}[plain]
\frametitle{}
\[  \sum^8_{i=1} \beta_i Royalties_{mt} \times Before_t \times Mining_{m} \times Poor_{m}  \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Min_bef_poorreg3} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}[label=Bin_reg]
\frametitle{Heterogeneous effects by amount of royalties}
\[ \sum^5_{i=1} \beta_i (0.05*(i-1)<Royalties_{mt}<0.05*i) \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Bin_reg} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}
\hyperlink{tmi_reg}{\beamergotobutton{Return}}
\end{document}

