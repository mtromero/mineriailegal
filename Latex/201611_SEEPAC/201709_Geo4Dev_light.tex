%\documentclass[pdf]{beamer}
%For printing
\documentclass[handout]{beamer}
\setbeamertemplate{navigation symbols}{}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}  
\usepackage{rotating}
\usepackage[makeroom]{cancel}
\usepackage{nameref}
\usepackage{float}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{latexsym}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{ textcomp }
\mode<presentation>{}

\usetheme{boxes}
\usepackage{graphicx}

\newcommand{\backupbegin}{
   \newcounter{finalframe}
   \setcounter{finalframe}{\value{framenumber}}
}
\newcommand{\backupend}{
   \setcounter{framenumber}{\value{finalframe}}
}

\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Observing tax evasion]{Local incentives and national tax evasion: \ \ \ \ \ \ \  \large{The response of illegal mining  to a tax reform in Colombia}}

\author[Saavedra-Romero]{Santiago Saavedra (Universidad del Rosario) \\  Mauricio Romero (UCSD)  }

\normalsize

\date{September 6th 2017}


%\setbeameroption{show notes} 
\DeclareMathOperator*{\argmin}{arg\,min}
\begin{document}

\frame{\titlepage
\begin{center}
\includegraphics[height=1cm]{Images/Geo4Dev.png} 
\end{center}
\#Geo4Dev
} 

\begin{frame}
\frametitle{How does evasion respond to the revenue allocation?}

\large{
\begin{itemize}

\vfill \item Developing countries trend toward decentralized spending. \pause
\vfill \item However, tax revenue sources unchanged. \pause


\vfill \item How does tax evasion respond to the revenue allocation among municipalities? 

\vfill \item Does illegal activity have additional effects besides lost tax revenue?


\end{itemize}
}
\end{frame}


\begin{frame}
\frametitle{Evasion is observable in the case of illegal mining}
\begin{itemize}
 \item Main difficulty studying illegal activity is measuring its extent. \pause
%\item Illegal mining impacts can be tracked from space (Asner et al, 2013) \pause
\end{itemize}
\begin{center}
\begin{picture}(380,170)
\put(0,0){\includegraphics[viewport=40 420 550 750, clip,width=0.9\textwidth]{Images/GoogleMapsRemediosAntioquia.pdf} }
\put(138,100){\color{red} Mine}
\put(217,90){\color{blue} River}
\put(228,20){\color{yellow} Vegetation}
\end{picture}
\end{center}
\note{I went to Google maps, looked for a city renowned for illegal mining, and one minute later found a mine. The visible range in this picture is approximately 700m x 300m}
\end{frame}

\begin{frame}
\frametitle{Illegal mining is widespread in developing countries}
{\large
\begin{itemize}
\item Origin of minerals used could not be identified by 67\% of US companies.
\vfill \item Colombia: 78\% of gold area mined illegally \small{(UNODC,2016)}
\setlength\itemsep{2pt}
\vfill \item Two dimensions of illegal mining:
 \\[0.05cm]
\begin{itemize}
 \item Area mined without title.
\item Production that evades royalties taxes.\pause
\end{itemize}
\vfill \item Illegal mining is harmful for two main reasons:
\begin{itemize}
\item Tax evasion.
\item Environmental impacts.
\end{itemize}
\end{itemize}
}
\end{frame}

\begin{frame}
\frametitle{Reform reduced revenue share for mining municipalities}

\begin{center}
\includegraphics[height=4.5cm]{Images/GraphReform0.pdf} 
\end{center}
{\color{white} Standard bargaining model between bureaucrat and miner predicts increase in illegal mining.}
\end{frame}

\begin{frame}[label=context]
\frametitle{Reform reduced revenue share for mining municipalities}

\begin{center}
\includegraphics[height=4.5cm]{Images/GraphReform.pdf} 
\end{center}
\pause Standard bargaining model between bureaucrat and miner predicts increase in illegal mining.
\end{frame}



\begin{frame}
\frametitle{What we do in this paper}
{\large
\begin{itemize}
\vfill \item Study how evasion responds to the share of tax revenue earmarked for
the host municipality.   \pause
\vfill \item Build panel of illegal mining by municipality \pause
\begin{itemize}
{\normalsize
\vfill \item Use satellite imagery and machine learning algorithm. 
\vfill \item Predict mines location in Colombia and Peru, 2004-2014. \pause
}
\end{itemize}
\vfill \item Estimate the effect of the reform on illegal mining: 
\begin{itemize}
{\normalsize
\vfill \item Difference in differences strategy \pause
}
\end{itemize}
\vfill \item Estimate effect of illegal mining on newborn's health: 
\begin{itemize}
{\normalsize
\vfill \item IV strategy with the reform and river flow.
}
\end{itemize}

\end{itemize}
}
\end{frame}



\begin{frame}
\frametitle{Outline}

\begin{enumerate}
\vfill \item Constructing the illegal mining panel 
\vfill \item Main results
\vfill \item Differential health effects of illegal mining

\end{enumerate}

\end{frame}


\begin{frame}[label=graph_prepare]
\frametitle{Consolidate the information in a single dataset}
\includegraphics[height=0.9\textheight]{Images/Rastertodataframe.pdf} \hyperlink{prepare}{\beamergotobutton{*}}
\end{frame}

\begin{frame}[label=training_testing]
\frametitle{The precision of the mining prediction model is high}
\includegraphics[height=0.7\textheight]{Images/training_testing.pdf} 
Precision: Out of 100 pixels the model predict as mined, 79 are truly mined according to the testing data.
\hyperlink{algorithm}{\beamergotobutton{Data}} \hyperlink{ROC}{\beamergotobutton{ROC}}
\end{frame}



\begin{frame}[label=graph_predlegal]
\frametitle{After predicting mining, we assess legality}

\begin{center}
\begin{picture}(380,170)
\put(0,0){\includegraphics[width=0.95\textwidth]{Images/PredLegal.png} }
\put(58,100){\color{red} {\Large Illegal mines}}
\put(188,155){\color{black} {\Large Legal title}}
\put(177,75){\color{brown} {\Large Legally mined areas}}
\end{picture}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Illegal mining increased more in Colombia}
\begin{table}
\centering
\resizebox{\textwidth}{!}{

\begin{tabular}{l*{4}{c}}
\toprule          
                 Dependent variable: &   \multicolumn{3}{c}{\% mined area mined illegally           }    \\
                  &  \multicolumn{2}{c}{Only Colombia }  &  \multicolumn{1}{c}{With Peru}    \\
                  &All   &Excl NP  &    All  \\
                    &  \multicolumn{1}{c}{(1)}  &  \multicolumn{1}{c}{(2)}  &  \multicolumn{1}{c}{(3)}    \\
\midrule
\input{Stata/fminednwl_reg} 
\bottomrule
\end{tabular}
}
\begin{tablenotes}
{ \scriptsize
\item All regressions include municipality fixed effects. Standard errors, clustered by municipalities, are in parentheses.   $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }
\end{tablenotes}
\end{table}
\end{frame}


\begin{frame}[label=het_plot]
\frametitle{Heterogeneous effects}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\textwidth]{Stata/hetplot.pdf}
\end{center}
\end{figure}
\hyperlink{reg_het1}{\beamergotobutton{Reg1}}
\hyperlink{reg_het2}{\beamergotobutton{Reg2}}
\hyperlink{balancestats_inst}{\beamergotobutton{Summary Statistics}}

\end{frame}


\begin{frame}[label=reg_main]
\frametitle{Robustness checks}
\begin{itemize}

\item Mining prediction cutoff \hyperlink{reg_c12}{\beamergotobutton{*}}
\item Pixels mined probabilities \hyperlink{reg_probpixmined}{\beamergotobutton{*}}
\item Border municipalities \hyperlink{reg_peruRD}{\beamergotobutton{*}}
\item Unobservables \hyperlink{altonji_mined}{\beamergotobutton{*}}
\item Optimal controls \hyperlink{reg_lasso_continuous}{\beamergotobutton{*}}
\item Weights \hyperlink{reg_weights}{\beamergotobutton{*}}
\item Adjusted predictions \hyperlink{reg_adj}{\beamergotobutton{*}}
\item State trends \hyperlink{reg_continuous_deptr}{\beamergotobutton{*}}
\item Other measures \hyperlink{reg_otherm}{\beamergotobutton{*}}
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Health effects}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\textwidth]{Images/Zaragoza2012_arrow.jpg}
\end{center}
\end{figure}
\end{frame}



\begin{frame}[label=reg_healthIVfs]
\frametitle{First stage: Instrument with the reform}
\begin{table}
\centering
\resizebox{\textwidth}{!}{
\begin{tabular}{l*{3}{c}}
\toprule          
       \multicolumn{3}{c}{ Dependent variable: Downstream from illegal mining}             \\
      
                    &  \multicolumn{1}{c}{(1)}  &  \multicolumn{1}{c}{(2)}  \\
\midrule
\input{Stata/healthIVfsbaby_reg} 
\bottomrule

\end{tabular}}
\begin{tablenotes}
{ \tiny
\item All regressions include mother characteristics, municipality FE, week FE, year FE, and state trends. Standard errors, clustered by municipalities, are in parentheses.   $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }
\end{tablenotes}
\end{table}

\end{frame}

\begin{frame}[label=reg_healthIVnsm]
\frametitle{IV: downstream from illegal mining with the reform}
\begin{table}
\centering
\resizebox{\textwidth}{!}{
\begin{tabular}{l*{3}{c}}
\toprule          
       \multicolumn{3}{c}{ Dependent variable: High APGAR}             \\
      
                    &  \multicolumn{1}{c}{(1)}  &  \multicolumn{1}{c}{(2)}    \\
\midrule
\input{Stata/healthIVnsmbaby_reg} 
\bottomrule

\end{tabular}}
\begin{tablenotes}
{ \tiny
\item All regressions include mother characteristics, municipality FE, week FE, year FE, and state trends. Standard errors, clustered by municipalities, are in parentheses.   $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }
\end{tablenotes}
\end{table}

\end{frame}


\begin{frame}
\frametitle{Conclusions}

\large{
\begin{itemize}

\vfill \item Evasion responds to the tax revenue allocation in the case of illegal mining in Colombia:
\begin{itemize}
\item \large{7-20 \textcent \  lost through evasion per \$ redistributed.} 
\end{itemize}

\vfill \item Another unintended consequence is higher environmental and health impacts:
\begin{itemize}
\item \large{4-14 \textcent \  lost through evasion per \$ redistributed.} 
\end{itemize}

\end{itemize}
}
\end{frame}



\begin{frame}[plain, noframenumbering]
\frametitle{Thank you}
\begin{itemize}
\item Gracias
\item Asante Sana
\item Merci
\item Obrigado
\item Grazie
\end{itemize}
\end{frame}


\end{document}