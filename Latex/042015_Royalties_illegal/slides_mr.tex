\documentclass[11pt]{beamer}
\usetheme{Madrid}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{lmodern}
\usepackage{dcolumn}
\author{Santiago Saavedra and Mauricio Romero}
\title{Illegal Mining Detection using Remote Sensing.}
%\setbeamercovered{transparent} 
\setbeamertemplate{navigation symbols}{} 
%\logo{} 
%\institute{} 
\date{} 
%\subject{} 
\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Table of Contents}
\tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}{Introduction}
\begin{itemize}
\item Remote sensing of mining sites
\item Landsat 7 data 
\item Machine Learning using Colombian mining census of 2010-2011.
\end{itemize} 
\end{frame}

\begin{frame}{Introduction}
\includegraphics[scale=0.5]{Boyaca.png}  
\end{frame}

%Ahora hablar sobre la misión LANDSAT 7


\frame{\frametitle{Landsat 7}
\begin{itemize}
\item Launched in 1999
\item Multispectral images of the Earth
\begin{itemize}
\item Eight color bands (30x30 mts) and one panchromatic band (60x60 mts)
\item In each band, values are coded in 8-bit data ranging from 1 to 255 according to reflected energy
\end{itemize}
\item Each scene covers an area of about 49660 $km^2$
\begin{itemize}
\item It takes 4 scenes to cover the median Colombian department.
\item Images are taken every 16 days
\end{itemize}
\item Since 2003, due to Scan Line Corrector (SLC) failure, 22\% of pixels damaged.
\item \textbf{Cloud coverage}
\end{itemize}

}


\frame{\frametitle{All layers in a Scene from Boyacá}
\includegraphics[scale=0.43]{AllLayers.png} 
}

\frame{\frametitle{Mining census 2010-2011}
%En este bloque hablamos de resolución, tamaño de las esenas, número de departamentos, pixeles dañados
%codificación del color, cobertura de nubes.
\begin{itemize}
\item Mining census in charge of the Mines and Energy Ministry of Colombia
\item First phase covered 6 departments. Second phase covered 14.
\item The census covered 14,357 observations mines across 20 departments, \textbf{62\% of which are not formally registered}. 
\item Measurement problems with the georeferencing
\begin{itemize}
\item Over 400 coordinates exceed the possible format.
\item Over 2000 observations are registered to different municipalities than the ones they are actually in
\end{itemize}
\end{itemize}
}


\frame{\frametitle{Municipalities in Chocó covered by census}
\includegraphics[scale=0.45]{MuniChoco.png} 
}

\section{Data preprocessing}
\frame{\frametitle{New Municipalities}
\begin{itemize}
\item The municipalities listed in the census were matched to a 2007 shapefile of Colombian municipalities using the DANE code.
\item Some municipalities didn't have a match, because they were created after 2007.
\item To fix this problem, we used a data base of DANE which provided information on 
the pre-2007 municipalities from which the new ones were formed.
\end{itemize}
}

\frame{\frametitle{Mismatched municipalities}
\begin{itemize} 
\item Using the functions \textbf{gContains()} and \textbf{gDistance()} from package {$Rgeos$}, we were able to verify whether the coordinates of a given observation were contained in the registered municipality.
\item \textbf{2.374} mines had a mismatch.
\item It is possible, however, that some mines were registered as belonging to the municipality from which it's easier to access them by road. Which is not necessarily the one were they are geographically located.
\end{itemize}
}

\frame{\frametitle{Distribution of distance to municipality}
\begin{table}
\centering
\begin{tabular}{|c|c|}
\hline 
Distance & Mismatched mines \\ 
\hline
\hline 
$<10$ M & 62 \\ 
\hline 
$<100$ M & 413 \\ 
\hline 
$<1$ KM & 1275 \\ 
\hline 
$<2$ KM & 1521 \\ 
\hline 
$<10$ KM & 1965 \\ 
\hline 
$<20$ KM & 2114 \\ 
\hline 
\end{tabular}
\end{table} 

}

\frame{\frametitle{Mismatched municipalities}
\begin{exampleblock}{Solution}
To avoid excluding from the analysis such mines that are tagged to the most accessible municipality, we created a 2 kilometer buffer around all municipalities, and then dropped all mines tagged to those municipalities that fell outside such buffer.
\end{exampleblock}
}
\frame{\frametitle{Municipality of Marmato, Caldas with Buffer}
\includegraphics[scale=0.6]{Marmato.png} 
}

\frame{\frametitle{Mismatched municipalities}
\begin{alertblock}{Potential bias}
However, the mines that were excluded due to mismatching are systematically different from the others, as this mean-difference analysis reveals:
\end{alertblock}
}
\frame{\frametitle{Mean Difference Analysis}
\includegraphics[scale=0.5]{difmed1.png} 
}
\frame{\frametitle{Mean Difference Analysis}
\includegraphics[scale=0.5]{difmed2.png} 
}

\section{Cloud preprocessing}
\frame{\frametitle{Goal}
\begin{itemize}
\item In order to train a model in "recognizing" mines, a cloudless image of the relevant territory for the relevant dates is needed.
\item Very few scenes in this tropical part of the world have cloud coverage below 50\%
\end{itemize}
}

\frame{\frametitle{Clouds render some pixels useless}
\includegraphics[scale=0.4]{BuenavenCloud.png} 
}

\frame{\frametitle{Cloud Masking}
\begin{itemize}
\item Clouds can be identified, since they have values between 120 and 255 in the red band, and values between 102 and 128 in band 6.1, therefore a cloud mask can be easily built.
\item To fill up the "holes" in scenes after removing the clouds, we take an average of the cloudless images of the entire 4 month period.
\item As an added benefit, this also deals with those 22\% damaged pixels and cancels out random deviations in colors due to haze, humidity and time of day.
\end{itemize}
}

\frame{\frametitle{Some Scenes from Caldas after removing Clouds}

\includegraphics[scale=0.4]{cloudless.png}  
}

\frame{\frametitle{Cloudless composition}
\begin{exampleblock}{Implementation}
The command \textbf{clouds()} from package {$Landsat$} allows the user to adjust a tolerance level in the identification of clouds, and produces the desired mask. Every cloudless scene is then saved to disk.
\end{exampleblock}

\begin{alertblock}{Complications}
Damaged pixels are inconveniently set to 0. This is problematic when taking an average through different scenes, since 0 values are averaged with real values and corrupt the result. It's necessary to set 0 values to \textbf{NA}. 
To reduce computation time, the cloud masking and cloud composition process is performed only on municipalities covered by the mining census. To do this we mask the scenes to the shape of such municipalities.
\end{alertblock}
}

\frame{\frametitle{Cloudless mosaic}
\begin{itemize}
\item In order to merge or mosaic Rasters, these need to be "compatible". That is, have the same resolution and origin.
\item Two rasters are compatible if the "extension" of the grid of pixels of one overlaps perfectly with the grid of the other.
\item Scenes for the same department do not have "compatible" extents, in a series of satellite captures of the same location, slight shifts occur. 
\item In order to create the mosaic, the different scenes need to be made compatible. 
\end{itemize}
}

\frame{\frametitle{Example of unaligned Rasters with equal resolution}
\includegraphics[scale=0.7]{unaligned.png} 
}


\frame{\frametitle{Cloudless mosaic}
\begin{exampleblock}{Implementation}
The process of making different Rasters compatible is called \textbf{resampling}. It takes a target raster with the desired resolution and origin and interpolates the values of centres using a bilinear interpolation of the values in the corners (which can be retrieved from the original raster). R implements this with the call \textbf{resample(input.raster, target.raster, method="bilinear")}.
\end{exampleblock}

\begin{alertblock}{Limitations}
This process is very computationally expensive, and increasingly expensive depending on the size of the Raster. To make things feasible, the resolution of every raster is previously reduced by a factor of 3. This is accomplished with the function \textbf{aggregate(input.raster, fact=3, fun=mean, expand=TRUE, na.rm=TRUE)} of the package {$Raster$}. 
This process may reduce prediction power.
\end{alertblock}
}

\frame{\frametitle{Cloudless mosaic}
\begin{exampleblock}{Implementation}
The compilation of compatible rasters is then done using the call \textbf{do.call(mosaic, resampled.rasters)} which results in a large cloudless raster for each department.
\end{exampleblock}
\begin{alertblock}{Limitations}
Putting together the mosaic requires writing each scene several times in RAM. This can overload even highly endowed computers. To avoid this, it's necessary to "split" the process a few times.
\end{alertblock}
}

\frame{\frametitle{Mosaic with no clouds}
After the mosaic, we obtain a virtually cloudless composite of the scene:
\includegraphics[scale=0.5]{CloudComp.png} 
}

\section{Data extraction}
\frame{\frametitle{Supervised classification}
\begin{itemize}
\item We want a model that can classify every pixel as having a mine or not.
\item The coordinates of a mine are a single point in space. Mines occupy a certain area.
\item Using the function \textbf{gBuffer()} of the package {$Rgeos$}, we create a 200 meter buffer about the mines and signal those areas as having a mine.
\item Then we create an additional layer in the large cloudless raster signaling the mines.
\end{itemize}
}

\frame{\frametitle{Supervised classification}
\begin{exampleblock}{Implementation}
To create the "Mine" layer, we rasterize the shapefile of the buffer around the mines. This is done with the function \textbf{rasterize()} of the package {$Raster$}. This function takes the resolution and origin of a template raster; in this case we can take any of the layers of the cloudless raster.
\end{exampleblock}
}

\frame{\frametitle{Creating a "mine" layer}
\includegraphics[scale=0.4]{CalMinas.png} 
}

\frame{\frametitle{Extraction}
\begin{itemize}
\item The input for the classification model is a data frame that contains a row for each pixel
and in the columns it has the value of each band and the dummy variable "Mine".
\item To get further prediction power in the future, it is convenient to have the municipality code of each pixel. This way we can attach more variables at the municipality level.
\item We would also like to select 80\% of municipalities as \textbf{training sample} and the rest as a \textbf{test sample} to perform cross validation.
\end{itemize}
}

\frame{\frametitle{Extraction}
\begin{exampleblock}{extract()}
For each department we extract, municipality by municipality using the command \textbf{extract()} of the package {$Raster$}, and setting as a parameter the municipality to be extracted. To the resulting data.frame we add a column with the municipality DANE code and in 80\% of municipalities (chosen randomly) we add a column with a boolean set to \textbf{TRUE}. In the others, we set it to \textbf{FALSE}.
\end{exampleblock}
}

\frame{\frametitle{Extraction}
The resulting data.frame looks like this:
\includegraphics[scale=0.35]{TheDataFrame.png} 
}

\section{Principal Components Analysis}
\frame{\frametitle{Principal Components}
\begin{itemize}
\item The combined data.frame of all municipalities is quite large, running the model on fewer variables\\
can result in considerably shorter computation times.
\item Additionally, different bands of a satellite images are often highly correlated making them redundant.
\item Performing Principal Component Analysis we effectively reduce the dimension of the problem and \\
eliminate potential problems derived from high correlation between the independent variables.
\end{itemize}
}

\frame{\frametitle{Principal Components}
\begin{exampleblock}{Implementation}
The principal components of the bands are obtained using the command \textbf{prcomp()} of the basic R package. Tolerance is set as to only include components whose standard deviations exceed 10\% of the main component's standard deviation.
\end{exampleblock}
}

\frame{\frametitle{Principal Components for Caldas Example}
In a pilot example for Caldas, we obtain 3 components:
\includegraphics[scale=0.7]{PCAMatrix.png} 
}
\section{Subbagging and Logit Model}
\frame{\frametitle{Unbalanced classification}
\begin{itemize}
\item Our training sample has very unbalanced classes. Over 99.9\% of observations are not mines.
\item Simply running a Logit model could yield a prediction of \textbf{NO} everywhere and have an error\\
rate of less than 0.1\% 
\item The solution is to take several "balanced" subsamples, and calibrate the model there. Then take an average of the resulting coefficients. This is known as "Subbagging".
\end{itemize}
}

\frame{\frametitle{Subbagging}
 \begin{itemize}
 \item To do the Subbagging, we create 1000 index vectors consisting of the observations with mine and an equal number of randomly selected observations with no mine.
 \item For each of them we calibrate a Logit model and store the resulting $\beta$ in a list.
 \item We then take the average of the estimated coefficients to be our true calibration.
 \end{itemize}
} 
 \frame{\frametitle{Cross Validation}
 \begin{exampleblock}{Implementation}
 In order to test our model, we run it on the test municipalities, producing a column of \textbf{predicted probabilities} which we can use to compute a \textbf{confussion matrix}, and measure the \textbf{power of prediction} with a \textbf{ROC curve}.
 \end{exampleblock}
 }

\frame{\frametitle{Confussion Matrix for Caldas}
\includegraphics[scale=0.5]{confussion.png}  
}
\frame{\frametitle{ROC curve for Caldas}
\includegraphics[scale=0.5]{ROC.png} 
}
\section{Raster of Predictions}
\frame{ \frametitle{Predicting}
\begin{itemize}
\item Using the estimated coefficient of the previous excercise and the transformation matrix of the PCA\\
we can build a Raster that "maps" the prediction of a mine over an existing Raster.
\item In the following example we map the predictions of our model over a scene that covers part of the department of Vichada.
\end{itemize}
}

\frame{\frametitle{Prediction on Vichada}
IMAGE MISSING
}
\end{document}