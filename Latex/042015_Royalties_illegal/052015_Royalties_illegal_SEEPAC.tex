\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}  
\usepackage{rotating}

\usepackage{nameref}
\usepackage{float}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{latexsym}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}

\usepackage{apacite}
\mode<presentation>{}
\usetheme{Warsaw}
\usepackage{graphicx}



\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Illegal mining]{Local incentives and illegal mining: The effect of a royalties' law reform}

\author{Mauricio Romero (UCSD) \\ Santiago Saavedra }

\normalsize

\date{May 18th 2015}




\begin{document}

\frame{\titlepage} 


\begin{frame}

\frametitle{Motivation}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.47\textwidth]{Ilegal_amazon.png} \quad
\includegraphics[width=0.47\textwidth]{Flood_mine.png}
%\caption{\tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
%\label{fig:evol_mineria}
\end{center}
\end{figure}

\end{frame}


\begin{frame}

\frametitle{Motivation}
\begin{itemize}
\item The literature on natural resources and political economy broadly finds that more decentralization exacerbates environmental externalities (Burgess et al, 2012 and Lipscomb-Mobarak, 2014 )
\item 63\% of the mines in Colombia are illegal
\begin{itemize}
\item Less likely to pay royalties
\item Not adhere to environmental standards
\end{itemize}
\item ``Recentralization reform'' in 2012
\begin{itemize}
\item Reduced direct transfers to mining municipalities from 55\% to around 10\%
\item Distribute funds nationally according to poverty 
\item Increased oversight by the National Government
\end{itemize}



\end{itemize}
\end{frame}


\begin{frame}

\frametitle{Research question}

What are the effects of reducing direct royalties to mining municipalities and redistributing to other municipalities?
\begin{itemize}
\item What are the costs in terms of illegal mining of changing local incentives?
\item What are the benefits on health and education of larger municipality budgets?

\end{itemize}
\end{frame}



\begin{frame}

\frametitle{What this paper does}
\begin{itemize}
\item Study the effect of a law reform in Colombia that reduced the share of royalties for the mining municipality from 55\% to 10\%
\item Theoretical framework: The reform reduced the incentive for local municipalities 
\begin{itemize}
\item To encourage new mines to become legal
\item To have existing mines accurately report production
\end{itemize}
\item We look at the impact of the reform on:
\begin{itemize}
\item Illegal mining
\item Health and education
\end{itemize}

\end{itemize}
\end{frame}

\begin{frame}

\frametitle{Table of contents}

\begin{enumerate}
\item Details of the reform
\item Data
\item Identification strategy
\item Preliminary results

\end{enumerate}
\end{frame}


\begin{frame}

\frametitle{Related literature}

\begin{itemize}

\item Decentralization and environmental impacts
\begin{itemize}
\item Burgess etal (2012)
\item Lipscomb-Mobarak(2014)
\item Sigman(2005)
\end{itemize}
\item Mining in Colombia
\begin{itemize}
\item Idrobo et al (2012)
\item Martinez (2015)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label=bef_reform]

\frametitle{Before the reform}



\begin{figure}[H]
\begin{center}
\includegraphics[width=1\textwidth]{bef_reform.pdf} 
%\caption{\tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
%\label{fig:evol_mineria}
\end{center}
\end{figure}
 \hyperlink{roybymin}{\beamergotobutton{Pctg by mineral}}
\end{frame}

\begin{frame}

\frametitle{After the reform}
\begin{figure}[H]
\begin{center}
\includegraphics[width=1.1\textwidth]{aft_reform.pdf} 
%\caption{\tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
%\label{fig:evol_mineria}
\end{center}
Poverty defined with index of unmet needs ($NBI$)  \hyperlink{nbi}{\beamergotobutton{NBI}}
\end{figure}

\end{frame}

\begin{frame}

\frametitle{Further details of the reform}

\begin{itemize}
\item Transition period of direct royalties share (25 \%, 17.5 \%, 12.5\%
\item Guarantee to mining municipalities that 2012-2014 royalties don't fall below 50\% of 2007-2010 average, (40 \% 2015-2020)
\item The regional development fund resources are distributed among states according to:
\[ w_i=\left( \frac{Population_i}{Population_{COL}} \right)^{0.6} \left( \frac{NBI_{i}}{NBI_{COL}} \right)^{0.4} \]
\item The regional compensation fund resources are distributed among poor states ($NBI>30$) according to:
\[ w_i=\left( \frac{Population_i}{Population_{COL}} \right)^{0.4} \left( \frac{NBI_{i}}{NBI_{COL}} \right)^{0.5} \left( \frac{Unemployment_{i}}{Unemployment_{COL}} \right)^{0.1} \]
\end{itemize}
\end{frame}

\begin{frame}[label=thfram]

\frametitle{Theoretical framework}
\begin{itemize}

\item The reform reduced the percentage of mining revenue that goes to the mining municipality. This reduced the incentive to
\begin{itemize}
\item Encourage new mines to become legal
\item Monitor existing mines accurately reporting production
\end{itemize}

 \hyperlink{thmod}{\beamergotobutton{Model}}
\end{itemize}
\end{frame}



\begin{frame}

\frametitle{Variables I can analyze}
\begin{itemize}
\item Costs
\begin{itemize}
\item Illegal mining  
\item Change in production reporting 
\end{itemize}
\item Benefits
\begin{itemize}
\item Infant mortality rate 
\item  Schooling rates 
\item  Poverty using lights
\end{itemize}
\item Municipality elections 
\end{itemize}
\end{frame}

\begin{frame}

\frametitle{Variables I can analyze}
\begin{itemize}
\item Costs
\begin{itemize}
\item Illegal mining  {\color{red} Under construction. Process today}
\item {\color{gray} Change in production reporting } Data requested
\end{itemize}
\item Benefits
\begin{itemize}
\item Infant mortality rate {\color{red} Today}
\item  {\color{gray} Schooling rates } 2014 not yet available 
\item {\color{gray} Poverty using lights} 2013-4 available Jan 2016
\end{itemize}
\item {\color{gray} Municipality elections} October 2015 (but no reelection)
\end{itemize}
\end{frame}



\begin{frame}

\frametitle{Data sources}

\begin{itemize}
\item Satellite imagery from Landsat 7
\item Mining census 2010-2011 for training
\item Royalties by municipality 2004-2014 (National Planning Department)
\item Mortality rate (DANE)
\item Socio-economic characteristics of municipalities CEDE


\end{itemize}
\end{frame}

\begin{frame}{Identifying illegal mines}
\includegraphics[scale=0.45]{Boyaca.png}  
\end{frame}

\frame{\frametitle{}
\includegraphics[scale=0.4]{AllLayers.png} 
}

\begin{frame}

\frametitle{Identifying Illegal Mines}
\begin{enumerate}

\item Remove clouds (Goslee 2011)
\item Cloudless Composite per Department
\begin{itemize}
\item Eight color bands (30x30mts) with values from 1 to 255 according to reflected energy
\item A mosaic is created using scenes of the trimester after reducing resolution(90x90mts)
\end{itemize}
\item Subbagging: Logit on several balanced subsamples (Paleologo et. al. 2010)
\item Cross-validation (20\% for testing)
\item Prediction
\item ``Substract'' legal mines
\end{enumerate}
\end{frame}

\frame{\frametitle{ROC curve for one state}
\includegraphics[scale=0.4]{ROC.png} 
}

\begin{frame}[label=sumstat]
\frametitle{Summary statistics}
{\tiny \input{sumstats}}
{\tiny The royalties per capita are approximately 12U\$ per person per year. We are missing fiscal information for 2013-2014. The poverty index updated every census (2005), and special adjustment 2011 for the reform. }
\end{frame}

\begin{frame}

\frametitle{Effect of the reform on municipality budget}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.87\textwidth]{Muni_minero_affectedTotal.pdf} 

%\caption{\tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
%\label{fig:evol_mineria}
\end{center}
\end{figure}

\end{frame}

\begin{frame}

\frametitle{Infant mortality rate}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\textwidth]{tmi.pdf} 
%\caption{\tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
%\label{fig:evol_mineria}
\end{center}
\end{figure}

\end{frame}

\begin{frame}

\frametitle{Identification strategy}


\[Y_{mt}= \sum_{i \in \Omega} \beta_i Royalties_{mt}  \times Before_t \times Mining_{m} \times Poor_{m} +X_{mt} \alpha+\gamma_m+ \gamma_{t}+\varepsilon_{mt}\]

\begin{itemize}
\item $Y_{mt}$ is the outcome of interest: share of illegal mining in new mining area, infant mortality in municipality $m$ at time $t$
\item $Royalties_{mt}$ is the amount of royalties allocated 
\item $X_{mt}$ is a set of time varying controls which will include tax revenue
\item $\gamma_m$ municipality fixed effects
\item $\gamma_t$ time fixed effects
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Royalties affecting all equally}
\[  \beta Royalties_{mt}\]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Basic_reg} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
\multicolumn{2}{c}{\scriptsize Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
The mean of the independent variable is 0.03 or U\$ 12 per capita. That is at the mean the TMI is reduced by 1/1000
\end{frame}

\begin{frame}
\frametitle{Differential effects}
\[ \sum^2_{i=1} \beta_i Royalties_{mt}  \times Before_t \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Bef_reg} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Differential effects}
\[ \sum^4_{i=1} \beta_i Royalties_{mt}  \times Before_t \times Mining_{m}  \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Min_bef_reg1} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Differential effects}
\[ \sum^4_{i=1} \beta_i Royalties_{mt} \times Before_t \times Mining_{m}   \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Min_bef_reg2} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}[plain]
\frametitle{}
\[  \sum^8_{i=1} \beta_i Royalties_{mt} \times Before_t \times Mining_{m} \times Poor_{m}  \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Min_bef_poorreg1} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}[plain]
\frametitle{}
\[  \sum^8_{i=1} \beta_i Royalties_{mt} \times Before_t \times Mining_{m} \times Poor_{m} \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Min_bef_poorreg2} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}[plain]
\frametitle{}
\[  \sum^8_{i=1} \beta_i Royalties_{mt} \times Before_t \times Mining_{m} \times Poor_{m}  \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Min_bef_poorreg3} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Heterogeneous effects by amount of royalties}
\[ \sum^5_{i=1} \beta_i (0.05*(i-1)<Royalties_{mt}<0.05*i) \]
\begin{center}
\resizebox{!}{0.35\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule          
                  \multicolumn{2}{c}{Dependent variable: Infant mortality rate}\\
                    &  \multicolumn{1}{c}{(1)}   \\
\midrule

\input{Bin_reg} 
Time FE & Yes \\
Municipality FE & Yes \\
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Royalties in billions ($10^9$) COP 2012 Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{2}{c}{\scriptsize Royalties in millions COP 2012 per capita. Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{2}{c}{\scriptsize $^{*}$ \(p<0.10\),  $^{**}$ \(p<0.05\),  $^{***}$ \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}




\begin{frame}

\frametitle{Future work}

\begin{itemize}
\item Finish the illegal mining detection exercise
\item Get all the data
\item $Royalties_{mt}$ could include money from state level funds. Its value is affected by institutional capacity, as is illegal mining. 
\begin{itemize}
\item Instrument the amount of royalties with the law hypothetical royalties
\[ {Royalties}_{mt}=\alpha {LawRoyalties}_{mt} \]
\end{itemize}
\item RD with funds cutoff
\item Mineral heterogeneity 
\begin{itemize}


\item Using satelite we can mostly detect illegal mining of precious metal and construction materials
\item There is also illegal mining of coal, but mostly underground
\item Oil and gas are hard to extract illegally

\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[plain]
\frametitle{Thank you}
\begin{itemize}
\item Gracias
\item Asante Sana
\item Merci
\item Obrigado
\item Grazie
\end{itemize}

\end{frame}


\begin{frame}

\frametitle{Discontinuity on FCR fund}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.75\textwidth]{FDR_NBI_PD.pdf} 
%\caption{\tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
%\label{fig:evol_mineria}
\end{center}
\end{figure}

\end{frame}




\begin{frame}[label=roybymin]
\frametitle{Royalties by mineral}

\begin{itemize}

\item Hydrocarbons 8-25\% $^*$
\item Coal 5-10\% $^*$
\item Nickel 12\%
\item Alluvial gold 6\%
\item Copper, iron 5\%
\item Gold, silver 4\%
\item Gems 3\%
\item Construction materials 1\%
\end{itemize}
* Depends on volume extracted
\hyperlink{bef_reform}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[label=nbi]
\frametitle{How the NBI poverty index is constructed}
Percentage of population with at least one unmet need:
\begin{itemize}

\item Inadequate housing (floor, walls)
\item Lack of toilet, sewage or water
\item More than 3 people per room
\item $6-12$ years old kid not attending school
\item Dependency ratio $>3$ or household head 2 years of education
\end{itemize}

\hyperlink{aft_reform}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[label=sgp]
\frametitle{Basic services provision in Colombia}

\begin{itemize}

\item States and municipalities responsible for the provision of education, health, drinking water and sanitation
\item 63 \% of municiaplities revenue SGP (transfers from Central Government) earmarked for school feeding, health and education
\item Local taxes 13 \%
\item Royalties 5\%
\item By law at least 75\% of royalties invested on the basic services
\item Education infrastructure, school equipment or school transportation
\begin{itemize}

\item Vaccination campaigns
\item Health posts for common infant diseases
\end{itemize}
\end{itemize}
\hyperlink{aft_reform}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[label=thmod]
\frametitle{Theoretical framework}
Consider now the profits if the firm is legal/illegal
\[ \pi_L= pq(1-\alpha)-C(q)-T\]
\[ \pi_I= pq-C(q)- \Omega(\alpha_m,q_I) K\]
Consider also the local government payouts in each case:
\[G_L=f(pq\alpha \alpha_m +R) - \gamma_L q \]
\[G_I=f(R) - \gamma_I q - \Omega(\alpha_m,q_I) V \]

\begin{itemize}
\item Using the same notation as in the previous model, plus:
\item T, the cost of the mining title process
\item The cost function $C(q)$
\item $K$ is the capital (machinery) that is destroyed if caught
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Theoretical model}
The ``surplus'' of illegal mining is $S=\pi_L-\pi_I+G_I-G_L$, and the firm gets $\theta S$ assuming Nash bargaining

\begin{itemize}
\item If national government doesn't change enforcement ($\frac{\partial  \Omega(\alpha_m,q_I) }{\partial \alpha_m}=0$) then as $\alpha_m \downarrow, R \uparrow \Rightarrow (G_I-G_L) \uparrow \Rightarrow S \uparrow$ so illegal mining more likely?
\item But if the government increases enforcement after the reform the effect is undetermined
\end{itemize}
\hyperlink{thfram}{\beamergotobutton{Return}}
\end{frame}

\end{document}

