%\input{tcilatex}
\documentclass{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amssymb}
\usepackage{dcolumn}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{mathrsfs,amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{nameref}
\usepackage[all]{hypcap}
\usepackage [autostyle, english = american]{csquotes}
\usepackage{apacite}


\usepackage[cm]{fullpage}
\MakeOuterQuote{"}
\setcounter{MaxMatrixCols}{10}
%TCIDATA{OutputFilter=LATEX.DLL}
%TCIDATA{Version=5.50.0.2953}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{LastRevised=Wednesday, October 10, 2012 16:40:37}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{Language=American English}
%\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\newtheorem{summary}{Summary}
%\usepackage{geometry}
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\date{}     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
 
\date{\today}     % Deleting this command produces today's date.

\title{Local incentives and illegal mining: the effect of a royalties' law reform}
%\thanks{}
 \date{\today}
\author{Mauricio Romero and Santiago Saavedra\thanks{Romero: University of California - San Diego, \textit{e-mail contact:} \url{mtromero@ucsd.edu}. Saavedra: Stanford University, \textit{e-mail contact:} \url{santisap@stanford.edu}.}}
            % End of preamble and beginning of text.
                   % Produces the title.

\begin{document} 



\maketitle


\section{Overview}

Illegal mining in developing countries has become a widespread problem that is now large enough to be tracked from space (e.g., see \citeA{Swenson2011,Asner2013}). Illegal mining, which is largely composed by artisanal and small-scale mining, is often thought as a consequence of poverty, driven by a poorly educated population with few other employment opportunities (for example, see the World Bank's webpage on small-scale mining \url{http://go.worldbank.org/6OCES521R0}). However, many factors such as global demand, institutional capacity, regulatory framework, and environmental regulation also influence the prevalence of illegal mining. In this paper we study the effect of a large ``recentralization reform'' in Colombia that reduced the share of royalties to mining municipalities from 55\% to 10\%. Theoretically, the reform could lead to an increase in illegal mining as local politicians have less incentives to monitor mines and ensure they comply with the law. In order to identify illegal mining we use Landsat satellite images and machine learning algorithms.

\section{Background}


According to the mining census of 2010-2011, 63\% of the mines operate without a mining permit in Colombia. Legal mines (with permits) have to pay royalties to the national government. The rate varies for each mineral and amount extracted. Before 2012, 55\% of the royalties were transferred to the municipalities where the minerals were extracted, 30\% to the mining state, 11\% to a national fund and the remaining 4\% to port municipalities. Legislative Act 05 of 2011 changed the allocation formula dramatically According to the new law, 10\% of royalties must be allocated to a Science, Technology and Innovation fund; 10\% to unbudgeted pensions; and 30\% to a savings and stabilization fund. Of the remaining funds 20\% are directly transferred to the municipalities where the minerals are extracted and 80\% are distributed to other states and municipalities on the basis of poverty, population and unemployment. Additionally, the reform increase the oversight of the national government over the expenditure of royalties. Figure \ref{fig:reforma} shows how royalties were distributed before and after the reform. 

%The regional development fund resources are distributed among states proportionally according to the weights in equation \ref{eq:rdf} and the regional compensation fund resources are distributed among poor states proportionally according to the weights in equation \ref{eq:rcf}, where $Population_i$ is the population of state $i$, $NBI_i$ is the proportion of the population with unmet needs, measured using the NBI (\textit{Necesidades Basicas Insatisfechas}), and $Unemployment_i$ is the unemployment rate of municipality $i$. 
%
%\begin{equation}
%\label{eq:rdf}
%w_i=\left( \frac{Population_i}{Population_{COL}} \right)^{0.6} \left( \frac{NBI_{i}}{NBI_{COL}} \right)^{0.4} 
%\end{equation}
%
%\begin{equation}
%\label{eq:rcf}
%w_i=1_{NBI_{i}>30} \left( \frac{Population_i}{Population_{COL}} \right)^{0.4} \left( \frac{NBI_{i}}{NBI_{COL}} \right)^{0.5} \left( \frac{Unemployment_{i}}{Unemployment_{COL}} \right)^{0.1} 
%\end{equation}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.37\textwidth]{bef_reform.pdf}  \quad
\includegraphics[width=0.53\textwidth]{aft_reform.pdf}  \quad
\caption{Royalties distribution before (left panel) and after (right panel) the reform of 2012.}
\label{fig:reforma}
\end{center}
\end{figure}



\section{Research plan}

To calculate the impact of the reform on illegal mining, we compare municipalities affected differentially by the reform across time, controlling for time and municipality fixed effects. This identification strategy relies on the assumption that, after controlling for time fixed effects, municipality fixed effects and time varying controls (like price of minerals), the extent of illegal mining is only affected differentially by the impact of the reform in the municipality budget.

A necessary input to perform this analysis is a time series of illegal mining. Previous papers have used static measures of illegal mining in their analysis (e.g., \cite{Idrobo2014,Santos2014}) based on the Census and other municipality characteristics. Since we require a time-varying measure of illegal mining we rely on remote sensing. Using a small grant provide by the Latin American and Caribbean Environmental Economics Program (LACEEP) we started analyzing Satellite imagery from Landsat 7. The general idea is to use the information of the mining census of 2010-2011, which has the geolocation of over 14,000 mines (of which over 9,000 are illegal), to train a machine learning model (such as a random forest) that detects illegal mining. Afterwards we intend to use this model with Landsat 7 imagery from other years to get a proxy of the mining activity in each municipality across time. 

Currently, we have downloaded most of the Landsat 7 images of Colombia from 2006-2014, and are in the process of creating a cloudless composite of Colombia for each year. The next step, is the training of the machine learning algorithm and the estimation of the illegal mining time series at the municipality level. The LACEEP funds have provided an invaluable start capital for this project, but we originally underestimated how much time and expertise (in machine learning) this project would take. We now require additional funds to continue the development of the machine learning algorithm with the help of a research assistant specialized in machine learning\footnote{The bulk of the training of the machine learning algorithm will be done by \textit{Quantil | Matematicas Aplicadas} a consulting firm specialized in applied mathematics who have agreed to give us a dedicated research assitant to this project as long as we cover his wage, without charging any overhead.}. Additionally, we need funds to visit a random sample of sites where the model predicts there is mining, in order to verify its accuracy.


Our analysis will produce a time series of the evolution of illegal mining, which we intend to share in our personal webpages and make available as a public good to the research community. Furthermore, in the future we would like to explore if our machine learning algorithm can be applied to other countries. 

\section*{Budget}


\begin{itemize}
\item Landsat 7 imagery processing $\$5,000$
\item Total: $\$5,000$
\end{itemize}







\bibliographystyle{apacite}
 \bibliography{bibille}
 
 


\appendix







\end{document}

