\documentclass[12pt]{article}

\usepackage[margin=1.1in]{geometry}
\usepackage{nameref}
\usepackage{float}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage[all]{hypcap}
\usepackage{apacite}
\usepackage{multicol}
\usepackage{setspace}
\usepackage{color}
%\usepackage[nolists]{endfloat}
\usepackage{hyperref}
% JEL Classifications
\long\gdef\@JEL{}
\long\def\JEL#1{\long\gdef\@JEL{#1}}
% Keywords
\long\gdef\@Keywords{}
\long\def\Keywords#1{\long\gdef\@Keywords{#1}}
\newenvironment{tablenotes}[1][Note]{\begin{minipage}[t]{\linewidth} \footnotesize }{\end{minipage}}
\newenvironment{figurenotes}[1][Note]{\begin{minipage}[t]{\linewidth} \footnotesize}{\end{minipage}}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\DeclareMathOperator*{\argmin}{arg\,min}
\begin{document} 
\date{\today}     % Deleting this command produces today's date.
\title{Local incentives and national tax evasion: The response of illegal mining to a tax reform in Colombia  \thanks{ We are grateful to  Prashant Bharadwaj, Arun Chandrasekhar, Pascaline Dupas,  Larry Goulder and Melanie Morten for invaluable guidance. This paper was enriched by comments from Juan Castillo, Thomas Ginn,  Marcel Fafchamps, Mounu Prem, Mark Rosenzweig,  and seminar participants at Stanford University, UC San Diego and Universidad del Rosario. Francisco Barrera, Muhammad Islam, Fabian Latorre and Michael Wang provided excellent assistance constructing the illegal mining data. This study was carried out with support from the Latin American and Caribbean Environmental Economics Program (LACEEP) and Policy Design and Evaluation Lab (PDEL). Saavedra acknowledges support of the Stanford Institute for Economic Policy Research through the Kohlhagen Fellowship Fund. } \\
\small{PRELIMINARY DO NOT CITE OR CIRCULATE} }

%\thanks{}

\author{Santiago Saavedra\thanks{Stanford University Department of Economics, \textit{e-mail:} \url{santisap@stanford.edu}.} \\ (Job Market Paper) \and  Mauricio Romero\thanks{University of California - San Diego Department of Economics, \textit{e-mail :} \url{mtromero@ucsd.edu}.  }    }
 \date{\today \\  \small{Latest version available in: \url{http://web.stanford.edu/~santisap/}} }

            % End of preamble and beginning of text.
                   % Produces the title.
\Keywords{Illegal mining, Satellite Images, Colombia}
\JEL{} 
\maketitle
\begin{abstract}
Taxation is the main source of government revenue. Yet, national governments can only tax what they can directly observe or what is reported by local authorities of the economic activity they witness. In this paper we investigate how illegal mining, a very common phenomenon in Colombia, changed with a reform that reduced the share of taxes transferred back to mining municipalities.  To overcome the challenge of measuring illegal activity, we construct a novel dataset using machine learning predictions on satellite imagery features. Theoretically we expect illegal mining to increase, because the amount required to bribe the local authority is smaller after the reform. We find that, after the reform, the fraction of a municipality area mined illegally increased by \input{mainresrelmean}\%. As predicted, the effect is concentrated among municipalities that lost revenue with the reform.  These results illustrate unintended effects of tax revenue redistribution, when taxes are spent where they are not paid.
\end{abstract}

\newpage
\onehalfspacing

\section{Data}\label{sec:data}
We rely on three main sources of data for our analysis. First, the panel of illegal mining by municipality we constructed, whose details will be explained in the next sub-section.We also use data from the government's mineral information system SIMCO\footnote{http://www.simco.gov.co/} on reported production and prices. Finally, we use the municipality panel from the Center for Studies of Economic Development (CEDE) from Universidad de los Andes \cite{panelcede} for information on royalties, municipalities' budgets, homicides of armed groups, and other characteristics of municipalities. 
\\[0.4cm]
Summary statistics at the municipality level are presented in Table \ref{tab:sumstats_muni}. We observe that of the \input{Nmunis} municipalities with minerals in the subsoil, most of them benefited from the reform. The winners and losers are balanced on the production of precious metals, but most of the losers are producers of fossil fuels.  Most of the mines are open pit and therefore can, in principle, be observed from space. Finally, note that \input{pctgag} \% of the municipalities had some presence of armed groups before the reform. 

\begin{table}[H]
\caption{Summary statistics for municipalities used in the analysis}
\label{tab:sumstats_muni}
\resizebox{\textwidth}{!}{
 \input{summary_balance}
 }
\begin{tablenotes}
Summary statistics for municipalities used in the analysis. An observation is a municipality. There are \input{Nmunis} municipalities, of which \input{Nlosers} are negatively affected. All data comes from CEDE's municipalities panel, except the row that indicates is from the 2010 Mining Census. Calculations: Authors.
\end{tablenotes}
\end{table}
 \subsection{Constructing the illegal mining panel}
 There are three main steps to construct the panel of illegal mining by municipality. First, we need to prepare the satellite data to be usable in the prediction model. Second, we need to construct a model to predict whether a certain area has a mine. And finally, we predict mining presence in all the pixels, identify whether the pixels is inside a title and collapse the results at the municipality level.
 \begin{figure}[H]
\begin{center}
\caption{Image of a mine in the municipality of Remedios}
\label{fig:mine_highres}
\begin{picture}(380,170)
\put(0,0){\includegraphics[viewport=40 420 580 750, clip,width=0.9\textwidth]{GoogleMapsRemediosAntioquia.pdf} }
\put(168,110){\color{red} Mine}
\put(283,90){\color{blue} River}
\put(290,40){\color{yellow} Vegetation}
\end{picture}
\begin{figurenotes}
In the image above we observe in white the footprint of a mine, the river in brown and some vegetation in green. Source: Digital Globe-Google Maps.
\end{figurenotes}
\end{center}
\end{figure}
 We use data from NASA's LANDSAT 7 satellite\footnote{These data are distributed by the Land Processes Distributed Active Archive Center (LP DAAC), located at USGS/EROS, Sioux Falls, SD. http://lpdaac.usgs.gov} for the years 2004-2014 for the 66 path-rows (squares) that cover Colombia at a resolution of 30m $\times$ 30m pixels (squares). The area of Colombia is $1'138,903 km^2$, so we have a total of 1.27 x $10^{10}$ pixels to analyze for illegal mining. The satellite captures every point on the earth's surface every two weeks, but due to the presence of clouds we needed to create cloudless composites at the year level.\footnote{We use Alex Zvoleff's open source algorithms http://azvoleff.com/teamlucc.html that also apply topographic correction to each image} We excluded from the analysis pixels with forests using Hansen's deforestation data \cite{Hansen_etal}. 
 \\[0.4cm]
 The Mining Census of the Ministry of Mines  gives us the location and area of the mines in 2010 for half of the municipalities.\footnote{Although there might be a concern that the Census sampled municipalities were selected on certain characteristics, we show in the on-line appendix Table \ref{tab:sumstats_censed} that this is not the case. Municipalities included and not included in the Census are balanced in presence of armed groups, change in royalties with the reform and production of different minerals.} We validated this information using manual inspection of high resolution images  to draw the exact shape of the mine. We also use the identified shape of mines in Open Street Map \footnote{https://www.openstreetmap.org}. Our final data set consists of the label on whether the pixels has a mine and eight dependent variables to train the statistical model: six satellite surface reflectance measures for different bands \footnote{Different wave lengths are captured in different bands. Specifically we use  Band 1 (blue), Band 2 (Green), Band 3 (Red), Band 4 (Near infra-red), Band 5 (Short wave infra-red 1) and Band 7 (Short wave infra-red 2)}, ecosystem type \cite{Etter2006} and deforestation year \cite{Hansen_etal}. 
 \\[0.4cm]
 Given this data set one could code a fix rule for declaring a pixel as mined or one can let the machine ``learn'' the optimal rule based on the characteristics of the known mines. For example, we could impose a rule that every pixel with deforestation, not in a dessert and color close to white is a mine. Instead, we let the computer try different nested binary decision rules (trees \footnote{The name ``tree'' comes from the graphical representation of the nested binary decision rules.}) and find one that is accurate at predicting mines and doesn't create many false positives. We split the sample leaving 75\% observations for training (learning) and 25\% for testing. We calibrated decision tree models in the training data because we expect the relationship between the existence of a mine and the satellite bands measurements to be highly non-linear and complex \cite{mlbible}. Specifically we used a random forest model with one hundred trees. A random forest, as its names indicates, is a collection of many binary decision trees where in each node the candidate subset of explanatory variables to use in the binary partition is random. 
  \\[0.4cm]
The random forest prediction model give us a probability that a certain pixel is a mine by the fraction of the hundred trees that declare the pixel as a mine, but we need to decide a cutoff to finally declare the pixel as a mine. For each cutoff we plot in Figure \ref{fig:ROC} the associated True Positive Rate (TPR) and False Positive Rate (FPR) in the testing sample. Ideally we want to have a 100\% TPR and 0\% FPR (upper left corner). As we lower the cutoff, we improve the TPR but also increase the FPR. In the literature it is standard to choose the cutoff $\rho$ that $\max_{\rho} TPR(\rho)-FPR(\rho)$. However, in our case we are using the predictions in a regression framework, and our sample consists mostly of no mine pixels, so we need to choose a different cutoff.   Otherwise we end up with millions of false positives. In the next sub-section we explain how we chose the cutoff marked with the red point below.
 
 \begin{figure}[H]
\begin{center}
\caption{ROC curve for the illegal mining prediction model.}
\label{fig:ROC}
\includegraphics[scale=0.7]{ROC.pdf} 
\begin{figurenotes}
The Receiver Operating Characteristic- ROC curve plots the performance of a binary classification model when varying the cutoff threshold. The False Positive Rate (FPR), the percentage of true NO mine pixels incorrectly classified as a mine, is on the x-axis. The True Positive Rate (TPR), the percentage of true mine pixels correctly classified as a mine, is on the y-axis. As we decrease the cutoff to declare a mine, we accurately classify more true mine pixels as mines, but also increase the number of NO mine pixels incorrectly classified as mines.
\end{figurenotes}
\end{center}
\end{figure}
\subsection{Econometric analysis of the error term}
It is important to analyze how the errors in the individual mine pixel predictions might affect our estimation of the effect of the reform in illegal mining. In this subsection we explain how the errors at the pixel level aggregate to our measure of illegal mining area by municipality, and in turn how this affect the regression. Our estimated measure of mining area ($\widehat{y_{mt}}$) in municipality $m$ at time $t$ can be expressed as the sum of true mine pixels correctly identified plus the true non mine pixels misclassified: 
\[ \widehat{y_{mt}}=\sum_{i \in Mines}(Pred(pix_i)=1) + \sum_{i \notin Mines}(Pred(pix_i)=1)\]
In each true mine pixel the probability of predicting a mine is TPR and in each pixel that is truly mine-free, the probability of predicting a mine is  FPR, where $TPR$ and $FPR$ are the true and false positive rates of the prediction model. In each pixel the random variable can be modeled as a Bernoulli, and, assuming independence\footnote{We don't need to assume independence to prove a weaker version of the law of large numbers if we assume that the correlation between pixels far apart decays geometrically with distance. See appendix for details.} and identical distribution, their sum is binomial. As the number of pixels is large, we can approximate the sum with a normal. Thus $ \widehat{y_{mt}}=y_{mt}TPR+y_{Nmt}FPR+\epsilon_{mt}$, where $y_{mt}$ is the true number of mine pixels, $y_{Nmt}$ the true number of NO mine pixels and $\epsilon_{mt} \sim N(0,y_{mt}TPR(1-TPR)+y_{Nmt}FPR(1-FPR))$. Finally using that the total area of the municipality ($Y_m$) is fixed ( $y_{Nmt}=Y_m-y_{mt}$) we obtain that the fraction of the municipality area that is mined can be expressed as:
\begin{equation}\label{eq:adjpred}
\frac{\widehat{y_{mt}}}{Y_m}= \frac{y_{mt}}{Y_m}\left( TPR-FPR \right)+ FPR+\upsilon_{mt}
\end{equation}
Where
\[ \upsilon_{mt} \sim N \left(0,\frac{y_{mt}TPR(1-TPR)+y_{Nmt}FPR(1-FPR)}{Y^2_m} \right) \]

That is, the raw predicted fraction of the total municipality area that is mined, underestimates the true fraction that is mined by a factor of (TPR-FPR) plus an additive error term of FPR. When we use the predictions as the dependent variable in our regression analysis, FPR will be absorbed by the municipality fixed effects. For completeness in the results section we present regressions with the raw predictions and the adjusted predictions.
Our objective to choose the optimal cutoff is therefore:
\[ \rho^*= \argmin_{\rho} \sum_m \left( TPR(\rho)\frac{y_{m,2010}}{Y_{m,2010}}+ FPR(\rho)\left(1-\frac{y_{m,2010}}{Y_{m,2010}} \right) -\frac{y_{m,2010}}{Y_{m,2010}} \right)^2 \]
since 2010 is our training year from the mining Census. Note that, as the fraction of total municipality area that is mined is around 1\% then the error of our predictions is approximately $ 1\% TPR+99\% FPR$. This is why our red cutoff point prioritizes having a small FPR. 

Two more points to note: the variance is smaller for municipalities with larger area, and, when we measure illegal mining as the fraction of the predicted mining area (instead of total municipality area), we do not know exactly the behavior of the error term, because we will be taking the ratio of two terms measured with error.
  \\[0.4cm]
We present below the confusion matrix for the optimally chosen cutoff. The precision is \input{ppv_opt}\%, that is, of the pixels we predict as mines almost four fifths are truly mines according to the testing data. Our model correctly classifies \input{tpr_opt}\% of true mine pixels (TPR), and wrongly classifies as mines \input{fpr_opt}\% of pixels without an actual mine. These numbers are good compared to values in the literature, for example 76\% and 13.4\%, respectively for detecting oil spills with satellite data \cite{kubatetal1998}. The Area Under the Curve of our prediction model is 87\%, which is far from the 50\% of a random classifier and close to the 95\% of very good classifiers \cite{mlbible}. %Page 147
 We are currently working on improving these measures and therefore all results are preliminary. 


\begin{table}[H]
\centering
\caption{Confusion matrix for optimal threshold}
%\resizebox{\textwidth}{!}{
\begin{tabular}{l*{3}{c}}

\input{mat_conf}

\end{tabular}
%}
\label{tab:conf_mat}
\begin{tablenotes}
\end{tablenotes}
\end{table}

We smooth our predictions over time for a given pixel so we smooth them, in order to avoid switching back and forth between mine and no mine. \footnote{We do this by calculating the monotonic sequence of 0's (NO mine) and 1's (mine) that is closer to the predictions of the pixel } After predicting the pixels with mining, we ``substract'' the pixels inside legal titles to obtain the illegally mined pixels. Locations and exact shapes for legal mines were obtained from the NGO Tierra Minada\footnote{The full data set can be downloaded from \url{https://sites.google.com/site/tierraminada/}} that digitized official government records from the \textit{Catastro Minero Colombiano}. Finally, we collapse the predictions at the municipality level to be used in the regression analysis. See Appendix \ref{sec:ilegaldata} for further details on our procedure. 
  \\[0.4cm]
Table \ref{tab:sumstats_regvar} presents the summary statistics of our predictions. We estimate that the average municipality has less than \input{prop_illegal}percent of its total area illegally mined. However, expressed as the fraction of only the mining area, our estimates imply that \input{propmined_illegal}percent of the mining area is exploited without a title.

\begin{table}[H]
\caption{Summary statistics illegal mining municipality panel}
\label{tab:sumstats_regvar}
 \input{sumstats_regvar}
 \begin{tablenotes}
Summary statistics for illegal mining by municipality according to the random forest model. AG denotes armed groups. Calculations: Authors.
\end{tablenotes}
\end{table}



\section{Constructing the illegal mining data}\label{sec:ilegaldata}
As illegal mining is not observable in government records, we are going to use satellite images and a statistical model to detect the evolution of illegal mining through time. This requires many steps and computing time as will be described below:
\begin{itemize}
\item Identify the satelite images from the satellite Landsat7 that cover Colombia for the years 2004-2014 in the webpage of the US Geological Survey http://earthexplorer.usgs.gov/ . The satellite takes a picture of each square (``path-row'') of the earth every two weeks.
\item Download the necessary images from http://espa.cr.usgs.gov/ in projection UTM-18 surface reflectance. There are on average 550 images per year, each one around 230MB when compressed. That is a total of around 1.5TB of raw data. 
\item Using the program teamlucc (http://azvoleff.com/teamlucc.html), with slight modification we encountered on the process, we remove clouds, and adjust for topography so that the data can be used.
\item Given the presence of clouds, we need to construct a cloudless composite for every year. That is we look for a cloudless image of each pixel and create a new image with information when the pixel was cloud free. This process took around 120 days of computer time.
\item The resolution of Landsat is 30x30m so we can not do shape recognition. See below for an illustration.
 \begin{figure}[H]
\begin{center}
\caption{}
\label{fig:res_ex}
\includegraphics[width=0.9\textwidth]{Resolution_examples.png} 
\begin{figurenotes}
Source: \cite{Jensen2007}
\end{figurenotes}
\end{center}
\end{figure}
\item In order to train the model we needed to show images of mines. For this we use the Mining Census 2010 that give us the location and area of all the mines in half the municipalities of the country. Before using the Census data we had to remove mines whose coordinates where not inside the indicated municipality; whose coordinates had missing values; or when the values for minutes or second where not between 0 and 60. Obviously we only included open pit mines.
\item We validated the presence of mines on the coordinates stated on the Census by using high resolution images on XXX mines.
\item We also classified as mine any pixel inside a circle of area the size of the mine and center the location of the mine, that was not classified as NO mine in the high resolution inspection. Note that the circular assumption is necessary because we do not know the exact shape of the mine, only the coordinates of the mouth of mine and its area. 
\item We classified as NO mine any pixel in a census municipality that was not classified as mine in the high resolution inspection or in the circle around census mines.
\item Our training data frame consist of matrix with 8 columns (variables) and XXX rows (observations or pixels). The columns are the 6 bands of the satellite information, the information on how long ago the pixel was deforested and an indicator on whether the pixel is a mine or not.
\item We split the sample in training and testing sets, by dividing the country in XX squares. And further subdividing each square in 4 squares and randomly choosing one for testing and the other for training. The reason for not taking a random 25\% sample for testing is that a pixel is pretty similar to its neighbor, so is better to stratify this way.
\item We tried boosting, support vector machines with radial kernels and random forest models in a small subsample of the data. For all three models we tried down-sampling and smote. Best parameters for each case where chosen by 10-fold cross validation.Based on the results in the subsample we decided to fit a random forest by down-sampling in the whole dataset.
\item The random forest consist of 100 trees so is hard to represent its structure. However we can consider the ``importance'' of each variable for the prediction.
 \begin{figure}[H]
\begin{center}
\caption{}
\label{fig:rf_varimportance}
\includegraphics[scale=0.7]{RF_varimportance.png} 
\begin{figurenotes}

\end{figurenotes}
\end{center}
\end{figure}
\end{itemize}

\begin{figure}[H]
\begin{center}
\caption{}
\label{figa:evol_prmined}
\includegraphics[width=0.9\textwidth]{Predmined_area.pdf} 

\end{center}
\end{figure}
\subsection{Weak law of large numbers for correlated Bernoulli's among pixels}
Let's assume that $|cov(X_i,X_j)| \leq c^{dist(i,j)}$. We need to find a bound for $\sum_{j=1}^n cov(X_i,X_j)$. The largest sum will be for a pixel right in the center, because it will have all the shorter distance. For ease of exposition let's assume $n=(2k+1)^2$, and consider pixel $i$ in the center. This pixel will have its 8 neighbors, the 16 pixels surrounding them, and on. The exact expression is:
\[ \sum_{j=1}^n cov(X_i,X_j) \leq c+8c^2+16c^3+\ldots +8kc^{k+1}\]
With some manipulation it can be shown that 
\[ \sum_{j=1}^n cov(X_i,X_j) \leq c+\frac{8c^2(1-c^k)k}{1-c} \]
Consequently using Chebyshev's inequality 

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%


\end{document}
