\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Arag\'{o}n%
\ \BBA {} Rud%
}{%
Arag\'{o}n%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2015}}%
}]{%
Aragon2015}
\APACinsertmetastar {%
Aragon2015}%
\begin{APACrefauthors}%
Arag\'{o}n, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Polluting Industries and Agricultural Productivity:
  Evidence from Mining in Ghana} {Polluting industries and agricultural
  productivity: Evidence from mining in ghana}.{\BBCQ}
\newblock
\APACjournalVolNumPages{The Economic Journal}{}{}{1-31}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Athey%
\ \BBA {} Imbens%
}{%
Athey%
\ \BBA {} Imbens%
}{%
{\protect \APACyear {2016}}%
}]{%
AtheyImbens2016}
\APACinsertmetastar {%
AtheyImbens2016}%
\begin{APACrefauthors}%
Athey, S.%
\BCBT {}\ \BBA {} Imbens, G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Recursive partitioning for heterogeneous causal effects}
  {Recursive partitioning for heterogeneous causal effects}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Proceedings of the National Academy of
  Sciences}{113}{27}{7353--7360}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Banerjee%
, Mullainathan%
\BCBL {}\ \BBA {} Hanna%
}{%
Banerjee%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2012}}%
}]{%
Banerjeeetal2012}
\APACinsertmetastar {%
Banerjeeetal2012}%
\begin{APACrefauthors}%
Banerjee, A.%
, Mullainathan, S.%
\BCBL {}\ \BBA {} Hanna, R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{April}{}.
\newblock
\APACrefbtitle {Corruption} {Corruption}\ \APACbVolEdTR {}{Working Paper\
  \BNUM\ 17968}.
\newblock
\APACaddressInstitution{}{National Bureau of Economic Research}.
\newblock
\begin{APACrefURL} \url{http://www.nber.org/papers/w17968} \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.3386/w17968} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Belloni%
, Chernozhukov%
\BCBL {}\ \BBA {} Hansen%
}{%
Belloni%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2014}}%
}]{%
Bellonietal2012}
\APACinsertmetastar {%
Bellonietal2012}%
\begin{APACrefauthors}%
Belloni, A.%
, Chernozhukov, V.%
\BCBL {}\ \BBA {} Hansen, C.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {High-Dimensional Methods and Inference on Structural and
  Treatment Effects} {High-dimensional methods and inference on structural and
  treatment effects}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Economic Perspectives}{28}{2}{29-50}.
\newblock
\begin{APACrefURL}
  \url{http://www.aeaweb.org/articles.php?doi=10.1257/jep.28.2.29}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1257/jep.28.2.29} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Burgess%
, Hansen%
, Olken%
, Potapov%
\BCBL {}\ \BBA {} Sieber%
}{%
Burgess%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2012}}%
}]{%
Burgess2012}
\APACinsertmetastar {%
Burgess2012}%
\begin{APACrefauthors}%
Burgess, R.%
, Hansen, M.%
, Olken, B\BPBI A.%
, Potapov, P.%
\BCBL {}\ \BBA {} Sieber, S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The Political Economy of Deforestation in the Tropics}
  {The political economy of deforestation in the tropics}.{\BBCQ}
\newblock
\APACjournalVolNumPages{The Quarterly Journal of Economics}{}{}{}.
\newblock
\begin{APACrefURL}
  \url{http://qje.oxfordjournals.org/content/early/2012/10/04/qje.qjs034.abstract}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1093/qje/qjs034} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Cai%
\ \BBA {} Treisman%
}{%
Cai%
\ \BBA {} Treisman%
}{%
{\protect \APACyear {2004}}%
}]{%
caitreisman2004}
\APACinsertmetastar {%
caitreisman2004}%
\begin{APACrefauthors}%
Cai, H.%
\BCBT {}\ \BBA {} Treisman, D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {State corroding federalism} {State corroding
  federalism}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Public Economics}{88}{3}{819--843}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Chetty%
}{%
Chetty%
}{%
{\protect \APACyear {2009}}%
}]{%
Chetty2009}
\APACinsertmetastar {%
Chetty2009}%
\begin{APACrefauthors}%
Chetty, R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{August}{}.
\newblock
{\BBOQ}\APACrefatitle {Is the Taxable Income Elasticity Sufficient to Calculate
  Deadweight Loss? The Implications of Evasion and Avoidance} {Is the taxable
  income elasticity sufficient to calculate deadweight loss? the implications
  of evasion and avoidance}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Journal: Economic
  Policy}{1}{2}{31-52}.
\newblock
\begin{APACrefURL} \url{http://www.aeaweb.org/articles?id=10.1257/pol.1.2.31}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1257/pol.1.2.31} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Faber%
\ \BBA {} Gaubert%
}{%
Faber%
\ \BBA {} Gaubert%
}{%
{\protect \APACyear {2016}}%
}]{%
faber2016}
\APACinsertmetastar {%
faber2016}%
\begin{APACrefauthors}%
Faber, B.%
\BCBT {}\ \BBA {} Gaubert, C.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
\APACrefbtitle {Tourism and Economic Development: Evidence from Mexico's
  Coastline.} {Tourism and economic development: Evidence from mexico's
  coastline.}
\newblock
\APACrefnote{National Bureau of Economic Research}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Foster%
, Gutierrez%
\BCBL {}\ \BBA {} Kumar%
}{%
Foster%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2009}}%
}]{%
Foster2009}
\APACinsertmetastar {%
Foster2009}%
\begin{APACrefauthors}%
Foster, A.%
, Gutierrez, E.%
\BCBL {}\ \BBA {} Kumar, N.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Voluntary Compliance, Pollution Levels, and Infant
  Mortality in Mexico} {Voluntary compliance, pollution levels, and infant
  mortality in mexico}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Review}{99}{2}{191-97}.
\newblock
\begin{APACrefURL}
  \url{http://www.aeaweb.org/articles.php?doi=10.1257/aer.99.2.191}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1257/aer.99.2.191} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Guiteras%
, Jina%
\BCBL {}\ \BBA {} Mobarak%
}{%
Guiteras%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2015}}%
}]{%
Guiterasetal}
\APACinsertmetastar {%
Guiterasetal}%
\begin{APACrefauthors}%
Guiteras, R.%
, Jina, A.%
\BCBL {}\ \BBA {} Mobarak, A\BPBI M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{May}{}.
\newblock
{\BBOQ}\APACrefatitle {Satellites, Self-Reports, and Submersion: Exposure to
  Floods in Bangladesh} {Satellites, self-reports, and submersion: Exposure to
  floods in bangladesh}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Review}{105}{5}{232-36}.
\newblock
\begin{APACrefURL}
  \url{http://www.aeaweb.org/articles?id=10.1257/aer.p20151095}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1257/aer.p20151095} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Henderson%
, Storeygard%
\BCBL {}\ \BBA {} Weil%
}{%
Henderson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2012}}%
}]{%
Henderson2012}
\APACinsertmetastar {%
Henderson2012}%
\begin{APACrefauthors}%
Henderson, J\BPBI V.%
, Storeygard, A.%
\BCBL {}\ \BBA {} Weil, D\BPBI N.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{April}{}.
\newblock
{\BBOQ}\APACrefatitle {{Measuring Economic Growth from Outer Space}}
  {{Measuring Economic Growth from Outer Space}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Review}{102}{2}{994-1028}.
\newblock
\begin{APACrefURL}
  \url{https://ideas.repec.org/a/aea/aecrev/v102y2012i2p994-1028.html}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Idrobo%
, Mejia%
\BCBL {}\ \BBA {} Tribin%
}{%
Idrobo%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2014}}%
}]{%
Idrobo2014}
\APACinsertmetastar {%
Idrobo2014}%
\begin{APACrefauthors}%
Idrobo, N.%
, Mejia, D.%
\BCBL {}\ \BBA {} Tribin, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Illegal Gold Mining and Violence in Colombia} {Illegal
  gold mining and violence in colombia}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Peace Economics, Peace Science and Public
  Policy}{20}{}{83-111}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Jayachandran%
}{%
Jayachandran%
}{%
{\protect \APACyear {2009}}%
}]{%
Jayachandran2009}
\APACinsertmetastar {%
Jayachandran2009}%
\begin{APACrefauthors}%
Jayachandran, S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Air quality and early-life mortality evidence from
  Indonesia's wildfires} {Air quality and early-life mortality evidence from
  indonesia's wildfires}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Human Resources}{44}{4}{916--954}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Khan%
, Khwaja%
\BCBL {}\ \BBA {} Olken%
}{%
Khan%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2016}}%
}]{%
Khanetal2016}
\APACinsertmetastar {%
Khanetal2016}%
\begin{APACrefauthors}%
Khan, A.%
, Khwaja, A.%
\BCBL {}\ \BBA {} Olken, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Tax farming redux: experimental evidence on performance
  pay for tax collectors} {Tax farming redux: experimental evidence on
  performance pay for tax collectors}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Quarterly Journal of Economics}{}{}{219-271}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Lipscomb%
\ \BBA {} Mobarak%
}{%
Lipscomb%
\ \BBA {} Mobarak%
}{%
{\protect \APACyear {2013}}%
}]{%
Lipscomb2013}
\APACinsertmetastar {%
Lipscomb2013}%
\begin{APACrefauthors}%
Lipscomb, M.%
\BCBT {}\ \BBA {} Mobarak, A\BPBI M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Decentralization and pollution spillovers: evidence from
  the re-drawing of county borders in Brazil} {Decentralization and pollution
  spillovers: evidence from the re-drawing of county borders in brazil}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Unpublished Manuscript, University of Virginia and Yale
  University}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Ministerio de Minas y Energia}%
}{%
{Ministerio de Minas y Energia}%
}{%
{\protect \APACyear {2003}}%
}]{%
MinMinas2003}
\APACinsertmetastar {%
MinMinas2003}%
\begin{APACrefauthors}%
{Ministerio de Minas y Energia}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003}{}{}.
\newblock
\APACrefbtitle {Glosario Tecnico Minero.} {Glosario tecnico minero.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Romero%
\ \BBA {} Saavedra%
}{%
Romero%
\ \BBA {} Saavedra%
}{%
{\protect \APACyear {2015}}%
}]{%
Romero2015}
\APACinsertmetastar {%
Romero2015}%
\begin{APACrefauthors}%
Romero, M.%
\BCBT {}\ \BBA {} Saavedra, S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
\APACrefbtitle {The Effect of Gold Mining on the Health of Newborns.} {The
  effect of gold mining on the health of newborns.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Tolonen%
}{%
Tolonen%
}{%
{\protect \APACyear {2015}}%
}]{%
Tolonen2015}
\APACinsertmetastar {%
Tolonen2015}%
\begin{APACrefauthors}%
Tolonen, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Local Industrial Shocks, Female Empowerment and Infant
  Health: Evidence from Africa's Gold Mining Industry} {Local industrial
  shocks, female empowerment and infant health: Evidence from africa's gold
  mining industry}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Job Market Paper}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
von~der Goltz%
\ \BBA {} Barnwal%
}{%
von~der Goltz%
\ \BBA {} Barnwal%
}{%
{\protect \APACyear {2016}}%
}]{%
Goltz2016}
\APACinsertmetastar {%
Goltz2016}%
\begin{APACrefauthors}%
von~der Goltz, J.%
\BCBT {}\ \BBA {} Barnwal, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{October}{}.
\newblock
{\BBOQ}\APACrefatitle {Mines: The Local Wealth and Health Effects of Mineral
  Mining in Developing Countries} {Mines: The local wealth and health effects
  of mineral mining in developing countries}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Working paper}{}{}{}.
\newblock
\begin{APACrefURL} \url{http://www.aeaweb.org/articles?id=10.1257/pol.1.2.31}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\end{thebibliography}
