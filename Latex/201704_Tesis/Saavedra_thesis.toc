\select@language {english}
\contentsline {chapter}{\numberline {1}Local incentives and national tax evasion: The response of illegal mining to a tax reform in Colombia}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Colombian context and details of the reform}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}Theoretical framework}{7}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Setup}{7}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}The effect of the reform on illegal mining}{8}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Heterogeneous effects of the reform}{9}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}Data}{11}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Constructing the illegal mining panel}{12}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Econometric analysis of the error term and implications for the optimal cutoff}{15}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}From pixel predictions to municipality panel}{17}{subsection.1.4.3}
\contentsline {section}{\numberline {1.5}The effect of the reform on illegal mining}{18}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Identification strategies}{18}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Main results}{20}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Heterogeneous effects of the reform}{24}{subsection.1.5.3}
\contentsline {section}{\numberline {1.6}Health effects}{27}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Identification strategies}{28}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}Results}{29}{subsection.1.6.2}
\contentsline {section}{\numberline {1.7}Conclusions }{30}{section.1.7}
\contentsline {chapter}{\numberline {2}The Effects of Gold Mining on Newborns' Health}{32}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{32}{section.2.1}
\contentsline {section}{\numberline {2.2}Mercury and human health}{35}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Mercury and amalgamation in gold mining}{35}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Health effects of mercury}{35}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Colombian context and data}{36}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Data on mining activity}{37}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Data on newborns}{38}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Identification strategies}{39}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Spatial exposure to mining}{39}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Holy Week}{41}{subsection.2.4.2}
\contentsline {section}{\numberline {2.5}Results}{42}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Spatial exposure}{42}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Holy Week}{44}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Robustness checks}{47}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Other measures of mining activity}{47}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Price of gold}{47}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}Fiscal effect on municipalities}{47}{subsection.2.6.3}
\contentsline {subsection}{\numberline {2.6.4}Birth and mother characteristics}{48}{subsection.2.6.4}
\contentsline {subsection}{\numberline {2.6.5}Illegal mining}{48}{subsection.2.6.5}
\contentsline {subsection}{\numberline {2.6.6}Using only the centroids of the mines}{49}{subsection.2.6.6}
\contentsline {section}{\numberline {2.7}Conclusions and future work}{49}{section.2.7}
\contentsline {chapter}{\numberline {3}The Daily Grind: Cash Needs, Labor Supply and Self-Contro}{50}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{50}{section.3.1}
\contentsline {section}{\numberline {3.2}Sample and Data}{53}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Bike-Taxi Driving}{53}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Sampling Frame}{54}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Data}{54}{subsection.3.2.3}
\contentsline {subsubsection}{Baseline Survey}{54}{section*.4}
\contentsline {subsubsection}{Logs}{55}{section*.5}
\contentsline {subsection}{\numberline {3.2.4}Experimental Income Shocks}{56}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Sample Characteristics}{56}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Summary Statistics from Logs}{57}{subsection.3.2.6}
\contentsline {subsection}{\numberline {3.2.7}Correlates of needs}{58}{subsection.3.2.7}
\contentsline {section}{\numberline {3.3}Results}{60}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Reduced form: Daily life events and labor supply}{60}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Reported Cash Need and Daily Labor Supply}{61}{subsection.3.3.2}
\contentsline {subsubsection}{Cross-Sectional Evidence}{61}{section*.6}
\contentsline {subsubsection}{Within-Driver Variation Across Days}{61}{section*.7}
\contentsline {subsubsection}{Within-Driver, Within-Day Hazard Analysis}{63}{section*.8}
\contentsline {subsection}{\numberline {3.3.3}Labor supply responses to earning opportunities}{65}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Economic Significance and Rationale }{68}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Time costs of targeting}{68}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Proposed Model: Earned Income Targeting as Morphine}{68}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Calibration}{71}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Simulation Results}{72}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Who is a target earner?}{75}{subsection.3.4.5}
\contentsline {section}{\numberline {3.5}Alternate hypotheses}{75}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Risk Sharing}{76}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Intra-Household Issues}{76}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Conclusion}{77}{section.3.6}
\contentsline {chapter}{\numberline {A}Appendix to Chapter 1}{91}{Appendix.1.A}
\contentsline {section}{\numberline {A.1}Additional Figures and Tables}{91}{section.1.A.1}
\contentsline {section}{\numberline {A.2}Constructing the illegal mining data}{101}{section.1.A.2}
\contentsline {subsection}{\numberline {A.2.1}Weak law of large numbers for correlated Bernoulli's random variables among pixels}{106}{subsection.1.A.2.1}
\contentsline {section}{\numberline {A.3}Further analysis}{107}{section.1.A.3}
\contentsline {subsection}{\numberline {A.3.1}Heterogeneous treatment effects in loser municipalities}{107}{subsection.1.A.3.1}
\contentsline {subsection}{\numberline {A.3.2}Positive effects of the reform?}{108}{subsection.1.A.3.2}
\contentsline {subsection}{\numberline {A.3.3}Effects on municipalities with mineral resources and prone to satellite detection}{109}{subsection.1.A.3.3}
\contentsline {subsection}{\numberline {A.3.4}Endogenous response of municipalities to budget change}{110}{subsection.1.A.3.4}
\contentsline {section}{\numberline {A.4}Online Appendix}{112}{section.1.A.4}
\contentsline {chapter}{\numberline {B}Appendix to Chapter 2}{118}{Appendix.1.B}
\contentsline {section}{\numberline {B.1}Continuous measures of pollution}{118}{section.1.B.1}
\contentsline {section}{\numberline {B.2}Extra Tables}{120}{section.1.B.2}
\contentsline {section}{\numberline {B.3}Extra Figures}{127}{section.1.B.3}
\contentsline {subsection}{\numberline {B.3.1}Impacts of different levels of mining}{131}{subsection.1.B.3.1}
\contentsline {chapter}{\numberline {C}Appendix to Chapter 3}{133}{Appendix.1.C}
\contentsline {chapter}{References}{149}{chapter*.14}
\contentsline {chapter}{References}{149}{section*.15}
