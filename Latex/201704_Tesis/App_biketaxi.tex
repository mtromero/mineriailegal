
\section*{ Robustness of need measures}

This section discusses two potential threats to the analysis above.
First, there may exist experimenter effects, given the high frequency
and nature of the data collected. Second, it might be possible that
the timing of cash needs is endogenous. 


\subsection*{Experimenter effects}

The log asked individuals to record their cash need at the beginning
of every day. One may worry that simply asking this question made
that specific amount salient in respondents' minds, especially those
with a lower level of education. It is also possible that respondents
felt an experimenter demand effect, i.e. that respondents believed
that the researchers expected them to work up to the need, and then
quit thereafter. In this section we argue that these two types of
experimenter effects are unlikely to be driving our results.

The most convincing test of the presence of such experimenter effects
would be if we had a comparable group of bicycle taxi drivers who
were asked to fill logs similar to those we used\emph{, }except for
the question on the daily cash need. We could then check whether workers
who were not asked to state their cash need still exhibit a positive
relationship between expected demands on income (e.g. ROSCA payments
due) and labor supply. Though we cannot test this directly since all
of the workers in our study were asked about the need, we can compare
the variance in hours we observe in our sample to that of bicycle
taxi drivers followed in Dupas and Robinson (2013). While that data
was collected between 2006 and 2008 (i.e. 1 to 3 years earlier than
the present study), it was collected using almost identical logbooks
except that they did not include the question on the day's needs.
Interestingly, we find comparable (and if anything, \emph{larger)}
within-worker variance in hours worked across days in that earlier
sample: 2.74 compared to 2.16 in the sample considered in the present
paper. This at least suggests that the large within-individual variance
in daily labor supply observed in the present study is not an artifact
of our data collection protocol.%
\footnote{One question which we cannot answer is whether keeping any type of
log in the first place affects behavior. %
}

A second way to test whether the data collection made needs particularly
salient is to check how persistent the effects are. If people were
not income targeting at all before the study, but then began to do
so after keeping the logs since the cash needs became salient, then
such respondents should eventually have switched back to their previous
behavior after some time. When we run the hazard analysis separately
for the first and last month during which individuals were keeping
the logs, however, we find the exact same pattern of results, with
the same magnitude, for both time periods, suggesting no fading out.
This further suggests that experimenter effects are unlikely explanations
for our results. 


\subsection*{Endogenous timing of needs}

While many of the determinants of the cash needs reported by our study
participants are almost certainly exogenous and unexpected (e.g. health
shocks, funerals), some can be anticipated (e.g. food for the household).
For such anticipated needs, workers may choose the days in which they
decide to ``deal'' with those -- for example, they may decide to
purchase food on the day they expect to make more money, or they may
decide to pay school fees on the day they wake up feeling in particularly
good health. If that is the case, workers would mechanically report
higher needs on days in which they expect to make more money, explaining
the positive correlation we observe between needs and labor supply.
While this may be the case on the extensive margin -- on Sundays,
which is much less likely to be a work day than other days, respondents
typically report smaller cash needs -- this does not appear to be
the case on the intensive margin. What's more, as shown in Table A2,
people report needs such as savings club payments exactly on the days
in which these are paid (and these savings club payments are on fixed
schedule that workers cannot unilaterally decide on). Finally, if
we restrict the sample to individual-days with only unexpected needs,
we see the same pattern of results.


\subsection*{Ex-post rationalization of labor supply}

Another concern is that people may have felt that they were ``supposed
to'' make at least as much as the need, and therefore filled in the
needs at the end of the day to match whatever they made that day.
There are several pieces of evidence against this. First, respondents
were of course instructed to fill the log in order. While there is
no way of checking they did this, there is no obvious reason not to
-- it is not clear why people would feel that earning enough for a
need was socially desirable. What's more, during weekly recall surveys
we checked whether the logs were correctly filled (i.e. whehter the
log had been filled up to the current time) and only paid respondents
who had done so, building incentives to fill the logs correctly. Second,
reported needs are highly correlated with shocks reported in the weekly
survey. Third, the reduced form relationship between shocks and labor
supply exists without any reliance on the reported need amounts. Fourth
and most important, while the amount that people earn is correlated
with the need, it is not the case that people often report earning
just barely enough to cover the need. In fact, people only make enough
for the need on 41\% of days, and only make 20 Ksh or less over the
need 8\% of the time. This is consistent with the model predictions
-- if the need is sufficiently low or the wage is sufficiently high,
people will continue to work beyond the need level.

\includepdf[pages=-,pagecommand={},width=\textwidth]{Input_Biketaxi/FigC12345.pdf}

\includepdf[pages=-,pagecommand={},width=\textwidth]{Input_Biketaxi/TabC1.pdf}
\includepdf[pages=-,pagecommand={},width=\textwidth]{Input_Biketaxi/TabC2.pdf}
\includepdf[pages=-,pagecommand={},width=\textwidth]{Input_Biketaxi/TabC3.pdf}
\includepdf[pages=-,pagecommand={},width=\textwidth]{Input_Biketaxi/TabC456.pdf}
\includepdf[pages=-,pagecommand={},width=\textwidth]{Input_Biketaxi/TabC78.pdf}