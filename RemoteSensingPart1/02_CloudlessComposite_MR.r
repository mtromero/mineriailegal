#Cargamos los archivos que se van a consolidar, es decir, escenas sin nubes
#Del mismo lugar geogr�fico y con extents casi iguales.
#Para que funcione, vamos a hacerlo layer por layer.

path<-"E:\\Bulk downloads\\"
for( dpto in   c("ATLANTICO")){
  paths<-list.files(paste(path,"EscenasSinNubes\\",dpto, "\\",sep=""),full.names=TRUE)
  files<-list.files(paste(path,"EscenasSinNubes\\",dpto, "\\",sep=""),full.names=FALSE)
  #Creamos un vector con los sufijos de cada banda
  bnd<-c("_B1","_B2","_B3","_B4","_B5","_B6_VCID_1","_B6_VCID_2","_B7")
  k<-as.integer(length(files))
  scenes.input=list()
  for(i in 1:k){
    name<- files[[i]]            
    #Metemos todo en un RasterStack:
    b1.r<-raster(paste(path,"EscenasSinNubes\\",dpto, "\\",name,"\\",name,"_CL_B1.TIF",sep=""))
    b2.r<-raster(paste(path,"EscenasSinNubes\\",dpto, "\\",name,"\\",name,"_CL_B2.TIF",sep=""))
    b3.r<-raster(paste(path,"EscenasSinNubes\\",dpto, "\\",name,"\\",name,"_CL_B3.TIF",sep=""))
    b4.r<-raster(paste(path,"EscenasSinNubes\\",dpto, "\\",name,"\\",name,"_CL_B4.TIF",sep=""))
    b5.r<-raster(paste(path,"EscenasSinNubes\\",dpto, "\\",name,"\\",name,"_CL_B5.TIF",sep=""))
    b6_1.r<-raster(paste(path,"EscenasSinNubes\\",dpto, "\\",name,"\\",name,"_CL_B6_VCID_1.TIF",sep=""))
    b6_2.r<-raster(paste(path,"EscenasSinNubes\\",dpto, "\\",name,"\\",name,"_CL_B6_VCID_2.TIF",sep=""))
    b7.r<-raster(paste(path,"EscenasSinNubes\\",dpto, "\\",name,"\\",name,"_CL_B7.TIF",sep=""))
    scene<-stack(b1.r,b2.r,b3.r,b4.r,b5.r,b6_1.r,b6_2.r,b7.r)
    rm(b1.r,b2.r,b3.r,b4.r,b5.r,b6_1.r,b6_2.r,b7.r)
    scenes.input[[i]]=scene
  } #close all the scenes in a state
    gc()
  #Una vez ya tenemos los rasters aqui, hay que dividirlos en 2 listas de rasters
  #Los que tengan UTM 18 y los que tengan UTM 19
  #
  ##Para esto necesitamos crear un vector de Booleans que tengan los UTM 18 como TRUE
  boolean<-rep(FALSE,length(scenes.input))
  utm18<-"+proj=utm +zone=18 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"
  
  #Ahora lo llenamos con los verdaderos valores usando un loop
  for(x in 1:length(boolean)){
    proyeccion<-proj4string(scenes.input[[x]])
    if(proyeccion==utm18){
      boolean[x]<-TRUE
    }
  }
  scenes.input18<-scenes.input[boolean]
  scenes.input19<-scenes.input[!boolean]
  scenes.input.total<-list(scenes.input18,scenes.input19)
  #Ya teniendo el boolean que nos dice qu� hacer, tenemos que hacer en dos partes
  #La parte para utm18
  for(j in 1:2){
    scenes.input<-scenes.input.total[[j]]
    scenes.input$fun <- mean
    mos <- do.call(mosaic, scenes.input) 
    rm(scenes.input)
    gc()
    mos<-round(mos)
    #Y finalmente la guardamos en un archivo tipo tiff
    if(sum(!boolean)==0) tipo=""
    if(sum(!boolean)!=0) tipo<-toString(j)
    dir.create(paste("CreatedData\\EscenasSinNubes\\",dpto,tipo,"\\",sep=""),recursive=TRUE)
    for(banda in 1:8){
      writeRaster(raster(mos,layer=banda),file=paste("CreatedData\\EscenasSinNubes\\",dpto,tipo,"\\","composite_",bnd[banda],".tif",sep=""),format="GTiff",overwrite=TRUE)
      }
    rm(mos,resampled.rasters)
    gc()
    print(dpto)
    print(tipo)
    print(i)
  }#close tipo
}#close loop dpto