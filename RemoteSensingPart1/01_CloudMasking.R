#Enmascaramiento y remoci�n de nubes para un departamento
#INPUT: Todas las escenas de cierto semestre que cubran cierto departamento y que tengan
#menos de 80% de cobertura de Nubes.
#OUTPUT: Cada una de las escenas pero sin nubes y donde hab�a nubes tiene NA en vez de 0.
#
##MEJORAMIENTO DE EFICIENCIA: Las escenas t�picamente tienen muchas partes que no estan 
# en el departamento, se puede iniciar haciendo un CROP al extent del departamento usando el 
# shapefile  
########################################################################################
#Inicialmente definimos el nombre del departamento al que se le va a hacer el proceso
#El formato va en
for(dpto in c("CAQUETA","ANTIOQUIA","BOLIVAR")){

#Dado que las escenas son muy grandes para meterlas en COPY, el proceso se va a hacer
#en distintos computadores, por eso se necesita el path donde est�n guardados los departamentos
#EN ESA CARPETA DEBE HABER UNA CARPETA SEPARADA POR CADA DEPARTAMENTO
path<- "C:/Users/USER/Desktop/Programas/BULK/bda/DEPARTAMENTOS/"
#Creamos una lista con el nombre de todas las escenas a las que les vamos a sacar
#las nubes
files<-list.files(paste(path, dpto,"\\",sep=""),)
#Para fines pr�cticos, se crea un vector de strings con el c�digo de cada layer
bnd<-c("_B1","_B2","_B3","_B4","_B5","_B6_VCID_1","_B6_VCID_2","_B7")
#Ahora para cada uno de los nombres de las escenas, hacemos el proceso de enmascaramiento
#Cargamos el dataFrame de los municipios censados
load("CreatedData\\Rdata\\censadosNew.Rdata")
k<-as.integer(length(files))
for(i in 1:k){
  #Primero obtenemos el nombre de la escena que se va a usar
  name<- files[[i]]
  #Metemos todo en un RasterStack:
  b1.r<-raster(paste(path,dpto, "\\",name,"\\",name,"_B1.TIF",sep=""))
  b2.r<-raster(paste(path,dpto, "\\",name,"\\",name,"_B2.TIF",sep=""))
  b3.r<-raster(paste(path,dpto, "\\",name,"\\",name,"_B3.TIF",sep=""))
  b4.r<-raster(paste(path,dpto, "\\",name,"\\",name,"_B4.TIF",sep=""))
  b5.r<-raster(paste(path,dpto, "\\",name,"\\",name,"_B5.TIF",sep=""))
  b6_1.r<-raster(paste(path,dpto, "\\",name,"\\",name,"_B6_VCID_1.TIF",sep=""))
  b6_2.r<-raster(paste(path,dpto, "\\",name,"\\",name,"_B6_VCID_2.TIF",sep=""))
  b7.r<-raster(paste(path,dpto, "\\",name,"\\",name,"_B7.TIF",sep=""))
  scene<-stack(b1.r,b2.r,b3.r,b4.r,b5.r,b6_1.r,b6_2.r,b7.r)
  rm(b1.r,b2.r,b3.r,b4.r,b5.r,b6_1.r,b6_2.r,b7.r)
  gc()
  #####
  #Las escenas t�picamente contienen mucha mas �rea que la de un departamento, y esto hace
  #que todo se demore mucho, solo nos interesa enmascarar con nubes al departamento en cuestion
  #Potencialmente con un buffer de tama�o d (por lo que admitimos minas a 2km del mpio)
  #
  censadosBuffer<-spTransform(censadosBuffer,CRS(proj4string(scene)))
  #Ahora queremos hacer el subset de los municipios censados en el departamento en cuesti�n
  # y luego recortar la escena de acuerdo a los pol�gonos que obtenemos, es decir, enmascarar.
  censados.dpto.sf<-subset(censadosBuffer,censadosBuffer$DEPARTAMEN==dpto)
  #Estos municipios son de donde vamos a extraer los datos, y representan una peque�a
  #porcion de la escena en muchos casos. Por eso podemos desechar el resto, enmascarando.
  #Queremos que todo lo que no esta en la mascara se vuelva 0.
      #Solo queremos continuar si los extents se yuxtaponen, crop bota error si no
      scene.small<-tryCatch(crop(scene,censados.dpto.sf), error=function(e) NA)    
  if(!length(scene.small)==1){
      scene.small=reclassify(scene.small, cbind(0, NA) )
      scene.small<-mask(scene.small,censados.dpto.sf,updatevalue=NA)
      #Al final quedamos con un raster que solo tiene datos 2 km a la redonda de los mpios
      #censados.
      #####
      #Para sacar la mascara de nubes, se necesitan las bandas 1 y 6_1 del stack
      #Ademas es necesario que esten en formato SpatialGridDataFrame
      b.1<-as(raster(scene.small,layer=1), 'SpatialGridDataFrame')
      b.61<-as(raster(scene.small,layer=6),'SpatialGridDataFrame')
      #Note que los SpatialGridDataFrame se almacenan en memoria, por lo que esto puede
      #Ser una limitaci�n computacional, periodicamente se puede usar gc() para liberar 
      #Memoria de objetos borrados
      gc()
      #Ahora se crea la m�scara de nubes
      ptm<-proc.time()
      cloud.mask<-clouds(b.1,b.61,level=1)
      proc.time()-ptm
      #Se demora montones de tiempo (obviamente)
      ####
      #Ahora con la m�scara de nubes, enmascaramos el raster original
      #_CL representar� CloudLess, notemos que para enmascarar se puede usar un raster
      #Antes de poder enmascarar necesitamos que el mask est� en tipo raster, esto toma unos segundos
      #Originalmente viene en formato SpatialGridDataFrame
      cloud.maskr<-raster(cloud.mask)
      #Ahora si enmascaramos el raster
      scene.small.masked<-mask(scene.small,cloud.maskr,maskvalue=1,updatevalue=NA)
      #Ahora para cada uno de los layers, cambiemos los 0 por NA, escribamos un archivo
      #.tif, borramos de la memoria y pasamos el garbage collector.
      dir.create(paste(path,"EscenasSinNubes\\",dpto, "\\",name,sep=""),recursive=TRUE)
      for(i in 1:8){
        layer<-raster(scene.small.masked,layer=i)
        #Y finalmente la guardamos en un archivo, dentro de nuestro created DATA
        writeRaster(layer,file=paste(path,"EscenasSinNubes\\",dpto, "\\",name,"\\",name,"_CL",bnd[i],".tif",sep=""),format="GTiff",overwrite=TRUE)
        rm(layer)
        gc()
        
        }
      print(i)
      }
  }
}
###
###
