rm(list=ls())
setwd("C:/Users/USER/Copy/MineriaIlegal")
setwd("H:\\Copy\\MineriaIlegal")
setwd("C:\\Users\\santi\\Copy\\MineriaIlegal")

CRANMirror="http://cran.stat.ucla.edu/"
#Load libraries
Loaded=require(foreign)
if(Loaded==F) install.packages("foreign", repos = CRANMirror); library(foreign)
Loaded=require(rgdal)
if(Loaded==F) install.packages("rgdal", repos = CRANMirror); library(rgdal)
Loaded=require(rgeos)
if(Loaded==F) install.packages("rgeos", repos = CRANMirror); library(rgeos)
Loaded=require(data.table)
if(Loaded==F) install.packages("data.table", repos = CRANMirror); library(data.table)
Loaded=require(maptools)
if(Loaded==F) install.packages("maptools", repos = CRANMirror); library(maptools)
Loaded=require(sp)
if(Loaded==F) install.packages("sp", repos = CRANMirror); library(sp)
Loaded=require(raster)
if(Loaded==F) install.packages("raster", repos = CRANMirror); library(raster)
Loaded=require(landsat)
if(Loaded==F) install.packages("landsat", repos = CRANMirror); library(landsat)
Loaded=require(RColorBrewer)
if(Loaded==F) install.packages("RColorBrewer", repos = CRANMirror); library(RColorBrewer)
Loaded=require(ggmap)
if(Loaded==F) install.packages("ggmap", repos = CRANMirror); library(ggmap)
Loaded=require(foreach)
#if(Loaded==F) install.packages("foreach", repos = CRANMirror); library(foreach)
Loaded=require(parallel)
if(Loaded==F) install.packages("parallel", repos = CRANMirror); library(parallel)
#
#Number of clusters of your pc minus 1, so you can still do some basic work in your pc
numCores <- detectCores()-1

      removeTmpFiles(2)