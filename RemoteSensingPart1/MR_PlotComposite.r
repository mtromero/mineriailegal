
for( dpto in   c("ATLANTICO","BOYACA","CALDAS","CAUCA","CHOCO","CORDOBA","CUNDINAMARCA","HUILA","LA GUAJIRA")){
#Creamos un vector con los sufijos de cada banda
bnd<-c("_B1","_B2","_B3","_B4","_B5","_B6_VCID_1","_B6_VCID_2","_B7")
#Definamos algunos parametros que cambian en cada computador
name<- "composite_"
#En primer lugar cargamos el compuesto sin nubes, que es un raster
b1.r<-raster(paste("CreatedData\\EscenasSinNubes\\",dpto,"\\","composite_",bnd[1],".tif",sep=""))
b2.r<-raster(paste("CreatedData\\EscenasSinNubes\\",dpto,"\\","composite_",bnd[2],".tif",sep=""))
b3.r<-raster(paste("CreatedData\\EscenasSinNubes\\",dpto,"\\","composite_",bnd[3],".tif",sep=""))
b4.r<-raster(paste("CreatedData\\EscenasSinNubes\\",dpto,"\\","composite_",bnd[4],".tif",sep=""))
b5.r<-raster(paste("CreatedData\\EscenasSinNubes\\",dpto,"\\","composite_",bnd[5],".tif",sep=""))
b6_1.r<-raster(paste("CreatedData\\EscenasSinNubes\\",dpto,"\\","composite_",bnd[6],".tif",sep=""))
b6_2.r<-raster(paste("CreatedData\\EscenasSinNubes\\",dpto,"\\","composite_",bnd[7],".tif",sep=""))
b7.r<-raster(paste("CreatedData\\EscenasSinNubes\\",dpto,"\\","composite_",bnd[8],".tif",sep=""))
scene<-stack(b1.r,b2.r,b3.r,b4.r,b5.r,b6_1.r,b6_2.r,b7.r)
pdf(paste("Results\\Graphs\\export_",dpto,".pdf",sep=""))
plotRGB(scene, r=3, g=2, b=1,scale=255,maxpixels=500000*10,colNA="white")
dev.off()


#mb_df <- raster::as.data.frame(scene, xy=T)
#mb_df <- data.frame(x=mb_df$x, y=mb_df$y, r=mb_df$composite__B3, g=mb_df$composite__B2, b=mb_df$composite__B1)
#
#pdf("export2.pdf")
#ggplot(data=mb_df, aes(x=x, y=y, fill=rgb(r,g,b, maxColorValue = 255))) +
#  coord_equal() + theme_bw() + geom_tile() + scale_fill_identity() +
# scale_x_continuous(breaks=range(mb_df$x)*c(1.01, 0.99), labels=range(mb_df$x), expand = c(0,0)) +
#scale_y_continuous(breaks=range(mb_df$y)*c(0.99, 1.01), labels=range(mb_df$y), expand = c(0,0)) +
#theme(panel.grid=element_blank(), plot.title = element_text(size = 10))+
#labs(list(title="Landsat 7 TM derived RGB composite of Cundinamarca", x="Lon", y="Lat"))
#dev.off()
#
}


#Creamos un vector con los sufijos de cada banda
bnd<-c("_B1","_B2","_B3","_B4","_B5","_B6_VCID_1","_B6_VCID_2","_B7")
#Definamos algunos parametros que cambian en cada computador
 path<- "F:/EscenasRaw/"
 anio=2010
name<- "LE70080532010254ASN00"
#En primer lugar cargamos el compuesto sin nubes, que es un raster
  b1.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B1.TIF",sep=""))
  b2.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B2.TIF",sep=""))
  b3.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B3.TIF",sep=""))
  b4.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B4.TIF",sep=""))
  b5.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B5.TIF",sep=""))
  b6_1.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B6_VCID_1.TIF",sep=""))
  b6_2.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B6_VCID_2.TIF",sep=""))
  b7.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B7.TIF",sep=""))
  scene<-stack(b1.r,b2.r,b3.r,b4.r,b5.r,b6_1.r,b6_2.r,b7.r)
 scene<-stack(b1.r,b2.r,b3.r,b4.r,b5.r,b6_1.r,b6_2.r,b7.r)
pdf(paste("Results\\Graphs\\Escena",anio,name,".pdf",sep=""))
plotRGB(scene, r=3, g=2, b=1,scale=255,maxpixels=500000*10, stretch='lin',colNA="black")
dev.off()

#mb_df <- raster::as.data.frame(scene, xy=T)
#mb_df <- data.frame(x=mb_df$x, y=mb_df$y, r=mb_df$composite__B3, g=mb_df$composite__B2, b=mb_df$composite__B1)
#
#pdf(paste("Results\\Graphs\\b_Escena",anio,name,".pdf",sep=""))
#ggplot(data=mb_df, aes(x=x, y=y, fill=rgb(r,g,b, maxColorValue = 255))) +
#  coord_equal() + theme_bw() + geom_tile() + scale_fill_identity() +
# scale_x_continuous(breaks=range(mb_df$x)*c(1.01, 0.99), labels=range(mb_df$x), expand = c(0,0)) +
#scale_y_continuous(breaks=range(mb_df$y)*c(0.99, 1.01), labels=range(mb_df$y), expand = c(0,0)) +
#theme(panel.grid=element_blank(), plot.title = element_text(size = 10))+
#labs(list(title="Landsat 7 TM derived RGB composite of scene", x="Lon", y="Lat"))
#dev.off()
#