This zipfile contains the stata programs (and ado files), as well as the R programs used to generate the data and figures in
Saavedra & Romero "Local Incentives and National Tax Evasion:The Response of Illegal Mining to a Tax Reform in Colombia"
Published in the European Economic Review

Replication code and data for this article can also be found at https://doi.org/10.7910/DVN/NML0MG
The full code to construct the illegal mining data can also be found at https://dataverse.harvard.edu/dataverse/illegal_mining/
Read the PDF EER_instructions for step by step instructions

The user only needs to change the directories in the stata file 00_MasterCode and that will call the rests of the stata files
The R codes outside the MiningPredictions folder generate three figures, and should be run after all the Stata codes.

