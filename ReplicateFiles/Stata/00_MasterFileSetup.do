** Download Stata code for Lasso from http://faculty.chicagobooth.edu/christian.hansen/research/
** Save the ado file in your local folder
** In Stata type sysdir and it tells u where Stata look for files
** Put the ado file in one of those folders for example C:\Program Files (x86)\Stata14\ado\base\

** Download the Stata code for Altonji https://ideas.repec.org/c/boc/bocode/s457677.html

**Install a couple of extra packages
* ssc install psacalc
* ssc install distinct
* cap ado uninstall ftools
* ssc install ftools
* ftools, compile
* ssc install reghdfe


clear all
version 16.1
set more off
set maxvar 20000 
set matsize 11000 
set scheme uncluttered, permanent /*https://gray.kimbrough.info/uncluttered-stata-graphs/ */

local corredor=c(username)



if "`corredor'"=="santi" {
	global mipath "C:/Users/santi/Dropbox/MineriaIlegal/"
	global gitpath "C:/Users/santi/Documents/mineriailegal/"
	}
	if "`corredor'"=="Santi" {
	global mipath "C:/Users/Santi/Dropbox/MineriaIlegal/"
	global gitpath "C:/Users/Santi/Documents/mineriailegal/"
	}
	
		if "`corredor'"=="santiago.saavedrap" {
	global mipath "E:/Dropbox/Research/MineriaIlegal/"
	global gitpath "C:/Users/santiago.saavedrap/Documents/mineriailegal/"
	}
	
	if "`corredor'"=="Mauricio" {
	global mipath "C:/Users/Mauricio/Dropbox/MineriaIlegal/"
	global gitpath "C:/Users/Mauricio/Documents/git/mineriailegal"
	}
	
	if "`corredor'"=="mauri" {
	global mipath "C:/Users/mauri/Dropbox/MineriaIlegal/"
	global gitpath "C:/Users/mauri/Documents/git/mineriailegal"
	}
	
	if "`corredor'"=="MROMEROLO" {
	global mipath "D:/Dropbox/MineriaIlegal/"
	global gitpath "D:/git/mineriailegal"
	}
	
	if "`corredor'"=="mauricio" {
	global mipath "/media/mauricio/TeraHDD2/Dropbox/MineriaIlegal"
   	global gitpath "/media/mauricio/TeraHDD2/git/mineriailegal"
	}



*
* Path Tree
  global base_in 	"$mipath/RawData"
  global base_out   "$mipath/CreatedData"
 
  
  global dir_do "$gitpath/ReplicateFiles/Stata" 
  global Tables  "$gitpath/Latex/2021_acceptedEER/Tables"
  global Figures "$gitpath/Latex/2021_acceptedEER/Figures"

**********
***** MAURO'S FUNCTION FOR BALANCE TABLES
**********


capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_0 mu_1 mu_2 mu_3 se_0 se_1 se_2 se_3 d_p
capture drop TD*
tab `by' , gen(TD)
foreach var of local varlist {
	 reg `var' TD1 TD2  `if', nocons 
	 test (_b[TD1]- _b[TD2]== 0)
	 mat `d_p'  = nullmat(`d_p'),r(p)
	 matrix A=e(b)
	 matrix B=e(V)
	 mat `mu_3' = nullmat(`mu_3'), A[1,2]-A[1,1]
	 lincom(TD1-TD2)
	 mat `se_3' = nullmat(`se_3'), r(se)
	 sum `var' if TD1==1 & e(sample)==1
	 mat `mu_1' = nullmat(`mu_1'), round(r(mean),0.01)
	 mat `se_1' = nullmat(`se_1'), r(sd)
	 sum `var' if TD2==1 & e(sample)==1
	 mat `mu_2' = nullmat(`mu_2'),round(r(mean),0.01)
	 mat `se_2' = nullmat(`se_2'), r(sd)
	 sum `var' if  e(sample)==1
	 mat `mu_0' = nullmat(`mu_0'), round(r(mean),0.01)
	 mat `se_0' = nullmat(`se_0'), r(sd)
 
}
foreach mat in mu_0 mu_1 mu_2 mu_3 se_0 se_1 se_2 se_3 d_p {
	mat coln ``mat'' = `varlist'
}
 local cmd "my_ptest"
foreach mat in mu_0 mu_1 mu_2 mu_3 se_0 se_1 se_2 se_3 d_p {
	eret mat `mat' = ``mat''
}
end

capt prog drop my_ptestw
*Only works if weights w_var are defined for each variable
program my_ptestw, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_0 mu_1 mu_2 mu_3 se_0 se_1 se_2 se_3 d_p
capture drop TD*
tab `by' , gen(TD)
foreach var of local varlist {
	 reg `var' TD1 TD2 [aw=w_`var'] `if', nocons 
	 test (_b[TD1]- _b[TD2]== 0)
	 mat `d_p'  = nullmat(`d_p'),r(p)
	 matrix A=e(b)
	 matrix B=e(V)
	 mat `mu_3' = nullmat(`mu_3'), A[1,2]-A[1,1]
	 lincom(TD1-TD2)
	 mat `se_3' = nullmat(`se_3'), r(se)
	 sum `var' [aw=w_`var'] if TD1==1 & e(sample)==1
	 mat `mu_1' = nullmat(`mu_1'), round(r(mean),0.01)
	 mat `se_1' = nullmat(`se_1'), r(sd)
	 sum `var' [aw=w_`var'] if TD2==1 & e(sample)==1
	 mat `mu_2' = nullmat(`mu_2'),round(r(mean),0.01)
	 mat `se_2' = nullmat(`se_2'), r(sd)
	 sum `var' [aw=w_`var'] if  e(sample)==1
	 mat `mu_0' = nullmat(`mu_0'), round(r(mean),0.01)
	 mat `se_0' = nullmat(`se_0'), r(sd)
 
}
foreach mat in mu_0 mu_1 mu_2 mu_3 se_0 se_1 se_2 se_3 d_p {
	mat coln ``mat'' = `varlist'
}
 local cmd "my_ptestw"
foreach mat in mu_0 mu_1 mu_2 mu_3 se_0 se_1 se_2 se_3 d_p {
	eret mat `mat' = ``mat''
}
end


**********
***** ACTUALLY PROCESS THE DATA AND CREATE TABLES AND FIGURES
**********

do "$dir_do/01_Create_Stata_DataSet_forreg"
do "$dir_do/02_Create_Stata_DataSet_forreg1517"
do "$dir_do/03_add_causalForest_leafs"
do "$dir_do/04_DiDCountries"
do "$dir_do/05_DiDCountries_1517"
do "$dir_do/06_DiD_Mineral"
do "$dir_do/07_DiD_Mineral_1517"
do "$dir_do/08_doubleLassoPeru"

