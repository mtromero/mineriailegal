use "$base_out/panel_final.dta", clear
replace after_x_col=(ind_col==1 & ano>=2011)
replace ind_after=1 if ano>=2011


gen fr_titulosarea_sqkm=100*titulosarea_sqkm/areamuni_sqkm
gen fr_titulosConcarea_sqkm=100*titulosConcarea_sqkm/areamuni_sqkm







********************
* TABLE 1
*****************
*I manually fix decimals and remove Min Max titles


sum cm_pctgarea_illegal if inmainreg_first==1 & country=="Col" & cm_area_sqkm!=0 
local tempm=string(r(mean), "%9.0fc")+"\%"
file open newfile using "$Tables/pctg_cmarea.tex", write replace
file write newfile "`tempm'"
file close newfile


	
sum unodc_propmined_illegal if inmainreg_first==1 & country=="Col" & cm_area_sqkm!=0 
local tempm=string(r(mean), "%9.0fc")+"\%"
file open newfile using "$Tables/pctg_UNODCarea.tex", write replace
file write newfile "`tempm'"
file close newfile




*Sum stats Colombia
*Sum stats ML training
qui eststo clear
qui estpost tabstat cm_censed  cm_pctgarea_illegal  cm_pctgmines_cielo_abierto cm_pctgarea_illegal_CA unodc_propmined_illegal unodc_propmined_illegal_censed   if inmainreg_first==1 & country=="Col"   , statistics(mean p50 sd min max N) columns(statistics) 

qui esttab using "$Tables/sumstats_muniA.tex", cells("mean(fmt(%9.2fc) label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1fc)  label(Max)) count(fmt(%9.0gc)  label(Obs.))") nomtitle nonumber label  replace noobs  booktabs fragment nodep nonum noli nomtitles

*Sum stats reform 
qui eststo clear
qui estpost tabstat  pctg_budget_roy_change pctg_budget_roy_change_ifloss pctg_budget_roy_change_ifwin if inmainreg_first==1 & country=="Col"   , statistics(mean p50 sd min max N) columns(statistics) 

qui esttab using "$Tables/sumstats_muniB.tex", cells("mean(fmt(%9.2fc) label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1fc)  label(Max)) count(fmt(%9.0gc)  label(Obs.))") nomtitle nonumber label  replace noobs  booktabs fragment nodep nonum noli nomtitles


*Sum stats socio-eco 
qui eststo clear
qui estpost tabstat pobl_tot areamuni_sqkm titulosarea_km2  if inmainreg_first==1 & country=="Col"   , statistics(mean p50 sd min max N) columns(statistics) 

qui esttab using "$Tables/sumstats_muni.tex", cells("mean(fmt(%9.2fc) label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1fc)  label(Max)) count(fmt(%9.0gc)  label(Obs.))") nomtitle nonumber label  replace noobs  booktabs fragment nodep nonum noli nomtitles

*Add Stats for PERU
qui eststo clear
qui estpost tabstat pobl_tot areamuni_sqkm titulosarea_km2 if inmainreg_first==1 & country=="Peru"  , statistics(mean p50 sd min max N) columns(statistics)

qui esttab using "$Tables/sumstats_muni_peru.tex", cells("mean(fmt(%9.2fc) label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1gc)  label(Max)) count(fmt(%9.0gc)  label(Obs.)) ") nomtitle nonumber label  replace noobs  booktabs fragment


********************
* TABLE 2
*****************

*Confusion Matrix from R


********************
* TABLE 3
*****************

****************************
*DD Col-Peru
*****************************


preserve
mat ddmat=J(6,3,0)

reg propmined_illegal ind_after ind_col after_x_col

matrix A=e(b)
matrix B=e(V)

mat ddmat[5,3] =round(A[1,3],0.01)
mat ddmat[6,3] =round(sqrt(B[3,3]),0.01)

keep if e(sample)==1

sum propmined_illegal if ind_after==0 & ind_col==0 
mat ddmat[1,1] =round(r(mean),0.01)
mat ddmat[2,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_col==0
mat ddmat[3,1] =round(r(mean),0.01)
mat ddmat[4,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==0 & ind_col==1 
mat ddmat[1,2] =round(r(mean),0.01)
mat ddmat[2,2] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_col==1 &  e(sample)==1
mat ddmat[3,2] =round(r(mean),0.01)
mat ddmat[4,2] = round(r(sd),0.01)

reg propmined_illegal ind_after if ind_col==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,1] =round(A[1,1],0.01)
mat ddmat[6,1] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_after if ind_col==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,2] =round(A[1,1],0.01)
mat ddmat[6,2] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_col if ind_after==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[1,3] =round(A[1,1],0.01)
mat ddmat[2,3] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_col if ind_after==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[3,3] =round(A[1,1],0.01)
mat ddmat[4,3] =round(sqrt(B[1,1]),0.01)

svmat ddmat
gen c=_n if ddmat3!=.
gen odd=mod(c,2)

gen t1=abs((ddmat3[_n]/ddmat3[_n+1])*odd)
gen start_col="***" if t1 >= 2.576 & t1!=. 
replace start_col="**" if t1 < 2.576 & t1 >=2.326 & t1!=.
replace start_col="*" if t1 < 2.326 & t1 >=1.96 & t1!=.
drop t1

gen t_col1=abs((ddmat1[5]/ddmat1[6])*odd) if c==5
gen start_col1="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col1="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col1="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen t_col1=abs((ddmat2[5]/ddmat2[6])*odd) if c==5
gen start_col2="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col2="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col2="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen t_col1=abs((ddmat2[5]/ddmat2[6])*odd) if c==5
gen start_col3="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col3="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col3="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen brace_left="(" if odd!=.
gen brace_right=")" if odd!=.

egen concat_se1=concat(brace_left ddmat1 brace_right) if odd==0 
egen concat_se2=concat(brace_left ddmat2 brace_right) if odd==0 
egen concat_se3=concat(brace_left ddmat3 brace_right) if odd==0 
drop brace_left brace_right

gen ddmat1_odd=ddmat1 if odd==1
egen concat_col1=concat(ddmat1_odd start_col1)
replace concat_col1=concat_se1 if concat_col1=="."

gen ddmat2_odd=ddmat2 if odd==1
egen concat_col2=concat(ddmat2_odd start_col2)
replace concat_col2=concat_se2 if concat_col2=="."

gen ddmat3_odd=ddmat3 if odd==1
egen concat_col3=concat(ddmat3_odd start_col)
replace concat_col3=concat_se3 if concat_col3=="."

keep ind_after propmined_illegal ind_col after_x_col concat_col1 concat_col2 concat_col3

foreach i in 1 2 3 {
foreach j in 1 2 3 4 5 6 {
local col`i'`j'=concat_col`i'[`j'] 
}
}
*




file open DiD using "$Tables/sumstats_ddperu.tex", write replace
file write DiD "\begin{tabular}{lccc}" _n
file write DiD "\midrule" _n
file write DiD " \% of mined area mined illegally          & Peru 			  & Colombia 			 & Difference \\ " _n
file write DiD "\toprule" _n
file write DiD "Before the reform    & `col11'   & `col21'     & `col31'         \\" _n
file write DiD "           & `col12'   & `col22'	 & `col32'        \\" _n
file write DiD "After the reform   & `col13'   & `col23'	 & `col33'         \\" _n
file write DiD "          & `col14'   & `col24'	 & `col34'        \\" _n
file write DiD "Difference & `col15'   & `col25'	 & `col35'         \\" _n
file write DiD "           & `col16'   & `col26'     & `col36'        \\ \hline" _n
file write DiD "\bottomrule" _n
file write DiD "\end{tabular}" _n
file close DiD

restore




********************
* TABLE 5
*****************

*************************************
**Quantity reported production
**********************************


qui eststo clear

** By product
qui eststo m1prod: xi: reghdfe prod_parea_carbon after_x_col     , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)


qui eststo m2prod: xi: reghdfe prod_parea_oro after_x_col    , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui eststo m3prod: xi: reghdfe prod_parea_plata after_x_col    , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui eststo m4prod: xi: reghdfe prod_parea_platino after_x_col   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)




 estout m1prod m2prod m3prod   using "$Tables/prod_norm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) prefoot(\midrule ) stats(N N_Clust_Colombia N_Clust_Peru ymean r2 r2_within, fmt( %9.2gc %9.2gc a2 a2 a2) labels ("N. of obs." "Colombian municipalities" "Peruvian municipalities" "Mean of dep. var." "\$R^2\$" "Within \$R^2\$")) replace


********************
* TABLE A8 A9 FigureA6
*****************

*************************************
*** Heterogeneous effect of main regression


qui eststo m1hetNobs: xi: reghdfe propmined_illegal after_x_col     if after_x_ind_leaf2!=. , absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_ind_leaf2 "After X Col X Weak Institutions"
replace after_x_ind_leaf2=1 if ind_after==1 & after_x_ind_leaf2==1
qui eststo m1hetins: xi: reghdfe propmined_illegal after_x_col   after_x_ind_leaf2  if after_x_ind_leaf2!=. , absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'


label var after_x_ag "After X Col X Armed Groups"
replace after_x_ag=1 if ind_after==1 & ind_armedgroup_b2012==1
qui eststo m1hetag: xi: reghdfe propmined_illegal after_x_col after_x_ag  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'


label var after_x_jds "After X Col X Judiciary Strength"
replace after_x_jds=1 if ind_after==1 & justice_percent_d==1
qui eststo m1judstrg: xi: reghdfe propmined_illegal after_x_col after_x_jds  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_poor "After X Col X Poor"
gen poor=(after_x_poor==1)
bys codmpio: egen poor_max=max(poor)
replace after_x_poor=1 if ind_after==1 & poor_max==1
qui eststo m1hetnbi: xi: reghdfe propmined_illegal after_x_col after_x_poor  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_loser "After X Col X Loser"
replace after_x_loser=1 if ind_after==1 & ind_loser==1

qui eststo m1hetloser: xi: reghdfe propmined_illegal after_x_col after_x_loser  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_pctg_loss "After X Col X \% Budget Loss"
replace after_x_pctg_loss=pctg_loss*ind_after if ind_after==1 & ind_col==1
qui eststo m1hetpbudc: xi: reghdfe propmined_illegal after_x_col after_x_pctg_loss  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_pctgloss_if "After X Col X \% Budget Loss if Loss"
label var after_x_pctgwin_if "After X Col X \% Budget Win if Won"
replace after_x_pctgloss_if=pctg_loss*ind_after*ind_loser if ind_after==1 & ind_loser==1 & ind_col==1
replace after_x_pctgwin_if=-pctg_loss*ind_after*(1-ind_loser) if ind_after==1 & ind_loser==0 & ind_col==1

qui eststo m1hetpbudwl: xi: reghdfe propmined_illegal after_x_col after_x_pctgloss_if after_x_pctgwin_if  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_censed "After X Col X Censed"
replace after_x_censed=1 if ind_after==1 & cm_censed==1
qui eststo m1hetcensed: xi: reghdfe propmined_illegal after_x_col   after_x_censed  if after_x_ind_leaf2!=. , absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_oil "After X Col X Oil"
qui eststo m1hetoil: xi: reghdfe propmined_illegal after_x_col after_x_oil  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_pop "After X Col X High population"
qui eststo m1hetpop: xi: reghdfe propmined_illegal after_x_col after_x_pop  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_area "After X Col X Large area"
qui eststo m1hetarea: xi: reghdfe propmined_illegal after_x_col after_x_area if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

estout m1hetNobs m1hetloser m1hetpbudc m1hetpbudwl m1hetcensed m1hetins  m1hetag    using "$Tables/fminedhet1_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_Clust_Colombia N_Clust_Peru ymean r2 r2_within , fmt( %9.2gc %9.2gc %9.2gc a2 a2) labels ( "N. of obs." "Colombian municipalities" "Peruvian municipalities" "Mean of dep. var." "\$R^2\$" "Within \$R^2\$" )) replace



*****
* Visual diff-diff
*******



preserve
collapse (mean) propmined_illegal, by(ano ind_col)
label var propmined_illegal "% of mined area mined illegaly"
twoway (line propmined_illegal ano if ind_col==1, sort lw(thick)) ///
     (line propmined_illegal ano if ind_col==0, sort lw(thick)), ///
	  xline(2010) legend(label(1 "Colombia") label(2 "Peru")) legend(on)

graph export "$Figures/partrends_colperu_raw.pdf", replace
restore

preserve
fvset base 2010 ano
char ano[omit] 2010
label var ind_col "Colombia"

tab ano, gen(I)
forvalues i = 1(1)11{
gen int_`i' = ind_col * I`i'
}
gen base = 0

global dynamic int_1 int_2 int_3 int_4 int_5 int_6 base int_8 int_9 int_10 int_11

reghdfe propmined_illegal $dynamic   , absorb(codmpio ano) vce(cluster codmpio)
matrix B = e(b)
matrix V = e(V)
mat2txt , matrix(B) saving("$base_out/partrends_colperu_betahat.txt") replace 
mat2txt , matrix(V) saving("$base_out/partrends_colperu_Variancehat.txt") replace 

coefplot, graphregion(color(white)) baselevels omitted  keep($dynamic) xtitle("Years") ytitle("Coefficient") ci vertical yline(0) xline(7) rename( int_1=2004 ///
int_2=2005 ///
int_3=2006 ///
int_4=2007 ///
int_5=2008 ///
int_6=2009 ///
base=2010 ///
int_8=2011 ///
int_9=2012 ///
int_10=2013 ///
int_11=2014)
graph export "$Figures/partrends_colperu.pdf", replace
restore


**Concurrent mining titles

preserve

collapse (mean) titulosConcarea_sqkm, by(ano ind_col)
label var titulosConcarea_sqkm "Area concurrent mining permits (km2)"
twoway (line titulosConcarea_sqkm ano if ind_col==1, sort lw(thick)) ///
     (line titulosConcarea_sqkm ano if ind_col==0, sort lw(thick)), ///
	  xline(2010) legend(label(1 "Colombia") label(2 "Peru")) legend(on)

graph export "$Figures/partrends_colperu_raw_titulosConcarea_sqkm.pdf", replace
restore

preserve
fvset base 2010 ano
char ano[omit] 2010
label var ind_col "Colombia"

tab ano, gen(I)
forvalues i = 1(1)11{
gen int_`i' = ind_col * I`i'
}
gen base = 0

global dynamic int_1 int_2 int_3 int_4 int_5 int_6 base int_8 int_9 int_10 int_11

reghdfe titulosConcarea_sqkm $dynamic   , absorb(codmpio ano) vce(cluster codmpio)

coefplot, graphregion(color(white)) baselevels omitted  keep($dynamic) xtitle("Years") ytitle("Coefficient") ci vertical yline(0) xline(7) rename( int_1=2004 ///
int_2=2005 ///
int_3=2006 ///
int_4=2007 ///
int_5=2008 ///
int_6=2009 ///
base=2010 ///
int_8=2011 ///
int_9=2012 ///
int_10=2013 ///
int_11=2014)




graph export "$Figures/partrends_colperu_titulosConcarea_sqkm.pdf", replace

restore



preserve
replace propmined_illegal_tityear=propmined_illegal if propmined_illegal_tityear==.
tab ano, gen(I)
forvalues i = 1(1)11{
gen int_`i' = ind_col * I`i'
}
gen base = 0

global dynamic int_1 int_2 int_3 int_4 int_5 int_6 base int_8 int_9 int_10 int_11

reghdfe propmined_illegal_tityear $dynamic   , absorb(codmpio ano) vce(cluster codmpio)


coefplot, graphregion(color(white)) baselevels omitted   keep($dynamic) xtitle("Years") ytitle("Coefficient") ci vertical yline(0) xline(7) rename( int_1=2004 ///
int_2=2005 ///
int_3=2006 ///
int_4=2007 ///
int_5=2008 ///
int_6=2009 ///
base=2010 ///
int_8=2011 ///
int_9=2012 ///
int_10=2013 ///
int_11=2014)


graph export "$Figures/partrends_tityear.pdf", replace
restore



**** Census balance

eststo clear
qui xi: my_ptest pctg_loss ind_metalespreciosos_ever ind_Hidrocarburos_ever ///
        ind_armedgroup_b2012 pobl_tot areamuni_sqkm  if ano==2014 & areamuni_sqkm!=., by(cm_censed) 



esttab using "$Tables/censed_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Censused" "Not Censused" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 








********************
* TABLE 4
*****************
replace after_x_ind_leaf2=0 if country=="Peru"
*************************************
**Main results
**********************************


***Column 1
qui eststo m4nwl: xi: reghdfe propmined_illegal after_x_col , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"
matrix tempm=e(b)




local peruddafterresult=tempm[1,1]
qui local peruddafterresult=round(`peruddafterresult'*100)/100

file open newfile using "$Tables/peruddafterresult.tex", write replace
file write newfile "`peruddafterresult'"
file close newfile



distinct codmpio  if country=="Col" & e(sample)==1
qui local mainresult= string(r(ndistinct), "%9.2gc") 

file open newfile using "$Tables/Nmunis.tex", write replace
file write newfile "`mainresult'"
file close newfile

distinct codmpio  if country=="Peru" & e(sample)==1
qui local mainresult=string(r(ndistinct), "%9.2gc") 

file open newfile using "$Tables/Nmunis_peru.tex", write replace
file write newfile "`mainresult'"
file close newfile

***Column 2

qui eststo n3: xi: reghdfe newpropmined_illegal after_x_col if inmainreg==1, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit: xi: reghdfe titulosConcarea_sqkm after_x_col if inmainreg==1, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"




qui estout m4nwl n3 m3tit  using "$Tables/fminedstocknew_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) prefoot(\midrule ) stats(  N N_Clust_Colombia N_Clust_Peru  ymean r2 r2_within , fmt(  %9.2gc %9.2gc %9.2gc a2 a2) labels ( "N. of obs." "Colombian municipalities" "Peruvian municipalities" "Mean of dep. var." "\$R^2\$" "Within \$R^2\$" )) replace

qui estout m4nwl n3 m3tit  using "$Tables/fminedstocknew_reg_lean.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N , fmt(  %9.2gc ) labels ( "N. of obs."  )) replace


*************************************
**Main results - lag treatment
**********************************

gen after_x_col_lagged=(ind_col==1 & ano>=2012)

***Column 1
qui eststo m4nwl_lag : xi: reghdfe propmined_illegal after_x_col_lagged , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

***Column 2

qui eststo n3_lag : xi: reghdfe newpropmined_illegal after_x_col_lagged, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit_lag : xi: reghdfe titulosConcarea_sqkm after_x_col_lagged, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

label var after_x_col_lagged "After x Colombia"

estout m4nwl_lag  n3_lag  m3tit_lag  using "$Tables/fminedstocknew_reg_lagged.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col_lagged  ) stats(  N , fmt(  %9.2gc 2 ) labels ( "N. of obs." )) replace




*************************************
**Main results - without 2011
**********************************


***Column 1
qui eststo m4nwl_n2011 : xi: reghdfe propmined_illegal after_x_col if ano!=2011, absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

***Column 2

qui eststo n3_n2011 : xi: reghdfe newpropmined_illegal after_x_col if ano!=2011, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit_n2011 : xi: reghdfe titulosConcarea_sqkm after_x_col if ano!=2011, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"



estout m4nwl_n2011  n3_n2011  m3tit_n2011  using "$Tables/fminedstocknew_reg_n2011.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N  , fmt(  %9.2gc) labels ( "N. of obs." )) replace


*************************************
**Main results - 2007-2014
**********************************


***Column 1
qui eststo m4nwl_2007_2014 : xi: reghdfe propmined_illegal after_x_col if ano>=2007 & ano<=2014, absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

***Column 2

qui eststo n3_2007_2014 : xi: reghdfe newpropmined_illegal after_x_col  if ano>=2007 & ano<=2014, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit_2007_2014 : xi: reghdfe titulosConcarea_sqkm after_x_col  if ano>=2007 & ano<=2014, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"



estout m4nwl_2007_2014  n3_2007_2014  m3tit_2007_2014  using "$Tables/fminedstocknew_reg_2007_2014.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N  , fmt(  %9.2gc) labels ( "N. of obs." )) replace



*************************************
**Main results - 2007-2010+ 2014
**********************************


***Column 1
qui eststo m4nwl_long : xi: reghdfe propmined_illegal after_x_col if ano<=2010 | ano==2014, absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

***Column 2

qui eststo n3_long : xi: reghdfe newpropmined_illegal after_x_col  if ano<=2010 | ano==2014, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit_long : xi: reghdfe titulosConcarea_sqkm after_x_col  if ano<=2010 | ano==2014, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"



estout m4nwl_long  n3_long  m3tit_long  using "$Tables/fminedstocknew_reg_long.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N  , fmt(  %9.2gc) labels ( "N. of obs." )) replace


*************************************
**Main results - muni trends
**********************************


***Column 1
qui eststo m4nwl_muni : xi: reghdfe propmined_illegal after_x_col , absorb(codmpio#c.ano codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

***Column 2

qui eststo n3_muni : xi: reghdfe newpropmined_illegal after_x_col  , absorb(codmpio#c.ano codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit_muni : xi: reghdfe titulosConcarea_sqkm after_x_col , absorb(codmpio#c.ano codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"



estout m4nwl_muni  n3_muni  m3tit_muni  using "$Tables/fminedstocknew_reg_muni_trends.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N  , fmt(  %9.2gc) labels ( "N. of obs." )) replace


***************************
***************************
***************************
***************************
gen after_x_colFar=after_x_col*(dist_border_km>=1000)
label var after_x_colFar "After x Col x Far"
qui gen w_analizedpctgarea=round( 100*analizedarea_sqkm/ areamuni_sqkm )

qui eststo m12: xi: reghdfe c12_propmined_illegal after_x_col , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "TimeFE"

qui eststo m4_1000: xi: reghdfe propmined_illegal after_x_col after_x_colFar   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo madj: xi: reghdfe propadjmined_illegal after_x_col    , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo mw: xi: reghdfe propmined_illegal after_x_col   [aw=w_analizedpctgarea], absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

**** Probability mined
qui eststo m1_prob: xi: reghdfe propmined_illegal_prob after_x_col  , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
qui sum propmined_illegal_prob if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'


*National Parks

qui eststo m1SP: xi: reghdfe propminedSP_illegal after_x_col  , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
qui sum propminedSP_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'




qui estout m4nwl m1SP m1_prob m4_1000   m12 madj mw    using "$Tables/robust_reg_unificada.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_colFar ) prefoot(\midrule ) stats(   N N_clust ymean r2 , fmt(%9.2gc %9.2gc a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace





*Homicides armed groups
eststo clear
qui eststo m4: xi: reghdfe tasa_homag after_x_pctg_loss price_index_u   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"


qui eststo m5: xi: reghdfe tasa_homag after_x_pctg_loss price_index_u  if ind_armedgroup_b2012==0 , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui eststo m6: xi: reghdfe tasa_homag after_x_pctg_loss price_index_u  if ind_armedgroup_b2012==1 , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui estout m4 m5 m6 using "$Tables/tasa_homag_creg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_pctg_loss price_index_u ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt(%fmt %9.2gc %9.2gc a2 a2 ) labels ("Time FE" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


****************************
******* Updated above here
*******************************


qui sum lumpy_share if ano==2014, d
qui local temp=round(r(mean))
file open newfile using "$Tables/lumpy_mean.tex", write replace
file write newfile "`temp'"
file close newfile

qui local temp=round(r(p25))
file open newfile using "$Tables/lumpy_p25.tex", write replace
file write newfile "`temp'"
file close newfile

qui local temp=round(r(p50))
file open newfile using "$Tables/lumpy_p50.tex", write replace
file write newfile "`temp'"
file close newfile

qui local temp=round(r(p75))
file open newfile using "$Tables/lumpy_p75.tex", write replace
file write newfile "`temp'"
file close newfile




