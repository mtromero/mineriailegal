
qui foreach x in A B C {
	import delimited "$base_out/output`x'_causfor.csv", clear
	drop v1
	rename leaf causfor_leafval
	gen causfor_sample="`x'"
	saveold "$base_out/Temporary/output2`x'_causfor.dta", replace
}

qui foreach x in A B  {
	append using "$base_out/Temporary/output2`x'_causfor.dta"
}

merge 1:m codmpio using "$base_out/Temporary/panel_quasifinal.dta"

drop _merge

replace ind_after=1 if ano>=2011

qui tab causfor_leafval, gen(ind_leaf)
qui foreach x of varlist ind_leaf*{
	gen after_x_`x'=ind_after*`x'
	gen loser_x_`x'=ind_loser*`x'
	gen after_x_loser_x_`x'=ind_loser*ind_after*`x'
}
label var after_x_ind_leaf2 "After X Group"
label var after_x_loser_x_ind_leaf2 "After X Loser X Group"

foreach x in carbon oro plata platino {
	qui gen prod_parea_`x'=(prod_`x'/(areaprmined_sqkm-areaprilegal_sqkm))/10^6
}

gen country="Col" 
append using "$base_out/Temporary/panel_prillegal_Peru.dta"



** Indicator for municipalities in all years
qui bysort codmpio: egen maxanal=max(analizedarea_sqkm)
qui gen fr_maxanal=analizedarea_sqkm/maxanal

gen ind_visibleyear=(fr_maxanal>0.1) if fr_maxanal!=.
qui bysort codmpio: egen sumano=sum(ind_visibleyear)
qui gen ind_muni_allyears=0
qui replace ind_muni_allyears=1 if sumano==11
qui drop sumano

qui gen trend_col=0
replace trend_col=trend if country=="Col"
qui gen trend_peru=0
replace trend_peru=trend if country=="Peru"

qui gen ind_col=0
qui replace ind_col=1 if country=="Col"

gen after_x_col=0
replace after_x_col=1 if ind_after==1 & country=="Col"
label var after_x_col "After x Colombia"

gen after_x_peru=0
replace after_x_peru=1 if ind_after==1 & country=="Peru"
label var after_x_peru "After x Peru"

replace after_x_pctg_loss=0 if after_x_pctg_loss==.

label var after_x_ind_leaf2 "After X Weak Institutions"
 qui sum nbi_2011 if ano==2014, d
qui gen after_x_poor=ind_after*(nbi_2011>r(p50))
label var after_x_poor "After X Poor"

replace ORO_GRAMOS=0 if ORO_GRAMOS==.
replace Nminers=0 if Nminers==.
qui gen after_x_gold=(ORO_GRAMOS>0)*ind_after
label var after_x_gold "After X Produced Gold"

qui gen after_x_censed=after_x_col*cm_censed
replace after_x_censed=0 if after_x_censed==.
label var after_x_censed "After X Censed"
qui gen after_x_oil=ind_after*ind_Hidrocarburos_ever
label var after_x_oil "After X Oil-Gas"
qui gen after_x_coal=ind_after*ind_carbon_ever
label var after_x_coal "After X Coal"
qui sum pobl_tot if ano==2014, d
qui gen after_x_pop=ind_after*(pobl_tot >r(p50))
label var after_x_pop "After X High population"
qui sum areamuni_sqkm if ano==2014, d
qui gen after_x_area=ind_after*(areamuni_sqkm>r(p50))
label var after_x_area "After X Large area"
gen titulosarea_ha=titulosarea_sqkm*100
label var titulosarea_ha "Area legal titles in hectares"
gen titulosConcarea_ha=titulosConcarea_sqkm*100
replace titulosConcarea_ha=titulosarea_ha if country=="Peru"
label var titulosConcarea_ha "Area legal titles in hectares"
gen mined_lglzation=100*legalm_pastilm/(legalm_pastilm+(illegalm_pastm-illegalm_pastlm))
label var mined_lglzation "Share of area illegally mined that legalized"
gen log_illegal= log(areaprilegal_sqkm+1)

foreach x of varlist propmined_illegal  titulosarea_ha newpropmined_illegal mined_lglzation c12_propmined_illegal propadjmined_illegal prop_illegal {
	qui xi: areg `x' trend if ano<2011 & country=="Col", absorb(codmpio) vce(cluster codmpio)
	qui gen `x'_dt=`x'-_b[trend]*trend if country=="Col"

	qui xi: areg `x' trend if ano<2011 & country=="Peru", absorb(codmpio) vce(cluster codmpio)
	qui replace `x'_dt=`x'-_b[trend]*trend if country=="Peru"
}

*** I do this because a lot of the adjusted predictions are zero all year so dropped from the regression without trend
** If I dont do this I am basically adding a lot of munis that fit exactly hte trend
qui eststo m1: xi: areg propadjmined_illegal after_x_col after_x_peru trend  , absorb(codmpio) vce(cluster codmpio)
replace propadjmined_illegal_dt=. if !e(sample) 

foreach x of varlist propmined_illegal_prob propminedSP_illegal propmined_illegal_tityear {
	qui xi: areg `x' trend if ano<2011 & country=="Col", absorb(codmpio) vce(cluster codmpio)
	qui gen `x'_dt=`x'-_b[trend]*trend if country=="Col"
}

qui replace propmined_illegal_dtst=propmined_illegal_dt if country=="Peru"

qui replace cm_pctgmines_illegal=cm_pctgmines_illegal*100
qui replace cm_pctgmines_cielo_abierto= cm_pctgmines_cielo_abierto*100
label var cm_censed "On 2010 Mining Census"
replace cm_pctgarea_illegal=cm_pctgarea_illegal*100
gen titulosarea_km2=titulosarea_ha/100
label var titulosarea_km2 "Area mining titles (km2)"




replace titulosarea_ha=900*titulosarea_ha /*We forgot to multiply by 900m2, which is the size of a pixel*/
replace titulosarea_sqkm=titulosarea_ha*0.01 /*We forgot to multiply by 900m2, which is the size of a pixel*/
replace titulosarea_km2=titulosarea_ha*0.01 /*We forgot to multiply by 900m2, which is the size of a pixel*/

replace titulosConcarea_ha=900*titulosConcarea_ha /*We forgot to multiply by 900m2, which is the size of a pixel*/
replace titulosConcarea_sqkm=titulosConcarea_ha*0.01 /*We forgot to multiply by 900m2, which is the size of a pixel*/



saveold "$base_out/panel_final.dta", replace

use "$base_in/Judicial_FP.dta", clear
collapse (mean)  justice_percent , by(M_code)
rename M_code codmpio
qui sum justice_percent , d
qui gen justice_percent_d=(justice_percent>r(p50))
merge 1:m codmpio using "$base_out/panel_final.dta"
drop if _merge==1
drop _merge



qui gen after_x_jds=ind_after*justice_percent_d
qui label var after_x_jds "After x Judiciary Strength"

qui gen after_x_petotalins=ind_after*petotalins_pc_2002
qui label var after_x_petotalins "After x Institutions"

label var areamuni_sqkm "Area (km2)"

egen ind_mineros=rowtotal(ind_azufre_ever-  ind_piedraspreciosos_ever ind_Caliza_ever- ind_Yeso_ever), missing
replace ind_mineros=(ind_mineros>0) if !missing(ind_mineros)
label var ind_mineros "Mining production"
label var ind_Hidrocarburos_ever "Oil production"
label var pctg_budget_roy_change_ifloss "\% budget loss if loss"
label var pctg_budget_roy_change_ifwin "\% budget win if won"
label var pctg_budget_roy_change "\% budget change"


qui xi: areg propmined_illegal after_x_col  i.ano  , absorb(codmpio) vce(cluster codmpio)
qui gen inmainreg=e(sample)
label var inmainreg "Observation in main regression"
bys codmpio: egen minmainreg=min(ano) if inmainreg==1
qui gen inmainreg_first=(minmainreg==ano) if inmainreg==1
label var inmainreg_first "First observation of muni in main regression"
drop minmainreg

replace cm_pctgarea_illegal=100 if cm_pctgarea_illegal>=100 & !missing(cm_pctgarea_illegal)
foreach y in ag jds poor loser oil pop area ind_leaf2 pctgloss_if pctgwin_if {
	qui replace after_x_`y'=0 if country=="Peru"
}

*Nice Table 1
label var cm_pctgarea_illegal_CA "\% illegal area | open pit (Census)"
label var unodc_propmined_illegal "\% illegal area (UNODC)"
gen unodc_propmined_illegal_censed=unodc_propmined_illegal if cm_censed==1
label var unodc_propmined_illegal_censed "\% illegal area (UNODC) | Censed"
replace cm_pctgarea_illegal_CA=cm_pctgarea_illegal_CA*100
replace unodc_propmined_illegal=unodc_propmined_illegal*100	
replace unodc_propmined_illegal_censed=unodc_propmined_illegal_censed*100

foreach var of varlist  cm_pctgarea_illegal  cm_pctgmines_cielo_abierto cm_pctgarea_illegal_CA unodc_propmined_illegal unodc_propmined_illegal_censed {
	replace `var'=. if   cm_area_sqkm==0 & country=="Col"
}

save "$base_out/panel_final.dta",replace

use "$base_out/panel_final.dta",clear
drop if ano==.
xtset codmpio ano
tsfill , full
*drop if ano==2004
by codmpio: ipolate propmined_illegal ano, epolate gen( propmined_illegal_ip)
keep propmined_illegal propmined_illegal_ip ano codmpio after_x_col country ind_after ind_col
drop if propmined_illegal_ip==.
bys codmpio: egen trick=sum(ano)
drop if trick<2015
keep propmined_illegal_ip codmpio ano
reshape wide propmined_illegal_ip, i(codmpio) j(ano)

gsort -codmpio
preserve
drop codmpio
export delimited using "$base_out/Temporary/data_for_synthdid.csv", novarnames replace
restore

gen trickid=_n
keep trickid codmpio
save "$base_out/Temporary/trickid_codmpio.dta",replace

import delimited "$base_out/Temporary/weights_synthdid.csv", clear 
rename v1 trickid
rename x synthdd_weight
merge 1:1 trickid using "$base_out/Temporary/trickid_codmpio.dta"
drop _merge
merge 1:m codmpio using "$base_out/panel_final.dta"

foreach x in carbon oro plata platino  { 
	gen t_prod_parea=(prod_parea_`x'!=.)
	bys codmpio: egen ind_P`x'_ever=max(t_prod_parea)
	drop t_prod_parea
}

keep if (ind_Poro_ever+ind_Pcarbon_ever+ind_Pplata_ever+ind_Pplatino_ever>0)
drop if ano==.
xtset codmpio ano
tsfill , full


sort codmpio ano
by codmpio: ipolate propmined_illegal ano, epolate gen( propmined_illegal_ip)
keep propmined_illegal propmined_illegal_ip ano codmpio after_x_col country ind_after ind_col
drop if propmined_illegal_ip==.
bys codmpio: egen trick=sum(ano)
drop if trick<2015
keep propmined_illegal_ip codmpio ano
reshape wide propmined_illegal_ip, i(codmpio) j(ano)

gsort -codmpio
preserve
drop codmpio
export delimited using "$base_out/Temporary/data_for_synthdidMet.csv", novarnames replace
restore
