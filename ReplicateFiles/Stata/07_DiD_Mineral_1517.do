

use "$base_out/Temporary/panel_prillegalMi.dta", clear
append using "$base_out/Temporary/panel_prillegalMi1516t14.dta"

gen ind_gold=(mineral==1)
replace ind_after=1 if ano>=2011
replace afterxgold=ind_gold*ind_after
replace afterxregalia=regalia_mine*ind_after

sort mineral codmpio ano
gen propmined_illegal_miL=propmined_illegal_mi[_n-1] if mineral==mineral[_n-1] & codmpio==codmpio[_n-1] & ano-1==ano[_n-1]
gen areaminedsqkm_miL=areaminedsqkm_mi[_n-1] if mineral==mineral[_n-1] & codmpio==codmpio[_n-1] & ano-1==ano[_n-1]


preserve
tab ano, gen(I)
forvalues i = 1(1)13{
	gen int_`i' = regalia_mine * I`i'
}
gen base = 0

global dynamic int_1 int_2 int_3 int_4 int_5 int_6 base int_8 int_9 int_10 int_11 int_12 int_13

reghdfe propmined_illegal_mi $dynamic if mineral==1 | mineral==4 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
matrix B = e(b)
matrix V = e(V)
*mat2txt , matrix(B) saving("$base_out/partrends_munimi_alpha17_betahat.txt") replace 
*mat2txt , matrix(V) saving("$base_out/partrends_munimi_alpha17_Variancehat.txt") replace 

coefplot, graphregion(color(white)) baselevels omitted keep($dynamic) xtitle("Years") ytitle("Coefficient") ci vertical yline(0) xline(7)  rename( ///
int_1=2004 ///
int_2=2005 ///
int_3=2006 ///
int_4=2007 ///
int_5=2008 ///
int_6=2009 ///
base=2010 ///
int_8=2011 ///
int_9=2012 ///
int_10=2013 ///
int_11=2014 ///
int_12=2015 ///
int_13=2016)
graph export "$Figures/partrends_munimi_alpha_1517.pdf", replace
restore


preserve
collapse (mean) propmined_illegal_mi if mineral==1 | mineral==4 , by(ano mineral)
label var propmined_illegal_mi "% of mined area mined illegaly"
label var ano "Year"
twoway (line propmined_illegal_mi ano if mineral==1, sort lw(thick)) ///
     (line propmined_illegal_mi ano if mineral==4, sort lw(thick)), ///
	  xline(2010) legend(label(1 "Gold") label(2 "Coal")) legend(on) xlabel(2004(2)2016) 
graph export "$Figures/partrends_munimi_alpha_1517_raw.pdf", replace
restore

