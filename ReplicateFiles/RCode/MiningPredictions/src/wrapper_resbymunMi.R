wrapper_resbymunMi<-function(year,mainPath,usar2014=0){
gitpath <- paste0(mainPath,'/ecosystems/src/')
source(paste0(gitpath,"mosaicPolygon.R"))
source(paste0(gitpath,"allResultsByMunicipioMi.R"))
source(paste0(gitpath,"municipioResultsMi.R"))
source(paste0(gitpath,"rowsToDataFrame.R"))

#pathPredictionsPost<-file.path(mainPath,'predictionsPost')
pathPredictionsPost<-file.path(mainPath,'sandwichRemoved')
suffix<-'Rf10onlymlOSMecopot'
numCores <- 16


file.create(file.path(mainPath, paste0('log_municipiosMi',year,'.txt')))
sink()
sink(file.path(mainPath, paste0('log_municipiosMi',year,'.txt')))

#titmask <-'titulosVIG'
titmask <-'titulosMasks'
usar2014=1

allResultsByMunicipioMi(   year=year, 
	                  suffix=suffix,
	                  mainPath=mainPath,
                          pathPredictionsPost=pathPredictionsPost,
	                  numCores=numCores,
                    usar2014=usar2014,titmask=titmask)

}