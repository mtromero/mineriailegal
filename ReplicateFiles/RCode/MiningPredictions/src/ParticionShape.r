# 
# library(rgdal)
# setwd("/media/mauricio/TeraHDD1/Copy/CloudsMauricio/")
# shapefile<-(readOGR(dsn="COL_adm",layer="COL_adm0"))
# shapefile=spTransform(shapefile,CRS("+proj=utm +zone=19 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"))
# 

##This function takes the shapefile, it splits it into boxes of side "box_size" km, then each box is spllitted into 4 boxes (i.e., double the resolution)
##3 out of this box are not selected, and 1 box is
## the projection needs to be in meters!!
##it returns a spatial polygon data frame, where "selected" is the important column
##created by : Mauricio Romero, 30/03/2016
ParticionShape=function(shapefile,box_size=100){
if(!grep("+units=m",CRS(proj4string(shapefile))@projargs)) stop("projection needs to be in meters")

require(sp)
require(Grid2Polygons)
require(rgeos)
require(raster)
require(data.table)


bb <- bbox(shapefile)
cs <- c(1, 1)*box_size*1000  # cell size 6km x 6km (for illustration)
                                # 1 ft = 3.28084 m
cc <- bb[, 1] + (cs/2)  # cell offset
cd <- ceiling(diff(t(bb))/cs)  # number of cells per direction
grd <- GridTopology(cellcentre.offset=cc, cellsize=cs, cells.dim=cd)



                               
sp_grd=as.SpatialPolygons.GridTopology(grd, proj4string = CRS(proj4string(shapefile)))

sp_grd <- SpatialPolygonsDataFrame(sp_grd,
                                data=data.frame(TopId=IDvaluesGridTopology(grd)),F)
                                


S=2 #scale to which it is to be converted

bb<-sp_grd@bbox #bounding box where "c" is the old grid

cs<-c(1, 1)*box_size*1000/S    # psdeg is the pixel size of old grid
cc <- bb[, 1] + (cs/2) # new cell centre
cd <- ceiling(diff(t(bb))/cs) # new cell dimensions

grd_sub <- GridTopology(cellcentre.offset=cc,cellsize=cs,cells.dim=cd) # grid formation
sp_grd_sub=as.SpatialPolygons.GridTopology(grd_sub, proj4string = CRS(proj4string(shapefile)))
Match=over(gCentroid(sp_grd_sub,byid=T),sp_grd)

sp_grd_sub <- SpatialPolygonsDataFrame(sp_grd_sub,
                                data=data.frame(TopIdNew=IDvaluesGridTopology(grd_sub),TopId=Match),F)


A=over(sp_grd_sub,shapefile)
sp_grd_sub=sp_grd_sub[!is.na(A$GADMID),]

data=sp_grd_sub@data
data=setDT(data)
data[,Rand:=sample(4,length(TopIdNew)),by="TopId"]
data=data[order(TopId,TopIdNew,Rand)]

data[,selected:=0]
data[Rand==4,selected:=1]

sp_grd_sub@data=data


plot(sp_grd_sub,lwd=1,col=sp_grd_sub$selected+2)
plot(shapefile,lwd=2,add=T)

return(sp_grd_sub)

}






