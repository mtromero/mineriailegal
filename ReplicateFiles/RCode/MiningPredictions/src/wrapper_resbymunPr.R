wrapper_resbymunPr<-function(year,mainPath,usar2014=0){
ps=2
  #ps=0 only predictions, for example if we are going to smooth afterwards
  #ps=1 do post prediction and result by mun
  #ps=10 do predictions and post prediction and result by mun
  #ps=5 result by mun after sandwich removed
  #ps=2 instead of using mined dummies add probability that certain pixel is mined


modelName<-'Rf_10_only_mlOSM_nostrata_ecos_pot_100.Rda'
modelnick<-'Rf10onlymlOSMecopot'
opt_thresh<-0.7778

numCores <- c(7,12)


modelPath <- file.path(mainPath, 'models')
modelPathName<- file.path(modelPath, modelName)

file.create(file.path(mainPath, paste0('log_municipiosPr',year,'.txt')))
sink()
sink(file.path(mainPath, paste0('log_municipiosPr',year,'.txt')))

varnames <- c('band1', 'band2', 'band3', 'band4', 'band5', 'band6','lossyear')
inputFolders <- data.frame(folder=c(NA,NA), suffix=c(NA,NA))
inputFolders$folder <- c(file.path(mainPath, 'noTree', year),
	                    file.path(mainPath, 'lossyear', year)
                          )

inputFolders$suffix <- c('.tif','.tif' )

print('number of cores to be used')
print(numCores)
titmask <-'titulosMasks'

allResultsByMunicipioPr(opt_thresh=opt_thresh,ps=ps,modelPathName = modelPathName,
	                  year=year, 
	                  inputFolders=inputFolders,
	                  suffix=modelnick,
	                  mainPath=mainPath,
	                  varnames=varnames,
	                  numCores=numCores,
                    usar2014=usar2014,titmask=titmask)

}