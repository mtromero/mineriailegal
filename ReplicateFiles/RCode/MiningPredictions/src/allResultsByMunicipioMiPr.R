allResultsByMunicipioMiPr<-function( year,suffix, mainPath,pathPredictionsPost, numCores=c( 2, 2, 2, 2),usar2014=0,titmask='titulosMasks'){
	library(rgdal)
        library(parallel)


###anado sufijo dependiendo de que estamos haciendo
if(usar2014==0){
titsuffix=""
}
if(usar2014==1){
titsuffix="tit2014"
}

   
    
    pathCopy<-file.path(mainPath, 'copy')
    pathTitulos<-file.path(mainPath, titmask)
   
    

    CRS <- CRS('+proj=utm +zone=18 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0')

	#get results by municipio

    municipios<-readOGR(dsn=pathCopy, layer='Municipios')
   
     
     municipios<-municipios[!is.na(municipios@data$CODANE2),]
    municipios@data$CODANE2<-as.character(municipios@data$CODANE2)
    municipios <- spTransform(municipios, CRS)

    pathMineral<-file.path(mainPath, 'minerpots')

    outfile <- file.path(mainPath, paste0('results_municipiosMiPr',year,'_log.txt'))

    cl<-makeCluster(numCores, outfile=outfile)
    
    clusterSetRNGStream(cl,123)
    clusterExport(cl, c('municipios',
                        'year',
                        'pathPredictionsPost',
                        'suffix',
                        'pathTitulos',
                        'mainPath','pathMineral'), envir=environment())

       if (!dir.exists(file.path(mainPath, 'municipiosMiPr'))) {
        dir.create(file.path(mainPath, 'municipiosMiPr'))
  }


      if (!dir.exists(file.path(mainPath, 'municipiosMiPr', paste0(year, suffix,titsuffix)))) {
        dir.create(file.path(mainPath, 'municipiosMiPr', paste0(year, suffix,titsuffix)))
  }



    results <- parLapply(cl, (1:nrow(municipios)), function(x){
        print(x)
        if (!file.exists(file.path(mainPath, 'municipiosMiPr', paste0(year, suffix,titsuffix), paste0(x, '.Rda')))){
        library(raster)
    
     source(file.path(mainPath,'ecosystems', 'src', 'municipioResultsMiPr.R'))
        source(file.path(mainPath,'ecosystems', 'src', 'mosaicPolygon.R'))
        
        
        ##If usar2014==0, then use same year as the actual year of predictions
        if(usar2014==0){
          myRes <- tryCatch({res <- municipioResultsMiPr(municipio = municipios[x,],
                           year = year,
                           pathPredictions = file.path(pathPredictionsPost, paste0(year, suffix)),
                           pathTitulos = file.path(pathTitulos, year),pathMineral)
  
          saveRDS(res, file.path(mainPath, 'municipiosMiPr', paste0(year, suffix,titsuffix), paste0(x, '.Rda')))
  
          return(res)
          }, error = function(e){
              print(paste0('error at municipio ', x))
             res <- c(NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA)
  
             saveRDS(res, file.path(mainPath, 'municipiosMiPr', paste0(year, suffix,titsuffix), paste0(x, '.Rda')))
  
             return(res)
          })
        }  #cerrar if de usar ano adecuado
        
        if(usar2014==1){
          myRes <- tryCatch({res <- municipioResultsMiPr(municipio = municipios[x,],
                           year = year,
                           pathPredictions = file.path(pathPredictionsPost, paste0(year, suffix)),
                           pathTitulos = file.path(pathTitulos, 2014), pathMineral)
  
          saveRDS(res, file.path(mainPath, 'municipiosMiPr', paste0(year, suffix,titsuffix), paste0(x, '.Rda')))
  
          return(res)
          }, error = function(e){
              print(paste0('error at municipio ', x))
             res <- c(NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA)
  
            saveRDS(res, file.path(mainPath, 'municipiosMiPr', paste0(year, suffix,titsuffix), paste0(x, '.Rda')))
  
             return(res)
          })
        }#cerrar if de usar 2014
       
         } else{
        myRes<-readRDS(file.path(mainPath, 'municipiosMiPr', paste0(year, suffix,titsuffix), paste0(x, '.Rda')))

       }
       return(myRes)
        }) #Finish result by muni



    stopCluster(cl)
    
    dataframe <- rowsToDataFrame(results, names=c('codane',
                                                  'year',
                                                  'analizedArea',
                                                  'municipioArea',
                                                  'sumPrmines',
                                                  'sumPrareaMines',
                                                  'titulosArea',
                                                   'sumPrillegalMines',
                                                  'sumPrareaIllegalMines',
                                                   'sumPrareaGoldMines',
                                                   'sumPrareaIllegalGoldMines',
                                                   'sumPrareaPlatinumMines',
                                                   'sumPrareaIllegalPlatinumMines',
                                                   'sumPrareaCoalMines',
                                                   'sumPrareaIllegalCoalMines'))

   write.csv(dataframe, file.path(mainPath, paste0('muniMiPrResults_',titsuffix, suffix, '_', year, '.csv'))) 
	print('finished results by municipio')

}
 

