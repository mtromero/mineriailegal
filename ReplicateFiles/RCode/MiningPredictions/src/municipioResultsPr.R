municipioResultsPr <- function(municipio, year, pathPredictions, pathTitulos){
	print(paste0('Results for ', municipio@data$MUNICIPIO))
	codane <- municipio@data$CODANE2
    municipioArea<-sapply(slot(municipio, "polygons"), slot, "area")

    
    rasterPredicted <- mosaicPolygon(municipio, pathPredictions)
   
    if (is.null(rasterPredicted)){
      
    	return(c(codane, year, 0, municipioArea, NA, NA, NA, NA, NA))
    }
    
    rasterTitulos <- mosaicPolygon(municipio, pathTitulos)

    analizedArea <- sum(!is.na(rasterPredicted[]))*900
    
    # 
    sumPrmines <- sum(rasterPredicted[], na.rm=TRUE)
    sumPrareaMines <- 900 * sumPrmines
    titulosArea <- sum(rasterTitulos[]==1, na.rm=TRUE)
    
    
    sumPrillegalMines <- sum((rasterTitulos[]==0)*rasterPredicted[], na.rm=TRUE)
    sumPrareaIllegalMines <- sumPrillegalMines * 900
    


    return(c(codane,
             year,
             analizedArea,
             municipioArea,
             sumPrmines,
             sumPrareaMines,
             titulosArea,
             sumPrillegalMines,
             sumPrareaIllegalMines))
}


