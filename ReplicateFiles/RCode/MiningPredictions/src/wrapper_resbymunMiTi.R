wrapper_resbymunMiTi<-function(year,mainPath){
gitpath <- paste0(mainPath,'/ecosystems/src/')
source(paste0(gitpath,"mosaicPolygon.R"))
source(paste0(gitpath,"allResultsByMunicipioMiTi.R"))
source(paste0(gitpath,"municipioResultsMiTi.R"))
source(paste0(gitpath,"rowsToDataFrame.R"))

pathMiTi<-file.path(mainPath,'titulosMi',year)

numCores <- 16


file.create(file.path(mainPath, paste0('log_municipiosMiTi',year,'.txt')))
sink()
sink(file.path(mainPath, paste0('log_municipiosMiTi',year,'.txt')))




allResultsByMunicipioMiTi(   year=year, 
	                   mainPath=mainPath,
                          pathMiTi=pathMiTi,
	                  numCores=numCores)

}