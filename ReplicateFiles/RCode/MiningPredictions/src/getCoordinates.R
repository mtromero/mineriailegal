#' @export
#' @title Get coordinates
#'
#' @description Changes a set of coordinates from longlat to other coordinate System
#'
#' @param coords data frame with longitude and latitude values. Each row is a point
#' @param cSystem object of class CRS corresponding to the new coordinate system
#'
#' @return data.frame with the coordinates in the new system   
#'
#' @examples \dontrun{getCoordinates(c(-70,5), CRS("+proj=utm18"))}
getCoordinates<-function(coords, cSystem, spatial=FALSE){
	library(sp)
	points<-SpatialPoints(coords, proj4string=CRS("+proj=longlat +datum=WGS84"))
	points<-spTransform(points, CRS=cSystem)
	if (spatial) return(points)
	points@coords
}