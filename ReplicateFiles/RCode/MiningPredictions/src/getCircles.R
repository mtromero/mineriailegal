#' get circles
#'
#' creates circles as SpatialPolygons around some coordinates in lonlat with a certain radius
#'
#' @param coords matrix with coordinates in lonlat, each row is a point
#' @param pad vector with the radii for each circle
#' @CRS object of class CRS with the corresponding coordinate system
#'
#' @return list with objects of class SpatialPolygons
getCircles<-function(coords, pad, CRS, ids){
  library(raster)
  library(rgeos)
  
  print(paste0("getting circles"))
  coords<-as.matrix(coords)
  nCoords<-getCoordinates(coords, CRS, spatial=TRUE)
  
  circles<-lapply(1:nrow(nCoords@coords), FUN=function(i){
	  c<-nCoords[i]
	  r<-pad[i]
    id <- ids[i]
	  gBuffer(c, width=r, id=id)
  })
  circles
}
	
