#' @export
#' @title Download GFC images
#'
#' @description Downloads the GFC images that cover Colombia
#'
#' @param dirImages path to the directory where the images will be downloaded. Needs to be created first.
#'
#' @return -
#'
#' @examples \dontrun{downloadTreeImages("tree")}
downloadTreeImages<-function(dirImages){
	
	library(stringr)
	
	# Makes the grid of strings to download the images
	lats<-20-(0:2)*10
	lats[lats>0]<-paste0(lats[lats>0], "N")
	lats[lats==0]<-"00N"
	lats[lats<0]<-paste0(lats[lats<0], "S")
	
	longs<-(-90)+(0:2)*10
	longs[longs>0]<-paste0(longs[longs>0], "E")
	longs[longs=0]<-"00E"
	longs[longs<0]<-paste0(longs[longs<0], "W")
	longs<-gsub(pattern="-", replacement="", longs)
	
	longs<-str_pad(longs, 4, pad="0")
	
	tiles<-as.vector(sapply(lats, function(x) paste0(x, "_", longs)))
	
	types<-c("loss", "gain", "lossyear", "datamask", "treecover2000")
	
	# downloads the images
	if(.Platform$OS.type=="unix"){
		method<-"curl"
		print(paste0("current method: ", method))
		for (x in tiles) {
			for (y in types){
				print(paste0("looking for file ", dirImages,"/Hansen_GFC2015_" , y, "_", x, ".tif"))
					if(!file.exists(paste0(dirImages,"/Hansen_GFC2015_" , y, "_", x, ".tif"))){		
					print(paste0("downloading to ", paste0(dirImages,"/Hansen_GFC2015_" , y, "_", x, ".tif")))	
					download.file(paste0("https://storage.googleapis.com/earthenginepartners-hansen/GFC2015/Hansen_GFC2015_", y, "_", x, ".tif"), 
									  	  paste0(dirImages,"/Hansen_GFC2015_" , y, "_", x, ".tif"), method=method)
					}
					else print(paste0("file ", dirImages,"/Hansen_GFC2015_" , y, "_", x, ".tif", " already downloaded")) 	
			}
		}
		
	} 
    else if (.Platform$OS.type=="windows"){
    	method<-"wininet"
		print(paste0("current method: ", method))
		for (x in tiles) {
			for (y in types){
				    print(paste0("looking for file ", dirImages,"/Hansen_GFC2015_" , y, "_", x, ".tif"))
					if(!file.exists(paste0(dirImages,"/Hansen_GFC2015_" , y, "_", x, ".tif"))){		
					print(paste0("downloading to ", paste0(dirImages,"/Hansen_GFC2015_" , y, "_", x, "W.tif")))	
					download.file(paste0("https://storage.googleapis.com/earthenginepartners-hansen/GFC2015/Hansen_GFC2015_", y, "_", x, ".tif"), 
									  	  paste0(dirImages,"/Hansen_GFC2015_" , y, "_", x, ".tif"), method=method, mode = "wb")
					}
					else print(paste0("file ", dirImages,"/Hansen_GFC2015_" , y, "_", x, ".tif", " already downloaded")) 	
			}
		}
    }		
}

