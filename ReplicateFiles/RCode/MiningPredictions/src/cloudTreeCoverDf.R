#' @export
#' @title Make Cloud Tree Cover Data Frame
#' 
#' @description For a set of pathrows and a list of years, finds the cloud percentage remaining in each pathrow, 
#'              and the percentage of tree pixels removed from the original image.
#'
#' 
#' @param pathImages path to the folder /final where the images processed by teamlucc are found
#' @param pathNoTree path to the folder /noTree where the images processed by 
#'                   batchRemoveTree function are found.
#' @param year integer or character vector with the years for which the percentages will be calculated.
#' @param pathRows character vector with the pathrows for which the percentages will be calculated.
#'                 format="ppprrr". If some images can't be found, the percentage will be NA.
#' @param numCores number of cores to run in parallel.
#'
#' @return data.frame
#'
#' @examples \dontrun{cloudTreeCoverDf("final", "noTree/2010", [2010:2011], "009057", numCores=2)}
cloudTreeCoverDf<-function(pathImages, pathNoTree, year, pathRows, numCores=NULL){
    library(raster)
    library(plyr)
  
    library(parallel)
    availableCores<-detectCores()-1
    if(!is.null(numCores)) availableCores<-min(availableCores, numCores)
    outfile<-paste0(pathImages,"/cloudTreeCoverDf_log.txt" )
    cl<-makeCluster(availableCores, outfile=outfile)
	clusterSetRNGStream(cl,123)
    clusterEvalQ(cl, suppressMessages(library(raster)))
    e <- environment()
    clusterExport(cl, c('year', 'pathImages', 'pathNoTree'),envir = e)

    listByPr<-parLapply(cl, pathRows, function(pr){
		prData<-rep(NA, {length(year)*2+1})
		prData[1]<-pr
		for (i in 1:length(year)){
		    y<-year[i]
		
		    if (file.exists(paste0(pathImages, "/",y,"/", pr, "_masks.tif"))){
		        img<-brick(paste0(pathImages, "/",y,"/", pr, "_masks.tif"))
		        img <- img[[2]]
		        # calculate cloud cover
		        cloudPixels <- sum(img[]==2 | img[]==4, na.rm=TRUE)
		        print(paste0("cloud pixels for pathrow", pr, " is ", cloudPixels))
		
		        totalPixels <- sum(img[]!=255, na.rm=TRUE)
		        print(paste0("total pixels for pathrow", pr, " is ", totalPixels))
 		        cloudPercentage <- cloudPixels/totalPixels
                print(paste0("remaining Cloud percentage for pathrow ", pr, " is ", cloudPercentage))
		        NA_pixels <- sum(img[]==255, na.rm=TRUE)
		        prData[2*i] <- cloudPercentage
		         if (file.exists(paste0(pathNoTree, "/",y,"/", pr, ".tif")) & exists('NA_pixels')){
		            img<-raster(paste0(pathNoTree, "/",y,"/", pr, ".tif"))
		            removedTreePixels <- sum(is.na(img[]), na.rm=T) - NA_pixels
		            removedTreePercentage<-removedTreePixels/totalPixels
		            print(paste0("removed Tree Percentage for pathrow ", pr, " is ", removedTreePercentage))
		            prData[2*i+1] <- removedTreePercentage
		        }
		    } 
		}
		return(prData)
	})
	
	table<-do.call(rbind, listByPr)	
	table<-as.data.frame(table)
	
	names<-rep(NA, length(year)*2+1)
	for (i in 1:length(year)){
		y<-year[i]
		names[2*i]<-paste0(y, "_cloudPercentage")
		names[2*i+1]<-paste0(y, "_treePercentage")
	}
	names[1]<-"pathrow"
	names(table)<-names
	saveRDS(table, paste0(pathImages, "/", "cloudTreeCoverTable.Rda"))
	write.csv(x=table, file=paste0(pathImages, "/", "cloudTreeCoverTable.csv"))
	table 
}