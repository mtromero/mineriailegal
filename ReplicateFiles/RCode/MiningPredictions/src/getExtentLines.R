#' @export
#' @title Get extent Lines
#'
#' @description Draws a rectangular polygon around an image extents
#' 
#' @param image Raster* object 
#'
#' @return SpatialLines
#'
#' @examples \dontrun{getExtentLines(raster)}
getExtentLines<-function(image){
	projstring<-proj4string(image)
	ex<-extent(image)
	xmin<-ex@xmin
	xmax<-ex@xmax
	ymin<-ex@ymin
	ymax<-ex@ymax
	line1<-Line(rbind(c(xmin, ymin), c(xmin, ymax)))
	line2<-Line(rbind(c(xmin, ymin), c(xmax, ymin)))
	line3<-Line(rbind(c(xmax, ymin), c(xmax, ymax)))
	line4<-Line(rbind(c(xmin, ymax), c(xmax, ymax)))
	lines<-list(line1,line2, line3, line4)
	
	lines<-Lines(lines, "foo")
	
	SpatialLines(list(lines), CRS(projstring))	
		
}