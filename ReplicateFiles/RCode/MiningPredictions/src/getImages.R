#' @export
#' @title Get images
#' 
#' @description creates a list of raster bricks from a list of coordinates
#'
#' @param Coords Data frame of longitude and latitude. Each row is a point.          
#' @param pad vector that determines the size of padding around each point. 
#' @param pathrow pathrow from which the images will be extracted. Format ="ppprrr"
#' @param pathImagenes directory where the satellite images are found.
#' @param pathTree directory where the GFC images are found.
#' 
#' @return list with raster bricks that correspond to each point.
#' 
#' @examples \dontrun{getImages(c(-70, 5), "009057", 100, "final/2010", "tree")}
getImages<-function(coords, pathrow, pad, pathImagenes, pathTree){
  library(raster)
  
  print(paste0("getting images for pathrow ", pathrow))
  
  coords<-as.matrix(coords)
  
  img1<-brick(paste0(pathImagenes, "/",pathrow, ".tif"))
  img2<-brick(paste0(pathTree,"/", pathrow,"_tree", ".tif"))
  img<-brick(c(img1,img2))
  
  coordinateSystem<-proj4string(img)
  nCoords<-getCoordinates(coords, coordinateSystem)
  cells<-cellFromXY(img, nCoords)
  centers<-getCenter(cells, nrow(img), ncol(img))
  res<-cbind(centers, pad)
  apply(X=res, FUN=function(x){
  	getCropImage(x[1:2], x[3], img)
  } , MARGIN=1)
  
}