CodigoCompleto_s <- function(cores=NULL, index=NULL, year, download = F, skip=NULL, 
                             dir_copy, dir_imagenes,dir_temp, Orden_ESPA) {
  
  require(parallel)
  
  # **************************
  # Los parametros iniciales 
  
  pathrows <- c( "007054", "007055", "007056", "007058", "007059", "007060", "008052", "008053", 
                 "008054", "008055", "008056", "008058", "008059", "008060", "009052",  "009053", 
                 "009058", "009059", "009060", "010053", "003057", "003058", "003059", "004056", 
                 "004057", "004058", "004059", "004060", "004061", "004062", "004063", "005055", 
                 "005056", "005057", "005058", "005059",  "005060", "005061", "005062", "006055", 
                 "006056", "006057", "006058", "006059", "006060", "006061", "006062", "007051", 
                 "007052", "007057", "007061", "008051", "008057",  "009054", "009055", "009056", 
                 "009057", "010054", "010055", "010056", "010057", "010058", "010059", "011054", 
                 "011055", "011059")
  
  # Si solo le decimos que procese algunos pathrows pasandole un index, solo procesa esos pathrows
  if (!is.null(index)) pathrows <- pathrows[index]
  # skip some pathrows from processing
  if (!is.null(skip)) pathrows <- pathrows[!(pathrows %in% skip)]
  
  download_folder <- file.path(dir_temp, 'Input')
  # Aqui toca bajarse las imagenes 
  # Vamos a usar python... este codigo llama a python y le dice donde guardar las cosas
  # Esta usa la versionn 2.0 con las credenciales de Sergio, usar con responsabilidad
  # si se le pasa el parametro download=FALSE se salta este paso
  if (download){
    emailstr <- strsplit(Orden_ESPA, split="-")[[1]][2]
    print(paste0('Downloading Order ', Orden_ESPA))
    system(paste0("python /home/groups/pdupas/DataNubes/copy/espa_bulk_downloader_v2.0.0/download_espa_order.py -e ", 
                  emailstr," -o ", Orden_ESPA, " -d ", download_folder,
                  ' -u sergioperilla -p economiateamo22 -i http://espa.cr.usgs.gov'))
    print(paste0('Finished downloading Order ', Orden_ESPA))
  }
  
  outfile <- file.path(dir_temp, paste0('topocorr',year,'_log.txt'))


  cl<-makeCluster(cores, outfile=outfile)
  
  clusterSetRNGStream(cl,123)
  clusterExport(cl, c(  'year',   'dir_copy',   'dir_imagenes', 'dir_temp', 'Orden_ESPA', 'download_folder'), envir=environment())
  
  
  # **************************
  # Aca empieza el Loop
  
  ## For every PR (starting with the PRs of the Airforce)
  parLapply(cl, pathrows, function(pathrow_loop){
    
    final_folder <- file.path(dir_imagenes, 'final_s',year)
    if (!file.exists(paste0(final_folder, "/", pathrow_loop, ".tif"))) {
      
      
      if (Sys.info()[["user"]]=="USUARIO") {
        
        Order <- read.csv(file=paste0(dir_copy,"/Escenas_2015_2017_missing_PR_Progreso.csv"), header=TRUE, sep=",")
      }
      if (Sys.info()[["user"]]=="Santi") {
        
        Order <- read.csv(file=paste0(dir_copy,"/Escenas_2015_2017_missing_PR_Progreso.csv"), header=TRUE, sep=",")
      }
      if (Sys.info()[["user"]]=="santisap") {
        
        .libPaths("/share/sw/free/R/3.3.3/intel/lib64/R/library")
        #I had to remove the degree symbol from the Escenas file because Sherlock was having troubles
        Order <- read.csv2(file=paste0(dir_copy,"/Escenas_2015_2017_missing_PR_Progreso.csv"), header=TRUE, sep=",")
      }
      
      require('teamlucc')
      require('SDMTools')
      require('stringr')
      require('tools')
      require('rgdal')
      
      if (Sys.info()[["user"]]=="admin") source('C:/Users/admin/Downloads/Download SP/Funciones_Codigo_Nuevo/extract_espa.R')
      if (Sys.info()[["user"]]=="santisap") source(paste0(dir_imagenes,'/ecosystems/src/extract_espa.R'))
      if (Sys.info()[["user"]]=="Santi") source('C:/Users/Santi/Documents/ecosystems/src/extract_espa.R')
      
      
      extract_folder <- file.path(dir_temp, paste0('extract_new', year,pathrow_loop))
      
      sapply(c(download_folder, extract_folder, final_folder),
             function(x) if (!dir.exists(x)) dir.create(x, recursive=TRUE))
      
      # *****************
      # Unzip the image
      
      # Primero se borra todo lo que este en el extract folder
      unlink(dir(extract_folder, full.names = TRUE), recursive = TRUE, force = TRUE)
      # Ahora se extraen las fotos de la escena (path/row) para un ano dado
      
      print(paste0('Extracting ', file.path(download_folder, Orden_ESPA)))
      print(paste0('Pathrow: ', pathrow_loop))
      
      # Intenta extraer los archivos dentro del rango de fechas especificado
      # Si no encuentra archivos de ese pathrow en ese rango de fechas,
      # pasar� a procesar el siguiente.
      
      ## SERGIO Extract only if image not topocorr Linea 165. You have to be careful so you can keep extracting the names
      topcorr_path <- dir(file.path(dir_temp, 'extract_TOPCORR', as.character(year), pathrow_loop), full.names = T)
      topcorr_band7 <- dir(topcorr_path, pattern = 'band7')
      
      if ((!(length(topcorr_path)==length(topcorr_band7))) | ((length(topcorr_path)==0) & (length(topcorr_band7)==0))) {
        result <- tryCatch({
          espa_extract_sergio(file.path(download_folder, Orden_ESPA), 
                              extract_folder,
                              pathrows   = pathrow_loop)
          1
        }, error = function(e){
          print(paste0('no images found for pathrow ', pathrow_loop))
          0
        })
          
      }
      result <- 1
      
      if (result != 0) {
        
        print('finished extracting.')
        
        if (length(dir(extract_folder))!=0) {
          # *****************************************
          # For every image of that PR in that year
          
          # Primero digame cuales son las carpetas que tienen las imagenes
          image_dirs <- dir(extract_folder,
                            pattern='^[0-9]{3}-[0-9]{3}_[0-9]{4}-[0-9]{3}_((LT[45])|(LE07))$',
                            full.names=TRUE)
          image_ids=image_dirs
          for (i in 1:length(image_dirs)) { 
            image_ids[i]<- file_path_sans_ext(dir(image_dirs[i], pattern='band1', full.names=F))
          }
          image_ids=gsub("_sr_band1","",image_ids)
          
          IndexOrders = sapply(image_ids, function(x) grep(x, Order$Landsat.Product.Identifier))
          OrderScenes=sort.int(Order$Land.Cloud.Cover[IndexOrders], index.return=TRUE)$ix
          CloudCover = Order$Land.Cloud.Cover[IndexOrders][OrderScenes]
          
          EscenasNecesarias =min(which(1-cumprod((CloudCover/100))>0.99))
          # We always want at least two images if posible
          if (EscenasNecesarias == 1 & length(image_dirs)>1) EscenasNecesarias <- EscenasNecesarias+1
          # If possible, lets just keep the images we need
          if (EscenasNecesarias < Inf) image_dirs <- image_dirs[OrderScenes][1:EscenasNecesarias]
          if (EscenasNecesarias < Inf) image_ids <- image_ids[OrderScenes][1:EscenasNecesarias]
          
          # *************************
          # Order files
          pre <- file.path(dir_temp, 'extract_TOPCORR', as.character(year), pathrow_loop)
          t <- lapply(list.files(extract_folder),  function(x) file.path(pre, x))
          if (EscenasNecesarias < Inf) t <- t[OrderScenes][1:EscenasNecesarias]
          for (i in 1:length(t)) {
            dir.create(t[[i]], recursive = T)
          }
          
          
          Path=substr(pathrow_loop,1,3)
          Row=substr(pathrow_loop,4,6)
          # Segundo cargue el slopeaspect , si no ha acabado el ultimo de los topocorr
          if (!file.exists(file.path(t[[length(image_dirs)]],paste0(image_ids[length(image_dirs)],'_sr_band7.tif')))) {
            slopeaspect <- brick(file.path(dir_imagenes, 'DEM_output', paste0('slopeaspect_', Path, '-',Row,'.tif')))
            #slopeaspect <-crop(slopeaspect,extent(715810,732980,1189350,1254500))
            slopeaspect <- brick(raster(slopeaspect, layer=1)/10000,raster(slopeaspect, layer=2)/1000)  
            print('Ya carge el slope')
          }
          
          # Tercero montar el loop por carpeta de images
          for (i in 1:length(image_dirs)) { 
            
            
            # Cuarto los datos de la imagen
            img_data <- Order[Order$Landsat.Product.Identifier==image_ids[i], ]
            
            if (Sys.info()[["user"]]!="santisap") {
            Sun.Azimuth <- img_data$Sun.Azimuth
            Sun.Elevation <- img_data$Sun.Elevation
            } else {
            Sun.Azimuth <- as.numeric(paste(img_data$Sun.Azimuth))
            Sun.Elevation <-as.numeric(paste( img_data$Sun.Elevation))

            }
            
            # Quinto cargue cloud_qa y reesamplee, si no existe
            if (!file.exists(file.path(t[[i]], basename(file.path(image_dirs[i],paste0(image_ids[i], '_sr_cloud_qa.tif')))))) {
              cloud <- raster(file.path(image_dirs[i],paste0(image_ids[i], '_sr_cloud_qa.tif'))) 
              cloud <- resample(cloud, raster(slopeaspect, layer=1), "ngb")
              cloud <- crop(cloud,extent(slopeaspect))
              
              writeRaster(x=cloud, filename=
                            file.path(t[[i]], basename(file.path(image_dirs[i],paste0(image_ids[i], '_sr_cloud_qa.tif')))), overwrite=T)
              print('Ya guarde cloud_qa')
            }
            
            # *************************
            # For everi band 1-5,7
            for (j in c(1:5,7)) {
              #check if file exists
              if (!file.exists(file.path(t[[i]],paste0(image_ids[i],'_sr_band',j, '.tif')))) {
                base <- raster(file.path(image_dirs[i], paste0(image_ids[i], '_sr_band', as.character(j) ,'.tif'))) 
                base <- resample(base,raster(slopeaspect, layer=1), "ngb")
                base <- crop(base,extent(slopeaspect))
                
                
                base_tc <- tryCatch({
                  topographic_corr(base, slopeaspect, sunelev=Sun.Elevation, sunazimuth=Sun.Azimuth,DN_min=0, DN_max=10000)
                }, error = function(err){
                  print(paste('error topocorr en la banda', as.character(j),'PR',pathrow_loop))
                })
                
                
                # *************************
                # Save base_tc
                if (class(base_tc)[1] == 'RasterLayer') writeRaster(x=base_tc, filename=file.path(t[[i]],paste0(image_ids[i],'_sr_band',j, '.tif'))
                                                                    , overwrite=T)
                if (class(base_tc)[1] == 'RasterLayer') print(paste('Guarde la imagen topocorregida en', as.character(j)))
              }
            }
            
          }
        }
        
        
        
        ## Cloud cleaning
        if (!file.exists(paste0(final_folder, "/", pathrow_loop, ".tif"))) {
        ##Cargue la imagen base
        pre <- file.path(dir_temp, 'extract_TOPCORR', as.character(year), pathrow_loop)
        image_dirs <- dir(pre,
                          pattern='^[0-9]{3}-[0-9]{3}_[0-9]{4}-[0-9]{3}_((LT[45])|(LE07))$',
                          full.names=TRUE)
        t <- lapply(list.files(pre),  function(x) file.path(pre, x))
        banfiles <-dir(t[[1]], pattern="band", full.names = TRUE)

        #Do cloud filling if topocorr succesful 
        if (file.exists(banfiles[6])) {

        # Borrar todo lo que este en el extract folder para ahorrar espacio
        unlink(dir(extract_folder, full.names = TRUE), recursive = TRUE, force = TRUE) 
        base_tc <- brick(raster(banfiles[1]),raster(banfiles[2]),raster(banfiles[3]),raster(banfiles[4]),raster(banfiles[5]),raster(banfiles[6]))

        if (length(image_dirs)>1) {
        ##Cargue la nube base
        base_fmask <- brick(dir(t[[1]], pattern="cloud", full.names = TRUE))
        base_cloud_mask <- (base_fmask == 2) |(base_fmask == 34) | (base_fmask == 4) | (base_fmask == 12) |
          (base_fmask == 20) |(base_fmask == 36) |(base_fmask == 52)
        
        base_cloud_mask[base_fmask == maxValue(base_fmask)] <- NA

        

        for (i in 2:length(image_dirs)) {
          base_cloud_mask[is.na(base_tc[[1]])] <- NA
          base_tc[base_cloud_mask] <- 0

          ##Cargue fills
          banfiles<-dir(t[[i]], pattern="band", full.names = TRUE)
          fill_tc <- brick(raster(banfiles[1]),raster(banfiles[2]),raster(banfiles[3]),raster(banfiles[4]),raster(banfiles[5]),raster(banfiles[6]))
          
          ##Cargue la nube base
          fill_fmask <- brick(dir(t[[i]], pattern="cloud", full.names = TRUE))
          fill_cloud_mask <- (fill_fmask == 2) |(fill_fmask == 34) | (fill_fmask == 4) | (fill_fmask == 12) |
            (fill_fmask == 20) |(fill_fmask == 36) |(fill_fmask == 52)
          fill_cloud_mask[fill_fmask == maxValue(fill_fmask)] <- NA
          fill_cloud_mask[is.na(fill_tc[[1]])] <- NA
          fill_tc[fill_cloud_mask] <- 0

          # Set clouds in fill image to NA in base mask:
          base_cloud_mask[fill_cloud_mask] <- NA
          # Set missing values in fill image to NA in base mask:
          base_cloud_mask[is.na(fill_cloud_mask)] <- NA
          
          base_tc <- tryCatch({
            base_cloud_mask_try <- ConnCompLabel(base_cloud_mask)
            cloud_remove(base_tc, fill_tc, base_cloud_mask_try, DN_min=0,
                         DN_max=10000, algorithm="teamlucc", verbose = T)
          }, error = function(err){
            cloud_remove(base_tc, fill_tc, base_cloud_mask, DN_min=0,
                         DN_max=10000, algorithm="teamlucc", verbose = T)
            
          })
          
          ## Cloud cleaning
          
        }
        }

        ##Guarde la imagen en el folder final/PR.tif
        writeRaster(base_tc,
                    filename=paste0(final_folder, "/", pathrow_loop, ".tif"),
                    overwrite=TRUE,
                    datatype='INT2S')
      }
      }
      }
    }
  })
  
  stopCluster(cl)
  
}