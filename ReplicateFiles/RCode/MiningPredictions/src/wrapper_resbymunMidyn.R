wrapper_resbymunMidyn<-function(year,mainPath){
gitpath <- paste0(mainPath,'/ecosystems/src/')
source(paste0(gitpath,"mosaicPolygon.R"))
source(paste0(gitpath,"allResultsByMunicipioMidyn.R"))
source(paste0(gitpath,"municipioResultsMidyn.R"))
source(paste0(gitpath,"rowsToDataFrame.R"))

pathPredictionsPost<-file.path(mainPath,'sandwichNARemoved')
suffix<-'Rf10onlymlOSMecopot'
numCores <- 16


file.create(file.path(mainPath, paste0('log_municipiosMidyn',year,'.txt')))
sink()
sink(file.path(mainPath, paste0('log_municipiosMidyn',year,'.txt')))

titmask <-'titulosMasks'


allResultsByMunicipioMidyn(   year=year, 
	                  suffix=suffix,
	                  mainPath=mainPath,
                          pathPredictionsPost=pathPredictionsPost,
	                  numCores=numCores,
                    titmask=titmask)

}