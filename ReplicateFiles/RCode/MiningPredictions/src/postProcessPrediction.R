#' post process a prediction raster
#' processes the raster of predicted probabilities
#'
#' @details the function takes a raster of predicted probabilites and
#'          one value either for false positive rate (fpr), true positive 
#'          rate (tpr) or a cutoff value (cut) from which the cutoff 
#'          probability is calculated. Then pixels that are predicted as a mine but 
#'          that fail to have at least one other neighbor classified as a mine
#'
#' @param predictionRaster raster to be post processed
#' @param pred object of class pred
#' @param tpr true positive rate
#' @param fpr false positive rate
#' @param cut probability cut value to define 0-1 classes
#'
#' @export
#'
#' @return raster, NA if NA in original image, 1 if contains mine, 0 else.
#' 
#' @examples \dontrun{postProcessPrediction(raster, pred, cut=0.5)}
postProcessPrediction<-function(predictionRaster, pred=NULL, tpr=NULL, fpr=NULL, cut=NULL){
	if (!sum(c(is.null(tpr), is.null(fpr), is.null(cut)))){
		stop('Exactly one of fpr, tpr, or threshold must be not null')
	}
    if (is.null(cut)){
        if (is.null(pred)) stop('pred object must be not null')
    perf<-performance(pred,'tpr','fpr')
    cutoffs <- data.frame(cut=perf@alpha.values[[1]], 
                          fpr=perf@x.values[[1]], 
                          tpr=perf@y.values[[1]])
    if(!is.null(tpr)){
    	varName<-'tpr'
    	var<-tpr
    } else if (!is.null(fpr)){
    	varnName <- 'fpr'
    	var <- fpr
    } else {
    	varName <- 'cut'
    	var <- cut
    }
    
    distances <- abs(cutoffs[,'varName']-var)
    cutoffs <- cutoffs[which.min(distances)]
    cut <- cutoffs$cut
    }

    predictionRaster <- predictionRaster >= cut
    ncol <- ncol(predictionRaster)
    nrow <- nrow(predictionRaster)

    N <- ncell(predictionRaster)
    peso=matrix(1,nrow=3,ncol=3)
    peso[2,2]=0
    ##ad fix para que mire solo vecinos directos
    peso[1,1]=0
    peso[1,3]=0
    peso[3,1]=0
    peso[3,3]=0
    ##
    newPrediction <- focal(predictionRaster, w=peso,fun=mean,na.rm=T,pad=T)
    #si todos mis vecinos son 1, y yo no lo soy, entonces pongame 1
    predictionRaster[newPrediction==1 & !is.na(predictionRaster)] <- 1
    predictionRaster[newPrediction==0 & !is.na(predictionRaster)] <- 0
    
###Nota para Mauricio del Futuro y Santiago de Futuro... hacer robustez a iteracion, o a no cambiar NAs rodeados    
#     newPrediction <- focal(predictionRaster, w=peso,fun=mean,na.rm=T,pad=T)
#     #si todos mis vecinos son 1, y yo no lo soy, entonces pongame 1
#     predictionRaster[newPrediction==1] <- 1
#     predictionRaster[newPrediction==0] <- 0
    rm(newPrediction)
    return(predictionRaster)                    
}