#' @export
#' @title Process GFC images
#'
#' @description Reprojects the tree images to the same CRS of landsat images
#'              then creates the tiles of the same extent and resolution of the pathrow tiles
#'
#' @param treeCoverDir path to the GFC images directory
#' @param landsatDir path to the landsat images
#' @param outputDir path where the resulting images will be stored
#' @param numCores number of cores to run in parallel
#'
#' @return -
#' 
#' @examples \dontrun{processTreeImages("tree", "final/2010", "treeProc/2010", numCores=2)}
processTreeImages<-function(treeCoverDir, landsatDir, outputDir, numCores=NULL){

    library(parallel)
	library(wrspathrow)

	cl <- makeCluster(numCores, outfile=file.path(outputDir, 'processTreeImages_log.txt'))

    pathrows <- c("003057", "003058", "003059", "004056", "004057", "004058", "004059", "004060",
	              "004061", "004062", "004063", "005055", "005056", "005057", "005058", "005059",
	              "005060", "005061", "005062", "006055", "006056", "006057", "006058", "006059",
	              "006060", "006061", "006062", "007051", "007052", "007054", "007055", "007056",
	              "007057", "007058", "007059", "007060", "007061", "008051", "008052", "008053",
	              "008054", "008055", "008056", "008057", "008058", "008059", "008060", "009052",
	              "009053", "009054", "009055", "009056", "009057", "009058", "009059", "009060",
	              "010053", "010054", "010055", "010056", "010057", "010058", "010059", "011054",
	              "011055", "011059")

    finalCrs <- '+proj=utm +zone=18 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0'
    finalCrs <- CRS(finalCrs)
    
    if (!dir.exists(outputDir)) dir.create(outputDir, recursive=TRUE)
    
    e <- environment()
    clusterExport(cl, varlist=c('outputDir', 'pathrowToPoly', 'mosaicPolygon'), envir=e)
    tryCatch({
        parSapply(cl, pathrows, function(pathrow) {
    
    	    filename <- file.path(outputDir, paste0(pathrow, '.tif'))
    	    if (file.exists(filename)){
    		    print(paste0('File ', filename, ' already exists.'))
    		    return(NULL)
    	    }
            pathrowShape <- pathrowToPoly(pathrowString=pathrow,
        	                              Crs=finalCrs)         
            pathrowRaster <- mosaicPolygon(spPolygon=pathrowShape,
        	                               imageDir=treeCoverDir)      
            if (!is.null(pathrowRaster)){
            	writeRaster(x=pathrowRaster,
        	            filename=filename,
        	            format='GTiff')
            }
        })
    }, error=function(e) {
        print(e)
        stopCluster(cl)
    })
    
    stopCluster(cl)
}