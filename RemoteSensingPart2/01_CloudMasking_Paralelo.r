#Enmascaramiento y remoci�n de nubes para un a�o
#INPUT: Todas las escenas de cierto semestre que cubran cierto departamento y que tengan
#menos de 80% de cobertura de Nubes.
#OUTPUT: Cada una de las escenas pero sin nubes y donde hab�a nubes tiene NA en vez de 0.
#
########################################################################################
#Inicialmente definimos el nombre del departamento al que se le va a hacer el proceso
#El formato va en

#Dado que las escenas son muy grandes para meterlas en COPY, el proceso se va a hacer
#Se necesita el path donde esten guardadas todas las escenas de distintos a�os
path<- "C:\\Users\\santi\\Desktop\\Research_bigdata\\Illegal_mining\\"
#El a�o en el que se va a hacer la remoci�n de las nubes
anio<- toString(2009)
#Creamos una lista con el nombre de todas las escenas a las que les vamos a sacar
#las nubes
files<-list.files(paste(path, anio,"\\",sep=""),)
#files=setdiff(files,"Bulk Order 501083") #para quitar lo q no se ha descrompimido
#Para fines pr�cticos, se crea un vector de strings con el c�digo de cada layer
bnd<-c("_B1","_B2","_B3","_B4","_B5","_B6_VCID_1","_B6_VCID_2","_B7")
#Se crea una funcion que quita las nubes de la escena name

fCloudMask=function(name){

  #Metemos todo en un RasterStack:
require(raster)
require(landsat)
require(rgeos)
#Se necesita el path donde esten guardadas todas las escenas de distintos a�os
path<- "C:\\Users\\santi\\Desktop\\Research_bigdata\\Illegal_mining\\"
#El a�o en el que se va a hacer la remoci�n de las nubes
anio<- toString(2009)
#Para fines pr�cticos, se crea un vector de strings con el c�digo de cada layer
bnd<-c("_B1","_B2","_B3","_B4","_B5","_B6_VCID_1","_B6_VCID_2","_B7")

  b1.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B1.TIF",sep=""))
  b2.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B2.TIF",sep=""))
  b3.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B3.TIF",sep=""))
  b4.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B4.TIF",sep=""))
  b5.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B5.TIF",sep=""))
  b6_1.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B6_VCID_1.TIF",sep=""))
  b6_2.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B6_VCID_2.TIF",sep=""))
  b7.r<-raster(paste(path,anio, "\\",name,"\\",name,"_B7.TIF",sep=""))
  scene<-stack(b1.r,b2.r,b3.r,b4.r,b5.r,b6_1.r,b6_2.r,b7.r)
  rm(b1.r,b2.r,b3.r,b4.r,b5.r,b6_1.r,b6_2.r,b7.r)
  gc()
  #####

  
      #Para sacar la mascara de nubes, se necesitan las bandas 1 y 6_1 del stack
      #Ademas es necesario que esten en formato SpatialGridDataFrame
      b.1<-as(raster(scene,layer=1), 'SpatialGridDataFrame')
      b.61<-as(raster(scene,layer=6),'SpatialGridDataFrame')
      #Note que los SpatialGridDataFrame se almacenan en memoria, por lo que esto puede
      #Ser una limitaci�n computacional, periodicamente se puede usar gc() para liberar 
      #Memoria de objetos borrados
      gc()
      #Ahora se crea la m�scara de nubes
      cloud.mask<-clouds(b.1,b.61,level=1)
      #Se demora montones de tiempo (obviamente)
      ####
      #Ahora con la m�scara de nubes, enmascaramos el raster original
      #_CL representar� CloudLess, notemos que para enmascarar se puede usar un raster
      #Antes de poder enmascarar necesitamos que el mask est� en tipo raster, esto toma unos segundos
      #Originalmente viene en formato SpatialGridDataFrame
      cloud.maskr<-raster(cloud.mask)
      #Ahora si enmascaramos el raster
      scene.masked<-mask(scene,cloud.maskr,inverse=TRUE)
      #Ahora para cada uno de los layers, cambiemos los 0 por NA, escribamos un archivo
      #.tif, borramos de la memoria y pasamos el garbage collector.
      dir.create(paste(path,"EscenasSinNubes\\",anio,"\\",name,sep=""),recursive=TRUE,showWarnings=FALSE)
      for(j in 1:8){
        layer<-raster(scene.masked,layer=j)
        layer[layer==0]<-NA
#        layer<-reclassify(layer, cbind(0, NA) )
        #Y finalmente la guardamos en un archivo, dentro de nuestro created DATA
        writeRaster(layer,file=paste(path,"EscenasSinNubes\\",anio, "\\",name,"\\",name,"_CL",bnd[j],".tif",sep=""),format="GTiff",overwrite=TRUE)
        rm(layer)
        gc()
        }
      #Y ahora para salvar espacio en el disco, le pedimos que borre el contenido de las carpetas
      #Que contienen las escenas, pero dejando la carpeta, para que no se confunda el c�digo
      #Mauricio: No voy a borrar nada por ahora!
#      do.call(file.remove,list(list.files(paste(path,anio, "\\",name,"\\",sep=""))))
      #Y ahora le pedimos que elimine los archivos temporales
      removeTmpFiles()
} 


cl <- makeCluster(numCores)  
parLapply(cl, files, fCloudMask)  
stopCluster(cl)  
