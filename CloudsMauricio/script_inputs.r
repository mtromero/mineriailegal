#install.packages("rgeos")
#install.packages("wrspathrow")
#install.packages("tools")
#install.packages("gdalUtils")
#install.packages("sp")
#install.packages("XML")
#install.packages("stringr")
#install.packages("raster")
#install.packages("maptools")
#install.packages("rgdal")
#install.packages("sp")
#install.packages("wrspathrow")
#install.packages("stringr")
#install.packages("foreach")
#install.packages("doParallel")
#library(devtools)
#install_github('mauricioromero86/teamlucc')
require(teamlucc)
require(maptools)
require(rgdal)
require(sp)
require(wrspathrow)
require(stringr)
require(foreach)
require(doParallel)
#setwd("C:/Users/Mauricio/Copy/CloudsMauricio")
setwd("/media/mauricio/HDD1/Dropbox/CloudsMauricio/")

municipios.sf<-readShapePoly("COL_adm/COL_adm2")
prj.from<-CRS(proj4string(readOGR(dsn="COL_adm",layer="COL_adm2")))
proj4string(municipios.sf)=prj.from
municipios.sf=spTransform(municipios.sf,CRS("+proj=utm +zone=19 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"))
#municipios.sf
#descomente la linea, si quiere solo unos departamentos
#municipios.sf=municipios.sf[which(municipios.sf$NAME_1=="Quindio" | municipios.sf$NAME_1=="Risaralda"),]
municipios.sf=unionSpatialPolygons(municipios.sf,ID=rep(1,dim(municipios.sf)[1]),threshold=10000)
#municipios.sf
#plot(municipios.sf)
municipios_pathrows <- pathrow_num(municipios.sf, as_polys=TRUE)
plot(municipios_pathrows)
text(coordinates(municipios_pathrows), labels=paste(municipios_pathrows$PATH,municipios_pathrows$ROW, sep=', '))
                                             
PathRowsPais=sort(paste(str_pad(municipios_pathrows$PATH,3,pad = "0"),str_pad(municipios_pathrows$ROW,3,pad = "0"),sep=""))

                                             
Order=ee_read("Inventario2003_2014_Completo.txt")
PathRowsOrden=sort(unique(paste(str_pad(Order$WRS.Path,3,pad = "0"),str_pad(Order$WRS.Row,3,pad = "0"),sep="")))

setdiff(PathRowsPais,PathRowsOrden)
setdiff(PathRowsOrden,PathRowsPais)             
PathRowsUso=intersect(PathRowsPais,PathRowsOrden)
PathRowsUso=setdiff(PathRowsUso,c("010060","010052")) #Este pathrow en realidad esta todo en Ecuador... es una perdida de tiempo
write.csv(PathRowsUso,"PathRowsUso.csv")
municipios_pathrows2=municipios_pathrows[which(paste0(municipios_pathrows$PATH,"/",municipios_pathrows$ROW) %in% paste0(as.numeric(substr(PathRowsUso,1,3)),"/",as.numeric(substr(PathRowsUso,4,6)))),]
pdf("Mapa_PathRows.pdf")
plot(municipios_pathrows2)
municipios.sf2=spTransform(municipios.sf, CRS(proj4string(municipios_pathrows2)))
plot(municipios.sf2, add=TRUE, lty=2, col="#00ff0050")
text(coordinates(municipios_pathrows2), labels=paste(municipios_pathrows2$PATH,municipios_pathrows2$ROW, sep=', '))
dev.off() 
#PathRowsUso="009057"
Order=Order[which(Order$Path_Row %in% paste(as.numeric(substr(PathRowsUso,1,3)),"/",as.numeric(substr(PathRowsUso,4,6)),sep="")),]

#ee_plot(Order, start_date= as.Date('2010/1/1'), end_date=as.Date('2010/12/31'), min_clear = 0.3,normalize=T)
#ee_plot(Order, start_date= as.Date('2011/1/1'), end_date=as.Date('2011/12/31'), , min_clear = 0.0,normalize=T)

##Guardamos todo lo que se va a usar despues
save(municipios.sf,Order,PathRowsUso,file="Input_Ordenes_PathRows.RData")

###############################################################################################
##Esto solo toca hacerlo 1 vez
#espa_scenelist(Order, as.Date('2010/1/1'), as.Date('2010/12/31'),'Order_STRM.txt',min_clear = 0.0)
#dem_files <- dir("DEM/", pattern='.tif$', full.names=TRUE,recursive=T)
#dems <- lapply(dem_files, raster)
#dem_extents <- get_extent_polys(dems)
#
#auto_setup_dem(municipios.sf, "DEM/", dem_extents, of = "GTiff", ext = "tif",
# n_cpus = 1, overwrite =F, crop_to_aoi = T, notify = print,
# verbose = T)
###############################################################################################


#esto es lo que se necesita para hacer la orden
for(ano in 2003:2014){
espa_scenelist(Order, as.Date(paste(ano,'/1/1',sep="")), as.Date(paste(ano,'/12/31',sep="")),paste('Order_',ano,'.txt',sep=""),min_clear = 0.3)
}