library(XML)
library(rgdal)
library(teamlucc)
library(maptools)
library(rgdal)
library(sp)
library(wrspathrow)
library(stringr)
library(foreach)
library(doParallel)
library(tools)
library(gdalUtils)
library(rgeos)
library(mgcv)

.calc_IL_vector <- function(slope, aspect, sunzenith, sunazimuth, IL.epsilon) {
    IL <- cos(slope) * cos(sunzenith) + sin(slope) * sin(sunzenith) * 
        cos(sunazimuth - aspect)
    IL[IL == 0] <- IL.epsilon
    return(IL)
}

.calc_IL <- function(slope, aspect, sunzenith, sunazimuth, IL.epsilon) {
    overlay(slope, aspect,
            fun=function(slope_vals, aspect_vals) {
                .calc_IL_vector(slope_vals, aspect_vals, sunzenith, sunazimuth, 
                               IL.epsilon)
            })
}


normalize_extent <- function(te, res=c(30, 30)) {
    # Setup xmin
    te[1] <- round(te[1] - te[1] %% res[1])
    # Setup ymin
    te[2] <- round(te[2] - te[2] %% res[2])
    # Setup xmax
    te[3] <- round(te[3] + res[1] - te[3] %% res[1])
    # Setup ymax
    te[4] <- round(te[4] + res[2] - te[4] %% res[2])
    stopifnot(round(te[1] / res[1]) == (te[1] / res[1]))
    stopifnot(round(te[2] / res[2]) == (te[2] / res[2]))
    stopifnot(round(te[3] / res[1]) == (te[3] / res[1]))
    stopifnot(round(te[4] / res[2]) == (te[4] / res[2]))
    return(te)
}



#' @import mgcv
.calc_k_table <- function(x, IL, slope, sampleindices, slopeclass,
                          coverclass, sunzenith) {
    if (!is.null(sampleindices)) {
        
        
        # Remember that the sample indices are row-major (as they were drawn 
        # for a RasterLayer), so the coverclass matrix needs to be transposed 
        # as it is stored in column-major order
        if(!is.null(coverclass)) coverclass <- t(coverclass)[sampleindices]
    } else {
        K <- data.frame(x=getValues(x), IL=getValues(IL), 
                        slope=getValues(slope))
    }

    if(!is.null(coverclass)) {
        K <- K[coverclass, ]
    }

    ## K is between 0 and 1
    # IL can be <=0 under certain conditions
    # but that makes it impossible to take log10 so remove those elements
    K <- K[!apply(K, 1, function(rowvals) any(is.na(rowvals))), ]
    K <- K[K$x > 0, ]
    K <- K[K$IL > 0, ]

    k_table <- data.frame(matrix(0, nrow=length(slopeclass) - 1, ncol=3))
    names(k_table) <- c('midpoint', 'n', 'k')
    k_table$midpoint <- diff(slopeclass)/2 + slopeclass[1:length(slopeclass) - 1]

    # don't use slopes outside slopeclass range
    K.cut <- as.numeric(cut(K$slope, slopeclass))
    if(nrow(k_table) != length(table(K.cut))) {
        stop("slopeclass is inappropriate for these data (empty classes)\n")
    }
    k_table$n <- table(K.cut)

    for(i in sort(unique(K.cut[!is.na(K.cut)]))) {
        k_table$k[i] <- coefficients(lm(log10(K$x)[K.cut == i] ~ 
                                        log10(K$IL/cos(sunzenith))[K.cut == i]))[[2]]
    }

    return(k_table)
}

# Function to combine classes defined by the upper limits 'lims' with their 
# smallest neighbors until each class has at least n members. 'counts' stores 
# the number of members in each class.
clean_intervals <- function(counts, lims, n) {
    while(min(counts) < n) {
        # The "rev" below is so that the classes at the end are combined first 
        # (as classes with higher slopes are more more likely to be more rare 
        # and therefore have fewer members)
        min_index <- length(counts) - match(TRUE,
                                            rev(counts == min(counts))) + 1
        if (min_index == length(counts)) {
            counts[min_index - 1] <- counts[min_index - 1] + counts[min_index]
            lims <- lims[-(min_index - 1)]
        } else if (min_index == 1) {
            counts[min_index + 1] <- counts[min_index + 1] + counts[min_index]
            lims <- lims[-min_index]
        } else if (counts[min_index - 1] < counts[min_index + 1]) {
            counts[min_index - 1] <- counts[min_index - 1] + counts[min_index]
            lims <- lims[-(min_index - 1)]
        } else {
            # We know counts[min_index - 1] > counts[min_index + 1]
            counts[min_index + 1] <- counts[min_index + 1] + counts[min_index]
            lims <- lims[-min_index]
        }
        counts <- counts[-min_index]
    }
    return(lims)
}


get_gdalinfo_item <- function(item, gdalinfo_text) {
    gdalinfo_text <- gdalinfo_text[grepl(paste0('^[ ]*', item), gdalinfo_text)]
    if (length(gdalinfo_text) > 1) stop('more than one item found')
    gdalinfo_text <- gsub(paste0('[ ]*', item, '='), '', gdalinfo_text)
    return(gdalinfo_text)
}

get_mtl_item <- function(item, mtl_txt) {
    mtl_txt <- mtl_txt[grepl(paste0('^[ ]*', item), mtl_txt)]
    if (length(mtl_txt) > 1) stop('more than one item found')
    mtl_txt <- gsub(paste0('[ ]*', item, ' = '), '', mtl_txt)
    # Strip leading/following quotes
    mtl_txt <- gsub('^"', '', mtl_txt)
    mtl_txt <- gsub('"$', '', mtl_txt)
    return(mtl_txt)
}

#' @importFrom foreach %do%
#' @importFrom stringr str_extract
#' @importFrom tools file_path_sans_ext
detect_ls_files <- function(folder) {
    stopifnot(file_test('-d', folder))
    ls_regex <- '^(lndsr.)?((LT4)|(LT5)|(LE7)|(LC8))[0-9]{6}[12][0-9]{6}[a-zA-Z]{3}[0-9]{2}'
    file_bases <- file_path_sans_ext(dir(folder, pattern=ls_regex, full.names=TRUE))
    # Select only unique file basenames
    file_bases <- file.path(folder, unique(str_extract(basename(file_bases), ls_regex)))
    file_base <- NULL # keep R CMD check happy
    out <- foreach (file_base=file_bases, .combine=rbind) %do% {
        if (file_test('-f', paste0(file_base, ".xml")) & 
            file_test('-f', paste0(file_base, "_sr_band1.img"))) {
            # ENVI file format
            file_format <- "ESPA_CDR_ENVI"
        } else if (file_test('-f', paste0(file_base, ".xml")) &
                   file_test('-f', paste0(file_base, "_sr_band1_hdf.img")) &
                   file_test('-f', paste0(file_base, ".hdf"))) {
            # HDF-EOS2 format (post-August 2014)
            file_format <- "ESPA_CDR_HDF"
        } else if (file_test('-f', paste0(file_base, ".xml")) &
                   file_test('-f', paste0(file_base, "_sr_band1.tif"))) {
            # TIFF format
            file_format <- "ESPA_CDR_TIFF"
        } else if (grepl('^lndsr\\.', basename(file_base)) &
                   file_test('-f', paste0(file_base, ".hdf")) &
                   file_test('-f', paste0(file_base, ".hdf.hdr")) &
                   file_test('-f', paste0(file_base, ".txt"))) {
            # Old format HDF (pre-August 2013)
            file_format <- "ESPA_CDR_OLD"
        } else {
            warning(paste0("Failed to detect image format for:", file_base))
            file_format <- NULL
        }
        # TODO: Autodetect L1T images
        # } else if (TODO)
        #     # L1T terrain corrected image
        #     formats <- c(formats, "L1T")
        # }
        return(data.frame(file_formats=file_format, file_bases=file_base,
                          stringsAsFactors=FALSE))
    }
    return(out)
}

#' @importFrom stringr str_extract
#' @importFrom gdalUtils gdalinfo
#' @importFrom XML xmlParse xmlToList
get_metadata <- function(file_base, file_format) {
    if (file_format %in% c("ESPA_CDR_TIFF", "ESPA_CDR_HDF", "ESPA_CDR_ENVI")) {
        meta_file <- paste0(file_base, ".xml")
    } else if (file_format == "ESPA_CDR_OLD") {
        meta_file <- paste0(file_base, ".hdf")
    } else if (file_format == "L1T") {
        meta_file <- paste0(file_base, "_MTL.txt")
    } else {
        stop(paste('unrecognized file_format', file_format))
    }
    meta <- list()
    if (file_format == "ESPA_CDR_OLD") {
        if (!grepl(".hdf$", meta_file)) {
            stop("meta_file must be a *.hdf file")
        }
        meta_file_gdalinfo <- gdalinfo(meta_file)
        aq_date <- get_gdalinfo_item('AcquisitionDate', meta_file_gdalinfo)
        meta$aq_date <- strptime(aq_date, format="%Y-%m-%dT%H:%M:%OSZ", tz="UTC")
        meta$WRS_Path <- sprintf('%03i', as.numeric(get_gdalinfo_item('WRS_Path', meta_file_gdalinfo)))
        meta$WRS_Row <- sprintf('%03i', as.numeric(get_gdalinfo_item('WRS_Row', meta_file_gdalinfo)))
        meta$sunelev <- 90 - as.numeric(get_gdalinfo_item('SolarZenith', meta_file_gdalinfo))
        meta$sunazimuth <- as.numeric(get_gdalinfo_item('SolarAzimuth', meta_file_gdalinfo))
        meta$short_name  <- get_gdalinfo_item('ShortName', meta_file_gdalinfo)
    } else if (file_format %in% c("ESPA_CDR_ENVI", "ESPA_CDR_TIFF", "ESPA_CDR_HDF")) {
        if (!grepl(".xml$", meta_file)) {
            stop("meta_file must be a *.xml file")
        }
        meta_list <- xmlToList(xmlParse(meta_file))
        aq_date <- meta_list$global_metadata$acquisition_date
        aq_time <- meta_list$global_metadata$scene_center_time
        meta$aq_date <- strptime(paste(aq_date, aq_time), format="%Y-%m-%d %H:%M:%OSZ", tz="UTC")
        pathrow <- meta_list$global_metadata$wrs
        meta$WRS_Path <- sprintf('%03i', as.numeric(pathrow[names(pathrow) == "path"]))
        meta$WRS_Row <- sprintf('%03i', as.numeric(pathrow[names(pathrow) == "row"]))
        solar <- meta_list$global_metadata$solar_angles
        meta$sunelev <- 90 - as.numeric(solar[names(solar) == "zenith"])
        meta$sunazimuth <- as.numeric(solar[names(solar) == "azimuth"])
        satellite <- meta_list$global_metadata$satellite
        stopifnot(grepl('^LANDSAT_', satellite))
        satellite_num <- str_extract(satellite, '[4578]')
        instrument <- meta_list$global_metadata$instrument
        if (instrument == "ETM") {
            instrument_char <- "E"
        } else if (instrument == "TM") {
            instrument_char <- "T"
        } else if (instrument == "OLI") {
            instrument_char <- "C"
        } else {
            stop("unrecognized instrument")
        }
        meta$short_name  <- paste0('L', satellite_num, instrument_char, 'SR')
    } else if (file_format == "L1T") {
        if (!grepl("_MTL.txt$", meta_file)) {
            stop("meta_file must be a *_MTL.txt file")
        }
        mtl_txt <- readLines(meta_file, warn=FALSE)
        aq_date <- get_mtl_item('DATE_ACQUIRED', mtl_txt)
        aq_time <- get_mtl_item('SCENE_CENTER_TIME', mtl_txt)
        meta$aq_date <- strptime(paste0(aq_date, "T", aq_time), format="%Y-%m-%dT%H:%M:%OSZ", tz="UTC")
        meta$WRS_Path <- sprintf('%03i', as.numeric(get_mtl_item('WRS_PATH', mtl_txt)))
        meta$WRS_Row <- sprintf('%03i', as.numeric(get_mtl_item('WRS_ROW', mtl_txt)))
        meta$sunelev <- as.numeric(get_mtl_item('SUN_ELEVATION', mtl_txt))
        meta$sunazimuth <- as.numeric(get_mtl_item('SUN_AZIMUTH', mtl_txt))
        # Build a shortname based on satellite and file_format that is consistent 
        # with the format of the CDR image shortnames
        satellite <- str_extract(get_mtl_item('SPACECRAFT_ID', mtl_txt), '[4578]')
        sensor_string <- str_extract(basename(file_base), '^((LT[45])|(LE7)|(LC8))')
        meta$short_name  <- paste0(substr(sensor_string, 1, 1),
                                   substr(sensor_string, 3, 3),
                                   substr(sensor_string, 2, 2), file_format)
    } else {
        stop(paste(file_format, "is not a recognized file_format"))
    }
    return(meta)
}

calc_cloud_mask <- function(mask_stack, mask_type, file_format, ...) {
    if (mask_type == 'fmask') {
        # Make a mask where clouds and gaps are coded as 1, clear as 0
        # fmask_band key:
        # 	0 = clear
        # 	1 = water
        # 	2 = cloud_shadow
        # 	3 = snow
        # 	4 = cloud
        # 	255 = fill value
        cloud_mask <- calc(mask_stack[[2]],
            fun=function(fmask) {
                return((fmask == 2) | (fmask == 4) | (fmask == 255))
            }, datatype='INT2S', ...)
    } else if (mask_type == '6S') {
        if (file_format == c('L1T')) {
            stop("can't use mask_type=\"6S\" with L1T imagery")
        }
        # This cloud mask includes the cloud_QA, cloud_shadow_QA, and 
        # adjacent_cloud_QA layers. Pixels in cloud, cloud shadow, or 
        # adjacent cloud are coded as 1.
        cloud_mask <- overlay(mask_stack[[1]], # fill_QA,
                              mask_stack[[3]], # cloud_QA, 
                              mask_stack[[4]], # cloud_shadow_QA, 
                              mask_stack[[5]], # adjacent_cloud_QA,
            fun=function(fill, clo, sha, adj) {
                return((fill == 255) | (clo == 255) | (sha == 255) | 
                       (adj == 255))
            }, datatype='INT2S', ...)

    } else if (mask_type == 'both') {
        if (file_format == c('L1T')) {
            stop("can't use mask_type=\"both\" with L1T imagery")
        }
        cloud_mask <- overlay(mask_stack[[2]], # fmask,
                              mask_stack[[3]], # cloud_QA, 
                              mask_stack[[4]], # cloud_shadow_QA, 
                              mask_stack[[5]], # adjacent_cloud_QA,
            fun=function(fmask, clo, sha, adj) {
                return((fmask == 2) | (fmask == 4) | (fmask == 255) | 
                       (clo == 255) | (sha == 255) | (adj == 255))
            }, datatype='INT2S', ...)
    } else {
        stop(paste0('unrecognized option "', cloud_mask, '" for mask_type"'))
    }
    return(cloud_mask)
}

#' @importFrom gdalUtils get_subdatasets gdalbuildvrt
build_band_vrt <- function(file_base, band_vrt_file, file_format) {
    image_bands <- c('band1', 'band2', 'band3', 'band4', 'band5', 'band7')
    image_band <- NULL # keep R CMD check happy
    if (file_format == "ESPA_CDR_OLD") {
        ls_file <- paste0(file_base, '.hdf')
        sds <- get_subdatasets(ls_file)
        band_sds <- foreach(image_band=image_bands) %do% {
            sds[grepl(paste0(':(', image_band, ')$'), sds)]
        }
        gdalbuildvrt(band_sds, band_vrt_file, separate=TRUE)
    } else if (file_format == "ESPA_CDR_HDF") {
        ls_file <- paste0(file_base, '.hdf')
        sds <- get_subdatasets(ls_file)
        band_sds <- foreach(image_band=image_bands) %do% {
            sds[grepl(paste0(image_band, '$'), sds)]
        }
        gdalbuildvrt(band_sds, band_vrt_file, separate=TRUE)
    } else if (file_format == "ESPA_CDR_ENVI") {
        envi_files <- paste0(paste(file_base, image_bands, sep='_sr_'), '.img')
        stopifnot(all(file_test('-f', envi_files)))
        gdalbuildvrt(envi_files, band_vrt_file, separate=TRUE)
    } else if (file_format == "ESPA_CDR_TIFF") {
        tiff_files <- paste0(paste(file_base, image_bands, sep='_sr_'), '.tif')
        stopifnot(all(file_test('-f', tiff_files)))
        gdalbuildvrt(tiff_files, band_vrt_file, separate=TRUE)
    } else if (file_format == "L1T") {
        ls_files <- dir(dirname(file_base),
                        pattern=paste0(basename(file_base), '_B[123457].((TIF)|(tif))$'),
                        full.names=TRUE)
        gdalbuildvrt(ls_files, band_vrt_file, separate=TRUE)

    } else {
        stop(paste(file_format, "is not a recognized file_format"))
    }
    return(image_bands)
}

#' @importFrom foreach foreach %do%
#' @importFrom gdalUtils get_subdatasets gdalbuildvrt
build_mask_vrt <- function(file_base, mask_vrt_file, file_format) {
    mask_band <- NULL # keep R CMD check happy
    if (file_format == "ESPA_CDR_OLD") {
        ls_file <- paste0(file_base, '.hdf')
        mask_bands <- c('fill_QA', 'cfmask_band', 'cloud_QA', 'cloud_shadow_QA', 
                        'adjacent_cloud_QA')
        sds <- get_subdatasets(ls_file)
        # Below is to support CDR imagery downloaded prior to late August 2014
        if (any(grepl("fmask_band", sds))) {
            warning('Using "fmask_band" instead of newer "cfmask_band" band name')
            mask_bands[grepl("^cfmask_band$", mask_bands)] <- "fmask_band"
        }
        mask_sds <- foreach(mask_band=mask_bands) %do% {
            sds[grepl(paste0(':(', mask_band, ')$'), sds)]
        }
        stopifnot(length(mask_sds) == 5)
        gdalbuildvrt(mask_sds, mask_vrt_file, separate=TRUE, srcnodata='None')
    } else if (file_format == "ESPA_CDR_TIFF") {
        mask_bands <- c('sr_fill_qa', 'cfmask', 'sr_cloud_qa', 
                        'sr_cloud_shadow_qa', 'sr_adjacent_cloud_qa')
        tiff_files <- paste0(paste(file_base, mask_bands, sep='_'), '.tif')
        stopifnot(all(file_test('-f', tiff_files)))
        stopifnot(length(tiff_files) == 5)
        gdalbuildvrt(tiff_files, mask_vrt_file, separate=TRUE, srcnodata='None')
    } else if (file_format == "ESPA_CDR_ENVI") {
        mask_bands <- c('sr_fill_qa', 'cfmask', 'sr_cloud_qa', 
                        'sr_cloud_shadow_qa', 'sr_adjacent_cloud_qa')
        envi_files <- paste0(paste(file_base, mask_bands, sep='_'), '.img')
        stopifnot(all(file_test('-f', envi_files)))
        stopifnot(length(envi_files) == 5)
        gdalbuildvrt(envi_files, mask_vrt_file, separate=TRUE, srcnodata='None')
    } else if (file_format == "ESPA_CDR_HDF") {
        ls_file <- paste0(file_base, '.hdf')
        mask_bands <- c('sr_fill_qa', 'cfmask', 'sr_cloud_qa', 
                        'sr_cloud_shadow_qa', 'sr_adjacent_cloud_qa')
        sds <- get_subdatasets(ls_file)
        mask_sds <- foreach(mask_band=mask_bands) %do% {
            sds[grepl(paste0(':(', mask_band, ')$'), sds)]
        }
        stopifnot(length(mask_sds) == 5)
        gdalbuildvrt(mask_sds, mask_vrt_file, separate=TRUE, srcnodata='None')
    } else if (file_format == "L1T") {
        mask_bands <- c('fill_QA', 'fmask_band')
        fmask_file <- dir(dirname(file_base),
                          pattern=paste0(basename(file_base), '_MTLFmask$'),
                          full.names=TRUE)
        # Calculate a QA mask file from the fmask file, since teamlucc expects 
        # this file as part of the mask stack.
        qa_mask_file <- extension(rasterTmpFile(), '.tif')
        # TODO: Check if this is proper coding - should it be reversed?
        qa_mask <- calc(raster(fmask_file),
                        fun=function(x) {
                            out <- x == 255
                            out[x == 255] <- 255
                            return(out)
                        }, datatype="INT2S", filename=qa_mask_file)
        # Note that allow_projection_difference is used below as GDAL thinks 
        # the two images have different projection systems, even though they 
        # are in identical projection systems.
        gdalbuildvrt(c(qa_mask_file, fmask_file), mask_vrt_file, separate=TRUE, 
                     allow_projection_difference=TRUE, srcnodata='None')
    } else {
        stop(paste(file_format, "is not a recognized file_format"))
    }
    return(mask_bands)
}


image_dirs=image_dirs[1]; prefix="VB"; tc = T;dem_path = "DEM/"
aoi = municipios.sf; output_path = landsat_folder; mask_type = "fmask"
mask_output = T; n_cpus = 1; cleartmp = T;hrs_temp=6; overwrite = T
of = "GTiff"; ext = "tif"; notify = print; verbose = T


    if (grepl('_', prefix)) {
        stop('prefix cannot contain underscores (_)')
    }
    if (tc && is.null(dem_path)) {
        stop('dem_path must be supplied if tc=TRUE')
    }
    if (tc && !file_test("-d", dem_path)) {
        stop(paste(dem_path, "does not exist"))
    }
    if (!is.null(output_path) && !file_test("-d", output_path)) {
        stop(paste(output_path, "does not exist"))
    }
    if (!is.null(aoi)) {
        if (length(aoi) > 1) {
            stop('aoi should be a SpatialPolygonsDataFrame of length 1')
        }
        stopifnot(is.projected(aoi))
    }

    ext <- gsub('^[.]', '', ext)
    
     image_dir <- NULL # Keep R CMD check happy
     
     image_dir=image_dirs[1]
     
      if (!file_test("-d", image_dir)) {
            stop(paste(image_dir, "does not exist"))
        }
        ls_files=detect_ls_files(image_dir)
    
    if (length(ls_files$file_bases) == 0) {
        stop('No Landsat files found')
    }

    if (any(ls_files$file_formats == "L1T")) {
        stopifnot(mask_type == 'fmask')
    } else {
        stopifnot(mask_type %in% c('fmask', '6S', 'both'))
    }

    file_base <- file_format <- NULL # keep R CMD check happy
    
    
    file_base=ls_files$file_bases
    file_format=ls_files$file_formats
    
    
     meta <- get_metadata(file_base, file_format)

        if (grepl('8', meta$short_name)) {
            warning(paste("Cannot process", file_base,
                          "- Landsat 8 imagery not yet supported"))
            return()
        }

        image_basename <- paste0(meta$WRS_Path, '-', meta$WRS_Row, '_',
                                 format(meta$aq_date, '%Y-%j'), '_', meta$short_name)

        if (is.null(output_path)) {
            this_output_path <- dirname(file_base)
        } else {
            this_output_path  <- output_path
        }

        if (tc) {
            output_filename <- file.path(this_output_path,
                                         paste0(prefix, '_', image_basename, 
                                                '_tc.', ext))
        } else {
            # Skip topographic correction, so don't append _tc to filename
            output_filename <- file.path(this_output_path,
                                         paste0(prefix, '_', image_basename, 
                                                '.', ext))
        }

        log_file <- file(paste0(file_path_sans_ext(output_filename), '_log.txt'), open="wt")
        msg <- function(txt) {
            cat(paste0(txt, '\n'), file=log_file, append=TRUE)
            print(txt)
        }

        timer <- Track_time(msg)
        timer <- start_timer(timer, label=paste('Preprocessing', image_basename))

        #######################################################################
        # Crop and reproject images to match the projection being used for this 
        # image.  This is either the projection of the aoi (if aoi is 
        # supplied), or the UTM zone of the centroid of this path and row.
        if (verbose) timer <- start_timer(timer, label='cropping and reprojecting')

        band_vrt_file <- extension(rasterTmpFile(), '.vrt')
        band_names <- build_band_vrt(file_base, band_vrt_file, file_format)
        mask_vrt_file <- extension(rasterTmpFile(), '.vrt')
        mask_band_names <- build_mask_vrt(file_base, mask_vrt_file, file_format)

        this_pathrow_poly <- pathrow_poly(as.numeric(meta$WRS_Path), 
                                          as.numeric(meta$WRS_Row))
        if (!is.null(aoi)) {
            to_srs <- proj4string(aoi)
        } else {
            to_srs <- utm_zone(this_pathrow_poly, proj4string=TRUE)
        }

        # Calculate minimum bounding box coordinates:
        this_pathrow_poly <- spTransform(this_pathrow_poly, CRS(to_srs))
        if (!is.null(aoi)) {
            # If an aoi IS supplied, match the image extent to that of the AOI 
            # cropped to the appropriate Landsat path/row polygon.
            crop_area <- gIntersection(this_pathrow_poly, aoi, byid=TRUE)
        } else {
            # If an aoi IS NOT supplied, match the image extent to the 
            # appropriate Landsat path/row polygon.
            crop_area <- this_pathrow_poly
        }
        out_te <- as.numeric(bbox(crop_area))

        # Ensure origin is set at 0,0
        to_res <- c(30, 30)
        out_te <- normalize_extent(out_te, to_res)

        image_stack_reproj_file <- extension(rasterTmpFile(), ext)
        image_stack <- gdalwarp(band_vrt_file,
                                dstfile=image_stack_reproj_file,
                                te=out_te, t_srs=to_srs, tr=to_res, 
                                r='cubicspline', output_Raster=TRUE, of=of, 
                                multi=TRUE, wo=paste0("NUM_THREADS=", n_cpus), 
                                overwrite=overwrite, ot='Int16')
        names(image_stack) <- band_names

        mask_stack_reproj_file <- extension(rasterTmpFile(), paste0('.', ext))
        mask_stack <- gdalwarp(mask_vrt_file,
                               dstfile=mask_stack_reproj_file,
                               te=out_te, t_srs=to_srs, tr=to_res, 
                               r='near', output_Raster=TRUE, of=of, 
                               multi=TRUE, wo=paste0("NUM_THREADS=", n_cpus), 
                               overwrite=overwrite, ot='Int16')
        # Can't just directly assign mask_bands as the names since the bands 
        # may have been read in different order from the HDF file
        names(mask_stack) <- mask_band_names

        if (verbose) timer <- stop_timer(timer, label='cropping and reprojecting')
        
         if (verbose) timer <- start_timer(timer, label='topocorr')
            slopeaspect_filename <- file.path(dem_path,
                                              paste0('slopeaspect_', 
                                                     meta$WRS_Path, '-', meta$WRS_Row, '.', ext))
            slopeaspect <- brick(slopeaspect_filename)
            
            if (!proj4comp(proj4string(image_stack), proj4string(slopeaspect))) {
                stop(paste0('slopeaspect and image_stack projections do not match.\nslopeaspect proj4string: ', 
                            proj4string(slopeaspect), '\nimage_stack proj4string: ',
                            proj4string(image_stack)))
            } else {
                # Projection strings are functionally identical - so make sure 
                # their textual representations are the same.
                proj4string(slopeaspect) <- proj4string(image_stack)
            }

            compareRaster(slopeaspect, image_stack, orig=TRUE)

            image_stack_mask <- calc_cloud_mask(mask_stack, mask_type, file_format)

            image_stack_masked <- image_stack
            image_stack_masked[image_stack_mask] <- NA
            if(image_stack_masked@data@min[1]==Inf){
              if (verbose) timer <- stop_timer(timer, label='topocorr')
            }
            NonMissings=sum(!is.na(getValues(image_stack_masked[[1]])))
             SlopeValues=length(unique(getValues(slope)[!is.na(getValues(x))]))
            if(NonMissings!=0){
              if (NonMissings > 500000) {
                  # Draw a sample for the Minnaert k regression. Note that 
                  # sampleRegular with cells=TRUE returns cell numbers in the 
                  # first column
                  sampleindices <- sampleRegular(image_stack_masked, size=500000, 
                                                 cells=TRUE)
                  sampleindices <- as.vector(sampleindices[, 1])
              } else {
                  sampleindices <- NULL
              }
              # Remember that slopeaspect layers are scaled to INT2S, but 
              # topographic_corr expects them as floats, so apply the scale factors 
              # used in auto_setup_dem
              slopeaspect_flt <- stack(raster(slopeaspect, layer=1) / 10000,
                                       raster(slopeaspect, layer=2) / 1000)
  }
  if(NonMissings==0){
  stop('All pixels are missing')
  }
  
 x= image_stack_masked;slopeaspect=slopeaspect_flt; sunelev=meta$sunelev; 
                                                 sunazimuth=meta$sunazimuth; 
                                                 method='minnaert_full'; 
                                                 asinteger=TRUE; scale_factor=1;
                                                 sampleindices=sampleindices;DN_min=NULL;DN_max=NULL
                                                 
                                                 
       if (!(class(x) %in% c('RasterLayer', 'RasterStack', 'RasterBrick'))) {
        stop('x must be a Raster* object')
    }
    if (!(class(slopeaspect) %in% c('RasterBrick', 'RasterStack'))) {
        stop('slopeaspect must be a RasterBrick or RasterStack object')
    }
    slope <- raster(slopeaspect, layer=1)
    aspect <- raster(slopeaspect, layer=2)
    stopifnot((sunelev >= 0) & (sunelev <= 90))
    stopifnot((sunazimuth >= 0) & (sunazimuth <= 360))

    # Set uncorr_layer to NULL to pass R CMD CHECK without notes
    uncorr_layer=NULL
    was_rasterlayer <- class(x) == "RasterLayer"
    # Convert x to stack so foreach will run
    x <- stack(x)          
    
                                           
    x=unstack(x)[[1]]; IL.epsilon=0.000001; slopeclass=NULL; coverclass=NULL; DN_min=NULL;  DN_max=NULL
    
    
    delta_seq=0.1
    while(length(slopeclass) <= 5 & delta_seq>0.001){
    ##New idea... lets make the slopeclass according to quantiles of the sampleindices
    
           if (is.null(sampleindices)) {
           SlopeValid=getValues(slope)[!is.na(getValues(x))]
           slopeclass <- setdiff(sort(unique(as.numeric(quantile(slope,seq(delta_seq,1,delta_seq),na.rm=T)))),0)
           }
           if (!is.null(sampleindices)) {
           SlopeValid=getValues(slope)[sampleindices][!is.na(getValues(x)[sampleindices])]
           slopeclass <- setdiff(sort(unique(as.numeric(quantile(SlopeValid,seq(delta_seq,1,delta_seq),na.rm=T)))),0)
           }
        

    if (is.null(sampleindices)) {
        counts <- raster::freq(raster::cut(slope, slopeclass,
                                           include.lowest=TRUE), 
                               useNA='no')
        # Eliminate empty bins:
        slopeclass <- slopeclass[c(TRUE, 1:(length(slopeclass) - 1) %in% counts[, 1])]
        counts <- counts[, 2]
    } else {
        counts <- as.numeric(table(cut(slope[sampleindices], slopeclass, 
                                       include.lowest=TRUE), useNA='no'))
    }
    
    # The [-1] below is because clean_intervals only needs the upper limits
     lower_limit=slopeclass[1]
    slopeclass <- clean_intervals(counts, slopeclass[-1], 100)
    delta_seq=delta_seq/2
    }
    if (length(slopeclass) <= 5) {
        stop('insufficient sample size to develop k model - try changing slopeclass or sampleindices')
    }
    slopeclass <- unique(sort(round(c(lower_limit, slopeclass),10))) #round to 10 decimal integers cause R is stupid and can't handle decimals well... to be fair no computer program can

    stopifnot(all((slopeclass >= 0) & slopeclass <= pi/2))

    # some inputs are in degrees, but we need radians
    stopifnot((sunelev >= 0) & (sunelev <= 90))
    stopifnot((sunazimuth >= 0) & (sunazimuth <= 360))
    sunzenith <- (pi/180) * (90 - sunelev)
    sunazimuth <- (pi/180) * sunazimuth

    IL <- .calc_IL(slope, aspect, sunzenith, sunazimuth, IL.epsilon)
    rm(aspect, sunazimuth)

    k_table <- .calc_k_table(x, IL, slope, sampleindices, slopeclass, 
                             coverclass, sunzenith)
    
    k_model <- with(k_table, bam(k ~ s(midpoint, k=length(midpoint) - 1), data=k_table))
    
    
    # If slope is greater than modeled range, use maximum of modeled range. If 
    # slope is less than modeled range, treat it as flat.
    slopeclass_max <- max(slopeclass)
    slopeclass_min <- min(slopeclass)
    slope <- calc(slope,
                  fun=function(vals) {
                      vals[vals > slopeclass_max] <- slopeclass_max
                      vals[vals < slopeclass_min] <- 0
                      return(vals)
                  })

    names(slope) <- 'midpoint'
    K.all <- predict(slope, k_model)
    K.all <- calc(K.all,
                  fun=function(vals) {
                      vals[vals > 1] <- 1
                      vals[vals < 0] <- 0
                      return(vals)
                  })

    # Perform correction
    xout <- overlay(x, IL, K.all,
                    fun=function(x_vals, IL_vals, K.all_vals) {
                        x_vals * (cos(sunzenith)/IL_vals) ^ K.all_vals
                    })
    # Don't correct flat areas
    xout[K.all == 0 & !is.na(K.all)] <- x[K.all == 0 & !is.na(K.all)]
     
    if ((!is.null(DN_min)) || (!is.null(DN_max))) {
        xout <- calc(xout, fun=function(vals) {
                        if (!is.null(DN_min)) vals[vals < DN_min] <- NA
                        if (!is.null(DN_max)) vals[vals > DN_max] <- NA
                        return(vals)
                     })
    }

    minnaert_data=list(classcoef=k_table, model=k_model, minnaert=xout, sampleindices=sampleindices)
    
    
    corr_layer <- minnaert_data$minnaert
    
    corr_img=corr_layer
     if (was_rasterlayer) corr_img <- corr_img[[1]]
    names(corr_img) <- paste0(names(x), 'tc')
    if (scale_factor != 1) {
        corr_img <- corr_img * scale_factor
    }
    if (asinteger) {
        corr_img <- round(corr_img)
    }