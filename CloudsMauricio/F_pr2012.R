codigoCompleto<-function(cores=NULL, index=NULL, year=2010, skip=NULL,dir_copy="C:/Users/admin/Copy/CloudsMauricio/",dir_imagenes="C:/DataNubes/",Orden_ESPA=NULL){
require(teamlucc)
require(maptools)
require(rgdal)
require(sp)
require(wrspathrow)
require(stringr)
require(foreach)
require(doParallel)
source(paste0(dir_copy,"/completedScenes.R"))

availableCores<-detectCores()-1
if (is.null(cores)) no_cores <- max(1, availableCores)
else no_cores<-min(cores, availableCores)
print(paste0("Number of cores= ", no_cores))

 # Initiate cluster
 cl <- makeCluster(no_cores)
registerDoParallel(cl)

#set seed for reproducible results
clusterSetRNGStream(cl,123)

setwd(dir_copy)
DirectorioImagenes=dir_imagenes

load("Input_Ordenes_PathRows.RData")

download_folder <- paste0(DirectorioImagenes,'Input/')
extract_folder <- paste0(DirectorioImagenes,'extract2012/')
final_folder <- paste0(DirectorioImagenes,'final/')
landsat_folder <- paste0(DirectorioImagenes,'out_landsat2012/')
   


#Aqui toca bajarse las imagenes 
#Vamos a usar python... este codigo llama a python y le dice donde guardar las cosas

emailstr<-strsplit(Orden_ESPA, split="-")[[1]][1]
system(paste0("python ", getwd(),"/espa_bulk_downloader_v1.0.0/download_espa_order.py -e ", 
                         emailstr," -o ",Orden_ESPA," -d ",DirectorioImagenes,"/Input"))



#########################

print(paste0("Total de PathRows= ", length(PathRowsUso)))

#TEMPORAL
if (!is.null(index)) PathRowsUso<-PathRowsUso[index]
else (PathRowsUso<-PathRowsUso[!(PathRowsUso %in% completedScenes(finalFolder=final_folder,ano=year))])

print(paste0("Faltan ", length(PathRowsUso), " PathRows por procesar para el ano ", year))

if(!is.null(skip)) PathRowsUso<-PathRowsUso[!(PathRowsUso %in% skip)]     

times<-list(length(PathRowsUso))


for(pathrow_loop in PathRowsUso){

  ptm<-proc.time()

##Primero se borra todo lo que este en el extract folder
unlink(dir(extract_folder, full.names = TRUE),recursive =T,force=T)
##Ahora se extraen las fotos de la escena (path/row) para un ano dado
espa_extract(paste0(download_folder,Orden_ESPA), extract_folder,start_date = as.Date(paste(year,'/1/1',sep="")), end_date = as.Date(paste(year,'/12/31',sep="")),pathrows=pathrow_loop)

##Ahora se le dice cuales son los archivos relevantes
image_dirs <- dir(extract_folder,
                  pattern='^[0-9]{3}-[0-9]{3}_[0-9]{4}-[0-9]{3}_((LT[45])|(LE7))$',
                  full.names=TRUE)
image_dirs2 <- dir(extract_folder,
                  pattern='^[0-9]{3}-[0-9]{3}_[0-9]{4}-[0-9]{3}_((LT[45])|(LE7))$',
                  full.names=F)
image_dirs2=gsub("-", "", image_dirs2)
image_dirs2=gsub("_", "", image_dirs2)
image_dirs2=gsub("LE7", "", image_dirs2)
#Cuales son esas ordenes
IndexOrders=sapply(image_dirs2,function(x)   grep(x, Order$Display.ID))
#Se ordenan en orden de cobertura de nubes
OrderScenes=sort.int(Order$Cloud.Cover[IndexOrders],index.return=T)$ix

CloudCover=Order$Cloud.Cover[IndexOrders][OrderScenes]
CloudCoverUL=Order$Cloud.Cover.Quad.Upper.Left[IndexOrders][OrderScenes]
CloudCoverUR=Order$Cloud.Cover.Quad.Upper.Right[IndexOrders][OrderScenes]
CloudCoverLL=Order$Cloud.Cover.Quad.Lower.Left[IndexOrders][OrderScenes]
CloudCoverLR=Order$Cloud.Cover.Quad.Lower.Right[IndexOrders][OrderScenes]

##Cual es la probabilidad de que un pixel dado no este cubierto por una nube en alguna escena? pues es 1 menos la probabilidad de que este cubierto en todas las escenas
##Bueno pues entonces quiero que esa probabilidad sea mayor a 0.99 para los pixeles de todos los cuadrantes
##Lo hacemos por cuadrantes para evitar siempre elegir la zona no nubosa de una escena o algo asi
EscenasUL=min(which(1-cumprod((CloudCoverUL/100))>0.99))
EscenasUR=min(which(1-cumprod((CloudCoverUR/100))>0.99))
EscenasLL=min(which(1-cumprod((CloudCoverLL/100))>0.99))
EscenasLR=min(which(1-cumprod((CloudCoverLR/100))>0.99))
##Este es el numero de fotos necesarias para que todos los pixeles tengan probabilidad de menos de 1% de estar cubierto en todas las fotos
EscenasNecesarias=max(EscenasUL,EscenasUR,EscenasLL,EscenasLR)

##We always want at least two images
if(EscenasNecesarias==1) EscenasNecesarias=EscenasNecesarias+1
#If possible, lets just keep the images we need
if(EscenasNecesarias<Inf) image_dirs=image_dirs[OrderScenes][1:EscenasNecesarias]
                
##Se borra lo que esa en copy tambien
unlink(dir(landsat_folder, full.names = TRUE),recursive =T,force=T)

##ahora le decimos que por favor las pre-procese

auto_preprocess_landsat(image_dirs, prefix="VB", tc = T,dem_path = "DEM/",
  aoi = municipios.sf, output_path = landsat_folder, mask_type = "fmask",
  mask_output = T, n_cpus = no_cores, cleartmp = T,hrs_temp=5, overwrite = T,
  of = "GTiff", ext = "tif", notify = print, verbose = T)

if(length(dir(landsat_folder, pattern="tc.tif", full.names = TRUE))==1){
ImagenFinal=brick(dir(landsat_folder, pattern="tc.tif", full.names = TRUE))
ImagenFinal_masks=brick(dir(landsat_folder, pattern="tc_masks.tif", full.names = TRUE))
mask_stack <- writeRaster(stack(ImagenFinal_masks[[1]], ImagenFinal_masks[[2]]),
                                      filename=paste0(final_folder,"/",year,"/",pathrow_loop,"_masks.tif"), 
                                      overwrite=T, datatype='INT2S')
image_stack <- writeRaster(ImagenFinal, filename=paste0(final_folder,"/",year,"/",pathrow_loop,".tif"), 
                                       overwrite=T, datatype='INT2S')
  }
if(length(dir(landsat_folder, pattern="tc.tif", full.names = TRUE))>1){                                       

cloud_filled_image <- auto_cloud_fill(landsat_folder, wrspath=as.numeric(substr(pathrow_loop,1,3)), wrsrow=as.numeric(substr(pathrow_loop,4,6)), start_date=as.Date(paste0(year,'/1/1')),end_date=as.Date(paste0(year,'/12/31')),out_name=paste0(final_folder,"/",year,"/",pathrow_loop),algorithm="simple",overwrite=T,DN_min = 0, DN_max = 10000,threshold = 1, max_iter = 6)
  }

print(paste0("Termino cloud fill ", pathrow_loop))
times[[which(PathRowsUso  == pathrow_loop)]]<- proc.time()-ptm
  
}


stopCluster(cl)
stopImplicitCluster()
return(times)

}

#Este es el script para correr la funcion y cosas similares
codigoCompleto(cores=6, index=NULL, year=2012, skip=c("010052"),dir_copy="/hsgs/projects/dupas/dupl/CloudsMauricio/",dir_imagenes="/hsgs/projects/dupas/dupl/DataNubes/",Orden_ESPA="latorrefabian@gmail.com-09142015-180905")