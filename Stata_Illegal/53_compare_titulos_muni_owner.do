use "C:\Users\santi\Dropbox\MineriaIlegal\CreatedData\Temporary\Codigos_DANE_sintilde.dta", clear
replace depto="Narino" if strpos(depto,"Nari")
replace depto=upper(depto)
replace municipio=upper(municipio)
save "C:\Users\santi\Dropbox\MineriaIlegal\CreatedData\Temporary\Codigos_DANE_MAYUSC.dta", replace


import delimited C:\Users\santi\Dropbox\MineriaIlegal\CreatedData\title_owner_scrapmuni.csv, clear
rename municipio munio
rename departamento deptoo
replace deptoo="NARINO" if deptoo=="NARIÃO"
replace munio="BOGOTA" if strpos(munio,"BOGOTA")
save C:\Users\santi\Dropbox\MineriaIlegal\CreatedData\Temporary\title_owner_scrapmuni.dta, replace

import delimited C:\Users\santi\Dropbox\MineriaIlegal\DataNubes\copy\TITULOS_PAIS.csv, clear
sort codigo_exp
drop if codigo_exp==codigo_exp[_n-1]


*** Extract all the munis of the title in different columns
gen munitemp=municipios
gen posslash=.
gen posguion=.
gen nmunis = length(municipios) - length(subinstr(municipios, "\", "", .))
qui sum nmunis
qui forval i = 1/`r(max)' {
replace posslash=strpos(munitemp,"\")
replace posslash=(length(munitemp)+1) if posslash==0
replace posguion=strpos(munitemp,"-")
gen munit`i'=substr(munitemp,1,posguion-1)
gen deptot`i'=substr(munitemp,posguion+1,posslash-posguion-1)
replace deptot`i'="NARINO" if strpos(deptot`i',"NARI?O")
replace deptot`i'="NORTE DE SANTANDER" if strpos(deptot`i',"NORTE SANTANDER")
replace munitemp=substr(munitemp,posslash+1,.)
}

merge 1:m codigo_exp using C:\Users\santi\Dropbox\MineriaIlegal\CreatedData\Temporary\title_owner_scrapmuni.dta
drop _merge
gen osamemunit=0
gen cod_osamemunit=0
qui forval i = 1/15 {
gen cod_osamemunit`i'=.
rename munit`i' municipio
rename deptot`i' depto
merge m:1 municipio depto using "C:\Users\santi\Dropbox\MineriaIlegal\CreatedData\Temporary\Codigos_DANE_MAYUSC.dta"
drop if _merge==2
drop _merge
rename municipio munit`i' 
rename depto deptot`i' 
rename codmpio codmpiot`i'
replace osamemunit=1 if munio==munit`i' & deptoo==deptot`i'
replace cod_osamemunit`i'=codmpiot`i' if munio==munit`i' & deptoo==deptot`i'
}

***Missing by muni stats on mines with local owner

collapse (max) persona osamemunit, by(codigo_exp)
*** Missing automatically report pctg mines with owner from the muni
