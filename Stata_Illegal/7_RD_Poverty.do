use "$base_out/panel_final.dta", replace
tabstat pct_roy, by(ind_after)
drop if nbi_2011<25
drop if nbi_2011>35
drop if ano<=2010
drop if ano>=2013
/*
egen bin_nbi_2011=cut(nbi_2011), at(25(1)35)
drop if missing(bin_nbi_2011)

*ind_after
collapse (mean) propmined_illegal_dt prop_illegal pctg_loss royalties ind_loser royalties_pc pct_roy, by(ind_after bin_nbi_2011)
*royalties
*/
* prop_illegal pctg_loss royalties ind_loser royalties_pc pct_roy 
foreach var in propmined_illegal_dt  {
	twoway ///
	scatter `var' nbi_2011 if ano<=2011 , graphregion(color(white))  mc(black) ms(square) msize(small) || ///
	scatter `var' nbi_2011 if ano>=2012 , mc(blue) || ///
	lfit `var' nbi_2011 if nbi_2011<30 & ano<=2011, lc(black) || ///
	lfit `var' nbi_2011 if nbi_2011>=30 & ano<=2011, lc(black) || ///
	lfit `var' nbi_2011 if nbi_2011<30 & ano>=2012 , lc(blue) || ///
	lfit `var' nbi_2011 if nbi_2011>=30 & ano>=2012 , lc(blue) xtitle("Poverty (NBI 2011)") xline(30) legend(order(1 2 ) rows(2))  

}
	graph export "$latexslides/VisualRD_Distancia_Lineal.pdf", as(pdf) replace

*** Poverty RD
qui eststo clear
qui eststo m1: xi: reghdfe propmined_illegal_dt ind_after  after_x_pctg_loss price_index_u   , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: reghdfe propmined_illegal_dt ind_after  after_x_pctg_loss price_index_u  if nbi_2011>25 & nbi_2011<35 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: reghdfe prop_illegal ind_after after_x_pctg_loss price_index_u price_index_m trend   , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: reghdfe prop_illegal ind_after after_x_pctg_loss price_index_u price_index_m trend  if nbi_2011>25 & nbi_2011<35 , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm



qui estout m1 m2 m3 m4 using "$latexslides/povertyRD_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( %9.2gc %9.2gc a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/povertyRD_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss ) prefoot(\midrule ) stats(  N N_clust ymean r2 , fmt(  %9.2gc %9.2gc a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

** For non-parametric estimation split in half winner and losers
local nlossgroups=4

qui forval i=1/`nlossgroups' {
gen D_lossG`i'=0
}
qui sum pctg_loss if pctg_loss<0, d
local lbloss=`r(p50)'
qui sum pctg_loss if pctg_loss>=0, d
local ubloss=`r(p50)'

replace D_lossG1=1 if pctg_loss<`lbloss'
replace D_lossG2=1 if pctg_loss>=`lbloss' & pctg_loss<0
replace D_lossG3=1 if pctg_loss<`ubloss' & pctg_loss>=0
replace D_lossG`nlossgroups'=1 if pctg_loss>=`ubloss' & pctg_loss!=.

forval i=1/`nlossgroups' {
gen after_x_lossG`i'=ind_after*D_lossG`i'
}

label var after_x_lossG2 "After X Bottom half winner"
label var after_x_lossG3 "After X Bottom half loser"
label var after_x_lossG4 "After X Top half loser"


qui eststo m5: xi: areg propmined_illegal_dt ind_after  after_x_lossG2-after_x_lossG4 price_index_u , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"
qui eststo m6: xi: areg prop_illegal ind_after  after_x_lossG2-after_x_lossG4 price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"

qui estout m1 m5 m3 m6 using "$latexslides/nonparametric_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss after_x_lossG* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m2 m5 m3 m4 m6 using "$latexpaper/nonparametric_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss after_x_lossG* price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace



*************************************
**Quantity index
**********************************
foreach x in carbon oro plata platino {
qui gen prod_parea_`x'=(prod_`x'/(areaprmined_sqkm-areaprilegal_sqkm))/10^6
}

qui eststo clear

** By product
qui eststo m3: xi: areg prod_parea_carbon ind_after  precio_carbon trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg prod_norm_gas ind_after  precio_Hidrocarburos trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m5: xi: areg prod_norm_petroleo ind_after  precio_Hidrocarburos trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m6: xi: areg prod_parea_oro ind_after  precio_oro trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m7: xi: areg prod_parea_plata ind_after  precio_plata trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m8: xi: areg prod_parea_platino ind_after  precio_platino trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m3 m4 m5 m6 m7 m8  using "$latexslides/prod_norm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after   ) prefoot(\midrule ) stats(N N_clust ymean r2 , fmt( %9.2gc %9.2gc a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m3 m4 m5 m6 m7 m8  using "$latexpaper/prod_norm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after  ) prefoot(\midrule ) stats(N N_clust ymean r2 , fmt( %9.2gc %9.2gc a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


***Production without normalizing
qui eststo clear
qui eststo m3: xi: areg prod_carbon ind_after after_x_loser precio_carbon trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg prod_gas ind_after after_x_loser precio_Hidrocarburos trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m5: xi: areg prod_petroleo ind_after after_x_loser precio_Hidrocarburos trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m6: xi: areg prod_oro ind_after after_x_loser precio_oro trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m7: xi: areg prod_plata ind_after after_x_loser precio_plata trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m8: xi: areg prod_platino ind_after after_x_loser precio_platino trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m3 m4 m5 m6 m7 m8  using "$latexslides/prod_raw_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser  ) prefoot(\midrule ) stats(N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m3 m4 m5 m6 m7 m8  using "$latexpaper/prod_raw_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser  ) prefoot(\midrule ) stats(N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

*With dept trends

qui eststo clear

qui eststo m1: xi: areg prop_illegal ind_after price_index_u  trend_depto* , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m2: xi: areg prop_illegal ind_after after_x_loser price_index_u  trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m3: xi: areg propmined_illegal ind_after  price_index_u trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m4: xi: areg propmined_illegal ind_after after_x_loser price_index_u  trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"
qui estout m1 m2 m3 m4 using "$latexslides/main_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("State Year FE" "State Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/main_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("State Year FE" "State Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

*Homicides armed groups
eststo clear
qui eststo m4: xi: areg tasa_homag after_x_pctg_loss price_index_u i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"


qui eststo m5: xi: areg tasa_homag after_x_pctg_loss price_index_u i.ano if ind_armedgroup_b2012==0 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui eststo m6: xi: areg tasa_homag after_x_pctg_loss price_index_u i.ano if ind_armedgroup_b2012==1 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui estout m4 m5 m6 using "$latexpaper/tasa_homag_creg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_pctg_loss price_index_u ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt(%fmt %9.2gc %9.2gc a2 a2 ) labels ("Time FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

e

/*
Size of illegal mines
xi: areg kstar_illegal after_x_col price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
graph dot (mean) kstar_illegal if country=="Col", over(ano) vertical ytitle("Total area (sqkm) mining titles by year")
*/


/*
**Define gap like Fisman and Wei
gen gap=log( areaprmined_sqkm)-log( areaprmined_sqkm- areaprilegal_sqkm)
replace gap=log( areaprmined_sqkm) if areaprmined_sqkm== areaprilegal_sqkm
xi: areg gap after_x_col   i.ano  , absorb(codmpio) vce(cluster codmpio)
*/

****************************
******* Updated above here
*******************************


qui sum lumpy_share if ano==2014, d
qui local temp=round(r(mean))
file open newfile using "$latexpaper/lumpy_mean.tex", write replace
file write newfile "`temp'"
file close newfile

qui local temp=round(r(p25))
file open newfile using "$latexpaper/lumpy_p25.tex", write replace
file write newfile "`temp'"
file close newfile

qui local temp=round(r(p50))
file open newfile using "$latexpaper/lumpy_p50.tex", write replace
file write newfile "`temp'"
file close newfile
file open newfile using "$latexslides/lumpy_p50.tex", write replace
file write newfile "`temp'"
file close newfile

qui local temp=round(r(p75))
file open newfile using "$latexpaper/lumpy_p75.tex", write replace
file write newfile "`temp'"
file close newfile

*Exclude Orinoquia/Amazonia
*qui drop if coddepto==18 | coddepto==50 | coddepto>80
**Replicate the graph from the theor framework

twoway (lfitci changeil_befaft  pctg_budget_roy_change if ano==2014 & changeil_befaft<1 & pctg_budget_roy_change>-25 & pctg_budget_roy_change<15, lw(thick) clc(orange) fi(inten10)) /// 
(scatter changeil_befaft  pctg_budget_roy_change if ano==2014 & changeil_befaft<1 & pctg_budget_roy_change>-25 & pctg_budget_roy_change<15, msymbol(circle_hollow) mc(dkgreen)) ///
 , scale(1.1) scheme(s1color) yscale(r(0 1)) ytitle("Change in percentage of area mined illegally")
graph export "$latexslides/emp_theo_pred.pdf", replace
graph export "$latexpaper/emp_theo_pred.pdf", replace

twoway (lfitci changeil_befaft  pctg_budget_roy_change if ano==2014 & changeil_befaft<1 & pctg_budget_roy_change>-25 & pctg_budget_roy_change<15, lw(thick) clc(orange) fi(inten10)) /// 
(scatter changeil_befaft  pctg_budget_roy_change if ano==2014 & changeil_befaft<1 & pctg_budget_roy_change>-25 & pctg_budget_roy_change<15, msymbol(circle_hollow) mc(dkgreen)) ///
 , scale(1.1) scheme(s1color) yscale(r(0 1)) ytitle("Change in percentage of area mined illegally")
graph export "$latexslides/emp_theo_pred_mined.pdf", replace
graph export "$latexpaper/emp_theo_pred_mined.pdf", replace

*************************************
** THE REASON I DO THIS REGRESSIONS FIRST IS THAT THEY ARE "FINAL"
** IE THEY DONT DEPEND ON THE ILLEGAL MINING PREDICTIONS
**********************************

*************************************
**Fiscal effort
**********************************

qui eststo clear


qui eststo m1: xi: areg pctg_tribut ind_after  after_x_loser trend   , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m2: xi: areg pctg_tribut after_x_loser  i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m3: xi: areg norm_tribut ind_after  after_x_loser trend   , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m4: xi: areg norm_tribut after_x_loser  i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui estout m1 m2 m3 m4 using "$latexslides/tribut_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after  after_x_loser  ) prefoot(\midrule ) stats(timefe ltrend N N_clust ymean r2 , fmt(%fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/tribut_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after  after_x_loser  ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*************************************
**Effects on Infant Mortality
**********************************

qui eststo clear

qui eststo m1: xi: areg tmi ind_after  after_x_loser trend  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m2: xi: areg tmi after_x_loser  i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m3: xi: areg tmi after_x_pctg_loss  i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m4: xi: areg tmi after_x_pctgloss_if after_x_pctgwin_if  i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui estout m1 m2 m3 m4  using "$latexslides/tmi_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after  after_x_loser after_x_pctg_loss after_x_pctgloss_if after_x_pctgwin_if) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4  using "$latexpaper/tmi_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after  after_x_loser after_x_pctg_loss after_x_pctgloss_if after_x_pctgwin_if) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace





graph dot (sum) analizedarea_sqkm, over(ano) vertical ytitle("Total area (sqkm) analyzed by year")
graph export "$latexslides/Analized_area.pdf", as(pdf) replace
graph export "$latexpaper/Analized_area.pdf", as(pdf) replace

graph dot (sum) areaprmined_sqkm , over(ano) vertical ytitle("Total area (sqkm) predicted as mined by year")
graph export "$latexslides/Predmined_area.pdf", as(pdf) replace
graph export "$latexpaper/Predmined_area.pdf", as(pdf) replace

graph dot (mean) prop_mined [fweight=round(areamuni_sqkm)] , over(ano) vertical ytitle("Fraction of analyzed area predicted as mined") 
graph export "$latexslides/Predmined_proparea.pdf", as(pdf) replace
graph export "$latexpaper/Predmined_proparea.pdf", as(pdf) replace


sort codmpio ano
gen newtitulosarea_sqkm= titulosarea_sqkm- titulosarea_sqkm[_n-1] if codmpio==codmpio[_n-1] & ano==(ano[_n-1]+1)
graph dot (sum) newtitulosarea_sqkm if country=="Col" , over(ano) vertical ytitle("New area (sqkm) mining titles by year")
graph export "$latexslides/newTitles_area.pdf", as(pdf) replace
graph export "$latexpaper/newTitles_area.pdf", as(pdf) replace

graph dot (sum) areaprilegal_sqkm , over(ano) vertical ytitle("Total area (sqkm) predicted illegal by year")
graph export "$latexslides/Predillegal_area.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_area.pdf", as(pdf) replace

graph dot (mean) prop_illegal [fweight=round(areamuni_sqkm)] , over(ano) vertical ytitle("Proportion of area mined illegaly")
graph export "$latexslides/Predillegal_proparea.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_proparea.pdf", as(pdf) replace

graph dot (mean) propmined_illegal [fweight=round(areamuni_sqkm)] , over(ano) vertical ytitle("Proportion of mined area mined illegaly")
graph export "$latexslides/Predillegal_propmined.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propmined.pdf", as(pdf) replace

*Adjusted predictions with TPR FPR formula
graph dot (mean) propadj_illegal [fweight=round(areamuni_sqkm)] , over(ano) vertical ytitle("Proportion of area mined illegaly")
graph export "$latexslides/Predillegal_propadjarea.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propadjarea.pdf", as(pdf) replace

graph dot (mean) propadjmined_illegal [fweight=round(areamuni_sqkm)] , over(ano) vertical ytitle("Proportion of mined area mined illegaly")
graph export "$latexslides/Predillegal_propadjmined.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propadjmined.pdf", as(pdf) replace

******************************
*************************************
**Summary statistics
**********************************
**********************************

qui sum ind_armedgroup_b2012 if ano==2014 & prop_illegal!=.
qui local temp=round(r(mean)*100)
file open newfile using "$latexpaper/pctgag.tex", write replace
file write newfile "`temp'"
file close newfile

qui sum cm_pctgmines_illegal if ano==2014 & prop_illegal!=.
qui local temp=round(r(mean)*100)
file open newfile using "$latexpaper/pctg_cmim_muni.tex", write replace
file write newfile "`temp'"
file close newfile



qui eststo clear
qui estpost tabstat price_index_u areaprmined_sqkm areaprilegal_sqkm prop_illegal propmined_illegal  if propmined_illegal!=., statistics(mean p50 sd min max count) columns(statistics)
qui esttab using "$latexslides/sumstats_muniyear.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs
qui esttab using "$latexpaper/sumstats_muniyear.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

qui sum prop_illegal if propmined_illegal!=.
qui local temp=round(r(mean))
file open newfile using "$latexpaper\prop_illegal.tex", write replace
file write newfile "`temp'"
file close newfile

qui sum propmined_illegal if propmined_illegal!=.
qui local temp=round(r(mean))
file open newfile using "$latexpaper\propmined_illegal.tex", write replace
file write newfile "`temp'"
file close newfile






** Sum stats of regression vars
eststo clear
qui xi: my_ptest prop_illegal_bef prop_illegal_aft pctg_budget_roy_change ///
 ind_armedgroup_b2012_munil prop_illegal_bef_ag prop_illegal_aft_ag, by(ind_loser) 

esttab using "$latexslides/sumstats_regvar.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

eststo clear
qui xi: my_ptest prop_illegal_bef prop_illegal_aft prop_illegal_bef_ag prop_illegal_aft_ag, by(ind_loser) 
esttab using "$latexpaper/sumstats_regvar.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 






*************************************
**Basic diff-diff VISUAL
**********************************


preserve


qui eststo clear
qui eststo t1: reg prop_illegal c.ano#c.ind_loser ind_loser ano  price_index_u price_index_m  , vce(cluster codmpio)
qui estadd ysumm
estadd local munife "No"
estadd local timefe "No"

qui eststo t2: xi: areg prop_illegal c.ano#c.ind_loser i.ano  price_index_u price_index_m  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
qui estout t1 t2 using "$latexslides/trends.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( c.ano#c.ind_loser ind_loser ano)  stats(munife timefe N N_clust ymean r2 , fmt(%fmt %fmt a2 a2 a2 a2 ) labels ("Municipality FE" "Time FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
restore



preserve
keep if ind_muni_allyears==1
collapse (mean) prop_illegal, by(ano ind_loser)

label var prop_illegal "Fraction of area mined illegal"
twoway (line prop_illegal ano if ind_loser==0, sort lw(thick))(line prop_illegal ano if ind_loser==1, sort lw(thick) ), title("Evolution of fraction of area mined illegally")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Winners") label(2 "Losers"))
graph export "$latexslides/diff_in_diff_basico_munially.pdf", replace
graph export "$latexpaper/diff_in_diff_basico_munially.pdf", replace
restore

preserve
keep if ind_armedgroup_b2012==0
collapse (mean) prop_illegal, by(ano ind_loser)

label var prop_illegal "Fraction of area mined illegal"
twoway (line prop_illegal ano if ind_loser==1, sort) (line prop_illegal ano if ind_loser==0, sort), xline(2011.5) legend(label(1 "Losers") label(2 "Winners"))
graph export "$latexslides/diff_in_diff_nag.pdf", replace
graph export "$latexpaper/diff_in_diff_nag.pdf", replace
restore

preserve
keep if ind_armedgroup_b2012==1
collapse (mean) prop_illegal, by(ano ind_loser)

label var prop_illegal "Fraction of area mined illegal"
twoway (line prop_illegal ano if ind_loser==1, sort) (line prop_illegal ano if ind_loser==0, sort), xline(2011.5) legend(label(1 "Losers") label(2 "Winners"))
graph export "$latexslides/diff_in_diff_ag.pdf", replace
graph export "$latexpaper/diff_in_diff_ag.pdf", replace
restore

preserve
collapse (mean) tmi, by(ano ind_loser)

label var tmi "Infant mortality rate"
twoway (line tmi ano if ind_loser==1, sort) (line tmi ano if ind_loser==0, sort), xline(2011.5) legend(label(1 "Losers") label(2 "Winners"))
graph export "$latexslides/diff_in_diff_tmi.pdf", replace
graph export "$latexpaper/diff_in_diff_tmi.pdf", replace
restore
/*
preserve
collapse (median) prop_illegal, by(ano ind_loser)
twoway (line prop_illegal ano if ind_loser==1, sort) (line prop_illegal ano if ind_loser==0, sort), legend(label(1 "Affected") label(2 "Not Affected"))
graph export "$latexslides/diff_in_diff_basico_median.pdf", replace
restore
*/






*************************************
**Main regression
**********************************

qui eststo clear

qui eststo m1: reg prop_illegal ind_after ind_loser after_x_loser price_index_u price_index_m trend , vce(cluster codmpio)
qui estadd ysumm
estadd local munife "No"
estadd local timefe "No"


qui eststo m2: xi: areg prop_illegal ind_after  after_x_loser price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local sampleused "All"
matrix tempm=e(b)
qui sum prop_illegal if e(sample)



local mainresult=tempm[1,1]
local mainloserresult=tempm[1,2]
qui local mainresrelmean=round(100*`mainresult'/`r(mean)')
qui local mainloserresrelmean=round(100*`mainloserresult'/`r(mean)')
qui local mainmean=round(`r(mean)'*100)/100
qui local mainresult=round(`mainresult'*100)/100
qui local mainloserresult=round(`mainloserresult'*100)/100

file open newfile using "$latexslides/mainmean.tex", write replace
file write newfile "`mainmean'"
file close newfile
file open newfile using "$latexpaper/mainmean.tex", write replace
file write newfile "`mainmean'"
file close newfile
file open newfile using "$latexslides/mainresult.tex", write replace
file write newfile "`mainresult'"
file close newfile
file open newfile using "$latexpaper/mainresult.tex", write replace
file write newfile "`mainresult'"
file close newfile
file open newfile using "$latexslides/mainloserresult.tex", write replace
file write newfile "`mainloserresult'"
file close newfile
file open newfile using "$latexpaper/mainloserresult.tex", write replace
file write newfile "`mainloserresult'"
file close newfile
file open newfile using "$latexslides/mainloserresrelmean.tex", write replace
file write newfile "`mainloserresrelmean'"
file close newfile
file open newfile using "$latexpaper/mainloserresrelmean.tex", write replace
file write newfile "`mainloserresrelmean'"
file close newfile
file open newfile using "$latexslides/mainresrelmean.tex", write replace
file write newfile "`mainresrelmean'"
file close newfile
file open newfile using "$latexpaper/mainresrelmean.tex", write replace
file write newfile "`mainresrelmean'"
file close newfile

qui eststo m3: xi: areg prop_illegal after_x_loser price_index_u price_index_m i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m4: reg propmined_illegal ind_after ind_loser after_x_loser price_index_u price_index_m  , vce(cluster codmpio)
qui estadd ysumm
estadd local munife "No"
estadd local timefe "No"

qui eststo m5: xi: areg propmined_illegal ind_after  after_x_loser price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local weights "No"
estadd local ltrend "Yes"

qui eststo m5a: xi: areg propmined_illegal c.ind_after##c.ind_loser price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local weights "No"
estadd local ltrend "Yes"

qui eststo m6: xi: areg propmined_illegal after_x_loser price_index_u price_index_m i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"




/*
qui tab depto, gen(dummy_dep)
forval i=1/29{
qui gen trend_dep`i'=trend*dummy_dep`i'
}
*/


**Heterogeneous results causal forest

qui eststo m2C: xi: areg prop_illegal ind_after after_x_loser   price_index_u   trend if causfor_sample=="C" , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local sampleused "C"

qui eststo mCF: xi: areg prop_illegal ind_after after_x_ind_leaf2 after_x_loser after_x_loser_x_ind_leaf2  price_index_u   trend if causfor_sample=="C" , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local sampleused "C"

qui estout m2  m2C mCF using "$latexslides/CFleaf_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_ind_leaf2 after_x_loser after_x_loser_x_ind_leaf2 ) prefoot(\midrule ) stats(sampleused N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Sample" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m2  m2C mCF using "$latexpaper/CFleaf_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_ind_leaf2 after_x_loser after_x_loser_x_ind_leaf2 ) prefoot(\midrule ) stats(sampleused N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Sample" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

**By importance of mining before reform

qui eststo mint1: xi: areg prop_illegal ind_after price_index_u price_index_m trend, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo mint2: xi: areg prop_illegal ind_after after_x_importance price_index_u price_index_m trend, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo mint3: xi: areg prop_illegal after_x_importance price_index_u price_index_m i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"
qui estout mint1 mint2 mint3 using "$latexslides/importance_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_importance  ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout mint1 mint2 mint3 using "$latexpaper/importance_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_importance  ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*************************************
**Separated by municipalities in conflict
**********************************

*New single regression
gen ind_after2=ind_after
gen ind_loser2=ind_loser

label var ind_after2 "After"
label var ind_loser2 "Loser"


qui eststo m7a: xi: areg prop_illegal c.ind_after##c.ind_loser price_index_u price_index_m  trend  if ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m7b: xi: areg prop_illegal c.ind_after##c.ind_loser price_index_u price_index_m  trend  if ind_armedgroup_b2012==0, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m7c: xi: areg prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.ind_armedgroup_b2012 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m8a: xi: areg propmined_illegal c.ind_after##c.ind_loser price_index_u price_index_m trend  if ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m8b: xi: areg propmined_illegal c.ind_after##c.ind_loser price_index_u price_index_m trend  if ind_armedgroup_b2012==0, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m8c: xi: areg propmined_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m trend)##c.ind_armedgroup_b2012  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"



qui estout m2 m7a m7b m7c using "$latexslides/violence_reg_prop.tex" , rename(c.ind_after2#c.ind_loser2#c.ind_armedgroup_b2012 c.ind_after#c.ind_loser c.ind_after2#c.ind_armedgroup_b2012 c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m2 m7a m7b m7c using "$latexpaper/violence_reg_prop.tex" , rename(c.ind_after2#c.ind_loser2#c.ind_armedgroup_b2012 c.ind_after#c.ind_loser c.ind_after2#c.ind_armedgroup_b2012 c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


qui estout m5 m8a m8b m8c using "$latexslides/violence_reg_propmined.tex" , rename(c.ind_after2#c.ind_loser2#c.ind_armedgroup_b2012 c.ind_after#c.ind_loser c.ind_after2#c.ind_armedgroup_b2012 c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m5 m8a m8b m8c using "$latexpaper/violence_reg_propmined.tex" , rename(c.ind_after2#c.ind_loser2#c.ind_armedgroup_b2012 c.ind_after#c.ind_loser c.ind_after2#c.ind_armedgroup_b2012 c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

** Left win guerrillas (Farc,Eln) Right wing (AUC, Bacrim)

gen Left=0
gen Right=0
replace Left=1 if ind_rightwag_b2012!=1 & ind_leftwag_b2012==1
replace Right=1 if ind_rightwag_b2012==1 & ind_leftwag_b2012!=1
replace Left=. if ind_rightwag_b2012==1 & ind_leftwag_b2012==1
replace Right=. if ind_rightwag_b2012==1 & ind_leftwag_b2012==1

qui eststo m9a: xi: areg prop_illegal c.ind_after##c.ind_loser  price_index_u price_index_m trend  if Left==1 & ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m9b: xi: areg prop_illegal c.ind_after##c.ind_loser  price_index_u price_index_m trend  if Left==0 & ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m9c: xi: areg prop_illegal c.(c.ind_after2##c.ind_loser2  price_index_u price_index_m trend)##c.Left if ind_armedgroup_b2012==1  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"


qui eststo m10a: xi: areg propmined_illegal c.ind_after##c.ind_loser  price_index_u price_index_m trend  if Left==1 & ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m10b: xi: areg propmined_illegal c.ind_after##c.ind_loser  price_index_u price_index_m trend  if Left==0 & ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m10c: xi: areg propmined_illegal c.(c.ind_after2##c.ind_loser2  price_index_u price_index_m trend)##c.Left if ind_armedgroup_b2012==1  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui estout m2 m9a m9b m9c using "$latexslides/violencerwlw_reg_prop.tex" , rename(c.ind_after2#c.ind_loser2#c.Left c.ind_after#c.ind_loser c.ind_after2#c.Left c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule )  stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m2 m9a m9b m9c using "$latexpaper/violencerwlw_reg_prop.tex" , rename(c.ind_after2#c.ind_loser2#c.Left c.ind_after#c.ind_loser c.ind_after2#c.Left c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule )  stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


qui estout m5 m10a m10b m10c using "$latexslides/violencerwlw_reg_propmined.tex" , rename(c.ind_after2#c.ind_loser2#c.Left c.ind_after#c.ind_loser c.ind_after2#c.Left c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser)  prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m5 m10a m10b m10c using "$latexpaper/violencerwlw_reg_propmined.tex" , rename(c.ind_after2#c.ind_loser2#c.Left c.ind_after#c.ind_loser c.ind_after2#c.Left c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule )  stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*** Institutional strength


qui eststo het1: xi: areg prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.notarias_pc_2005 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo het2: xi:areg prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.pehosclinc_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo het3: xi: areg prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.peofdereci_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo het4: xi: areg prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.petotalins_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
qui estadd local munife "Yes"
qui estadd local timefe "No"
qui estadd local ltrend "Yes"

qui estout het1 het2 het3 het4 using "$latexslides/inst_strength.tex" , rename(c.ind_after2#c.ind_loser2#c.notarias_pc_2005 Triple c.ind_after2#c.notarias_pc_2005 Doble ///
c.ind_after2#c.ind_loser2#c.pehosclinc_pc_2002 Triple c.ind_after2#c.pehosclinc_pc_2002 Doble ///
c.ind_after2#c.ind_loser2#c.peofdereci_pc_2002 Triple c.ind_after2#c.peofdereci_pc_2002 Doble ///
c.ind_after2#c.ind_loser2#c.petotalins_pc_2002 Triple c.ind_after2#c.petotalins_pc_2002 Doble ) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
varlabels(Triple "After $\times$ Loser $\times$ Institutional" Doble "After $\times$ Institutional")  ///
keep( ind_after2 c.ind_after2#c.ind_loser2 Doble Triple) prefoot(\midrule )  stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*** Institutional strength 2
foreach var in notarias_pc_2005 pehosclinc_pc_2002 peofdereci_pc_2002 petotalins_pc_2002{
qui sum `var',d
qui gen D_`var'=(`var'>r(p50)) if !missing(`var')
}

qui eststo het1: xi: areg prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.D_notarias_pc_2005 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
qui estadd local munife "Yes"
qui estadd local timefe "No"
qui estadd local ltrend "Yes"

qui eststo het2: xi:areg prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.D_pehosclinc_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo het3: xi: areg prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.D_peofdereci_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo het4: xi: areg prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.D_petotalins_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

estout het1 het2 het3 het4 using "$latexslides/inst_strength2.tex" , rename(c.ind_after2#c.ind_loser2#c.D_notarias_pc_2005 Triple c.ind_after2#c.D_notarias_pc_2005 Doble ///
c.ind_after2#c.ind_loser2#c.D_pehosclinc_pc_2002 Triple c.ind_after2#c.D_pehosclinc_pc_2002 Doble ///
c.ind_after2#c.ind_loser2#c.D_peofdereci_pc_2002 Triple c.ind_after2#c.D_peofdereci_pc_2002 Doble ///
c.ind_after2#c.ind_loser2#c.D_petotalins_pc_2002 Triple c.ind_after2#c.D_petotalins_pc_2002 Doble ) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
varlabels(Triple "After $\times$ Loser $\times$ Institutional" Doble "After $\times$ Institutional")  ///
keep( ind_after2 c.ind_after2#c.ind_loser2 Doble Triple) prefoot(\midrule )  stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*************************************
**Separated by satellite prone (open pit vs underground 
**********************************

qui eststo m9: xi: areg prop_illegal ind_after after_x_loser price_index_u trend if  satelite_prone==1 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m10: xi: areg prop_illegal ind_after after_x_loser price_index_u trend if   satelite_prone==0 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui estout m2 m9 m10 using "$latexslides/satprone_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m2 m9 m10 using "$latexpaper/satprone_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


************************
** Continuous measure
********************


*With dept trends

qui eststo clear

qui eststo m1: xi: areg prop_illegal ind_after after_x_pctg_loss price_index_u  trend_depto* , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m2: xi: areg prop_illegal after_x_pctg_loss price_index_u i.anodepto  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m3: xi: areg propmined_illegal ind_after  after_x_pctg_loss price_index_u trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m4: xi: areg propmined_illegal after_x_pctg_loss price_index_u i.anodepto  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"
qui estout m1 m2 m3 m4 using "$latexslides/continuous_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("State Year FE" "State Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/continuous_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("State Year FE" "State Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*************************************
**Homicides armed groups as dependent variable
**********************************

eststo clear
qui eststo m4: xi: areg homi_totalag after_x_loser price_index_u i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"


qui eststo m5: xi: areg homi_totalag after_x_loser price_index_u i.ano  if ind_armedgroup_b2012==0 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui eststo m6: xi: areg homi_totalag after_x_loser price_index_u i.ano if ind_armedgroup_b2012==1 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui estout m4 m5 m6 using "$latexslides/homicidesag_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_loser price_index_u ) prefoot(\midrule ) stats(timefe N N_clust ymean r2 , fmt( %fmt a2 a2 a2 a2 ) labels ( "Time FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m4 m5 m6 using "$latexpaper/homicidesag_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_loser price_index_u ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ( "Time FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

*Using pct loss instead of dummy
eststo clear
qui eststo m4: xi: areg homi_totalag after_x_pctg_loss price_index_u i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"


qui eststo m5: xi: areg homi_totalag after_x_pctg_loss price_index_u i.ano  if ind_armedgroup_b2012==0 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui eststo m6: xi: areg homi_totalag after_x_pctg_loss price_index_u i.ano if ind_armedgroup_b2012==1 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui estout m4 m5 m6 using "$latexpaper/homicidesag_creg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_pctg_loss price_index_u ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Time FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

*************************************
**Homicides RATE armed groups as dependent variable
**********************************
eststo clear
qui eststo m4: xi: areg tasa_homag after_x_loser price_index_u i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"


qui eststo m5: xi: areg tasa_homag after_x_loser price_index_u i.ano if ind_armedgroup_b2012==0 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui eststo m6: xi: areg tasa_homag after_x_loser price_index_u i.ano if ind_armedgroup_b2012==1 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui estout m4 m5 m6 using "$latexslides/tasa_homag_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_loser ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Time FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m4 m5 m6 using "$latexpaper/tasa_homag_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_loser price_index_u ) prefoot(\midrule ) stats(timefe N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Time FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

*/



** Calculate bounds of ommited variable bias
** See Altonji et al(2005) or Oster(2013)
qui reg prop_illegal ind_after after_x_loser price_index_u price_index_m trend i.codmpio
matrix tempm=e(b)
local ols_after=tempm[1,1]
local ols_afteraffi=tempm[1,2]

*Altonji indicator after
psacalc ind_after set
qui local absdif=abs(`r(output)'-`ols_after')
qui local altonjilb_after=round((`ols_after'-`absdif')*100)/100
file open newfile using "$latexslides/altonjilb_after.tex", write replace
file write newfile "`altonjilb_after'"
file close newfile
file open newfile using "$latexpaper/altonjilb_after.tex", write replace
file write newfile "`altonjilb_after'"
file close newfile

qui local altonjiub_after=round((`ols_after'+`absdif')*100)/100
file open newfile using "$latexslides/altonjiub_after.tex", write replace
file write newfile "`altonjiub_after'"
file close newfile
file open newfile using "$latexpaper/altonjiub_after.tex", write replace
file write newfile "`altonjiub_after'"
file close newfile

*Altonji indicator afterXLoser
psacalc after_x_loser set

qui local absdif=abs(`r(output)'-`ols_afteraffi')
qui local altonjilb_afteraffi=round((`ols_afteraffi'-`absdif')*100)/100
file open newfile using "$latexslides/altonjilb_afteraffi.tex", write replace
file write newfile "`altonjilb_afteraffi'"
file close newfile
file open newfile using "$latexpaper/altonjilb_afteraffi.tex", write replace
file write newfile "`altonjilb_afteraffi'"
file close newfile

qui local altonjiub_afteraffi=round((`ols_afteraffi'+`absdif')*100)/100
file open newfile using "$latexslides/altonjiub_afteraffi.tex", write replace
file write newfile "`altonjiub_afteraffi'"
file close newfile
file open newfile using "$latexpaper/altonjiub_afteraffi.tex", write replace
file write newfile "`altonjiub_afteraffi'"
file close newfile
*/

***** Individual regressions by municipality

/*
qui gen beta_aft_muni=.
qui gen std_aft_muni=.
egen muni_id = group(codmpio)

qui su muni_id, meanonly
local nmunis=`r(max)'
	forvalues i=1/`nmunis' {
				di "done with `i'"
				
					qui reg prop_illegal ind_after price_index_u  trend  if muni_id==`i' 

					qui replace beta_aft_muni=_b[ind_after] if muni_id==`i' 
					qui mat auch=e(V)
					qui replace std_aft_muni=(auch[3,3])^0.5 if muni_id==`i' 
					 
				 }
				
preserve
	
collapse  ind_armedgroup_b2012 ind_minerpot ind_Hidrocarburos_ever ind_metalespreciosos_ever ind_loser pctg_budget_roy_change beta_aft_muni std_aft_muni, by(codmpio municipio depto)
*/



** Calculate bounds of ommited variable bias to optimal controls
** It still not clear if we should apply both tests given that Lasso assumes w ehave everything

/*
qui merge 1:1 codmpio ano using "$base_out/Temporary/Lasso_vars.dta"
drop _merge
qui reg propmined_illegal ind_after after_x_loser ${yvSel} i.codmpio
matrix tempm=e(b)
local mainresult=tempm[1,1]
local afteraffiresult=tempm[1,2]
qui local altonjilb_after=round(`mainresult'*100)/100
file open newfile using "$latexslides/altonjilassolb_after.tex", write replace
file write newfile "`altonjilb_after'"
file close newfile
psacalc ind_after set
qui local altonjiub_after=round(`r(output)'*100)/100
file open newfile using "$latexslides/altonjilassoub_after.tex", write replace
file write newfile "`altonjiub_after'"
file close newfile

qui local altonjilb_afteraffi=round(`afteraffiresult'*100)/100
file open newfile using "$latexslides/altonjilassolb_afteraffi.tex", write replace
file write newfile "`altonjilb_afteraffi'"
file close newfile
psacalc after_x_loser set
qui local altonjiub_afteraffi=round(`r(output)'*100)/100
file open newfile using "$latexslides/altonjilassoub_afteraffi.tex", write replace
file write newfile "`altonjiub_afteraffi'"
file close newfile
*/
