
***Calculos Censo
/*

use "$base_in/CensoMinero/cm_bd_final.dta", clear
gen cielo_abierto=ag_ue_07-1
gen illegal=tipo_mina-1
gen orokg_ha_ano=12*(at_04/ag_ue_06)/1000 if at_pyr_03_01_07!=. & at_04_m==1
*Cut crazy values
sum orokg_ha_ano, d
sum orokg_ha_ano if orokg_ha_ano<360
sum orokg_ha_ano if orokg_ha_ano<360 & illegal==1
sum orokg_ha_ano if orokg_ha_ano<360 & illegal==0
sum orokg_ha_ano if orokg_ha_ano<360 & illegal==1 & cielo_abierto==1
sum orokg_ha_ano if orokg_ha_ano<360 & illegal==0 & cielo_abierto==1

gen coalton_ha_ano=12*at_04/ag_ue_06 if at_pyr_01==1 & at_04_m==3
sum coalton_ha_ano, d
sum coalton_ha_ano if coalton_ha_ano<10200
*/


use "$base_out/Temporary/panel_prillegalMi.dta", clear

gen ind_gold=(mineral==1)
replace ind_after=1 if ano>=2011
replace afterxgold=ind_gold*ind_after
replace afterxregalia=regalia_mine*ind_after


********************
* TABLE 3
*****************
*DD mineral


preserve
keep if mineral==1 | mineral==4
mat ddmat=J(6,3,0)

reg propmined_illegal ind_after ind_gold afterxgold

matrix A=e(b)
matrix B=e(V)

mat ddmat[5,3] =round(A[1,3],0.01)
mat ddmat[6,3] =round(sqrt(B[3,3]),0.01)

keep if e(sample)==1

sum propmined_illegal if ind_after==0 & ind_gold==0 
mat ddmat[1,1] =round(r(mean),0.01)
mat ddmat[2,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_gold==0
mat ddmat[3,1] =round(r(mean),0.01)
mat ddmat[4,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==0 & ind_gold==1 
mat ddmat[1,2] =round(r(mean),0.01)
mat ddmat[2,2] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_gold==1 &  e(sample)==1
mat ddmat[3,2] =round(r(mean),0.01)
mat ddmat[4,2] = round(r(sd),0.01)

reg propmined_illegal ind_after if ind_gold==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,1] =round(A[1,1],0.01)
mat ddmat[6,1] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_after if ind_gold==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,2] =round(A[1,1],0.01)
mat ddmat[6,2] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_gold if ind_after==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[1,3] =round(A[1,1],0.01)
mat ddmat[2,3] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_gold if ind_after==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[3,3] =round(A[1,1],0.01)
mat ddmat[4,3] =round(sqrt(B[1,1]),0.01)

svmat ddmat
gen c=_n if ddmat3!=.
gen odd=mod(c,2)

gen t1=abs((ddmat3[_n]/ddmat3[_n+1])*odd)
gen start_col="***" if t1 >= 2.576 & t1!=. 
replace start_col="**" if t1 < 2.576 & t1 >=2.326 & t1!=.
replace start_col="*" if t1 < 2.326 & t1 >=1.96 & t1!=.
drop t1

gen t_col1=abs((ddmat1[5]/ddmat1[6])*odd) if c==5
gen start_col1="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col1="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col1="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen t_col1=abs((ddmat2[5]/ddmat2[6])*odd) if c==5
gen start_col2="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col2="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col2="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen t_col1=abs((ddmat2[5]/ddmat2[6])*odd) if c==5
gen start_col3="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col3="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col3="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen brace_left="(" if odd!=.
gen brace_right=")" if odd!=.

egen concat_se1=concat(brace_left ddmat1 brace_right) if odd==0 
egen concat_se2=concat(brace_left ddmat2 brace_right) if odd==0 
egen concat_se3=concat(brace_left ddmat3 brace_right) if odd==0 
drop brace_left brace_right

gen ddmat1_odd=ddmat1 if odd==1
egen concat_col1=concat(ddmat1_odd start_col1)
replace concat_col1=concat_se1 if concat_col1=="."

gen ddmat2_odd=ddmat2 if odd==1
egen concat_col2=concat(ddmat2_odd start_col2)
replace concat_col2=concat_se2 if concat_col2=="."

gen ddmat3_odd=ddmat3 if odd==1
egen concat_col3=concat(ddmat3_odd start_col)
replace concat_col3=concat_se3 if concat_col3=="."

keep ind_after propmined_illegal ind_gold afterxgold concat_col1 concat_col2 concat_col3

foreach i in 1 2 3 {
foreach j in 1 2 3 4 5 6 {
local col`i'`j'=concat_col`i'[`j'] 
}
}
*



file open DiD using "$latexpaper/sumstats_ddmin.tex", write replace


file write DiD "\begin{tabular}{lccc}" _n
file write DiD "\midrule" _n
file write DiD " \% of mined area mined illegally          & Coal		  & Gold 			 & Difference \\ " _n
file write DiD "\toprule" _n
file write DiD "Before the reform    & `col11'   & `col21'     & `col31'         \\" _n
file write DiD "           & `col12'   & `col22'	 & `col32'        \\" _n
file write DiD "After the reform   & `col13'   & `col23'	 & `col33'         \\" _n
file write DiD "          & `col14'   & `col24'	 & `col34'        \\" _n
file write DiD "Difference & `col15'   & `col25'	 & `col35'         \\" _n
file write DiD "           & `col16'   & `col26'     & `col36'        \\ \hline" _n
file write DiD "\bottomrule" _n
file write DiD "\end{tabular}" _n


file close DiD

restore





***********************
***Parallel trends
************************


preserve
tab ano, gen(I)
forvalues i = 1(1)11{
	gen int_`i' = regalia_mine * I`i'
}
gen base = 0

global dynamic int_1 int_2 int_3 int_4 int_5 int_6 base int_8 int_9 int_10 int_11

reghdfe propmined_illegal_mi $dynamic precio if mineral==1 | mineral==4 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
matrix B = e(b)
matrix V = e(V)
mat2txt , matrix(B) saving("$base_out/partrends_munimi_alpha14_betahat.txt") replace 
mat2txt , matrix(V) saving("$base_out/partrends_munimi_alpha14_Variancehat.txt") replace 

coefplot, graphregion(color(white)) baselevels omitted keep($dynamic) xtitle("Years") ytitle("Coefficient") ci vertical yline(0) xline(7)  rename( ///
int_1=2004 ///
int_2=2005 ///
int_3=2006 ///
int_4=2007 ///
int_5=2008 ///
int_6=2009 ///
base=2010 ///
int_8=2011 ///
int_9=2012 ///
int_10=2013 ///
int_11=2014)
graph export "$latexslides/partrends_munimi_alpha14.pdf", replace
graph export "$latexpaper/partrends_munimi_alpha14.pdf", replace
restore


preserve
collapse (mean) propmined_illegal_mi if mineral==1 | mineral==4 , by(ano mineral)
label var propmined_illegal_mi "% of mined area mined illegaly"
twoway (line propmined_illegal_mi ano if mineral==1, sort lw(thick)) ///
     (line propmined_illegal_mi ano if mineral==4, sort lw(thick)), ///
	  xline(2010) legend(label(1 "Gold") label(2 "Coal")) legend(on)
graph export "$latexslides/partrends_munimi_alpha14_raw.pdf", replace
graph export "$latexpaper/partrends_munimi_alpha14_raw.pdf", replace
restore



preserve
gen ind_platinum=(mineral==2)
fvset base 2010 ano
char ano[omit] 2010

*La intente hacer con c.ind_oro##b2010.ano pero me dropeaba 2006 y 2012 :(
forval i=2004/2014 {
if `i'~=2010 {
gen ind_gold`i'=(mineral==1)*(ano==`i')

}
}

fvset base 2010 ano
*char ano[omit] 2010

qui reghdfe propmined_illegal_mi ind_gold2*  precio , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(ind_gold2*) ci vertical yline(0) xtitle("Years") ytitle("Coefficient") xline(2011) rename( ///
ind_gold2004=2004 ///
ind_gold2005=2005 ///
ind_gold2006=2006 ///
ind_gold2007=2007 ///
ind_gold2008=2008 ///
ind_gold2009=2009 ///
ind_gold2011=2011 ///
ind_gold2012=2012 ///
ind_gold2013=2013 ///
ind_gold2014=2014)
graph export "$latexslides/partrends_munimi.pdf", replace
graph export "$latexpaper/partrends_munimi.pdf", replace

qui reghdfe propmuni_illegal_mi ind_gold2*  precio , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(ind_gold2*) ci vertical yline(0) xtitle("Years") ytitle("Coefficient") xline(2011) rename( ///
ind_gold2004=2004 ///
ind_gold2005=2005 ///
ind_gold2006=2006 ///
ind_gold2007=2007 ///
ind_gold2008=2008 ///
ind_gold2009=2009 ///
ind_gold2011=2011 ///
ind_gold2012=2012 ///
ind_gold2013=2013 ///
ind_gold2014=2014)
graph export "$latexslides/partrends_propmuni_munimi.pdf", replace
graph export "$latexpaper/partrends_propmuni_munimi.pdf", replace
restore






eststo m1: reghdfe propmined_illegal_mi afterxregalia  precio if mineral==1 | mineral==4  , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
estadd scalar meany `auchi'



qui eststo m2: reghdfe newpropminedMi_illegal afterxregalia  precio if mineral==1 | mineral==4 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum newpropminedMi_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'



qui eststo m3: reghdfe frtitulosareaMi afterxregalia  precio if _est_m1==1 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum frtitulosareaMi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'



qui estout m1 m2 m3 using "$latexslides/munimi_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( afterxregalia ) prefoot(\midrule ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc  %9.2fc a2  ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m2 m3  using "$latexpaper/munimi_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( afterxregalia precio ) prefoot(\midrule ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc  %9.2fc a2  ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m2 m3  using "$latexpaper/munimi_reg_lean.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( afterxregalia precio ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc  %9.2fc a2  ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

************
*Robustness Lagged
************
gen afterxregalia_lag=afterxregalia
replace afterxregalia_lag=0 if ano==2011


eststo m1_lag: reghdfe propmined_illegal_mi afterxregalia_lag  precio if mineral==1 | mineral==4  , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
estadd scalar meany `auchi'



qui eststo m2_lag: reghdfe newpropminedMi_illegal afterxregalia_lag  precio if mineral==1 | mineral==4 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum newpropminedMi_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'



qui eststo m3_lag: reghdfe frtitulosareaMi afterxregalia_lag  precio if _est_m1_lag==1 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum frtitulosareaMi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'

label var afterxregalia_lag "After $\times \alpha$"

estout m1_lag m2_lag m3_lag  using "$latexpaper/munimi_reg_lag.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( afterxregalia_lag precio ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc  %9.2fc a2  ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

************
*Robustness minus 2011

eststo m1_n2011: reghdfe propmined_illegal_mi afterxregalia  precio if  ano!=2011 & ( mineral==1 | mineral==4)  , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
estadd scalar meany `auchi'



qui eststo m2_n2011: reghdfe newpropminedMi_illegal afterxregalia  precio if ano!=2011 & ( mineral==1 | mineral==4) , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum newpropminedMi_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'



qui eststo m3_n2011: reghdfe frtitulosareaMi afterxregalia  precio if ano!=2011 & (_est_m1_n2011==1) , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum frtitulosareaMi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'



estout m1_n2011 m2_n2011 m3_n2011  using "$latexpaper/munimi_reg_n2011.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( afterxregalia precio ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc  %9.2fc a2  ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace



************
*Robustness Mineral
************


qui eststo r1: reghdfe propmined_illegal_mi afterxregalia precio  if mineral==1 | mineral==4  , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'

qui eststo r2: reghdfe propmined_illegal_mi afterxregalia   if mineral==1 | mineral==4  ,absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'

qui eststo r3: reghdfe propmined_illegal_mi afterxregalia precio  if mineral==1 | mineral==4 | mineral==2  , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'

qui eststo r4: reghdfe propmined_illegal_mi afterxregalia   if mineral==1 | mineral==4 | mineral==2  , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'


qui eststo r5: reghdfe propmined_illegal_mi afterxregalia   if mineral==1 | mineral==4 | mineral==2 | mineral==7 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'



qui estout r1 r2 r3 r4 r5  using "$latexpaper/munimi_reg_robust.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(afterxregalia ) prefoot(\midrule ) stats(N N_clust meany r2 , fmt(%9.2gc %9.2gc %9.2fc %9.2gc) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

************
*Robustness
************

**No Parks
qui eststo m1SP: reghdfe propminedSP_illegal_mi afterxregalia  precio if mineral==1 | mineral==4 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum propminedSP_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'

**Probability
qui eststo m1_prob: reghdfe propminedMi_illegal_prob afterxregalia  precio if mineral==1 | mineral==4 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum propminedMi_illegal_prob if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'

**Cutoff

qui eststo mc12: reghdfe c12_propmined_illegal_mi afterxregalia  precio  if (mineral==1 | mineral==4) & _est_m1==1 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum c12_propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'

**Adjusted
qui eststo m1adj: reghdfe propadjminedMi_illegal afterxregalia  precio  if (mineral==1 | mineral==4) & _est_m1==1 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum propadjminedMi_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'

**Weights
qui eststo m1w: reghdfe propmined_illegal_mi afterxregalia  precio [aw=w_analizedpctgarea_mi] if mineral==1 | mineral==4 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum propmined_illegal_mi [aw=w_analizedpctgarea_mi] if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'

qui estout m1 m1SP m1_prob  mc12 m1adj m1w using "$latexpaper/robust_regMi_unificada.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( afterxregalia) prefoot(\midrule ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc  a2 a2  ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


qui eststo clear
qui eststo m1: reghdfe propmined_illegal_mi afterxgold  precio if mineral==1 | mineral==4  , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'



qui eststo m2: reghdfe newpropminedMi_illegal afterxgold  precio if mineral==1 | mineral==4 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum newpropminedMi_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'



qui eststo m3: reghdfe frtitulosareaMi afterxgold  precio if _est_m1==1 , absorb(codmpio##ano codmpio##mineral) vce(cluster codmpio)
qui sum frtitulosareaMi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd scalar meany `auchi'



qui estout m1 m2 m3 using "$latexslides/munimiGC_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( afterxgold ) prefoot(\midrule ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc  a2 a2  ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m2 m3  using "$latexpaper/munimiGC_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( afterxgold precio ) prefoot(\midrule ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc  a2 a2  ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace






