use "$base_in\CensoMinero\cm_bd_final.dta", clear
keep if at_13==2
*Calculate area in meters and radious of circle of equivalente area
gen r=(ag_ue_06*10000/3.14159)^0.5
rename id_plat identificador
keep identificador r
gen cielo_abierto=1
drop if r==0
export delimited using "$base_out\CensoMinero\Cielo_abierto_r.csv", replace
