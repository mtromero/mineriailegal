use "$base_out\Regalis_mineral_muni.dta", clear
drop if ano!=2011
collapse (sum) regalias, by(codmpio)
gen MuniMinero=(regalias>0)
drop regalias
merge 1:1 codmpio using "$base_out\Codigos_DANE.dta",
keep codmpio MuniMinero
replace MuniMinero=0 if MuniMinero==.
save "$base_out\MuniMinero.dta", replace


use "$base_out\Royalties1993_2014.dta", clear
drop if ano<2004
drop if (ano==2011 | ano==2012) & source==1
bys ano: egen TotalTransf=total(transf_resources)
gen PropTransf=transf_resources/TotalTransf
saveold "$base_out\RoyaltiesUse.dta", replace
merge m:1 codmpio using "$base_out\MuniMinero.dta"
drop if _merge==2
drop _merge


bys MuniMinero: egen mediaTransf=mean(transf_resources)
gen desv=transf_resources-mediaTransf
sum desv if ano==2007, d 
bro if desv>1.16e+09 & ano==2007 & !missing(desv)
bro if codmpio==70400


collapse (sum) transf_resources, by(MuniMinero ano)
bys ano: egen TotalTransf=total(transf_resources)
gen PropTransf=transf_resources/TotalTransf

 twoway (line PropTransf ano if MuniMinero==0, sort lcolor("black")) (line PropTransf ano if MuniMinero==1, sort lcolor("red")), graphregion(color(white)) legend(lab(1 "Non-Mining") lab(2 "Mining")) ytitle("Proportion of royalties")
  graph export "$graphs\Evolution.pdf", replace

