use "$mipath\RawData\PanelCEDE\PANEL CARAC. GENERALES.dta", clear
drop if ano<2005
merge 1:1 ano codmpio using "$mipath\CreatedData\TMI.dta"
keep TMI MortalidadInfantil _merge codmpio ano
