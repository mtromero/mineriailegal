/*
use "$mipath\CreatedData\CensoMod.dta", clear
latab mineral
latab mineral if illegal==1
latab mineral if illegal==1 & cielo_abierto==1
*/
use "$mipath\CreatedData\panel_forreg.dta", clear
qui sum royalties_pc, detail
qui local topcut=round(r(p95)*100)/100
drop if royalties_pc>`topcut'
drop if tmi==.
***Le pego la variable selec que indica los municipios que estudiamos en la sbaldosas jugosas
**Note que no le pego ano para que pueda mostrar en el tiempo esos muni como son diferentes
** Son menos pobres y menos mineros (el centro del pais Antioquia, Cundinamarca, un poco Choco

merge m:1 codmpio using "$base_out\Temporary\Select_muni_appLunch.dta"
drop _merge

***Ahora si le pego con ano el dato de mineria ilegal para poder hacer regresioes
merge 1:1 codmpio ano using "$base_out\Illegal_data.dta"
drop _merge
gen MinIlegal=pix_mina/pix_tot
preserve
collapse (sum) pix_mina pix_tot, by(ano)
gen MinIlegal=pix_mina/pix_tot
twoway (connected MinIlegal ano, sort),graphregion(color(white)) xline(2011) xtitle("Year") ytitle("Illegal mining (prop of total area)") title("Evolution of illegal mining")
 graph export "$latexslides\evolv_illegal.pdf", replace
restore


gen poor=0
sort codmpio ano
forval i=2005/2014{
replace poor=1 if nbi[_n+(2011-`i')]>30 & codmpio==codmpio[_n+(2011-`i')] & ano==`i'
}

areg tmi royalties_pc i.ano, absorb(codmpio) vce(cluster codmpio)
xtreg MinIlegal royalties_pc i.ano, fe i(codmpio) vce(cluster codmpio)

keep if selec==1
drop if ano!=2014 & ano!=2012 & ano!=2009 & ano!=2006 
tsset codmpio ano, yearly delta(2)

areg tmi royalties_pc i.ano, absorb(codmpio) vce(cluster codmpio)
xtreg tmi royalties_pc i.ano, fe i(codmpio) vce(cluster codmpio)

lowess MinIlegal royalties_pc

reg D.MinIlegal D.royalties_pc, vce(cluster codmpio)

gen CpcMinIlegal=D.MinIlegal/L.MinIlegal
gen Cpcroyalties_pc=D.royalties_pc/L.royalties_pc
reg CpcMinIlegal Cpcroyalties_pc, vce(cluster codmpio)

xtreg MinIlegal royalties_pc i.ano, fe i(codmpio) vce(cluster codmpio)
xtreg MinIlegal royalties i.ano, fe i(codmpio) vce(cluster codmpio)
xtreg MinIlegal logR i.ano, fe i(codmpio) vce(cluster codmpio)
tsset 

qui estadd ysumm
qui estadd local munife "Yes"
qui estadd local timefe "Yes"
qui estout m1 using "$latexslides\Basic_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( royalties_pc )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui local N=5

forval i=1/`N' {
local lb=((`topcut')*(`i'-1)/`N')
local ub=((`topcut')*`i'/`N')
qui gen bin_roy_pc_`i'=1 if royalties_pc>`lb' & royalties_pc<=`ub'
qui replace bin_roy_pc_`i'=0 if royalties_pc<=`topcut' & bin_roy_pc_`i'==.

label var bin_roy_pc_`i' "Royalties per capita `lb'-`ub'"
}

qui eststo m1: xi: areg tmi bin_roy_pc* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
qui estout m1 using "$latexslides\Bin_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( bin_roy_pc* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

/*
gen ind_after=1 if ano>2011
replace ind_after=0 if ano<2012
gen ind_pos=1 if affi>0.03
replace ind_pos=0 if affi<0.03 & affi>-0.03
gen after_x_pos=ind_after*ind_pos
eststo m1: xi: areg tmi ind_after ind_pos after_x_pos i.ano, absorb(codmpio) vce(cluster codmpio)
*/
local varlist="royalties_pc"
foreach x of local varlist {
gen `x'_bef=`x'*(ano<2012)
gen `x'_aft=`x'*(ano>=2012)

label var `x'_bef "Royalties X Before"
label var `x'_aft "Royalties X After"

}

qui eststo m1: xi: areg tmi royalties_pc_* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estout m1 using "$latexslides\Bef_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( royalties_pc* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
drop royalties_pc_*

local varlist="royalties_pc"
foreach x of local varlist {
gen `x'_min_bef=`x'*MuniMinero*(ano<2012)
gen `x'_min_aft=`x'*MuniMinero*(ano>=2012)
gen `x'_nom_bef=`x'*(1-MuniMinero)*(ano<2012)
gen `x'_nom_aft=`x'*(1-MuniMinero)*(ano>=2012)
label var `x'_min_bef "Royalties X Mining X Before"
label var `x'_min_aft "Royalties X Mining X After"
label var `x'_nom_bef "Royalties X NON Mining X Before"
label var `x'_nom_aft "Royalties X NON Mining X After"
}

qui eststo m1: xi: areg tmi royalties_pc_* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estout m1 using "$latexslides\Min_bef_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( royalties_pc* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
drop royalties_pc_*
*/

gen posi_affi=1 if affi>0.03 & affi!=.
*replace posi_affi=0 if affi>(-0.03) & affi<0.03
replace posi_affi=0 if posi_affi==.
gen nega_affi=1 if affi<(-0.03)
*replace nega_affi=0 if affi>(-0.03) & affi<0.03
replace nega_affi=0 if nega_affi==.
local varlist="royalties_pc"
foreach x of local varlist {
gen `x'_posi_bef=`x'*posi_affi*(ano<2012)
gen `x'_posi_aft=`x'*posi_affi*(ano>=2012)
gen `x'_nposi_bef=`x'*(1-posi_affi)*(ano<2012)
gen `x'_nposi_aft=`x'*(1-posi_affi)*(ano>=2012)
gen `x'_nega_bef=`x'*nega_affi*(ano<2012)
gen `x'_nega_aft=`x'*nega_affi*(ano>=2012)
label var `x'_posi_bef "Royalties x Benefited X Before"
label var `x'_posi_aft "Royalties x Benefited X After"
label var `x'_nposi_bef "Royalties x Unchanged X Before"
label var `x'_nposi_aft "Royalties x Unchanged X After"
label var `x'_nega_bef "Royalties x Affected X Before"
label var `x'_nega_aft "Royalties x Affected X After"
}
eststo m1: xi: areg tmi royalties_pc_posi* royalties_pc_nposi* royalties_pc_nega* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

eststo m2: xi: areg tmi royalties_pc_nega* royalties_pc_nposi* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estout m1 using "$latexslides\Aff_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( royalties_pc* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
drop royalties_pc_*

local varlist="royalties_pc"
foreach x of local varlist {
gen `x'_min_bef_poor=`x'*MuniMinero*(ano<2012)*poor
gen `x'_min_aft_poor=`x'*MuniMinero*(ano>=2012)*poor
gen `x'_nom_bef_poor=`x'*(1-MuniMinero)*(ano<2012)*poor
gen `x'_nom_aft_poor=`x'*(1-MuniMinero)*(ano>=2012)*poor
gen `x'_min_bef_nopoor=`x'*MuniMinero*(ano<2012)*(1-poor)
gen `x'_min_aft_nopoor=`x'*MuniMinero*(ano>=2012)*(1-poor)
gen `x'_nom_bef_nopoor=`x'*(1-MuniMinero)*(ano<2012)*(1-poor)
gen `x'_nom_aft_nopoor=`x'*(1-MuniMinero)*(ano>=2012)*(1-poor)
label var `x'_min_bef_poor "Royalties X Mining X Before X Poor"
label var `x'_min_aft_poor "Royalties X Mining X After X Poor"
label var `x'_nom_bef_poor "Royalties X NON Mining X Before X Poor"
label var `x'_nom_aft_poor "Royalties X NON Mining X After X Poor"
label var `x'_min_bef_nopoor "Royalties X Mining X Before X NON Poor"
label var `x'_min_aft_nopoor "Royalties X Mining X After X NON Poor"
label var `x'_nom_bef_nopoor "Royalties X NON Mining X Before X NON Poor"
label var `x'_nom_aft_nopoor "Royalties X NON Mining X After X NON Poor"
}

qui eststo m1: xi: areg tmi royalties_pc_* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estout m1 using "$latexslides\Min_bef_poorreg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( royalties_pc* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*Law not fullfilled
bys codmpio: egen avg_roy_bef_reform=mean(royalties) if ano>=2007 & ano<=2010
sort codmpio ano
replace avg_roy_bef_reform= avg_roy_bef_reform[_n-1] if ano==2011 & codmpio==codmpio[_n-1]
replace avg_roy_bef_reform= avg_roy_bef_reform[_n-1] if ano==2012 & codmpio==codmpio[_n-1]
replace avg_roy_bef_reform= avg_roy_bef_reform[_n-1] if ano==2013 & codmpio==codmpio[_n-1]
replace avg_roy_bef_reform= avg_roy_bef_reform[_n-1] if ano==2014 & codmpio==codmpio[_n-1]
count if royalties< avg_roy_bef_reform*.5 & ano==2013 & MuniMinero==1
count if royalties< avg_roy_bef_reform*.5 & ano==2014 & MuniMinero==1

use "$mipath\CreatedData\Temporary\Muni_affected_reform.dta", clear

twoway (histogram affi  if MuniMinero==1 & affi>-0.7 & affi<0.3, width(0.035)  fraction  fcolor(none) lcolor(red))  ///
       (histogram affi  if MuniMinero==0 & affi>-0.7 & affi<0.3, width(0.035)  fraction fcolor(none) lcolor(blue)), legend(order(1 "Mining" 2 "NON-Mining" )) 
qui graph export "$latexslides\Muni_minero_affectedTotal.pdf", as(pdf) replace

/*
sort codmpio ano
gen delta_tmi=tmi-tmi[_n-1] if codmpio==codmpio[_n-1] & ano==(ano[_n-1]+1)
local varlist="tmi royalties"
foreach x of local varlist {
bys MuniMinero ano: egen mean_`x'=mean(`x')
bys MuniMinero ano: egen sd_`x'=sd(`x')
gen std_`x'=(`x'-mean_`x')/sd_`x'
}
