local deptos "Antioquia  Atlantico BogotaDC Bolivar Boyaca Caldas Caqueta Cauca Cesar Cordoba Cundinamarca Choco Huila  LaGuajira  Magdalena  Meta  Narino  NorteDeSantander Quindio   Risaralda  Santander  Sucre Tolima Valle Arauca  Casanare Putumayo  SanAndres   Amazonas  Guainia   Guaviare  Vaupes  Vichada"

foreach iyear in 2012 2013 2014 {
foreach idepto of local deptos {                         
qui import excel "$mipath\RawData\SGR\SGR_`idepto'`iyear'.xls", sheet("Datos Reporte SGR") cellrange(B17) firstrow clear
qui gen ano=`iyear'
qui save "$mipath\CreatedData\Temporary\SGR_`idepto'`iyear'", replace
}
}


clear
*Note we aren't using SGR 2012 because good suspicion that is already included in CEDE(DNP) panel
foreach iyear in 2013 2014 {
foreach idepto of local deptos                     {                         
qui append using "$mipath\CreatedData\Temporary\SGR_`idepto'`iyear'"
}
}
rename Codigo codmpio
qui destring codmpio, force replace
rename Total transf_resources

save "$mipath\CreatedData\SGR_All_2013_4", replace
keep codmpio ano transf_resources
drop if mod(codmpio,1000)==0
save "$mipath\CreatedData\Temporary\SGR_formerge", replace

import excel "$mipath\RawData\inflacion_1954_2015.xlsx", sheet("ipc_total-ano") cellrange(A8:F746) firstrow clear
rename AoMes ano
rename ndice indice
keep ano indice
destring ano, force replace
replace indice=subinstr(indice,",",".",.)
destring indice, force replace
replace ano=ano-7
drop if mod(ano,100)!=0
replace ano=ano/100
drop if ano<1993 | ano>2014
sum indice if ano==2012
local scale=r(mean)
replace indice=indice/`scale'
save "$mipath\CreatedData\Temporary\inflation_formerge", replace

use "$mipath\RawData\PanelCEDE\PANEL FISCAL.dta", clear
qui gen transf_resources=y_cap_regalias

keep codmpio ano transf_resources 
gen source=1
append using "$mipath\CreatedData\Temporary\SGR_formerge"
sort codmpio ano source
replace transf_resources=transf_resources*10^6 if ano<=2012
merge m:1 ano using "$mipath\CreatedData\Temporary\inflation_formerge"
drop _merge
gen transf_resources_R=transf_resources/indice


/*
gen transf_scale=100 if ano==2012
forval i=1993/2011{
qui replace transf_scale=transf_resources/transf_resources[_n+(2012-`i')] if codmpio==codmpio[_n+(2012-`i')] & ano[_n+(2012-`i')]==2012 & source[_n+(2012-`i')]==1
*replace transf_scale=transf_resources/transf_resources[_n+(2013-`i')] if codmpio==codmpio[_n+(2013-`i')] & ano[_n+(2013-`i')]==2012 & source[_n+(2013-`i')]==1
}

forval i=2013/2014{
qui replace transf_scale=transf_resources/transf_resources[_n+(2012-`i')] if codmpio==codmpio[_n+(2012-`i')] & ano[_n+(2012-`i')]==2012 & source[_n+(2012-`i')]==.
*replace transf_scale=transf_resources/transf_resources[_n+(2011-`i')] if codmpio==codmpio[_n+(2011-`i')] & ano[_n+(2011-`i')]==2012 & source[_n+(2011-`i')]==.
}
*/
save "$mipath\CreatedData\Royalties1993_2014", replace
