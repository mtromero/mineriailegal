
foreach ano in 2006 2009 2012 2014{
use "$base_out\Preddiciones\\`ano'\Stata\1.dta", clear
forval i=2/4{
append using "$base_out\Preddiciones\\`ano'\Stata\\`i'.dta"
}

levelsof cods, local(crops)
local crop_labels : value label cods
gen codmpio = ""



foreach i of local crops {
qui local label`i' : label `crop_labels' `i'
qui replace codmpio = "`label`i''" if cods == `i'
}
destring codmpio, force replace
qui gen selec=1
collapse (sum) bald pix_tot pix_mina selec, by(codmpio)
drop if selec==2
drop selec
qui gen ano=`ano'
collapse (mean) pix_tot pix_mina ano, by( codmpio)
compress
save "$base_out\Temporary\Illegal_`ano'.dta", replace
}


clear
use "$base_out\Temporary\Illegal_2012.dta", clear
append using "$base_out\Temporary\Illegal_2014.dta"
append using "$base_out\Temporary\Illegal_2009.dta"
append using "$base_out\Temporary\Illegal_2006.dta"

save "$base_out\Illegal_data.dta", replace
gen selec=1
keep codmpio selec
collapse selec, by(codmpio)
save "$base_out\Temporary\Select_muni_appLunch.dta", replace
