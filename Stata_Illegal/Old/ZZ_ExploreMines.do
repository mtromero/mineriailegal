use "C:\Users\Mauricio\Copy\MineriaIlegal\RawData\CensoMinero\cm_bd_final.dta", clear
destring lg_grados lg_minutos lg_segundos lt_grados lt_minutos lt_segundos, replace dpcomma ignore("��/+") force
drop if lt_minutos>60
drop if lt_segundos>60
drop if lg_minutos>60
drop if lg_segundos>60
drop if lg_grados==0
drop if lt_grados==0
drop if tipo_mina==1
sort tamano tamano_2
gen latitud_dec=lt_grados+lt_minutos/60+lt_segundos/3600
gen longitud_dec=lg_grados+lg_minutos/60+lg_segundos/3600

bro latitud_dec longitud_dec
bro id_plat tamano tamano_2 latitud_dec longitud_dec lg_grados lg_minutos lg_segundos lt_grados lt_minutos lt_segundos if at_pyr_03_01_07==1 & ao_ex_05_15==1 & at_pyr_11>10
