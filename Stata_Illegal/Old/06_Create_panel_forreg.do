import excel "$mipath\RawData\DANE\Proyecciones_poblacion_1985-2020.xls", sheet("Mpios") cellrange(A10:DH1159) firstrow clear
rename DPMP codmpio
destring codmpio, force replace
drop if codmpio==0 | codmpio==.
gen indrural2013=(AE-BO)/AE
gen indrural2014=(AF-BP)/AF
rename AE pobl_tot2013
rename AF pobl_tot2014

keep codmpio indrural* pobl_tot*
reshape long indrural pobl_tot, i(codmpio) j(ano)
save "$mipath\CreatedData\Temporary\dane_proyecciones2013_4.dta", replace

use "$mipath\RawData\PanelCEDE\PANEL CARAC. GENERALES.dta", clear
keep codmpio ano pobl_tot indrural nbi ipm_accsalud_p
append using "$mipath\CreatedData\Temporary\dane_proyecciones2013_4.dta"
save "$mipath\CreatedData\Temporary\cede_gral_forpanel.dta", replace

use "$mipath\RawData\PanelCEDE\PANEL FISCAL.dta", clear
keep codmpio ano y_total ing_propios
save "$mipath\CreatedData\Temporary\cede_fiscal_forpanel.dta", replace

use "$mipath\CreatedData\TMI.dta", clear
merge 1:1 codmpio ano using "$mipath\CreatedData\Temporary\cede_gral_forpanel.dta"
drop _merge

merge 1:1 codmpio ano using "$mipath\CreatedData\Temporary\cede_fiscal_forpanel.dta"
drop _merge

merge 1:1 codmpio ano using "$mipath\CreatedData\Royalties1993_2014.dta"
keep if _merge==3
gen royalties=transf_resources_R/10^6
rename MortalidadInfantil tmi

replace y_total=y_total/indice
drop _merge indice source transf_resources*
merge m:1 codmpio using "$mipath\CreatedData\MuniMinero.dta"
drop _merge
sort codmpio ano
gen delta_royalties=royalties-royalties[_n-1] if codmpio==codmpio[_n-1] & ano==(ano[_n-1]+1)
drop if ano<2005
drop if ano==.
drop if mod(codmpio,1000)==0
drop if mod(codmpio,1000)==999
gen royalties_pc= royalties/ pobl_tot
qui gen pct_roy= 100*royalties/ y_total
label var royalties "Royalties in millions of COP2012"
label var tmi "Mortality rate per 1,000 newborns"
label var royalties_pc "Royalties per capita"
label var pobl_tot "Population"
label var nacimientos "Births"
label var nbi "Poverty index" 
label var y_total "Municipality income"
label var pct_roy "Royalties as pctg of muni income"
label var ing_propios "Local taxes as pctg of muni income"
label var ano "Year"
save "$mipath\CreatedData\panel_forreg.dta", replace

qui gen roy_bef=royalties if ano==2010 | ano==2011
qui gen roy_aft=royalties if ano==2013 | ano==2014
qui gen y_bef=y_total if ano==2010 | ano==2011
collapse tmi pobl_tot nbi y_total MuniMinero roy_bef roy_aft y_bef, by(codmpio)
gen aff=2*( roy_aft- roy_bef)/( roy_aft+ roy_bef)
gen affi=( roy_aft- roy_bef)/y_bef
drop if MuniMinero==1 & aff==2
keep codmpio MuniMinero aff affi
label var affi "Average change in royalties as pctg muni budget"
save "$mipath\CreatedData\Temporary\Muni_affected_reform.dta", replace

*I am doing this to get rid of Munis with royalties in SIMCO but 0 in DNP
qui merge 1:m codmpio MuniMinero using "$mipath\CreatedData\panel_forreg.dta"
drop if _merge!=3
drop _merge
save "$mipath\CreatedData\panel_forreg.dta", replace
