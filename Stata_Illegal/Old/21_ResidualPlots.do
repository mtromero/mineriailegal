use "$mipath\CreatedData\Temporary\Muni_affected_reform.dta", clear


twoway (histogram affi  if MuniMinero==1 & affi>-0.7 & affi<0.3, width(0.035)  fraction  fcolor(none) lcolor(red))  ///
       (histogram affi  if MuniMinero==0 & affi>-0.7 & affi<0.3, width(0.035)  fraction fcolor(none) lcolor(blue)), legend(order(1 "Mining" 2 "Non-Mining" )) 
qui graph export "$latexslides\Muni_minero_affectedTotal.pdf", as(pdf) replace
	   

use "$mipath\CreatedData\panel_forreg.dta", clear
qui sum royalties_pc, detail
qui local topcut=round(r(p95)*100)/100
drop if royalties_pc>`topcut'

gen poor=0
sort codmpio ano
forval i=2005/2014{
replace poor=1 if nbi[_n+(2011-`i')]>30 & codmpio==codmpio[_n+(2011-`i')] & ano==`i'
}

gen posi_affi=1 if affi>0.03 & affi!=.



areg tmi i.ano, absorb(codmpio) vce(cluster codmpio)
predict resid_tmi,resid 
areg royalties_pc i.ano, absorb(codmpio) vce(cluster codmpio)
predict resid_royalties_pc,resid 


lpoly resid_tmi resid_royalties_pc, ci noscatter addplot((histogram resid_royalties_pc, yaxis(2) fraction))

twoway (lpolyci tmi royalties_pc if ano<2012 & MuniMinero==1, clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & MuniMinero==1,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Mining Before" 4 "Mining After" )) xtitle(Royalties) xtitle(IMR)
qui graph export "$latexslides\MinerosBeforeAfter.pdf", as(pdf) replace
twoway (lpolyci tmi royalties_pc if ano<2012 & MuniMinero==0 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & MuniMinero==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Non-Mining Before" 4 "Non-Mining After" )) xtitle(Royalties) xtitle(IMR) 
qui graph export "$latexslides\NonMinerosBeforeAfter.pdf", as(pdf) replace

twoway (lpolyci tmi royalties_pc if ano<2012 & poor==1, clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & poor==1,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Poor Before" 4 "Poor After" )) xtitle(Royalties) xtitle(IMR)
qui graph export "$latexslides\PoorBeforeAfter.pdf", as(pdf) replace
twoway (lpolyci tmi royalties_pc if ano<2012 & poor==0 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & poor==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Non-Poor Before" 4 "Non-Poor After" )) xtitle(Royalties) xtitle(IMR)
qui graph export "$latexslides\NonPoorBeforeAfter.pdf", as(pdf) replace

twoway (lpolyci tmi royalties_pc if ano>=2012 & MuniMinero==1 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & MuniMinero==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Mining After" 4 "Non-Mining After" )) xtitle(Royalties) xtitle(IMR) 
qui graph export "$latexslides\AfterMineroNoMinero.pdf", as(pdf) replace
twoway (lpolyci tmi royalties_pc if ano>=2012 & poor==1 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & poor==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Poor After" 4 "Non-Poor After" )) xtitle(Royalties) xtitle(IMR) 
qui graph export "$latexslides\AfterPoorNoPoor.pdf", as(pdf) replace


****************
************
********** AHORA LAS MISMAS GRAFICAS CON RESIDUOS
****************************
***************
twoway (lpolyci resid_tmi resid_royalties_pc if ano<2012 & MuniMinero==1, clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & MuniMinero==1,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Mining Before" 4 "Mining After" )) xtitle(Residual Royalties) xtitle(Residual IMR)
qui graph export "$latexslides\ResidualMinerosBeforeAfter.pdf", as(pdf) replace
twoway (lpolyci resid_tmi resid_royalties_pc if ano<2012 & MuniMinero==0 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & MuniMinero==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Non-Mining Before" 4 "Non-Mining After" )) xtitle(Residual Royalties) xtitle(Residual IMR) 
qui graph export "$latexslides\ResidualNonMinerosBeforeAfter.pdf", as(pdf) replace

twoway (lpolyci resid_tmi resid_royalties_pc if ano<2012 & poor==1, clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & poor==1,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Poor Before" 4 "Poor After" )) xtitle(Residual Royalties) xtitle(Residual IMR)
qui graph export "$latexslides\ResidualPoorBeforeAfter.pdf", as(pdf) replace
twoway (lpolyci resid_tmi royalties_pc if ano<2012 & poor==0 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & poor==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Non-Poor Before" 4 "Non-Poor After" )) xtitle(Residual Royalties) xtitle(Residual IMR)
qui graph export "$latexslides\ResidualNonPoorBeforeAfter.pdf", as(pdf) replace

twoway (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & MuniMinero==1 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & MuniMinero==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Mining After" 4 "Non-Mining After" )) xtitle(Residual Royalties) xtitle(Residual IMR) 
qui graph export "$latexslides\ResidualAfterMineroNoMinero.pdf", as(pdf) replace
twoway (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & poor==1 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & poor==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Poor After" 4 "Non-Poor After" )) xtitle(Residual Royalties) xtitle(Residual IMR) 
qui graph export "$latexslides\ResidualAfterPoorNoPoor.pdf", as(pdf) replace



