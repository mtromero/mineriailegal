import delim using "$base_in\DCOILWTICO.csv", clear
rename date year
rename value Hidrocarburos
sum Hidrocarburos if year==2004
gen precio_Hidrocarburos=Hidrocarburos/r(mean)
drop Hidrocarburos
save "$base_out\Temporary\precios_wti", replace

import excel "$base_in\SIMCO\Precios_carbon1994_2012.xls", sheet("Precios_carbon1994_2012") cellrange(A4:E22) clear
rename A year
destring year, force replace
rename B precio_carbon
keep year precio_carbon
replace precio_carbon=subinstr(precio_carbon,".","",.)
replace precio_carbon=subinstr(precio_carbon,",",".",.)
destring precio_carbon, force replace

sum precio_carbon if year==2004
replace precio_carbon=precio_carbon/r(mean)
save "$base_out\Temporary\precios_carbon", replace

import excel "$base_in\SIMCO\Precios_minerales_1990_2012.xls", sheet("Precios_minerales_1990_2012") cellrange(A3:K26) firstrow clear
rename A year
destring year, force replace

foreach x in ORO PLATA PLATINO ESTAO PLOMO ALUMINIO COBRE ZINC NIQUEL HIERRO{
gen precio_`x'=subinstr(`x',".","",.)
replace precio_`x'=subinstr(precio_`x',",",".",.)
destring precio_`x', force replace
sum precio_`x' if year==2004
replace precio_`x'=precio_`x'/r(mean)
}
keep year precio*
merge 1:1 year using "$base_out\Temporary\precios_carbon"
drop _merge
merge 1:1 year using "$base_out\Temporary\precios_wti"
drop _merge
reshape long precio@, i(year) j(mineral) string

replace mineral="carbon" if mineral=="_carbon" 
replace mineral="metalespreciosos" if mineral=="_ORO"
replace mineral="metalespreciosos" if mineral=="_PLATA"
replace mineral="metalespreciosos" if mineral=="_PLATINO"
replace mineral="hierro" if mineral=="_HIERRO" 
replace mineral="Niquel" if mineral=="_NIQUEL" 
replace mineral="Hidrocarburos" if mineral=="_Hidrocarburos" 
collapse (mean) precio, by(year mineral)

*Reshape a columns year mineral price
*Merge con regalias x muni y generar indice precios x muni, o con Censo
rename year ano
merge 1:m  ano mineral using  "$base_out\Regalis_mineral_muni.dta"
drop if  mineral=="Caliza" | mineral=="Manganeso" | mineral=="Sal" | mineral=="Yeso" | mineral=="_ALUMINIO" | mineral=="_COBRE" | mineral=="_ESTAO" | mineral=="_PLOMO" | mineral=="_ZINC" |  mineral=="azufre" |  mineral=="piedraspreciosos" 
bys ano codmpio: egen double TotalRegalias=total(regalias)
gen Share=regalias/TotalRegalias
gen Index=Share*precio
collapse (mean) Index, by(ano codmpio)
save "$base_out\indexPrecios", replace
 


