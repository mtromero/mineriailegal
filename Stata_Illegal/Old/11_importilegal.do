
** Import the predictions from R
forval i=2004/2014 {
*This is temporary while we fix 2011 predictions
if `i'!=2011 {
qui import delimited "$base_out\Predicciones_Jan2016\Open_pit\results`i'.csv", clear
*This variable is in pixels and contains same info as area
qui drop mines illegalmines
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
save "$base_out\Temporary\results`i'.dta", replace
}
} 

forval i=2004/2013 {
*This is temporary while we fix 2011 predictions
if `i'!=2011 {

append using "$base_out\Temporary\results`i'.dta"
}
} 

gen analizedarea_sqkm=analizedarea/10^6
label var analizedarea_sqkm "Area (sqkm) of pixels analized in prediction"

gen areamuni_sqkm=municipioarea/10^6
label var areamuni_sqkm "Area (sqkm) of municipality in raster"

gen areaprmined_sqkm=areamines/10^6
label var areaprmined_sqkm "Area (sqkm) predicted as mined"

gen titulosarea_sqkm=titulosarea/10^6
label var titulosarea_sqkm "Area (sqkm) of mining titles"

gen titulosareaprnomined_sqkm=areatitulosnomine/10^6
label var titulosareaprnomined_sqkm "Area (sqkm) of mining titles predicted as NOT mined"

gen areaprilegal_sqkm=areaillegalmines/10^6
label var areaprilegal_sqkm "Area (sqkm) predicted as illegaly mined"

keep codane year *_sqkm

rename codane codmpio
rename year ano


save "$base_out\Temporary\panel_prillegal.dta", replace

use "$mipath\CreatedData\panel_forreg.dta", clear
qui sum royalties_pc, detail
qui local topcut=round(r(p95)*100)/100
drop if royalties_pc>`topcut'
**Add this line to exclude big cities
drop if pobl_tot>400000
***Add this line to exclude municipalities where TMI fluctuates just because sample size
drop if nacimientos<30
drop if tmi==.

merge 1:1 codmpio ano using "$base_out\Temporary\panel_prillegal.dta"
drop _merge

gen nega_affi=1 if affi<(-0.03)
replace nega_affi=0 if nega_affi==.

gen ind_after=1 if ano>2011 & ano!=.
replace ind_after=0 if ano<2012

gen after_x_affi=nega_affi*ind_after

gen prop_illegal= areaprilegal_sqkm/ analizedarea_sqkm
label var prop_illegal "Share of municipality area mined illegaly"

gen propmined_illegal= areaprilegal_sqkm/ areaprmined_sqkm
label var propmined_illegal "Share of municipality mined area mined illegaly"

save "$base_out\panel_final.dta", replace
