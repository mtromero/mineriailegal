use "$mipath\RawData\PanelCEDE\PANEL CARAC. GENERALES.dta", clear
collapse ano, by(municipio depto codmpio coddepto)
forval i=1/5{
qui replace municipio=subinstr(municipio,"(`i')","",.)
}
qui drop ano
qui replace municipio=subinstr(municipio,"(CD)","",.)
qui replace municipio=subinstr(municipio," ","",.)
qui save "$mipath\CreatedData\Codigos_DANE.dta", replace

qui replace municipio=subinstr(municipio,"�","a",.)
qui replace municipio=subinstr(municipio,"�","e",.)
qui replace municipio=subinstr(municipio,"�","i",.)
qui replace municipio=subinstr(municipio,"�","o",.)
qui replace municipio=subinstr(municipio,"�","u",.)
qui replace depto=subinstr(depto,"�","a",.)
qui replace depto=subinstr(depto,"�","e",.)
qui replace depto=subinstr(depto,"�","i",.)
qui replace depto=subinstr(depto,"�","o",.)
qui replace depto=subinstr(depto,"�","u",.)
save "$mipath\CreatedData\Temporary\Codigos_DANE_sintilde.dta", replace

forval iyear=2004/2011{
qui import excel "$mipath\RawData\ANH\HISTORICO DE REGAL�AS PAGADAS `iyear'.xlsx", sheet("page 1") firstrow clear

if `iyear'==2011{
qui keep BENEFICIARIO AE
qui rename AE TOTALPAGADO`iyear'
}
if `iyear'<=2010 & `iyear'>=2009 {
qui keep BENEFICIARIO AC
qui rename AC TOTALPAGADO`iyear'
}
if `iyear'==2008{
qui keep BENEFICIARIO TOTALPAGADO`iyear'
}
qui save "$mipath\CreatedData\Temporary\ANH_`iyear'_p1.dta", replace
local Npage=6
if `iyear'>2009{
local Npage=4
}
forval i=2/`Npage'{
qui import excel "$mipath\RawData\ANH\HISTORICO DE REGAL�AS PAGADAS `iyear'.xlsx", sheet("page `i'") firstrow clear
if (`i'>=4 & `iyear'>=2009) | (`i'>=3 & `iyear'>2009){
qui replace TOTALPAGADO`iyear'=AC if TOTALPAGADO`iyear'==""
}
if `iyear'==2004 & `i'>=2 {
qui rename TOTALPAGADO TOTALPAGADO`iyear'
qui replace TOTALPAGADO`iyear'=S if TOTALPAGADO`iyear'==""
}
qui keep BENEFICIARIO TOTALPAGADO`iyear'
qui save "$mipath\CreatedData\Temporary\ANH_`iyear'_p`i'.dta", replace
}

clear
forval i=1/`Npage'{
qui append using "$mipath\CreatedData\Temporary\ANH_`iyear'_p`i'.dta"
}

qui drop if TOTALPAGADO`iyear'=="-"
qui drop if TOTALPAGADO`iyear'==""
qui drop if BENEFICIARIO=="BENEFICIARIO"



qui replace BENEFICIARIO=subinstr(BENEFICIARIO,"PTO.","PUERTO",.)
qui replace BENEFICIARIO=subinstr(BENEFICIARIO,"�","E",.)
qui replace BENEFICIARIO=subinstr(BENEFICIARIO,"�","a",.)
qui replace BENEFICIARIO=subinstr(BENEFICIARIO,"�","i",.)
qui replace BENEFICIARIO=subinstr(BENEFICIARIO,"�","o",.)
qui replace TOTALPAGADO`iyear'=subinstr(TOTALPAGADO`iyear',".","",.)
qui rename BENEFICIARIO municipio
qui rename TOTALPAGADO`iyear' regalias
qui destring regalias, force replace
qui drop if municipio==""
qui sum regalias
qui local nobs=r(N)
qui local cdep=municipio[1]
gen depto=""
forval i=1/`nobs'{
qui replace depto="`cdep'" in `i'
if municipio[`i']=="TOTALES" & `i'!=`nobs'{
qui local cdep=municipio[`i'+1]
}
}

qui replace depto="Vichada" if depto=="DEPARTAMENTO VICHADA"
qui replace depto="Cordoba" if depto=="SAN ANTERO"
qui replace depto="Sucre" if depto=="COVE�AS"

qui drop if municipio=="TOTALES"
qui replace municipio=proper(municipio)
qui replace municipio=subinstr(municipio," ","",.)
qui replace municipio=subinstr(municipio,"�","�",.)
qui replace depto=proper(depto)
qui replace depto=subinstr(depto," ","",.)
qui replace depto=subinstr(depto,"�","�",.)
qui sort municipio
if `iyear'==2011{
qui replace municipio="Sahagun" if municipio=="SahagunPuebloNuevo" & regalias==101067005
qui replace municipio="PuebloNuevo" if municipio=="SahagunPuebloNuevo"
qui replace regalias=regalias+regalias[_n+1] if municipio=="Sahagun" & municipio[_n+1]=="Sahagun"
qui replace regalias=regalias+regalias[_n+1] if municipio=="PuebloNuevo" & municipio[_n+1]=="PuebloNuevo"
qui drop if municipio=="Sahagun" & municipio[_n-1]=="Sahagun"
qui drop if municipio=="PuebloNuevo" & municipio[_n-1]=="PuebloNuevo"
}
if `iyear'==2010{
qui replace municipio="SanBernardodelViento" if municipio=="SanBernardo" 
}
qui replace municipio="FuentedeOro" if municipio=="FuenteDeOro"
qui replace municipio="PuertoCaicedo" if municipio=="PuertoCaycedo"
qui replace municipio="SanLuisdeGaceno" if municipio=="SanLuisDeGaceno"
qui replace municipio="PazdeAriporo" if municipio=="PazDeAriporo"
qui replace municipio="Cubarral" if municipio=="Cuburral"
qui replace municipio="Vistahermosa" if municipio=="VistaHermosa"
qui replace municipio="Mo�itos" if municipio=="Mo�Itos"
qui replace municipio="ValledelGuamuez" if municipio=="Guamues"
qui replace municipio="RiodeOro" if municipio=="RioDeOro"
qui replace municipio="BarrancadeUpia" if municipio=="BarrancaDeUpia"
qui replace municipio="SanCarlosdeGuaroa" if municipio=="SanCarlosGuaroa"
qui replace municipio="SanJosedeUre" if municipio=="SanJoseDeUre"
qui replace municipio="SanAndresSotavento" if municipio=="SanAndresDeSotavento"
qui replace municipio="SanJuandeArama" if municipio=="SanJuanDeArama"
qui replace municipio="SabanadeTorres" if municipio=="SabanaTorres"
qui replace municipio="SanLuisdePalenque" if municipio=="SanLuisDePalenque"
qui replace municipio="Tibu" if municipio=="SabanaDeTibu"
qui replace municipio="CastillalaNueva" if municipio=="CastillaLaNueva"
qui replace municipio="PuertoSalgar" if municipio=="Puertosalgar"
qui replace municipio="Tierralta" if municipio=="TierraAlta"
qui replace municipio="Uribe" if municipio=="LaUribe"
qui replace municipio="SanVicentedeChucuri" if municipio=="SanVicente" & depto=="Santander"
qui replace municipio="Cove�as" if municipio=="Cove�As"
qui replace depto="La Guajira" if depto=="Guajira"
qui replace depto="Norte de Santander" if depto=="NorteDeSantander"
qui replace depto="Nari�o" if depto=="Nari�O"
qui replace municipio="ElCarmendeChucuri" if municipio=="ElCarmen" & depto=="Santander"
qui replace municipio="Buenavista" if municipio=="Buenavista(S)"
qui replace municipio="SanJuandeBetulia" if municipio=="SanJuanBetulia"
qui replace municipio="SantiagodeTolu" if municipio=="SantiagoDeTolu"
qui replace municipio="SanLuisdeSince" if municipio=="Since"
qui replace depto="Sucre" if depto=="SantiagoDeTolu"
qui replace depto="Bolivar" if municipio=="Cartagena"
qui replace depto="Nari�o" if municipio=="Tumaco"
qui replace depto="Archipielago de San Andres" if municipio=="SanAndresIslas"
qui replace municipio="SanAndres" if municipio=="SanAndresIslas"
qui replace depto="Santander" if municipio=="PuertoWilches-Santander"
qui replace municipio="PuertoWilches" if municipio=="PuertoWilches-Santander"
qui replace depto="Magdalena" if municipio=="SitioNuevo"
qui replace municipio="Sitionuevo" if municipio=="SitioNuevo"
qui replace depto="Antioquia" if municipio=="Yondo-Antioquia"
qui replace municipio="Yondo" if municipio=="Yondo-Antioquia"
qui replace depto="Bolivar" if municipio=="SanPablo-Bolivar"
qui replace municipio="SanPablo" if municipio=="SanPablo-Bolivar"
qui replace depto="Bolivar" if municipio=="Cantagallo-Bolivar"
qui replace municipio="Cantagallo" if municipio=="Cantagallo-Bolivar"
qui replace municipio="SanBernardodelViento" if municipio=="SanBernardo" & depto=="Cordoba"
qui replace depto="Atlantico" if municipio=="Barranquilla"
qui replace municipio="SanAndresdeTumaco" if municipio=="Tumaco"
qui replace depto="Santander" if municipio=="Barrancabermeja"
qui replace municipio="CienagadeOro" if municipio=="CienagaDeOro"
qui replace depto="Valle del Cauca" if municipio=="Buenaventura"
qui replace municipio="ElCarmendeChucuri" if municipio=="Elcarmen" & depto=="Santander"
qui replace depto="Antioquia" if municipio=="Turbo"
qui replace municipio="SanAndresSotavento" if municipio=="SanAndresDeStoavento"
qui replace depto="Antioquia" if municipio=="Bagre"
qui replace municipio="ElBagre" if municipio=="Bagre"
qui replace municipio="Simacota" if municipio=="Simatoca"
qui collapse (sum) regalias, by( municipio depto)
qui merge 1:1 municipio depto using "$mipath\CreatedData\Temporary\Codigos_DANE_sintilde.dta"
qui drop if _merge==2

qui replace codmpio=20000 if municipio=="Cesar"
qui replace codmpio=73000 if municipio=="Tolima"
qui replace codmpio=54000 if municipio=="NorteDeSantander"
qui replace codmpio=52000 if municipio=="Nari�O"
qui replace codmpio=41000 if municipio=="Huila"
qui replace codmpio=85000 if municipio=="Casanare"
qui replace codmpio=25000 if municipio=="Cundinamarca"
qui replace codmpio=68000 if municipio=="Santander"
qui replace codmpio=44000 if municipio=="Guajira"
qui replace codmpio=99000 if municipio=="DepartamentoVichada"
qui replace codmpio=86000 if municipio=="Putumayo"
qui replace codmpio=50000 if municipio=="Meta"
qui replace codmpio=5000 if municipio=="Antioquia"
qui replace codmpio=19000 if municipio=="Cauca"
qui replace codmpio=13000 if municipio=="Bolivar" & depto=="Bolivar"
qui replace codmpio=23000 if municipio=="Cordoba" & depto=="Cordoba"
qui replace codmpio=70000 if municipio=="DepartamentoDeSucre"
if `iyear'==2011{
qui replace codmpio=81000 if municipio=="Arauca" & regalias>279*10^9
qui replace codmpio=13000 if municipio=="Bolivar" & regalias>42*10^9
qui drop if codmpio==19100
qui replace codmpio=68101 if municipio=="Bolivar" & regalias<28*10^6
qui replace codmpio=20770 if municipio=="SanMartin" & regalias>12*10^9
qui replace codmpio=50689 if municipio=="SanMartin" & regalias<4*10^9
}
if `iyear'==2010{
qui drop if codmpio==76606 | codmpio==5615 | codmpio==68176 | codmpio==20443 |codmpio==18592
qui replace codmpio=81000 if municipio=="Arauca" & regalias>214*10^9
}

qui keep codmpio regalias
qui drop if codmpio==.
qui gen ano=`iyear'
qui gen mineral="Hidrocarburos"
qui save "$mipath\CreatedData\Temporary\ANH_`iyear'.dta", replace

}
**Need to do this for each year ANH
clear
forval iyear=2004/2011{
append using "$mipath\CreatedData\Temporary\ANH_`iyear'.dta"
}
save "$mipath\CreatedData\Temporary\Hidrocarburos2004_2012.dta", replace

*The original file Regalias_caliza2004_2012.xls was not suitable for Stata, I copy and transposed by hand
import excel "$mipath\RawData\SIMCO\Regalias_caliza2004_2012_forStata.xlsx", sheet("Sheet1") firstrow clear
drop A B C F
rename D depto
rename E municipio
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)

replace municipio=substr(municipio, 1, 1)+ lower(substr(municipio, 2, .))

rename G y2005
rename H y2006
rename I y2007
rename J y2008
rename K y2009
rename L y2010
rename M y2011
rename N y2012
merge 1:m municipio using "$mipath\CreatedData\Codigos_DANE.dta"
drop if _merge==2
replace codmpio=5000 if municipio=="Antioquia"
replace codmpio=68000 if municipio=="Santander"
replace codmpio=76000 if municipio=="Valle del cauca"
drop if municipio=="Varios"
reshape long y, i(codmpio) j(ano)
rename y regalias
replace regalias=subinstr(regalias,".","",.)
destring regalias, force replace
keep codmpio ano regalias
gen mineral="Caliza"
save "$mipath\CreatedData\Temporary\Caliza2004_2012.dta", replace

clear
generate codmpio = .
set obs 1
replace codmpio = 76109 in 1
set obs 2
replace codmpio = 76000 in 2
generate var2 = 1316352 in 1
replace var2 = 1645441 in 2
rename var2 regalias
gen ano=2005
gen mineral="Manganeso"
save "$mipath\CreatedData\Temporary\Manganeso2004_2012.dta", replace

*The original file Regalias_niquel1990_2012.xls was not suitable for Stata, I copy and transposed by hand
import excel "$mipath\RawData\SIMCO\Regalias_niquel1990_2012_forStata.xlsx", sheet("Sheet1") firstrow clear
drop A B C 
rename D depto
rename E municipio
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)

replace municipio=substr(municipio, 1, 1)+ lower(substr(municipio, 2, .))
replace municipio=proper(municipio)
replace municipio=subinstr(municipio," ","",.)
rename F y1990
rename G y1991
rename H y1992
rename I y1993
rename J y1994
rename K y1995
rename L y1996
rename M y1997
rename N y1998
rename O y1999
rename P y2000
rename Q y2001
rename R y2002
rename S y2003
rename T y2004
rename U y2005
rename V y2006
rename W y2007
rename X y2008
rename Y y2009
rename Z y2010
rename AA y2011
rename AB y2012

replace municipio="Montel�bano" if municipio=="Montel�Bano"

merge 1:m municipio using "$mipath\CreatedData\Codigos_DANE.dta"
drop if _merge==2
replace codmpio=23000 if municipio=="C�Rdoba"
drop if municipio=="Bellavista" & codmpio!=23079
drop if municipio=="Varios"
reshape long y, i(codmpio) j(ano)
rename y regalias
replace regalias=subinstr(regalias,".","",.)
destring regalias, force replace
keep codmpio ano regalias
gen mineral="Niquel"
save "$mipath\CreatedData\Temporary\Niquel2004_2012.dta", replace

*The original file Regalias_sal2004_2012.xls was not suitable for Stata, I copy and transposed by hand
import excel "$mipath\RawData\SIMCO\Regalias_sal2004_2012_forStata.xlsx", sheet("Sheet1") firstrow clear
drop A B C F G H
rename D depto
rename E municipio
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=substr(municipio, 1, 1)+ lower(substr(municipio, 2, .))
replace municipio=proper(municipio)
replace municipio=subinstr(municipio," ","",.)

rename I y2007
rename J y2008
rename K y2009
rename L y2010
rename M y2011
rename N y2012
replace municipio="Nemoc�n" if municipio=="Nemoc�N"
merge 1:m municipio using "$mipath\CreatedData\Codigos_DANE.dta"
drop if _merge==2
replace codmpio=13000 if municipio=="Bol�Var"
replace codmpio=25000 if municipio=="Cundinamarca"
replace codmpio=50000 if municipio=="Meta"
drop if municipio=="Varios"
drop if municipio=="Restrepo" & codmpio!=50606
reshape long y, i(codmpio) j(ano)
rename y regalias
replace regalias=subinstr(regalias,".","",.)
destring regalias, force replace
keep codmpio ano regalias
gen mineral="Sal"
save "$mipath\CreatedData\Temporary\Sal2004_2012.dta", replace


*The original file Regalias_yeso2004_2012.xls was not suitable for Stata, I copy and transposed by hand
import excel "$mipath\RawData\SIMCO\Regalias_yeso2004_2012_forStata.xlsx", sheet("Sheet1") firstrow clear
drop A B C F G H
rename D depto
rename E municipio
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=substr(municipio, 1, 1)+ lower(substr(municipio, 2, .))
replace municipio=proper(municipio)
replace municipio=subinstr(municipio," ","",.)

rename I y2007
rename J y2008
rename K y2009
rename L y2010
rename M y2011
rename N y2012
replace municipio="ValledeSanJuan" if municipio=="ValleDeSanJuan"
merge 1:m municipio using "$mipath\CreatedData\Codigos_DANE.dta"
drop if _merge==2
replace codmpio=15000 if municipio=="Boyac�"
replace codmpio=73000 if municipio=="Tolima"
replace codmpio=68000 if municipio=="Santander"
drop if municipio=="Varios"
drop if municipio=="Villanueva" & codmpio!=68872
reshape long y, i(codmpio) j(ano)
rename y regalias
replace regalias=subinstr(regalias,".","",.)
destring regalias, force replace
keep codmpio ano regalias
gen mineral="Yeso"
save "$mipath\CreatedData\Temporary\Yeso2004_2012.dta", replace


local minerals1 "azufre carbon hierro metalespreciosos piedraspreciosos"
foreach imineral of local minerals1 {
import delimited "$mipath\CreatedData\Regalias2004_2012\\`imineral'20042012.csv", clear
rename `imineral'mpio regalias
keep codmpio ano regalias
gen mineral="`imineral'"
destring regalias, force replace
save "$mipath\CreatedData\Temporary\\`imineral'2004_2012.dta", replace
}

**
**
**
*Finally put together the file of each mineral
clear
local minerals "azufre carbon hierro metalespreciosos piedraspreciosos Hidrocarburos Caliza Manganeso Niquel Sal Yeso"
foreach imineral of local minerals {

append using "$mipath\CreatedData\Temporary\\`imineral'2004_2012.dta"
}

save "$mipath\CreatedData\Regalis_mineral_muni.dta", replace
