
use "$mipath\CreatedData\Regalis_mineral_muni.dta", clear
gen keep_2011=1 if (mineral=="Caliza" | mineral=="metalespreciosos") & regalias!=. & regalias>0 & ano==2011
gen keep_2010=1 if (mineral=="Caliza" | mineral=="metalespreciosos") & regalias!=. & regalias>0 & ano==2010
collapse keep_2011 keep_2010, by(codmpio)
keep if keep_2011==1 & keep_2010==1
keep codmpio
gen detect_satelite=1
save "$mipath\CreatedData\Temporary\muni_detect_satelite.dta", replace

merge 1:m codmpio using "$mipath\CreatedData\Royalties1993_2014"

sum transf_resources if ano==2010 & detect_satelite==1
sum transf_resources if ano==2012 & detect_satelite==1
gen r2010=transf_resources if ano==2010 & detect_satelite==1
gen r2012=transf_resources if ano==2012 & detect_satelite==1
collapse r2010 r2012, by(codmpio)
count if r2010<r2012
