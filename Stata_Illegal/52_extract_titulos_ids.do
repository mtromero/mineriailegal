import delimited C:\Users\santi\Dropbox\MineriaIlegal\DataNubes\copy\TITULOS_PAIS.csv, clear

gen nids = length(titulares) - length(subinstr(titulares, "(", "", .))
gen titutemp=titulares
gen lp=.
gen rp=.
qui sum nids
forval i = 1/`r(max)' {
replace lp=strpos(titutemp,"(")
replace rp=strpos(titutemp,")")
gen id`i'=substr(titutemp,lp+1,rp-lp-1)
destring id`i', force replace
replace titutemp=substr(titutemp,rp+1,.)
}

sort codigo_exp
drop if codigo_exp==codigo_exp[_n-1]
reshape long id, i(codigo_exp) j(owner)
drop if id==.

gen persona=0
replace persona=1 if id<10^8 | (id>10^9 & id<2*10^9)

gen empresa=0
replace empresa=1 if strpos(titulares,"LTDA") | strpos(titulares,"S.A.")

keep codigo_exp id owner persona empresa

export delimited using "C:\Users\santi\Dropbox\MineriaIlegal\CreatedData\title_owner_ids.csv", replace
***Now run the webscrapper in R
