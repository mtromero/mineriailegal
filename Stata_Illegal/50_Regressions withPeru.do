



preserve
collapse (sum) areaprmined_sqkm, by(ano country)
label var areaprmined_sqkm "Area predicted as mined"
twoway (line areaprmined_sqkm ano if country=="Col", sort lw(thick))(line areaprmined_sqkm ano if country=="Peru", sort lw(thick) ), title("Evolution of area predicted as mined")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predmined_area.pdf", as(pdf) replace
restore

preserve
collapse (mean) prop_mined [fweight=round(areamuni_sqkm)], by(ano country)
label var prop_mined "Fraction of analyzed area predicted as mined"
twoway (line prop_mined ano if country=="Col", sort lw(thick))(line prop_mined ano if country=="Peru", sort lw(thick) ), title("Evolution of area predicted as mined")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predmined_proparea.pdf", as(pdf) replace
restore


preserve
collapse (sum) titulosarea_sqkm, by(ano country)
label var titulosarea_sqkm "Total area (sqkm) mining titles"
twoway (line titulosarea_sqkm ano if country=="Col", sort lw(thick))(line titulosarea_sqkm ano if country=="Peru", sort lw(thick) ), title("Total area mining titles by year")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Titles_area.pdf", as(pdf) replace
restore

preserve
collapse (sum) areaprilegal_sqkm, by(ano country)
label var areaprilegal_sqkm "Total area (sqkm) predicted illegal"
twoway (line areaprilegal_sqkm ano if country=="Col", sort lw(thick))(line areaprilegal_sqkm ano if country=="Peru", sort lw(thick) ), title("Total area predicted illegal by year")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predillegal_area.pdf", as(pdf) replace
restore

preserve
collapse (mean) prop_illegal [fweight=round(areamuni_sqkm)], by(ano country)
label var prop_illegal "Proportion of area mined illegaly"
twoway (line prop_illegal ano if country=="Col", sort lw(thick))(line prop_illegal ano if country=="Peru", sort lw(thick) ), title("Proportion of total area mined illegaly by year")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predillegal_proparea.pdf", as(pdf) replace
restore

preserve
collapse (mean) propmined_illegal [fweight=round(areamuni_sqkm)], by(ano country)
label var propmined_illegal "Proportion of mined area mined illegaly"
twoway (line propmined_illegal ano if country=="Col", sort lw(thick))(line propmined_illegal ano if country=="Peru", sort lw(thick) ), title("Proportion of mined area mined illegaly by year")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predillegal_propmined_peru.pdf", as(pdf) replace
restore



*Adjusted predictions with TPR FPR formula
preserve
collapse (mean) propadj_illegal [fweight=round(areamuni_sqkm)], by(ano country)
label var propadj_illegal "Proportion of area mined illegaly adjusted"
twoway (line propadj_illegal ano if country=="Col", sort lw(thick))(line propadj_illegal ano if country=="Peru", sort lw(thick) ), title("Proportion of total area mined illegaly adjusted  by year")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predillegal_propadjarea.pdf", as(pdf) replace
restore

preserve
collapse (mean) propadjmined_illegal [fweight=round(areamuni_sqkm)], by(ano country)
label var propadjmined_illegal "Proportion of mined area mined illegaly adjusted"
twoway (line propadjmined_illegal ano if country=="Col", sort lw(thick))(line propadjmined_illegal ano if country=="Peru", sort lw(thick) ), title("Proportion of mined area mined illegaly adjusted by year")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predillegal_propadjmined.pdf", as(pdf) replace
restore

qui gen ind_col=0
qui replace ind_col=1 if country=="Col"
preserve
fvset base 2010 ano
char ano[omit] 2010
qui areg prop_illegal c.ind_col##b2010.ano   , absorb(codmpio) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(*#c.ind_col 2010b.ano#co.ind_col) ci vertical yline(0) xline(2011) rename(2007.ano#c.ind_col=2007 ///
2004.ano#c.ind_col=2004 ///
2005.ano#c.ind_col=2005 ///
2006.ano#c.ind_col=2006 ///
2008.ano#c.ind_col=2008 ///
2009.ano#c.ind_col=2009 ///
2010b.ano#co.ind_col=2010 ///
2011.ano#c.ind_col=2011 ///
2012.ano#c.ind_col=2012 ///
2013.ano#c.ind_col=2013 ///
2014.ano#c.ind_col=2014)
graph export "$latexslides/diff_in_diff_avanzado.pdf", replace

qui areg propmined_illegal c.ind_col##b2010.ano   , absorb(codmpio) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(*#c.ind_col 2010b.ano#co.ind_col) ci vertical yline(0) xline(2011) rename(2007.ano#c.ind_col=2007 ///
2004.ano#c.ind_col=2004 ///
2005.ano#c.ind_col=2005 ///
2006.ano#c.ind_col=2006 ///
2008.ano#c.ind_col=2008 ///
2009.ano#c.ind_col=2009 ///
2010b.ano#co.ind_col=2010 ///
2011.ano#c.ind_col=2011 ///
2012.ano#c.ind_col=2012 ///
2013.ano#c.ind_col=2013 ///
2014.ano#c.ind_col=2014)
graph export "$latexslides/diff_in_diff_avanzado_mined.pdf", replace
restore


eststo clear
qui xi: my_ptest prop_illegal_bef prop_illegal_aft propmined_illegal_bef propmined_illegal_aft , by(country) 

esttab using "$latexslides/sumstats_regvar.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Colombia" "Peru" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 






qui eststo m1: xi: areg propmined_illegal after_x_col i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg propmined_illegal after_x_col after_x_pctg_loss  i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg propmined_illegal ind_after after_x_col trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg propmined_illegal ind_after after_x_col after_x_pctg_loss  trend_col trend_peru, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/fmined_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_col after_x_pctg_loss  )  stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace



qui eststo m1: xi: areg prop_illegal after_x_col i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg prop_illegal after_x_col after_x_loser  i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg propmined_illegal after_x_col i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg propmined_illegal after_x_col after_x_loser  i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/main_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_loser  )  stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

gen prop_illegal_2004=prop_illegal if ano==2004
bys codmpio: egen prop_illegal_base=mean(prop_illegal_2004)
gen prop_illegal_relbase=prop_illegal/prop_illegal_base

qui eststo m5: xi: areg prop_illegal_relbase after_x_col i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m6: xi: areg prop_illegal_relbase after_x_col after_x_loser  i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm


qui estout m5 m6 m3 m4 using "$latexslides/main_reg_relbase.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_loser  )  stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui eststo clear
qui eststo m1: xi: areg prop_illegal ind_after after_x_col trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg prop_illegal ind_after after_x_col after_x_loser  trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg propmined_illegal ind_after after_x_col trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg propmined_illegal ind_after after_x_col after_x_loser  trend_col trend_peru, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/ctrend_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_col after_x_loser  )  stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui eststo clear
qui eststo m1: xi: areg prop_illegal ind_after  price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg prop_illegal ind_after  after_x_loser price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg propmined_illegal ind_after  price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg propmined_illegal ind_after  after_x_loser price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/oldmain_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser  )  stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

** With continuous measure of losers

qui eststo m1: xi: areg prop_illegal after_x_col i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg prop_illegal after_x_col after_x_pctg_loss  i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg propmined_illegal after_x_col i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg propmined_illegal after_x_col after_x_pctg_loss  i.ano , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/continuous_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_pctg_loss  )  stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui eststo clear
qui eststo m1: xi: areg prop_illegal ind_after after_x_col trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg prop_illegal ind_after after_x_col after_x_pctg_loss  trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg propmined_illegal ind_after after_x_col trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg propmined_illegal ind_after after_x_col after_x_pctg_loss  trend_col trend_peru, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/continuousctrend_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_col after_x_pctg_loss  )  stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui eststo clear
qui eststo m1: xi: areg prop_illegal ind_after  price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg prop_illegal ind_after  after_x_pctg_loss price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg propmined_illegal ind_after  price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg propmined_illegal ind_after  after_x_pctg_loss price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/oldcontinuous_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss  )  stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui eststo clear
qui eststo m1: xi: areg propadj_illegal ind_after  price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg propadj_illegal ind_after  after_x_loser price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg propadjmined_illegal ind_after  price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg propadjmined_illegal ind_after  after_x_loser price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/oldmainadj_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser  )  stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui eststo clear
qui eststo m1: xi: areg prop_illegal ind_after if country=="Col", absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg prop_illegal ind_after  after_x_loser if country=="Col"  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg propmined_illegal ind_after if country=="Col" , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg propmined_illegal ind_after  after_x_loser if country=="Col" , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/oldntnpmain_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser  )  stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
