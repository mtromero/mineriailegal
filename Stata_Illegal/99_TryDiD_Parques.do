use "$base_out/panel_final.dta", replace
keep if country=="Col"
keep codmpio depto municipio ano pobl_tot MuniMinero analizedarea_sqkm areamuni_sqkm areaprmined_sqkm analizedareaSP_sqkm areamuniSP_sqkm areaprilegalSP_sqkm titulosareaSP_sqkm titulosarea_sqkm areaprilegalSP_sqkm propmined_illegal_tityear propmined_illegal_prob ind_after

gen Share_Illegal_Outside=areaprilegalSP_sqkm/(analizedareaSP_sqkm)
gen Share_Illegal_Inside=(areaprmined_sqkm-areaprilegalSP_sqkm)/(analizedarea_sqkm-analizedareaSP_sqkm)

reshape long Share_Illegal@, i(codmpio ano) j(inside) string

gen Outside=(inside=="_Outside")
encode inside, gen(inside2)
replace Share_Illegal=1 if Share_Illegal>1 & !missing(Share_Illegal)
replace Share_Illegal=0 if Share_Illegal<0 & !missing(Share_Illegal)

reghdfe Share_Illegal c.ind_after#c.Outside, abs(codmpio ano inside2) vce(cluster codmpio)
reghdfe Share_Illegal c.ind_after#c.Outside, abs(codmpio##ano codmpio##inside2) vce(cluster codmpio)

fvset base 2010 ano
char ano[omit] 2010
tab ano, gen(D_)
foreach var of varlist D_*{
	replace `var'=`var'*Outside
}
reghdfe Share_Illegal D_1-D_6 D_8-D_11, abs(codmpio##ano codmpio##inside2) vce(cluster codmpio)
			coefplot, graphregion(color(white)) baselevels keep(D_*) ci rename(D_1=2004 D_2=2005 D_3=2006 D_4=2007 D_5=2008 ///
			D_6=2009 D_8=2011 D_9=2012 D_10=2013 D_11=2014) yla(, ang(h))  vertical yline(0) xtitle("Time to reform", size(large)) ytitle("Coefficient", size(large)) 
			
			
reghdfe Share_Illegal D_1-D_6 D_8-D_11, abs(codmpio ano inside2) vce(cluster codmpio)
			coefplot, graphregion(color(white)) baselevels keep(D_*) ci rename(D_1=2004 D_2=2005 D_3=2006 D_4=2007 D_5=2008 ///
			D_6=2009 D_8=2011 D_9=2012 D_10=2013 D_11=2014) yla(, ang(h))  vertical yline(0) xtitle("Time to reform", size(large)) ytitle("Coefficient", size(large)) 