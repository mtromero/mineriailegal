import excel "$mipath/RawData/Contaduria/INDICADORES LEY 617 DE 2000.xlsx", sheet("T1") cellrange(B12:AN1155) firstrow clear
keep C CATEGORÍA K P U Z AE AJ D
replace C=substr(C,6,5)
rename CATEGORÍA Cat2009
rename K Cat2010
rename P Cat2011
rename U Cat2012
rename Z Cat2013
rename AE Cat2014
rename AJ Cat2015
rename C codmpio
drop if substr(codmpio,3,3)=="000"
drop D
destring, replace
compress
tempfile cat_fiscal
drop if codmpio==.
bys codmpio: gen N=_n
drop if N>1
drop N
reshape long Cat@, i(codmpio) j(ano)
rename Cat cat_fiscal
save `cat_fiscal', replace


use "$mipath/RawData/PanelCEDE/PANEL_CARACTERISTICAS_GENERALES.dta", clear
keep pobl_tot  codmpio ano
keep if ano>=2009 & ano<=2015
gen cat_pob="E" if pobl_tot>=500001 & !missing(pobl_tot)
replace cat_pob="1" if pobl_tot>=100001 & pobl_tot<=500000
replace cat_pob="2" if pobl_tot>=50001 & pobl_tot<=100000
replace cat_pob="3" if pobl_tot>=30001 & pobl_tot<=50000
replace cat_pob="4" if pobl_tot>=20001 & pobl_tot<=30000
replace cat_pob="5" if pobl_tot>=10001 & pobl_tot<=20000
replace cat_pob="6" if pobl_tot<=10000
drop pobl_tot
merge 1:1 codmpio ano using `cat_fiscal'
replace cat_pob="0" if cat_pob=="E"
replace cat_fiscal="0" if cat_fiscal=="E"
replace cat_fiscal="" if cat_fiscal=="NC"
destring, replace
gen categoria=cat_pob if cat_pob==cat_fiscal
replace categoria=cat_fiscal if cat_pob!=cat_fisca
*rename categoria categoria_propia
keep categoria codmpio ano
tostring categoria, replace
replace categoria="E" if categoria=="0"
tempfile categoria_propia
save `categoria_propia', replace

merge 1:1 codmpio ano using "$mipath/RawData/PanelCEDE/PANEL_BUEN_GOBIERNOnuevo13.dta", keepus(categoria) update replace
drop _merge
tab categoria ano
save `categoria_propia', replace

use "$mipath/RawData/PanelCEDE/PANEL_BUEN_GOBIERNOnuevo13.dta", clear
keep codmpio ano y_total y_cap_regalias SGR* SRA*
tempfile temp1

foreach var of varlist SGR*{
		replace `var'=`var'/1e3 /*manual dice que SGR esta en miles de pesos -- pasar a millones de pesos*/
	}
foreach var of varlist SRA*{
		replace `var'=`var'/1e6 /*manual dice que esta en pesos -- pasar a millones de pesos */
	}
	
/*
egen regalias=rowtotal(SGR_total SRAanh_regalias_productor SRAanh_regalias_puerto  SRAingeominasanh_giros_totales SRAingeominasanh_rendimientos), missing
cor regalias y_cap_regalias if ano<=2012 & y_cap_regalias!=0 & regalias!=0 & !missing(y_cap_regalias) & !missing(regalias)
plot regalias y_cap_regalias if ano<=2012 & y_cap_regalias!=0 & regalias!=0 & !missing(y_cap_regalias) & !missing(regalias)
reg regalias y_cap_regalias if ano<=2012 & y_cap_regalias!=0 & regalias!=0 & !missing(y_cap_regalias) & !missing(regalias)
*/
save `temp1'
*SRAanh_regalias_puerto
forvalues year=2012/2016{
	import excel "C:\Users\Mauricio\Dropbox\MineriaIlegal\RawData\SICODIS\SICODIS_`year'.xls", sheet("Datos Reporte SGR") cellrange(B18:AG1245) firstrow clear
	keep CodEntidad AsignacionesDirectasAD- Total 
	/*No incluir ni CARs, ni fondo de funcionamiento, ni de mantenimiento, ni otros */
	drop if substr(CodEntidad,1,1)=="B"
	drop if substr(CodEntidad,1,1)=="C"
	drop if substr(CodEntidad,1,1)=="F"
	drop if substr(CodEntidad,1,1)=="M"
	drop if substr(CodEntidad,3,3)=="000" /*drop departamentos */
	rename AsignacionesDirectasAD SGR_adirectas
	rename DirectasMinería SGR_adirecta_minera
	rename DirectasHidrocarburos SGR_adirecta_hidrocarburos
	rename FDRCompensarAD SGR_fdrcomp
	rename FDRInversión SGR_fdr
	rename FCR60 SGR_fcr
	rename FCRAsigEspecíficas SGR_especificas
	rename CienciayTecnología SGR_fcti
	rename FONPET SGR_fonpet
	rename FAE SGR_fae
	rename DesahorroFAE SGR_fae_desahorro
	rename RendimientosFinancieros SGR_rendfinancieros
	rename Total SGR_total
	rename CodEntidad codmpio
	keep codmpio SGR*
	/*ponga todo en millones de pesos*/
	foreach var of varlist SGR*{
		replace `var'=`var'/1e6
	}
	
	gen ano=`year'
	destring, replace
	tempfile temp`year'
	save `temp`year''
	use  `temp1', clear
	merge 1:1 codmpio ano using `temp`year'', update replace
	drop _merge
	save `temp1', replace
}

*Este es el total de regalias giradas, incluye el sistema antiguo (SRA, tanto hidrocarburos, como minerales, como rendimientos y otros), como el sistema nuevo SGR*/
egen regalias=rowtotal(SGR_total SRAanh_regalias_productor SRAanh_regalias_puerto  SRAingeominasanh_giros_totales SRAingeominasanh_rendimientos), missing
keep regalias codmpio ano y_total SGR_fcr SGR_especificas 
save "$base_out/Ingresos_CEDE_SICODIS.dta", replace 


use "$mipath/RawData/PanelCEDE/PANEL_CARACTERISTICAS_GENERALES.dta", clear
keep codmpio ano nbi pobl_tot
drop if ano!=2011
drop ano
rename nbi nbi_2011
merge 1:m codmpio using "$base_out/Ingresos_CEDE_SICODIS.dta"
drop _merge

merge 1:1 codmpio ano using `categoria_propia'
drop _merge 
save "$base_out/Ingresos_CEDE_SICODIS.dta", replace 


use "$base_out/Ingresos_CEDE_SICODIS.dta", clear
gen ind_after=(ano>=2012) & !missing(ano)
/*
drop if nbi_2011<20
drop if nbi_2011>40
drop if ano<2011
drop if ano>2012
*/

egen bin_nbi_2011=cut(nbi_2011), at(0(0.5)100)
drop if missing(bin_nbi_2011)

*ind_after

gen frac_regalias=regalias/y_total /*note that y_total ya esta en millones de pesos*/
sort codmpio -ano
*replace categoria=categoria[_n-1] if categoria=="" & codmpio==codmpio[_n-1]

gen regalias_pc=regalias/pobl_tot
gen SGR_especificas_pc=SGR_especificas/pobl_tot

forvalues year=2012/2016{
	preserve
	*drop if categoria=="4"
	*drop if categoria=="5"
	*drop if categoria=="6"
	collapse (mean) regalias frac_regalias SGR_especificas regalias_pc SGR_especificas_pc if ano==`year', by(bin_nbi_2011 categoria)
	rename bin_nbi_2011 nbi_2011
	*regalias frac_regalias
	foreach var in   SGR_especificas SGR_especificas_pc regalias_pc {
		twoway ///
		scatter `var' nbi_2011 if nbi_2011>=35 & !missing(categoria), graphregion(color(white))  mc(black) ms(square) msize(small) || ///
		scatter `var' nbi_2011 if nbi_2011<35 & (categoria=="4" | categoria=="5" | categoria=="6"),  mc(blue) ms(square) msize(small) || ///
		scatter `var' nbi_2011 if nbi_2011<35 & (categoria=="1" | categoria=="2" | categoria=="3" & categoria=="E"),  mc(red) ms(square) msize(small) 
		*lfit `var' nbi_2011 if nbi_2011<35 , lc(black) || ///
		*lfit `var' nbi_2011 if nbi_2011>=35, lc(black) ///
		*xtitle("Poverty (NBI 2011)") title(`year') xline(35) legend(order(1 2 ) rows(2)) 
		graph export "$graphs/VisualRD_`var'_`year'.pdf", as(pdf) replace
	}
	restore
}


exit

gen cutoff=(nbi_2011>=35)
gen recenter_nbi_2011=nbi_2011-35
gen categoria_baja=(categoria=="4" | categoria=="5" | categoria=="6") if !missing(categoria)

reghdfe SGR_especificas_pc c.cutoff c.categoria_baja#cutoff   c.recenter_nbi_2011#cutoff c.recenter_nbi_2011#c.categoria_baja c.recenter_nbi_2011#c.categoria_baja#c.cutoff , a(ano) vce(cluster codmpio)
reghdfe SGR_especificas_pc c.cutoff##c.categoria_baja##c.recenter_nbi_2011 , a(ano) vce(cluster codmpio)

reghdfe regalias_pc c.cutoff##c.categoria_baja##c.recenter_nbi_2011##c.ind_after , a(ano) vce(cluster codmpio)
reghdfe regalias_pc c.cutoff##c.categoria_baja##c.recenter_nbi_2011##c.ind_after if abs(recenter_nbi_2011)<10, a(ano) vce(cluster codmpio)

gen factor=((pobl_tot)^0.6)*(nbi_2011^0.4)
replace factor=. if nbi_2011>=35
replace factor=. if nbi_2011<35 & (categoria=="1" | categoria=="2" | categoria=="3" & categoria=="E")
bys ano: gen Suma_factor=sum(factor)
replace factor=factor/Suma_factor

reghdfe regalias_pc c.cutoff##c.categoria_baja##c.factor##c.ind_after , a(ano) vce(cluster codmpio)
reghdfe regalias_pc c.cutoff##c.categoria_baja##c.factor##c.ind_after if abs(recenter_nbi_2011)<10, a(ano) vce(cluster codmpio)



reghdfe regalias_pc c.cutoff#c.ind_after  c.recenter_nbi_2011##c.cutoff , a(ano) vce(cluster codmpio)


reghdfe regalias c.cutoff#c.ind_after  c.recenter_nbi_2011##c.cutoff , a(ano) vce(cluster codmpio)
reghdfe regalias c.cutoff#c.ind_after  c.recenter_nbi_2011##c.cutoff if abs(recenter_nbi_2011)<5 , a(ano) vce(cluster codmpio)


reghdfe frac_regalias c.cutoff#c.ind_after  c.recenter_nbi_2011##c.cutoff , a(ano) vce(cluster codmpio)
reghdfe frac_regalias c.cutoff#c.ind_after  c.recenter_nbi_2011##c.cutoff if abs(recenter_nbi_2011)<5 , a(ano) vce(cluster codmpio)


reghdfe SGR_especificas c.cutoff#c.ind_after  c.recenter_nbi_2011##c.cutoff , a(ano) vce(cluster codmpio)
reghdfe SGR_especificas c.cutoff#c.ind_after  c.recenter_nbi_2011##c.cutoff if abs(recenter_nbi_2011)<5 , a(ano) vce(cluster codmpio)
