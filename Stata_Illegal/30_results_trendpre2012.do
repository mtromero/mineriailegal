use "$base_out/panel_final.dta", replace

************
* Summary statistics
*******
graph dot (sum) titulosarea_sqkm if country=="Col", over(ano) vertical ytitle("Total area (sqkm) mining titles by year")
graph export "$latexslides/Titles_area.pdf", as(pdf) replace
graph export "$latexpaper/Titles_area.pdf", as(pdf) replace


qui eststo clear
qui estpost tabstat pobl_tot areamuni_sqkm MuniMinero ind_metalespreciosos_ever ind_Hidrocarburos_ever  pctg_budget_roy_change ind_armedgroup_b2012 cm_pctgmines_illegal cm_pctgmines_cielo_abierto if ano==2014 & pctg_budget_roy_change!=.  , statistics(mean p50 sd min max count) columns(statistics)
qui esttab using "$latexslides/sumstats_muni.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs  booktabs

qui sum cm_pctgmines_illegal [fw= cm_nmines] if ano==2010
qui local temp=round(r(mean)*100)
file open newfile using "$latexpaper/pctg_cmim.tex", write replace
file write newfile "`temp'\%"
file close newfile

eststo clear
qui xi: my_ptest pctg_budget_roy_change ind_metalespreciosos_ever ind_Hidrocarburos_ever ///
     cm_pctgmines_cielo_abierto ///
   ind_armedgroup_b2012 pobl_tot areamuni_sqkm  if ano==2014 & areamuni_sqkm!=., by(ind_loser) 

esttab using "$latexslides/summary_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

esttab using "$latexpaper/summary_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

eststo clear
qui xi: my_ptest propmined_illegal_bef propmined_illegal_aft pctg_budget_roy_change ///
 ind_armedgroup_b2012_munil propmined_illegal_bef_ag propmined_illegal_aft_ag, by(ind_loser) 

esttab using "$latexslides/sumstats_regvarmined.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

esttab using "$latexpaper/sumstats_regvarmined.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 


*************************************
**Summary statistics by type
**********************************
**********************************

qui sum codmpio if ano==2014 & areamuni_sqkm!=.
qui local mainresult=r(N)
file open newfile using "$latexslides/Nmunis.tex", write replace
file write newfile "`mainresult'"
file close newfile
file open newfile using "$latexpaper/Nmunis.tex", write replace
file write newfile "`mainresult'"
file close newfile

qui sum codmpio if ano==2014 & areamuni_sqkm!=. & ind_loser==1
qui local mainresult=r(N)
file open newfile using "$latexslides/Nlosers.tex", write replace
file write newfile "`mainresult'"
file close newfile
file open newfile using "$latexpaper/Nlosers.tex", write replace
file write newfile "`mainresult'"
file close newfile

qui sum codmpio if ano==2007 & areamuni_sqkm!=. & cm_censed==1
qui local mainresult=r(N)
file open newfile using "$latexslides/Ncensused.tex", write replace
file write newfile "`mainresult'"
file close newfile

**Analysis of tpr and fpr by winner losser
/*
preserve
eststo clear
qui gen w_tr_tpr=tr_mine 
qui gen w_tr_tprtitulo=tr_minetitulo 
qui gen w_tr_tprnotitulo=tr_minenotitulo 
qui gen w_tr_fpr=tr_nomine 
qui gen w_tr_fprtitulo=tr_nominetitulo
qui gen w_tr_fprnotitulo=tr_nominenotitulo
qui xi: my_ptestw tr_tpr tr_tprtitulo tr_tprnotitulo tr_fpr tr_fprtitulo tr_fprnotitulo , by(ind_loser) 

esttab using "$latexslides/sumstats_tprfpr.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 
restore
*/


** Analysis of tpr and fpr with legality in the columns
***
preserve
keep if tr_mine!=. | tr_nomine!=.
keep codmpio ind_loser tr_*
*Rename variables for the re-shape
rename tr_tprtitulo tr_tprtipo1
rename tr_tprnotitulo tr_tprtipo0
rename tr_fprtitulo tr_fprtipo1
rename tr_fprnotitulo tr_fprtipo0

rename tr_minetitulo tr_minetipo1
rename tr_minenotitulo tr_minetipo0
rename tr_nominetitulo tr_nominetipo1
rename tr_nominenotitulo tr_nominetipo0

**Reshape for a separate row for the legal and the illegal part of  amuni

reshape long tr_minetipo tr_nominetipo tr_tprtipo tr_fprtipo, i(codmpio) j(ind_legality)

qui gen tr_tprwinner=tr_tprtipo if ind_loser==0
qui gen tr_tprloser=tr_tprtipo if ind_loser==1
qui gen tr_fprwinner=tr_fprtipo if ind_loser==0
qui gen tr_fprloser=tr_fprtipo if ind_loser==1
eststo clear
qui gen w_tr_tpr=tr_mine 
qui gen w_tr_tprwinner=tr_minetipo if ind_loser==0
qui gen w_tr_tprloser=tr_minetipo if ind_loser==1
qui gen w_tr_fpr=tr_nomine 
qui gen w_tr_fprwinner=tr_nominetipo if ind_loser==0
qui gen w_tr_fprloser=tr_nominetipo if ind_loser==1
label var tr_tprwinner "TPR Winner"
label var tr_tprloser "TPR Loser"
label var tr_fprwinner "FPR Winner"
label var tr_fprloser "FPR Loser"
qui xi: my_ptestw tr_tprwinner tr_tprloser tr_fprwinner tr_fprloser , by(ind_legality) 

esttab using "$latexslides/sumstats_tprfpr_bylegal.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Illegal" "Legal" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 
restore

*****
* Visual diff-diff
*******
preserve
collapse (mean) propmined_illegal , by(ano country)
label var propmined_illegal "Proportion of mined area mined illegaly"
twoway (line propmined_illegal ano if country=="Col", sort lw(thick))(line propmined_illegal ano if country=="Peru", sort lw(thick) lp(dash) ), title("Proportion of mined area mined illegaly by year")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predillegal_propmined_peru.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propmined_peru.pdf", as(pdf) replace
restore

preserve
fvset base 2010 ano
char ano[omit] 2010
qui areg propmined_illegal c.ind_loser##b2010.ano price_index_u  , absorb(codmpio) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(*#c.ind_loser 2010b.ano#co.ind_loser) ci vertical yline(0) xline(2011) rename(2007.ano#c.ind_loser=2007 ///
2004.ano#c.ind_loser=2004 ///
2005.ano#c.ind_loser=2005 ///
2006.ano#c.ind_loser=2006 ///
2008.ano#c.ind_loser=2008 ///
2009.ano#c.ind_loser=2009 ///
2010b.ano#co.ind_loser=2010 ///
2011.ano#c.ind_loser=2011 ///
2012.ano#c.ind_loser=2012 ///
2013.ano#c.ind_loser=2013 ///
2014.ano#c.ind_loser=2014)
graph export "$latexslides/partrends_wl.pdf", replace
graph export "$latexpaper/partrends_wl.pdf", replace
restore

preserve
keep if ind_muni_allyears==1
collapse (mean) propmined_illegal , by(ano country)
label var propmined_illegal "Proportion of mined area mined illegally"
twoway (line propmined_illegal ano if country=="Col", sort lw(thick))(line propmined_illegal ano if country=="Peru", sort lw(thick) lp(dash) ), scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predillegal_propmined_peru_munially.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propmined_peru_munially.pdf", as(pdf) replace
restore

**Titles
preserve
collapse (sum) titulosarea_sqkm , by(ano country)
label var titulosarea_sqkm "Area titled"
twoway (line titulosarea_sqkm ano if country=="Col", sort lw(thick))(line titulosarea_sqkm ano if country=="Peru", sort lw(thick) lp(dash) ), title("Area legal titles by year")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/plotAreaTitles.pdf", as(pdf) replace
graph export "$latexpaper/plotAreaTitles.pdf", as(pdf) replace
restore

**Winner-losser
preserve
collapse (mean) prop_illegal, by(ano ind_loser)

label var prop_illegal "Fraction of area mined illegal"
twoway (line prop_illegal ano if ind_loser==0, sort lw(thick))(line prop_illegal ano if ind_loser==1, sort lw(thick) ), title("Evolution of fraction of area mined illegally")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Winners") label(2 "Losers"))
graph export "$latexslides/diff_in_diff_wl.pdf", replace
graph export "$latexpaper/diff_in_diff_wl.pdf", replace
restore



** Col winner vs Col losers vs Peru
/*
preserve
collapse (mean) propmined_illegal [fweight=round(areamuni_sqkm)], by(ano country ind_loser)
label var propmined_illegal "Proportion of mined area mined illegaly"
twoway (line propmined_illegal ano if country=="Col" & ind_loser==0, sort lw(thick)) ///
     (line propmined_illegal ano if country=="Col" & ind_loser==1, sort lw(thick)) ///
	 (line propmined_illegal ano if country=="Peru", sort lw(thick) ), ///
	 title("Proportion of mined area mined illegaly by year")scale(1.2)   scheme(s1color) xline(2011.5) legend(label(1 "Colombia-Winner") label(2 "Colombia-Loser") label(3 "Peru"))
graph export "$latexslides/Predillegal_propmined_peru.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propmined_peru.pdf", as(pdf) replace
restore
*/
***************
* Parallel trends
*********

qui gen ind_col=0
qui replace ind_col=1 if country=="Col"
preserve
fvset base 2010 ano
char ano[omit] 2010


qui areg propmined_illegal c.ind_col##b2010.ano   , absorb(codmpio) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels scheme(s1color) keep(*#c.ind_col 2010b.ano#co.ind_col) ci vertical yline(0) xline(2011) rename(2007.ano#c.ind_col=2007 ///
2004.ano#c.ind_col=2004 ///
2005.ano#c.ind_col=2005 ///
2006.ano#c.ind_col=2006 ///
2008.ano#c.ind_col=2008 ///
2009.ano#c.ind_col=2009 ///
2010b.ano#co.ind_col=2010 ///
2011.ano#c.ind_col=2011 ///
2012.ano#c.ind_col=2012 ///
2013.ano#c.ind_col=2013 ///
2014.ano#c.ind_col=2014)
graph export "$latexslides/partrends_colperu.pdf", replace
graph export "$latexpaper/partrends_colperu.pdf", replace
restore

** Sum stats of regression vars fraction MINED

** WARNING
*Need to automatize the last row of differences
** WARNING
/*
qui gen ind_peru=(country=="Peru")
eststo clear
qui xi: my_ptest propmined_illegal_bef propmined_illegal_aft ///
  , by(ind_peru) 

esttab using "$latexslides/sumstats_regvarperu.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Colombia" "Peru" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

esttab using "$latexpaper/sumstats_regvarperu.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Colombia" "Peru" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 
*/

preserve
qui gen prop_illegal_befcol=prop_illegal if country=="Col" & ano<2012
qui gen prop_illegal_befperu=prop_illegal if country=="Peru" & ano<2012
qui gen prop_illegal_aftcol=prop_illegal if country=="Col" & ano>=2012
qui gen prop_illegal_aftperu=prop_illegal if country=="Peru" & ano>=2012
qui label var prop_illegal_befcol "Pct. of area illegal Colombia before"
qui label var prop_illegal_befperu "Pct. of area illegal Peru before"
qui label var prop_illegal_aftcol "Pct. of area illegal Colombia after"
qui label var prop_illegal_aftperu "Pct. of area illegal Peru after"

eststo clear
qui xi: my_ptest prop_illegal_befcol prop_illegal_aftcol prop_illegal_befperu prop_illegal_aftperu , by(ind_loser) 

esttab using "$latexslides/sumstats_ftotalperu.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

esttab using "$latexpaper/sumstats_ftotalperu.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

restore

**** Census balance

eststo clear
qui xi: my_ptest pctg_loss ind_metalespreciosos_ever ind_Hidrocarburos_ever ///
        ind_armedgroup_b2012 pobl_tot areamuni_sqkm  if ano==2014 & areamuni_sqkm!=., by(cm_censed) 

esttab using "$latexslides/censed_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Censused" "Not Censused" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

esttab using "$latexpaper/censed_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Censused" "Not Censused" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 


**** Balance weak institutions

eststo clear
qui xi: my_ptest propmined_illegal_bef nbi_2011 pctg_loss ind_metalespreciosos_ever ind_Hidrocarburos_ever ///
        ind_armedgroup_b2012 pobl_tot areamuni_sqkm  if  areamuni_sqkm!=., by(ind_leaf2) 

esttab using "$latexslides/weakins_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Strong" "Weak" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 


********************************************************
* Main results slide fraction MINED different ID strategies
*********************************************************
qui eststo m1: xi: areg propmined_illegal after_x_col price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"
estadd local dptrends "No"
matrix tempm=e(b)

local mainafterresult=tempm[1,1]
qui local mainafterresult=round(`mainafterresult'*100)/100
file open newfile using "$latexpaper/mainafterresult.tex", write replace
file write newfile "`mainafterresult'"
file close newfile
file open newfile using "$latexslides/mainafterresult.tex", write replace
file write newfile "`mainafterresult'"
file close newfile

*** Heterogeneous effect of main regression
qui eststo m1hetins: xi: areg propmined_illegal after_x_col after_x_ind_leaf2 price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m1judstrg: xi: areg propmined_illegal after_x_col after_x_jds price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m1hetag: xi: areg propmined_illegal after_x_col after_x_ag price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm


qui eststo m1hetnbi: xi: areg propmined_illegal after_x_col after_x_poor price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m1hetloser: xi: areg propmined_illegal after_x_col after_x_loser price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm


qui eststo m1hetgold: xi: areg propmined_illegal after_x_col after_x_gold price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m1hetoil: xi: areg propmined_illegal after_x_col after_x_oil price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m1hetpop: xi: areg propmined_illegal after_x_col after_x_pop price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m1hetarea: xi: areg propmined_illegal after_x_col after_x_area price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg propmined_illegal after_x_col after_x_pctg_loss price_index_u price_index_m trend  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"
estadd local weights "No"
matrix tempm=e(b)


local mainpctlosresult=tempm[1,2]
qui local mainpctlosresult=round(`mainpctlosresult'*100)/10


file open newfile using "$latexpaper/mainpctlosresult.tex", write replace
file write newfile "`mainpctlosresult'"
file close newfile

qui eststo m3: xi: areg propmined_illegal after_x_col after_x_peru after_x_pctg_loss trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"
estadd local weights "No"

qui eststo m3nwl: xi: areg propmined_illegal after_x_col after_x_peru trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"
estadd local weights "No"
estadd local dptrends "No"
matrix tempm=e(b)

local peruddafterresult=tempm[1,1]
qui local peruddafterresult=round(`peruddafterresult'*100)/100

file open newfile using "$latexpaper/peruddafterresult.tex", write replace
file write newfile "`peruddafterresult'"
file close newfile

file open newfile using "$latexslides/peruddafterresult.tex", write replace
file write newfile "`peruddafterresult'"
file close newfile

replace after_x_ind_leaf2=0 if country=="Peru"


qui eststo m4: xi: areg propmined_illegal after_x_col  after_x_pctg_loss i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

qui eststo m4nwl: xi: areg propmined_illegal after_x_col  i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

********* Legal titles


qui eststo m1tit: xi: areg titulosarea_ha after_x_col price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"

qui eststo m2tit: xi: areg titulosarea_ha after_x_col after_x_peru trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"

qui eststo m3tit: xi: areg titulosarea_ha after_x_col   i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

qui estout m1 m3nwl m4nwl using "$latexslides/fminednwl_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  after_x_peru ) prefoot(\midrule ) stats( timefetrend N N_clust ymean r2 , fmt( %fmt a2 a2 a2 a2 ) labels ("Time FE-Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1tit m2tit m3tit using "$latexslides/titulosarea_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats( timefetrend N N_clust ymean r2 , fmt( %fmt a2 a2 a2 a2 ) labels ("Time FE-Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m3nwl m4nwl m1tit m2tit m3tit using "$latexpaper/fminednwl_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m1hetins m1hetag  using "$latexslides/fminedhetins_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m1hetins  m1judstrg m1hetoil m1hetpop m1hetarea using "$latexslides/fminedhet1_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m1hetloser m1hetag m1hetgold m1hetnbi  using "$latexslides/fminedhet2_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m1hetins  m1judstrg m1hetoil m1hetpop m1hetarea  using "$latexpaper/fminedhet1_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m1hetloser m1hetag m1hetgold m1hetnbi  using "$latexpaper/fminedhet2_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


qui estout m1 m2 m3 m4 using "$latexslides/fmined_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_pctg_loss after_x_peru ) prefoot(\midrule ) stats( timefetrend N N_clust ymean r2 , fmt( %fmt a2 a2 a2 a2 ) labels ("Time FE-Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/fmined_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_pctg_loss after_x_peru price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


******* Plot of heterogeneous effects
coefplot (m1hetins \ m1judstrg \ m1hetoil \ m1hetpop \ m1hetarea  \ m1hetloser \ m1hetag \ m1hetgold \ m1hetnbi), ///
scheme(s1color) keep(after_x_ind_leaf2  after_x_jds after_x_ag after_x_poor after_x_loser after_x_gold after_x_oil after_x_pop after_x_area) xline(0)

graph export "$latexslides/hetplot.pdf", replace
graph export "$latexpaper/hetplot.pdf", replace

** CLICK 1
** Calculate bounds of ommited variable bias
** See Altonji et al(2005) or Oster(2013)

/*
qui reg propmined_illegal after_x_col after_x_pctg_loss trend_col trend_peru i.codmpio
matrix tempm=e(b)
local ols_after=tempm[1,1]
local ols_afteraffi=tempm[1,2]

*Altonji indicator after
psacalc after_x_col set
qui local absdif=abs(`r(output)'-`ols_after')
qui local altonjilb_after=round((`ols_after'-`absdif')*100)/100
file open newfile using "$latexslides/altonjiminedlb_aftercol.tex", write replace
file write newfile "`altonjilb_after'"
file close newfile
file open newfile using "$latexpaper/altonjiminedlb_aftercol.tex", write replace
file write newfile "`altonjilb_after'"
file close newfile

qui local altonjiub_after=round((`ols_after'+`absdif')*100)/100
file open newfile using "$latexslides/altonjiminedub_aftercol.tex", write replace
file write newfile "`altonjiub_after'"
file close newfile
file open newfile using "$latexpaper/altonjiminedub_aftercol.tex", write replace
file write newfile "`altonjiub_after'"
file close newfile

*Altonji indicator afterXLoser
psacalc after_x_pctg_loss set

qui local absdif=abs(`r(output)'-`ols_afteraffi')
qui local altonjilb_afteraffi=round((`ols_afteraffi'-`absdif')*100)/100
file open newfile using "$latexslides/altonjiminedlb_afterploss.tex", write replace
file write newfile "`altonjilb_afteraffi'"
file close newfile
file open newfile using "$latexpaper/altonjiminedlb_afterploss.tex", write replace
file write newfile "`altonjilb_afteraffi'"
file close newfile

qui local altonjiub_afteraffi=round((`ols_afteraffi'+`absdif')*100)/100
file open newfile using "$latexslides/altonjiminedub_afterploss.tex", write replace
file write newfile "`altonjiub_afteraffi'"
file close newfile
file open newfile using "$latexpaper/altonjiminedub_afterploss.tex", write replace
file write newfile "`altonjiub_afteraffi'"
file close newfile
*/

** CLICK 2
**See Belloni et al for explanation
/*
preserve
do "$dir_do/32_singleLasso"
restore
*/


** CLICK 3
** Robustness to different weight


qui gen w_analizedpctgarea=round( 100*analizedarea_sqkm/ areamuni_sqkm )

qui eststo m5: xi: areg propmined_illegal after_x_col price_index_u price_index_m trend [fw=w_analizedpctgarea]  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local weights "Yes"

qui eststo m6: xi: areg propmined_illegal after_x_col after_x_peru trend_col trend_peru [fw=w_analizedpctgarea], absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local weights "Yes"

qui estout m1 m5 m3nwl m6 using "$latexslides/weights_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats(weights   N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Weights"  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m5 m3nwl m6 using "$latexpaper/weights_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats(weights   N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Weights"  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

** CLICK 4
** Robustness to state trends
qui foreach x of varlist trend_depto* {
replace `x'=0 if `x'==.
}

qui eststo m7: xi: areg propmined_illegal after_x_col price_index_u price_index_m trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local dptrends "Yes"

qui eststo m8: xi: areg propmined_illegal after_x_col after_x_peru trend_depto* trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local dptrends "Yes"

qui estout m1 m7 m3nwl m8 using "$latexslides/fmined_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats(dptrends   N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("State trends"  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m7 m3nwl m8 using "$latexpaper/fmined_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats(dptrends   N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("State trends"  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

** CLICK 6
** New mined area
qui eststo n1: xi: areg newpropmined_illegal after_x_col price_index_u price_index_m trend  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "Trend"

qui eststo n2: xi: areg newpropmined_illegal after_x_col after_x_peru trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "Trend"


qui eststo n3: xi: areg newpropmined_illegal after_x_col  i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "TimeFE"   


qui estout n1 n2 n3  using "$latexslides/newarea_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats( timefetrend  N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Time FE-Trend"  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout n1 n2 n3  using "$latexpaper/newarea_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru) prefoot(\midrule ) stats( timefetrend  N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Time FE-Trend"  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

*** CLICK 
** Legalizations


qui eststo n1: xi: areg mined_lglzation after_x_col price_index_u price_index_m trend  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "Trend"

qui eststo n2: xi: areg mined_lglzation after_x_col after_x_peru trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "Trend"


qui eststo n3: xi: areg mined_lglzation after_x_col  i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "TimeFE"   


qui estout n1 n2 n3  using "$latexslides/legalization_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats( timefetrend  N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Time FE-Trend"  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout n1 n2 n3  using "$latexpaper/legalization_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru) prefoot(\midrule ) stats( timefetrend  N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Time FE-Trend"  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


** Other cutoff 
qui eststo m10: xi: areg c12_propmined_illegal after_x_col price_index_u price_index_m trend  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "Trend"

qui eststo m11: xi: areg c12_propmined_illegal after_x_col after_x_peru trend_col trend_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "Trend"

qui eststo m12: xi: areg c12_propmined_illegal after_x_col  i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "TimeFE"

qui estout m10 m11 m12 using "$latexslides/fminedc12_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  after_x_peru ) prefoot(\midrule ) stats( timefetrend N N_clust ymean r2 , fmt( %fmt a2 a2 a2 a2 ) labels ("Time FE-Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


**CLICK 7
** RD Peru

qui eststo m3_500: xi: areg propmined_illegal after_x_col after_x_peru trend_col trend_peru if dist_border_km<500, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3_1000: xi: areg propmined_illegal after_x_col after_x_peru trend_col trend_peru if dist_border_km<1000, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m3nwl m3_1000 m3_500 using "$latexslides/peruRD_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru) prefoot(\midrule ) stats(   N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*** Title closure
*** 
qui eststo m1_tit2014: xi: areg propmined_illegal_tit2014 after_x_col price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m1_tit2014 using "$latexslides/tit2014_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col ) prefoot(\midrule ) stats(   N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


** CLICK 5
******
* Adjusted measures of mining based on fpr and tpr
****
qui eststo clear

qui eststo m2: xi: areg propadj_illegal ind_after  price_index_u price_index_m  trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg propadj_illegal ind_after after_x_pctg_loss price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm


qui eststo m5: xi: areg propadjmined_illegal ind_after  price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m6: xi: areg propadjmined_illegal ind_after after_x_pctg_loss price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm


qui estout m2 m3 m5 m6 using "$latexslides/adj_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss ) prefoot(\midrule ) stats(  N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m2 m3 m5 m6 using "$latexpaper/adj_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss price_index_u ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

*********
*** Click 8
*******

qui eststo m1: xi: areg prop_illegal after_x_col price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg prop_illegal after_x_col after_x_loser price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg areaprilegal_sqkm after_x_col price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

gen log_illegal= log(areaprilegal_sqkm+1)
qui eststo m4: xi: areg log_illegal after_x_col price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/otherm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_loser) prefoot(\midrule ) stats(  N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m2 m3 m4 using "$latexpaper/otherm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_loser) prefoot(\midrule ) stats(  N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

*** Poverty RD
qui eststo clear
qui eststo m1: xi: areg propmined_illegal ind_after  after_x_pctg_loss price_index_u price_index_m trend  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg propmined_illegal ind_after  after_x_pctg_loss price_index_u price_index_m trend if nbi_2011>25 & nbi_2011<35 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg prop_illegal ind_after after_x_pctg_loss price_index_u price_index_m trend   , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg prop_illegal ind_after after_x_pctg_loss price_index_u price_index_m trend  if nbi_2011>25 & nbi_2011<35 , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm



qui estout m1 m2 m3 m4 using "$latexslides/povertyRD_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/povertyRD_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss ) prefoot(\midrule ) stats(  N N_clust ymean r2 , fmt(  a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

** For non-parametric estimation split in half winner and losers
local nlossgroups=4

qui forval i=1/`nlossgroups' {
gen D_lossG`i'=0
}
qui sum pctg_loss if pctg_loss<0, d
local lbloss=`r(p50)'
qui sum pctg_loss if pctg_loss>=0, d
local ubloss=`r(p50)'

replace D_lossG1=1 if pctg_loss<`lbloss'
replace D_lossG2=1 if pctg_loss>=`lbloss' & pctg_loss<0
replace D_lossG3=1 if pctg_loss<`ubloss' & pctg_loss>=0
replace D_lossG`nlossgroups'=1 if pctg_loss>=`ubloss' & pctg_loss!=.

forval i=1/`nlossgroups' {
gen after_x_lossG`i'=ind_after*D_lossG`i'
}

label var after_x_lossG2 "After X Bottom half winner"
label var after_x_lossG3 "After X Bottom half loser"
label var after_x_lossG4 "After X Top half loser"

qui eststo m7: xi: areg prop_illegal ind_after  after_x_lossG2-after_x_lossG4 price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"
qui eststo m8: xi: areg propmined_illegal ind_after  after_x_lossG2-after_x_lossG4 price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"
qui estout m2 m7 m4 m8 using "$latexslides/nonparametric_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss after_x_lossG* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m2 m7 m3 m4 m8 using "$latexpaper/nonparametric_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss after_x_lossG* price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace



*************************************
**Quantity index
**********************************
foreach x in carbon oro plata platino {
qui gen prod_parea_`x'=(prod_`x'/(areaprmined_sqkm-areaprilegal_sqkm))/10^6
}

qui eststo clear

** By product
qui eststo m3: xi: areg prod_parea_carbon ind_after  precio_carbon trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg prod_norm_gas ind_after  precio_Hidrocarburos trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m5: xi: areg prod_norm_petroleo ind_after  precio_Hidrocarburos trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m6: xi: areg prod_parea_oro ind_after  precio_oro trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m7: xi: areg prod_parea_plata ind_after  precio_plata trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m8: xi: areg prod_parea_platino ind_after  precio_platino trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m3 m4 m5 m6 m7 m8  using "$latexslides/prod_norm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after   ) prefoot(\midrule ) stats(N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m3 m4 m5 m6 m7 m8  using "$latexpaper/prod_norm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after  ) prefoot(\midrule ) stats(N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


***Production without normalizing
qui eststo clear
qui eststo m3: xi: areg prod_carbon ind_after after_x_loser precio_carbon trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: areg prod_gas ind_after after_x_loser precio_Hidrocarburos trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m5: xi: areg prod_petroleo ind_after after_x_loser precio_Hidrocarburos trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m6: xi: areg prod_oro ind_after after_x_loser precio_oro trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m7: xi: areg prod_plata ind_after after_x_loser precio_plata trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m8: xi: areg prod_platino ind_after after_x_loser precio_platino trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m3 m4 m5 m6 m7 m8  using "$latexslides/prod_raw_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser  ) prefoot(\midrule ) stats(N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m3 m4 m5 m6 m7 m8  using "$latexpaper/prod_raw_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser  ) prefoot(\midrule ) stats(N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

*With dept trends

qui eststo clear

qui eststo m1: xi: areg prop_illegal ind_after price_index_u  trend_depto* , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m2: xi: areg prop_illegal ind_after after_x_loser price_index_u  trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m3: xi: areg propmined_illegal ind_after  price_index_u trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m4: xi: areg propmined_illegal ind_after after_x_loser price_index_u  trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"
qui estout m1 m2 m3 m4 using "$latexslides/main_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("State Year FE" "State Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/main_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("State Year FE" "State Trend" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
