*This code is based on LevittExample.do
* Available on http://faculty.chicagobooth.edu/christian.hansen/research/#Code
* Belloni_etal(2012) “High-Dimensional Methods and Inference on Structural and Treatment Effects”.



use "$base_out/panel_final.dta", replace

replace homi_totalag=0 if homi_totalag==. & country=="Peru"
replace price_index_u=price_index_u/100 if country=="Col"
tsset codmpio ano 

* Generate variables for LASSO 
** What are the illegal mining time varying controls?
** price, population, homicides
**This normalization is a little adhoc but aims to have bariables between 0 and 1

replace homi_totalag=homi_totalag/100
replace pobl_tot=pobl_tot/10^5



local xx = "price_index_u homi_totalag pobl_tot" 


* Squared Variable
local xx2 
foreach x of local xx { 
	gen `x'2 = `x'^2 
	local tempname = "`x'2" 
	local xx2 : list xx2 | tempname 
} 

*  Interactions
local xxInt 
local nxx : word count `xx' 
forvalues ii = 1/`nxx' { 
	local start = `ii'+1 
	forvalues jj = `start'/`nxx' { 
		local temp1 : word `ii' of `xx' 
		local temp2 : word `jj' of `xx' 
		gen `temp1'X`temp2' = `temp1'*`temp2' 
		local tempname = "`temp1'X`temp2'" 
		local xxInt : list xxInt | tempname 
	} 
} 	

* Lags 
local Lxx 
foreach x of local xx { 
	gen L`x' = L.`x' 
	local tempname = "L`x'" 
	local Lxx : list Lxx | tempname 
} 

* Squared Lags
local Lxx2 
foreach x of local Lxx { 
	gen `x'2 = `x'^2 
	local tempname = "`x'2" 
	local Lxx2 : list Lxx2 | tempname 
} 

* Means, squared means, initial values absorbed with muni fixed effects



* Interactions with trends 
local xxXtrend
foreach x of local xx0 { 
	gen `x'Xtrend=`x'*trend
	local tempname = "`x'Xtrend" 
	local xxXtrend : list xxXtrend | tempname 
	}
	
	* Interactions with trends squared
	gen trend2=trend*trend
local xxXtrend2
foreach x of local xx0 { 
	gen `x'Xtrend2=`x'*trend2
	local tempname = "`x'Xtrend2" 
	local xxXtrend2: list xxXtrend2 | tempname 
	}

local biglist : list xx | xx2 
local biglist : list biglist | xxInt 
local biglist : list biglist | Lxx 
local biglist : list biglist | Lxx2 
local biglist : list biglist | Mxx 
local biglist : list biglist | Mxx2 
local biglist : list biglist | xx0 
local biglist : list biglist | xx02 
local biglist : list biglist | xxXtrend
local biglist : list biglist | xxXtrend2 

			

			


qui eststo clear

* Regression using everything 


qui eststo m5: xi: areg propmined_illegal after_x_col `biglist'  i.ano , absorb(codmpio) vce(cluster codmpio) 
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local controls "All"

* Estimate baseline model con las mismas observaciones


qui eststo m4: xi: areg propmined_illegal after_x_col  i.ano if e(sample), absorb(codmpio) vce(cluster codmpio) 
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local controls "Main"



* Variable selection ;
qui xi: reg propmined_illegal i.codmpio i.ano

lassoShooting propmined_illegal  `biglist' , controls(_Icodmpio* _Iano*) lasiter(100) verbose(0) fdisplay(0) 
local yvSel1 `r(selected)' 
di "$`yvSel1'" 

lassoShooting after_x_col  `biglist' , controls(_Icodmpio* _Iano*) lasiter(100) verbose(0) fdisplay(0) 
local yvSel2 `r(selected)' 
di "$`yvSel2'" 

* Get union of selected instruments 
global yvSel : list yvSel1 | yvSel2 
di "${yvSel}" 



* Regression with selected controls 
qui eststo m6: xi: areg propmined_illegal after_x_col ${yvSel}  i.ano , absorb(codmpio) vce(cluster codmpio)  
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local controls "DLasso"
qui estout m4 m5 m6   using "$latexslides/lasso_withperu.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  )  stats(controls N N_clust meany r2 , fmt(%fmt  %9.2gc %9.2gc a2 a2 ) labels ("Controls" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
qui estout m4 m5 m6   using "$latexpaper/lasso_withperu.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col )  stats(controls N N_clust meany r2 , fmt(%fmt  %9.2gc %9.2gc a2 a2 ) labels ("Controls" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

preserve
keep codmpio ano ${yvSel} 
saveold "$base_out/Temporary/Lasso_vars.dta", replace
restore
