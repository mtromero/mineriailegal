import delimited C:\Users\santi\Dropbox\MineriaIlegal\DataNubes\dataframes\yearbandprob009055.csv, varnames(1) clear
forval i=1/12 {
local year=2009+floor((`i'-mod(`i'-1,6))/6)
local bandn=`i'-6*(`i'>6)
rename x009055`i' band`bandn'_`year'
qui destring band`bandn'_`year', force replace
*qui replace band`bandn'_`year'=. if band`bandn'_`year'==10000 | band`bandn'_`year'==0
}

rename x009055rf10onlymlosmecopot1 prob_2009
rename x009055rf10onlymlosmecopot2 prob_2010
destring prob_2009, force replace
destring prob_2010, force replace
forval i=1/6 {
qui gen ratio`i'=band`i'_2010/band`i'_2009
}

gen max_band=max( band1_2009, band1_2010, band2_2009, band2_2010, band3_2009, band3_2010, band4_2009, band4_2010, band5_2009, band5_2010, band6_2009, band6_2010 )
gen min_band=min( band1_2009, band1_2010, band2_2009, band2_2010, band3_2009, band3_2010, band4_2009, band4_2010, band5_2009, band5_2010, band6_2009, band6_2010 )
