*This code is based on LevittExample.do
* Available on http://faculty.chicagobooth.edu/christian.hansen/research/#Code
* Belloni_etal(2012) �High-Dimensional Methods and Inference on Structural and Treatment Effects�.

clear all
set more off

set maxvar 20000 
set matsize 11000 

use "$base_out/panel_final.dta", replace



* Normalized trend variable ;
gen trend = (ano - 2004)/2014 

tsset codmpio ano 

* Estimate baseline model in first-differences ;

** What are the illegal mining time varying controls?;
** price, population, homicides
xi: reg D.propmined_illegal D.after_x_indaffi D.price_index_u D.homi_totalag D. pobl_tot i.ano , cluster(codmpio) 


* Generate variables for LASSO 
**This normalization is a little adhoc but aims to have bariables between 0 and 1
replace price_index_u = price_index_u/100 
replace homi_totalag=homi_totalag/100
replace pobl_tot=pobl_tot/10^5


local tdums = "_Iano_2006 _Iano_2007 _Iano_2008 _Iano_2009 _Iano_2010 _Iano_2011 _Iano_2012 _Iano_2013 _Iano_2014" 
local xx = "price_index_u homi_totalag pobl_tot" 

* Differences 
local Dxx 
foreach x of local xx { 
	gen D`x' = D.`x' 
	local tempname = "D`x'" 
	local Dxx : list Dxx | tempname 
} 

* Squared Differences
local Dxx2 
foreach x of local Dxx { 
	gen `x'2 = `x'^2 
	local tempname = "`x'2" 
	local Dxx2 : list Dxx2 | tempname 
} 

* Difference Interactions
local DxxInt 
local nxx : word count `Dxx' 
forvalues ii = 1/`nxx' { 
	local start = `ii'+1 
	forvalues jj = `start'/`nxx' { 
		local temp1 : word `ii' of `Dxx' 
		local temp2 : word `jj' of `Dxx' 
		gen `temp1'X`temp2' = `temp1'*`temp2' 
		local tempname = "`temp1'X`temp2'" 
		local DxxInt : list DxxInt | tempname 
	} 
} 	

* Lags 
local Lxx 
foreach x of local xx { 
	gen L`x' = L.`x' 
	local tempname = "L`x'" 
	local Lxx : list Lxx | tempname 
} 

* Squared Lags
local Lxx2 
foreach x of local Lxx { 
	gen `x'2 = `x'^2 
	local tempname = "`x'2" 
	local Lxx2 : list Lxx2 | tempname 
} 

* Means 
local Mxx 
foreach x of local xx { 

	by codmpio: egen M`x' = mean(`x') 
	local tempname = "M`x'" 
	local Mxx : list Mxx | tempname 
} 

* Squared Means 
local Mxx2 
foreach x of local Mxx { 
	gen `x'2 = `x'^2 
	local tempname = "`x'2" 
	local Mxx2 : list Mxx2 | tempname 
} 

* Initial Levels 
local xx0 
foreach x of local xx { 
	by codmpio: gen `x'0 = `x'[1] 
	local tempname = "`x'0" 
	local xx0 : list xx0 | tempname 
} 



* Squared Initial Levels 
local xx02 
foreach x of local xx0 { 
	gen `x'2 = `x'^2 
	local tempname = "`x'2" 
	local xx02 : list xx02 | tempname 
} 

* Initial Differences 
local Dxx0 
foreach x of local Dxx { 
	by codmpio: gen `x'0 = `x'[2] 
	local tempname = "`x'0" 
	local xx0 : list xx0 | tempname 
} 

* Squared Initial Differences 
local Dxx02 
foreach x of local Dxx0 { 
	gen `x'2 = `x'^2 
	local tempname = "`x'2" 
	local Dxx02 : list Dxx02 | tempname 
} 

* Interactions with trends ;
local biglist : list Dxx | Dxx2 
local biglist : list biglist | DxxInt 
local biglist : list biglist | Lxx 
local biglist : list biglist | Lxx2 
local biglist : list biglist | Mxx 
local biglist : list biglist | Mxx2 
local biglist : list biglist | xx0 
local biglist : list biglist | xx02 
local biglist : list biglist | Dxx0 
local biglist : list biglist | Dxx02 

/*
*I am having trouble running Lasso with the double interaction time strend
local IntT 
local nxx : word count `biglist' 
foreach x of local biglist { 
	gen `x'Xt = `x'*trend 
	gen `x'Xt2 = `x'*(trend^2) 
	local tempname = "`x'Xt `x'Xt2" 
	local IntT : list IntT | tempname 
} 
	
local shared : list biglist | IntT 
*/
			

			
drop if trend == 0 
gen Dpropmined_illegal=D.propmined_illegal
gen Dafter_x_indaffi=D.after_x_indaffi
* Regression using everything 
reg Dpropmined_illegal Dafter_x_indaffi `biglist' `tdums' , cluster(codmpio) 


* Note that Stata and MATLAB results differ presumably due to how Stata
* implicitly regularizes the inverse involved in OLS 

* Variable selection ;


lassoShooting Dpropmined_illegal Dafter_x_indaffi `biglist' , controls(`tdums') lasiter(100) verbose(0) fdisplay(0) 


local yvSel `r(selected)' 
di "`yvSel'" 

* Selection for dependent variable
lassoShooting Dafter_x_indaffi `biglist' , controls(`tdums') lasiter(100) verbose(0) fdisplay(0) 
local xvSel `r(selected)' 
di "`xvSel'" 

* Get union of selected instruments 
local vDS : list yvSel | xvSel 

* Violence equation with selected controls 
reg Dpropmined_illegal Dafter_x_indaffi `vDS' `tdums' , cluster(codmpio) 
