use "$base_out\Temporary\precios_metales.dta", clear
rename precio_oro precio1
rename precio_platino precio2
drop precio_plata
reshape long precio, i( year) j(mineral)
append using "$base_out\Temporary\precios_carbon.dta"
keep if year>2003
rename year ano

replace mineral=4  if precio==. & precio_carbon!=.
replace precio=precio_carbon if precio==.

keep if ano<2015
keep ano mineral precio
save "$base_out\Temporary\precios_mineral.dta", replace

use "$base_out/Temporary/panel_prillegalMi.dta", clear
gen afterxgold=(ano>2011)*(mineral==1)
label var afterxgold "After $\times$ Gold"

gen afterxplatinum=(ano>2011)*(mineral==2)
label var afterxplatinum "After $\times$ Platinum"

merge m:1 mineral ano using "$base_out\Temporary\precios_mineral.dta"

gen regalia_mine=5
replace regalia_mine=6 if mineral==1
replace regalia_mine=3 if mineral==7
replace regalia_mine=12 if mineral==8
replace regalia_mine=10 if mineral==11
replace regalia_mine=1 if mineral==12
gen afterxregalia=(ano>2011)*regalia_mine
label var afterxregalia "After $\times \alpha$"

preserve
use "$base_out/panel_final.dta", replace
keep prod* codmpio ano
drop prod_norm_carbon- prod_norm_sal
drop *index*
reshape long prod_@, i(codmpio ano) j(mineral) string

replace mineral="1" if mineral=="oro"
replace mineral="2" if mineral=="platino"
replace mineral="4" if mineral=="carbon"

destring mineral, replace force
drop if mineral==.
rename prod_ prod
tempfile file1
save `file1'
restore

drop _merge
merge 1:1 codmpio ano mineral using `file1', keepus(prod)
drop _merge



eststo m1: reghdfe prod afterxgold afterxplatinum  precio  , absorb(codmpio#ano codmpio#mineral) vce(cluster codmpio)
qui sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

eststo m2: reghdfe prod afterxregalia  precio  , absorb(codmpio#ano codmpio#mineral) vce(cluster codmpio)
qui sum propmined_illegal_mi if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

estout m1 m2  using "$latexslides/munimi_prod_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( afterxgold afterxplatinum afterxregalia) prefoot(\midrule ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc  a2 a2  ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

estout m1 m2  using "$latexpaper/munimi_prod_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( afterxgold afterxplatinum afterxregalia) prefoot(\midrule ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc  a2 a2  ) labels (  "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

