clear all
**With only two variables the Altonji set is only one point (the biased OLS coefficient)
local Nobs=1000
matrix C = (1, 0, 1)
drawnorm x1 x2, n(`Nobs') corr(C) cstorage(lower)
gen e=rnormal(0,0.1)
gen y=1+x1+x2+e
reg y x1
psacalc x1 set

drop x1 x2
matrix C = (1, 0.5, 1)
drawnorm x1 x2, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+e
reg y x1
psacalc x1 set

drop x1 x2
matrix C = (1, -0.5, 1)
drawnorm x1 x2, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+e
reg y x1
psacalc x1 set


***************************
**With one observed variable the Altonji set is only one point (the biased OLS coefficient)
clear all

local Nobs=1000
matrix C = (1, 0, 1,0,0,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
gen e=rnormal(0,0.1)
gen y=1+x1+x2+x3+e
reg y x1
psacalc x1 set

drop x*
matrix C = (1, 0.5, 1,0,0,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+e
reg y x1
psacalc x1 set

drop x*
matrix C = (1, -0.5, 1,0,0,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+e
reg y x1
psacalc x1 set

***************************
**With two observed variables the set has upper/lower bound the OLS coefficient 
** depending on whether the correlation with the observed is positive or negative
clear all

local Nobs=1000
matrix C = (1, 0, 1,0,0,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
gen e=rnormal(0,0.1)
gen y=1+x1+x2+x3+e
reg y x1 x2
psacalc x1 set

drop x*
matrix C = (1, 0.5, 1,0,0,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+e
reg y x1 x2
psacalc x1 set

drop x*
matrix C = (1, -0.5, 1,0,0,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+e
reg y x1 x2
psacalc x1 set

***************************
**With two observed variables the set has upper/lower bound the OLS coefficient 
** depending on whether the correlation with the unobserved is negative/positive
clear all

local Nobs=1000
matrix C = (1, 0, 1,0,0,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
gen e=rnormal(0,0.1)
gen y=1+x1+x2+x3+e
reg y x1 x2
psacalc x1 set

drop x*
matrix C = (1, 0, 1,0.5,0,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+e
reg y x1 x2
psacalc x1 set

drop x*
matrix C = (1, 0, 1,-0.5,0,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+e
reg y x1 x2
psacalc x1 set

***************************
**With two observed variables the set has upper bound the OLS coefficient 
** regardless on whether the correlation of the other observed with the unobserved
**is negative/positive
clear all

local Nobs=1000
matrix C = (1, 0, 1,0,0,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
gen e=rnormal(0,0.1)
gen y=1+x1+x2+x3+e
reg y x1 x2
psacalc x1 set

drop x*
matrix C = (1, 0, 1,0,0.5,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+e
reg y x1 x2
psacalc x1 set

drop x*
matrix C = (1, 0, 1,0,-0.5,1)
drawnorm x1 x2 x3, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+e
reg y x1 x2
psacalc x1 set

***************************
**With two observed two ommited the identified set is a range
*but OLS is the lower bound regardless on whether the correlation with unobserved is positive or negative
clear all

local Nobs=1000
matrix C = (1, 0, 1,0,0,1,0,0,0,1)
drawnorm x1 x2 x3 x4, n(`Nobs') corr(C) cstorage(lower)
gen e=rnormal(0,0.1)
gen y=1+x1+x2+x3+x4+e
reg y x1 x2
psacalc x1 set

drop x*
matrix C = (1, 0, 1,0.5,0,1,0,0,0,1)
drawnorm x1 x2 x3 x4, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+x4+e
reg y x1 x2
psacalc x1 set

drop x*
matrix C = (1, 0, 1,-0.5,0,1,0,0,0,1)
drawnorm x1 x2 x3 x4, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+x4+e
reg y x1 x2
psacalc x1 set

***************************
**With two observed two ommited the identified set is a range
*but OLS is the lower bound regardless on whether the correlation with unobserved is positive or negative
clear all

local Nobs=1000
matrix C = (1, 0, 1,0,0,1,0,0,0,1)
drawnorm x1 x2 x3 x4, n(`Nobs') corr(C) cstorage(lower)
gen e=rnormal(0,0.1)
gen y=1+x1+x2+x3+x4+e
reg y x1 x2
psacalc x1 set

drop x*
matrix C = (1, 0, 1,0,0.5,1,0,0,0,1)
drawnorm x1 x2 x3 x4, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+x4+e
reg y x1 x2
psacalc x1 set

drop x*
matrix C = (1, 0, 1,0,-0.5,1,0,0,0,1)
drawnorm x1 x2 x3 x4, n(`Nobs') corr(C) cstorage(lower)
replace y=1+x1+x2+x3+x4+e
reg y x1 x2
psacalc x1 set


