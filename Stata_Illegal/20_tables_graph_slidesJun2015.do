/*
use "$mipath\CreatedData\CensoMod.dta", clear
latab mineral
latab mineral if illegal==1
latab mineral if illegal==1 & cielo_abierto==1
*/
use "$mipath\CreatedData\panel_forreg.dta", clear
qui sum royalties_pc, detail
qui local topcut=round(r(p95)*100)/100
drop if royalties_pc>`topcut'
**Add this line to exclude big cities
drop if pobl_tot>400000
***Add this line to exclude municipalities where TMI fluctuates just because sample size
drop if nacimientos<30
drop if tmi==.
***Le pego la variable selec que indica los municipios que estudiamos en la sbaldosas jugosas
**Note que no le pego ano para que pueda mostrar en el tiempo esos muni como son diferentes
** Son menos pobres y menos mineros (el centro del pais Antioquia, Cundinamarca, un poco Choco

merge m:1 codmpio using "$base_out\Temporary\Select_muni_appLunch.dta"
drop _merge

***Ahora si le pego con ano el dato de mineria ilegal para poder hacer regresioes
merge 1:1 codmpio ano using "$base_out\Illegal_data.dta"
drop _merge

merge 1:1 codmpio ano using "$base_in\Area_mineria_Legal.dta"
drop _merge
*A pixel is 30mx30m=900m2 so covert to km2
gen Area_Illegal_Km2=pix_mina*900/10^6
gen Prop_Mine_Illegal= Area_Illegal_Km2/( Area_Illegal_Km2+ AreaMinadaKm2)
/*
gen y_total_pc=y_total/pobl_tot
gen gov_trans_pc=y_total_pc*(1-ing_propios/100)
label var nbi "Poverty index (0=None poor, 100= All poor)"
label var royalties_pc "Royalties per capita (millions of COP)"
label var y_total_pc "Municipality budget per capita"
label var gov_trans_pc "National government transfers-per capita"

twoway scatter royalties_pc nbi [w=pobl_tot] if ano==2011, msymbol(circle_hollow)
graph export "$latexslides\roy_ineq.pdf", as(pdf) replace

twoway scatter gov_trans_pc nbi if ano==2011
graph export "$latexslides\transf_ineq.pdf", as(pdf) replace

twoway scatter y_total_pc nbi if ano==2011
graph export "$latexslides\budget_ineq.pdf", as(pdf) replace

label var nbi "Poverty index"
label var royalties_pc "Royalties per capita"


local x="tmi"
bys MuniMinero ano: egen mean_`x'=mean(`x')
label var mean_tmi "Mortality rate per 1,000 newborns"
twoway (line mean_`x' ano if MuniMinero==0, sort lcolor("black")) (line mean_`x' ano if MuniMinero==1, sort lcolor("red")), graphregion(color(white)) legend(lab(1 "Non-Mining") lab(2 "Mining")) ytitle("`x'")

bys MuniMinero ano: egen totalgroup_roy=total(royalties)
bys ano: egen total_roy=total(royalties)
gen prop_roy=totalgroup_roy/total_roy
twoway (line prop_roy ano if MuniMinero==0, sort lcolor("black")) (line prop_roy ano if MuniMinero==1, sort lcolor("red")), graphregion(color(white)) legend(lab(1 "Non-Mining") lab(2 "Mining")) ytitle("Proportion Royalties")


*/
gen poor=0
sort codmpio ano
forval i=2005/2014{
replace poor=1 if nbi[_n+(2011-`i')]>30 & codmpio==codmpio[_n+(2011-`i')] & ano==`i'
}
/*
bys MuniMinero poor ano: egen meang_tmi=mean(tmi)
label var meang_tmi "Mortality rate per 1,000 newborns"
qui twoway (line meang_`x' ano if MuniMinero==0 & poor==0, sort lcolor("black")) (line meang_`x' ano if MuniMinero==0 & poor==1, sort lcolor("brown")) (line meang_`x' ano if MuniMinero==1 & poor==0, sort lcolor("orange")) (line meang_`x' ano if MuniMinero==1 & poor==1, sort lcolor("red")), graphregion(color(white)) legend(lab(1 "Non-Mining Non-Poor") lab(2 "Non-Mining Poor") lab(3 "Mining Non-Poor") lab(4 "Mining Poor")) 
qui graph export "$latexslides\tmi.pdf", as(pdf) replace
*/
eststo clear
gen pct_roy_min=pct_roy if MuniMinero==1
label var pct_roy_min "Royalties as pctg if mining"
label var areaMuni_km2 "Area (km2)"
label var AreaMinadaKm2 "Mined area with title(km2)"
label var Area_Illegal_Km2 "Predicted mined area without title(km2)"

gen AreaMinadaKm2_selec=AreaMinadaKm2 if Area_Illegal_Km2!=.
label var AreaMinadaKm2_selec "Mined area selected sample"
qui estpost tabstat pobl_tot tmi nbi pct_roy pct_roy_min royalties_pc areaMuni_km2 AreaMinadaKm2 Area_Illegal_Km2 if pobl_tot!=., statistics(mean median sd max min count) columns(statistics)
qui esttab using "$latexslides\sumstats.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

/*
foreach x in pobl_tot tmi nbi pct_roy pct_roy_min royalties_pc {
qui gen `x'_selec=`x' if selec==1
}
label var pobl_tot_selec "Population selected sample"
label var tmi_selec "Mortality rate selected sample"
label var nbi_selec "Poverty index selected sample"
label var pct_roy_selec "Royalties as pctg selected sample"
label var pct_roy_min_selec "Roy pctg if mining selected"
label var royalties_pc_selec "Royalties per capita selected sample"
qui estpost tabstat pobl_tot pobl_tot_selec tmi tmi_selec nbi nbi_selec pct_roy pct_roy_selec pct_roy_min pct_roy_min_selec royalties_pc royalties_pc_selec , statistics(mean median sd max min count) columns(statistics)
qui esttab using "$latexslides\sumstats_selec.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs
*/

local varlist="royalties_pc"
foreach x of local varlist {
gen `x'_bef=`x'*(ano<2012)
gen `x'_aft=`x'*(ano>=2012)

label var `x'_bef "Royalties X Before"
label var `x'_aft "Royalties X After"

}

qui eststo m1: xi: areg tmi royalties_pc i.ano, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
qui estadd local munife "Yes"
qui estadd local timefe "Yes"
qui eststo m2: xi: areg tmi royalties_pc_* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
qui estout m1 m2 using "$latexslides\Basic_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( royalties_pc* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

drop royalties_pc_*

qui local N=5

forval i=1/`N' {
local lb=((`topcut')*(`i'-1)/`N')
local ub=((`topcut')*`i'/`N')
qui gen bin_roy_pc_`i'=1 if royalties_pc>`lb' & royalties_pc<=`ub'
qui replace bin_roy_pc_`i'=0 if royalties_pc<=`topcut' & bin_roy_pc_`i'==.

label var bin_roy_pc_`i' "Royalties per capita `lb'-`ub'"
}

qui eststo m1: xi: areg tmi bin_roy_pc* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
qui estout m1 using "$latexslides\Bin_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( bin_roy_pc* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
e
/*
gen ind_after=1 if ano>2011
replace ind_after=0 if ano<2012
gen ind_pos=1 if affi>0.03
replace ind_pos=0 if affi<0.03 & affi>-0.03
gen after_x_pos=ind_after*ind_pos
eststo m1: xi: areg tmi ind_after ind_pos after_x_pos i.ano, absorb(codmpio) vce(cluster codmpio)
*/


local varlist="royalties_pc"
foreach x of local varlist {
gen `x'_min_bef=`x'*MuniMinero*(ano<2012)
gen `x'_min_aft=`x'*MuniMinero*(ano>=2012)
gen `x'_nom_bef=`x'*(1-MuniMinero)*(ano<2012)
gen `x'_nom_aft=`x'*(1-MuniMinero)*(ano>=2012)
label var `x'_min_bef "Royalties X Mining X Before"
label var `x'_min_aft "Royalties X Mining X After"
label var `x'_nom_bef "Royalties X NON Mining X Before"
label var `x'_nom_aft "Royalties X NON Mining X After"
}

qui eststo m1: xi: areg tmi royalties_pc_* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estout m1 using "$latexslides\Min_bef_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( royalties_pc* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
drop royalties_pc_*
*/

gen posi_affi=1 if affi>0.03 & affi!=.
*replace posi_affi=0 if affi>(-0.03) & affi<0.03
replace posi_affi=0 if posi_affi==.
gen nega_affi=1 if affi<(-0.03)
*replace nega_affi=0 if affi>(-0.03) & affi<0.03
replace nega_affi=0 if nega_affi==.
local varlist="royalties_pc"
foreach x of local varlist {
gen `x'_posi_bef=`x'*posi_affi*(ano<2012)
gen `x'_posi_aft=`x'*posi_affi*(ano>=2012)
gen `x'_nposi_bef=`x'*(1-posi_affi)*(ano<2012)
gen `x'_nposi_aft=`x'*(1-posi_affi)*(ano>=2012)
gen `x'_nega_bef=`x'*nega_affi*(ano<2012)
gen `x'_nega_aft=`x'*nega_affi*(ano>=2012)
label var `x'_posi_bef "Royalties x Benefited X Before"
label var `x'_posi_aft "Royalties x Benefited X After"
label var `x'_nposi_bef "Royalties x Unchanged X Before"
label var `x'_nposi_aft "Royalties x Unchanged X After"
label var `x'_nega_bef "Royalties x Affected X Before"
label var `x'_nega_aft "Royalties x Affected X After"
}
eststo m1: xi: areg tmi royalties_pc_posi* royalties_pc_nposi* royalties_pc_nega* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

eststo m2: xi: areg tmi royalties_pc_nega* royalties_pc_nposi* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estout m1 using "$latexslides\Aff_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( royalties_pc* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
drop royalties_pc_*

local varlist="royalties_pc"
foreach x of local varlist {
gen `x'_min_bef_poor=`x'*MuniMinero*(ano<2012)*poor
gen `x'_min_aft_poor=`x'*MuniMinero*(ano>=2012)*poor
gen `x'_nom_bef_poor=`x'*(1-MuniMinero)*(ano<2012)*poor
gen `x'_nom_aft_poor=`x'*(1-MuniMinero)*(ano>=2012)*poor
gen `x'_min_bef_nopoor=`x'*MuniMinero*(ano<2012)*(1-poor)
gen `x'_min_aft_nopoor=`x'*MuniMinero*(ano>=2012)*(1-poor)
gen `x'_nom_bef_nopoor=`x'*(1-MuniMinero)*(ano<2012)*(1-poor)
gen `x'_nom_aft_nopoor=`x'*(1-MuniMinero)*(ano>=2012)*(1-poor)
label var `x'_min_bef_poor "Royalties X Mining X Before X Poor"
label var `x'_min_aft_poor "Royalties X Mining X After X Poor"
label var `x'_nom_bef_poor "Royalties X NON Mining X Before X Poor"
label var `x'_nom_aft_poor "Royalties X NON Mining X After X Poor"
label var `x'_min_bef_nopoor "Royalties X Mining X Before X NON Poor"
label var `x'_min_aft_nopoor "Royalties X Mining X After X NON Poor"
label var `x'_nom_bef_nopoor "Royalties X NON Mining X Before X NON Poor"
label var `x'_nom_aft_nopoor "Royalties X NON Mining X After X NON Poor"
}

qui eststo m1: xi: areg tmi royalties_pc_* i.ano, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estout m1 using "$latexslides\Min_bef_poorreg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( royalties_pc* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

e
*Law not fullfilled
bys codmpio: egen avg_roy_bef_reform=mean(royalties) if ano>=2007 & ano<=2010
sort codmpio ano
replace avg_roy_bef_reform= avg_roy_bef_reform[_n-1] if ano==2011 & codmpio==codmpio[_n-1]
replace avg_roy_bef_reform= avg_roy_bef_reform[_n-1] if ano==2012 & codmpio==codmpio[_n-1]
replace avg_roy_bef_reform= avg_roy_bef_reform[_n-1] if ano==2013 & codmpio==codmpio[_n-1]
replace avg_roy_bef_reform= avg_roy_bef_reform[_n-1] if ano==2014 & codmpio==codmpio[_n-1]
count if royalties< avg_roy_bef_reform*.5 & ano==2013 & MuniMinero==1
count if royalties< avg_roy_bef_reform*.5 & ano==2014 & MuniMinero==1

use "$mipath\CreatedData\Temporary\Muni_affected_reform.dta", clear

twoway (histogram affi  if MuniMinero==1 & affi>-0.7 & affi<0.3, width(0.035)  fraction  fcolor(none) lcolor(red))  ///
       (histogram affi  if MuniMinero==0 & affi>-0.7 & affi<0.3, width(0.035)  fraction fcolor(none) lcolor(blue)), legend(order(1 "Mining" 2 "NON-Mining" )) 
qui graph export "$latexslides\Muni_minero_affectedTotal.pdf", as(pdf) replace

merge 1:1 codmpio using "$base_out\Temporary\Select_muni_appLunch.dta"
drop _merge
keep if selec==1
twoway (histogram affi  if MuniMinero==1 & affi>-0.7 & affi<0.3, width(0.035)  fraction  fcolor(none) lcolor(red))  ///
       (histogram affi  if MuniMinero==0 & affi>-0.7 & affi<0.3, width(0.035)  fraction fcolor(none) lcolor(blue)), legend(order(1 "Mining" 2 "NON-Mining" )) 
qui graph export "$latexslides\Muni_minero_affectedTotal_selec.pdf", as(pdf) replace

/*
sort codmpio ano
gen delta_tmi=tmi-tmi[_n-1] if codmpio==codmpio[_n-1] & ano==(ano[_n-1]+1)
local varlist="tmi royalties"
foreach x of local varlist {
bys MuniMinero ano: egen mean_`x'=mean(`x')
bys MuniMinero ano: egen sd_`x'=sd(`x')
gen std_`x'=(`x'-mean_`x')/sd_`x'
}
*/

******
* This used to be code 21_ResidualPlots
**
use "$mipath\CreatedData\Temporary\Muni_affected_reform.dta", clear


twoway (histogram affi  if MuniMinero==1 & affi>-0.7 & affi<0.3, width(0.035)  fraction  fcolor(none) lcolor(red))  ///
       (histogram affi  if MuniMinero==0 & affi>-0.7 & affi<0.3, width(0.035)  fraction fcolor(none) lcolor(blue)), legend(order(1 "Mining" 2 "Non-Mining" )) 
qui graph export "$latexslides\Muni_minero_affectedTotal.pdf", as(pdf) replace
	   

use "$mipath\CreatedData\panel_forreg.dta", clear
qui sum royalties_pc, detail
qui local topcut=round(r(p95)*100)/100
drop if royalties_pc>`topcut'

gen poor=0
sort codmpio ano
forval i=2005/2014{
replace poor=1 if nbi[_n+(2011-`i')]>30 & codmpio==codmpio[_n+(2011-`i')] & ano==`i'
}

gen posi_affi=1 if affi>0.03 & affi!=.



areg tmi i.ano, absorb(codmpio) vce(cluster codmpio)
predict resid_tmi,resid 
areg royalties_pc i.ano, absorb(codmpio) vce(cluster codmpio)
predict resid_royalties_pc,resid 


lpoly resid_tmi resid_royalties_pc, ci noscatter addplot((histogram resid_royalties_pc, yaxis(2) fraction))

twoway (lpolyci tmi royalties_pc if ano<2012 & MuniMinero==1, clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & MuniMinero==1,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Mining Before" 4 "Mining After" )) xtitle(Royalties) xtitle(IMR)
qui graph export "$latexslides\MinerosBeforeAfter.pdf", as(pdf) replace
twoway (lpolyci tmi royalties_pc if ano<2012 & MuniMinero==0 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & MuniMinero==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Non-Mining Before" 4 "Non-Mining After" )) xtitle(Royalties) xtitle(IMR) 
qui graph export "$latexslides\NonMinerosBeforeAfter.pdf", as(pdf) replace

twoway (lpolyci tmi royalties_pc if ano<2012 & poor==1, clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & poor==1,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Poor Before" 4 "Poor After" )) xtitle(Royalties) xtitle(IMR)
qui graph export "$latexslides\PoorBeforeAfter.pdf", as(pdf) replace
twoway (lpolyci tmi royalties_pc if ano<2012 & poor==0 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & poor==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Non-Poor Before" 4 "Non-Poor After" )) xtitle(Royalties) xtitle(IMR)
qui graph export "$latexslides\NonPoorBeforeAfter.pdf", as(pdf) replace

twoway (lpolyci tmi royalties_pc if ano>=2012 & MuniMinero==1 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & MuniMinero==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Mining After" 4 "Non-Mining After" )) xtitle(Royalties) xtitle(IMR) 
qui graph export "$latexslides\AfterMineroNoMinero.pdf", as(pdf) replace
twoway (lpolyci tmi royalties_pc if ano>=2012 & poor==1 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci tmi royalties_pc if ano>=2012 & poor==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Poor After" 4 "Non-Poor After" )) xtitle(Royalties) xtitle(IMR) 
qui graph export "$latexslides\AfterPoorNoPoor.pdf", as(pdf) replace


****************
************
********** AHORA LAS MISMAS GRAFICAS CON RESIDUOS
****************************
***************
twoway (lpolyci resid_tmi resid_royalties_pc if ano<2012 & MuniMinero==1, clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & MuniMinero==1,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Mining Before" 4 "Mining After" )) xtitle(Residual Royalties) xtitle(Residual IMR)
qui graph export "$latexslides\ResidualMinerosBeforeAfter.pdf", as(pdf) replace
twoway (lpolyci resid_tmi resid_royalties_pc if ano<2012 & MuniMinero==0 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & MuniMinero==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Non-Mining Before" 4 "Non-Mining After" )) xtitle(Residual Royalties) xtitle(Residual IMR) 
qui graph export "$latexslides\ResidualNonMinerosBeforeAfter.pdf", as(pdf) replace

twoway (lpolyci resid_tmi resid_royalties_pc if ano<2012 & poor==1, clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & poor==1,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Poor Before" 4 "Poor After" )) xtitle(Residual Royalties) xtitle(Residual IMR)
qui graph export "$latexslides\ResidualPoorBeforeAfter.pdf", as(pdf) replace
twoway (lpolyci resid_tmi royalties_pc if ano<2012 & poor==0 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & poor==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Non-Poor Before" 4 "Non-Poor After" )) xtitle(Residual Royalties) xtitle(Residual IMR)
qui graph export "$latexslides\ResidualNonPoorBeforeAfter.pdf", as(pdf) replace

twoway (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & MuniMinero==1 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & MuniMinero==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Mining After" 4 "Non-Mining After" )) xtitle(Residual Royalties) xtitle(Residual IMR) 
qui graph export "$latexslides\ResidualAfterMineroNoMinero.pdf", as(pdf) replace
twoway (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & poor==1 , clcolor(red) ciplot(rline) blcolor(red) blpattern(dash)) (lpolyci resid_tmi resid_royalties_pc if ano>=2012 & poor==0 ,  clcolor(blue) ciplot(rline) blcolor(blue) blpattern(dash)),legend(order(2 "Poor After" 4 "Non-Poor After" )) xtitle(Residual Royalties) xtitle(Residual IMR) 
qui graph export "$latexslides\ResidualAfterPoorNoPoor.pdf", as(pdf) replace



