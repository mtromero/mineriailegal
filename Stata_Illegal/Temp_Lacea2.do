qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/results2014.csv", varnames(1) clear

qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.
foreach var of varlist analizedarea areaillegalmines areamines areatitulosnomine municipioarea titulosarea {
rename `var' `var'_old
}
saveold "$base_out/Temporary/oldresults2014.dta", replace

qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/LACEA/muniResults_Rf10onlymlOSMecopot_2014.csv", varnames(1) clear
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.

merge 1:1 codane year using "$base_out/Temporary/oldresults2014.dta"

foreach var of varlist analizedarea areaillegalmines areamines areatitulosnomine municipioarea titulosarea {
gen `var'_pc=100*((`var'/ `var'_old)-1)
}
