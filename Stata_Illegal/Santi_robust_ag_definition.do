qui gen after_x_ag2=ind_after*ind_armedgroup_2011
qui gen after_x_loser_x_ag2=after_x_loser*ind_armedgroup_2011
qui gen price_index_u_x_ag2=price_index_u*ind_armedgroup_2011
qui gen trend_x_ag2=trend*ind_armedgroup_2011

qui gen after_x_ag3=ind_after*ind_armedgroup_cede_2008
qui gen after_x_loser_x_ag3=after_x_loser*ind_armedgroup_cede_2008
qui gen price_index_u_x_ag3=price_index_u*ind_armedgroup_cede_2008
qui gen trend_x_ag3=trend*ind_armedgroup_cede_2008

xi: areg prop_illegal ind_after after_x_loser after_x_ag after_x_loser_x_ag price_index_u price_index_m price_index_u_x_ag trend trend_x_ag , absorb(codmpio) vce(cluster codmpio)
xi: areg prop_illegal ind_after after_x_loser after_x_ag2 after_x_loser_x_ag2 price_index_u price_index_m price_index_u_x_ag2 trend trend_x_ag2 , absorb(codmpio) vce(cluster codmpio)
xi: areg prop_illegal ind_after after_x_loser after_x_ag3 after_x_loser_x_ag3 price_index_u price_index_m price_index_u_x_ag3 trend trend_x_ag3 , absorb(codmpio) vce(cluster codmpio)
xi: areg propmined_illegal ind_after after_x_loser after_x_ag after_x_loser_x_ag price_index_u price_index_m price_index_u_x_ag trend trend_x_ag , absorb(codmpio) vce(cluster codmpio)
xi: areg propmined_illegal ind_after after_x_loser after_x_ag2 after_x_loser_x_ag2 price_index_u price_index_m price_index_u_x_ag2 trend trend_x_ag2 , absorb(codmpio) vce(cluster codmpio)
xi: areg propmined_illegal ind_after after_x_loser after_x_ag3 after_x_loser_x_ag3 price_index_u price_index_m price_index_u_x_ag3 trend trend_x_ag3 , absorb(codmpio) vce(cluster codmpio)
