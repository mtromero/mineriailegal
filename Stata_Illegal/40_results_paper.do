use "$base_out\panel_final.dta", replace



qui eststo clear
qui estpost tabstat MuniMinero pobl_tot affi ind_armedgroup_b2012 areamuni_sqkm cm_pctgmines_illegal cm_pctgmines_cielo_abierto if ano==2012 & propmined_illegal!=., statistics(mean p50 sd min max count) columns(statistics)
qui esttab using "$latexpaper\sumstats_muni.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

qui sum cm_pctgmines_illegal [fw= cm_nmines] if ano==2010
qui local temp=round(r(mean)*100)
file open newfile using "$latexpaper\pctg_cmim.tex", write replace
file write newfile "`temp'"
file close newfile

qui sum ind_armedgroup_b2012 if ano==2012 & propmined_illegal!=.
qui local temp=round(r(mean)*100)
file open newfile using "$latexpaper\pctgag.tex", write replace
file write newfile "`temp'"
file close newfile

qui sum cm_pctgmines_illegal if ano==2012 & propmined_illegal!=.
qui local temp=round(r(mean)*100)
file open newfile using "$latexpaper\pctg_cmim_muni.tex", write replace
file write newfile "`temp'"
file close newfile

qui eststo clear
qui estpost tabstat price_index_u areaprmined_sqkm areaprilegal_sqkm prop_illegal propmined_illegal  if propmined_illegal!=., statistics(mean p50 sd min max count) columns(statistics)
qui esttab using "$latexpaper\sumstats_muniyear.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

qui sum prop_illegal if propmined_illegal!=.
qui local temp=round(r(mean)*100)
file open newfile using "$latexpaper\prop_illegal.tex", write replace
file write newfile "`temp'"
file close newfile

qui sum propmined_illegal if propmined_illegal!=.
qui local temp=round(r(mean)*100)
file open newfile using "$latexpaper\propmined_illegal.tex", write replace
file write newfile "`temp'"
file close newfile
*************************************
**Including price index
**********************************

keep ${selectsam}
qui eststo clear

qui eststo m1: reg prop_illegal ind_after nega_affi after_x_affi price_index_*  , vce(cluster codmpio)
qui estadd ysumm
estadd local munife "No"
estadd local timefe "No"

qui eststo m2: xi: areg prop_illegal after_x_affi price_index_* i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
matrix tempm=e(b)
local mainresult=tempm[1,1]
qui local mainresult=round(`mainresult'*100)
file open newfile using "$latexpaper\mainarearesult.tex", write replace
file write newfile "`mainresult'"
file close newfile

qui eststo m3: reg propmined_illegal ind_after nega_affi after_x_affi price_index_*  , vce(cluster codmpio)
qui estadd ysumm
estadd local munife "No"
estadd local timefe "No"

qui eststo m4: xi: areg propmined_illegal after_x_affi price_index_* i.ano  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
matrix tempm=e(b)
local mainresult=tempm[1,1]
qui local mainresult=round(`mainresult'*100)
file open newfile using "$latexpaper\mainminedresult.tex", write replace
file write newfile "`mainresult'"
file close newfile
qui estout m1 m2 m3 m4 using "$latexpaper\price_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after nega_affi after_x_affi price_index_* )  stats(munife timefe N N_clust ymean r2 , fmt(%fmt %fmt a2 a2 a2 a2 ) labels ("Municipality FE" "Time FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*************************************
**Separated by municipalities in conflict
**********************************

qui eststo m5: xi: areg propmined_illegal after_x_affi price_index_* i.ano if ind_armedgroup_b2012==0 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui eststo m6: xi: areg propmined_illegal after_x_affi price_index_* i.ano if  ind_armedgroup_b2012==1 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui estout m4 m5 m6 using "$latexpaper\violence_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_affi price_index_* )  stats(munife timefe N N_clust ymean r2 , fmt(%fmt %fmta2 a2 a2 a2 ) labels ("Municipality FE" "Time FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace



