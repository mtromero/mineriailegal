

mat ddmat=J(6,3,0)

reg propmined_illegal ind_after ind_col after_x_col

matrix A=e(b)

matrix B=e(V)

mat ddmat[5,3] =round(A[1,3],0.01)

mat ddmat[6,3] =round(sqrt(B[3,3]),0.01)

keep if e(sample)==1

sum propmined_illegal if ind_after==0 & ind_col==0 
mat ddmat[1,1] =round(r(mean),0.01)
mat ddmat[2,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_col==0
mat ddmat[3,1] =round(r(mean),0.01)
mat ddmat[4,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==0 & ind_col==1 
mat ddmat[1,2] =round(r(mean),0.01)
mat ddmat[2,2] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_col==1 &  e(sample)==1
mat ddmat[3,2] =round(r(mean),0.01)
mat ddmat[4,2] = round(r(sd),0.01)

reg propmined_illegal ind_after if ind_col==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,1] =round(A[1,1],0.01)
mat ddmat[6,1] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_after if ind_col==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,2] =round(A[1,1],0.01)
mat ddmat[6,2] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_col if ind_after==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[1,3] =round(A[1,1],0.01)
mat ddmat[2,3] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_col if ind_after==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[3,3] =round(A[1,1],0.01)
mat ddmat[4,3] =round(sqrt(B[1,1]),0.01)

svmat ddmat
gen c=_n if ddmat3!=.
gen odd=mod(c,2)

gen t1=abs((ddmat3[_n]/ddmat3[_n+1])*odd)
gen start_col="***" if t1 >= 2.576 & t1!=. 
replace start_col="**" if t1 < 2.576 & t1 >=2.326 & t1!=.
replace start_col="*" if t1 < 2.326 & t1 >=1.96 & t1!=.
drop t1

gen t_col1=abs((ddmat1[5]/ddmat1[6])*odd) if c==5
gen start_col1="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col1="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col1="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen t_col1=abs((ddmat2[5]/ddmat2[6])*odd) if c==5
gen start_col2="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col2="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col2="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen t_col1=abs((ddmat2[5]/ddmat2[6])*odd) if c==5
gen start_col3="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col3="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col3="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen brace_left="(" if odd!=.
gen brace_right=")" if odd!=.

egen concat_se1=concat(brace_left ddmat1 brace_right) if odd==0 
egen concat_se2=concat(brace_left ddmat2 brace_right) if odd==0 
egen concat_se3=concat(brace_left ddmat3 brace_right) if odd==0 
drop brace_left brace_right

gen ddmat1_odd=ddmat1 if odd==1
egen concat_col1=concat(ddmat1_odd start_col1)
replace concat_col1=concat_se1 if concat_col1=="."

gen ddmat2_odd=ddmat2 if odd==1
egen concat_col2=concat(ddmat2_odd start_col2)
replace concat_col2=concat_se2 if concat_col2=="."

gen ddmat3_odd=ddmat3 if odd==1
egen concat_col3=concat(ddmat3_odd start_col)
replace concat_col3=concat_se3 if concat_col3=="."

keep ind_after propmined_illegal ind_col after_x_col concat_col1 concat_col2 concat_col3

foreach i in 1 2 3 {
foreach j in 1 2 3 4 5 6 {
local col`i'`j'=concat_col`i'[`j'] 
}
}
*

file open DiD using "$latexpaper/sumstats_ddperu.tex", write replace


file write DiD "\begin{tabular}{lccc}" _n
file write DiD "\midrule" _n
file write DiD " \% of mined area mined illegally          & Peru 			  & Colombia 			 & Difference \\ " _n
file write DiD "\toprule" _n
file write DiD "Before the reform    & `col11'   & `col21'     & `col31'         \\" _n
file write DiD "           & `col12'   & `col22'	 & `col32'        \\" _n
file write DiD "After the reform   & `col13'   & `col23'	 & `col33'         \\" _n
file write DiD "          & `col14'   & `col24'	 & `col34'        \\" _n
file write DiD "Difference & `col15'   & `col25'	 & `col35'         \\" _n
file write DiD "           & `col16'   & `col26'     & `col36'        \\ \hline" _n
file write DiD "\bottomrule" _n
file write DiD "\end{tabular}" _n


file close DiD

drop concat_col*
