sort codmpio ano
gen updownmuni=1 if ano==2013 & nega_affi==1 & propmined_illegal< propmined_illegal[_n+1] & propmined_illegal< propmined_illegal[_n-1]
egen updown=max(updownmuni), by(codmpio)
keep if updown==1
keep if ano>2011
keep codmpio ano analizedarea_sqkm areamuni_sqkm areaprmined_sqkm titulosarea_sqkm titulosareaprnomined_sqkm areaprilegal_sqkm
twoway line titulosarea_sqkm ano
