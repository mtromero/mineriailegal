import delimited C:\Users\santi\Dropbox\MineriaIlegal\DataNubes\dataframes\yearprob009055.csv, varnames(1) clear
forval i=1/11 {
local year=`i'+2003
rename x009055rf10onlymlosmecopot`i' p`year'
qui destring p`year', force replace
qui gen d`year'=(p`year'>0.778) if p`year'!=.
qui gen nna`year'=1 if p`year'!=.
}

egen nobs=rowtotal(nna20*)

forval i=2004/2013 {
local j=`i'+1
disp `i'
count if d`i'==1 
count if d`i'==1 & d`j'==1
count if d`i'==1 & d`j'==.
}

