use "$base_out/panel_final.dta", clear
replace after_x_col=(ind_col==1 & ano>=2011)
replace ind_after=1 if ano>=2011


gen fr_titulosarea_sqkm=100*titulosarea_sqkm/areamuni_sqkm
gen fr_titulosConcarea_sqkm=100*titulosConcarea_sqkm/areamuni_sqkm





/*
foreach var of varlist after_x*{
    replace `var'=0 if ano<2011
}
*/





********************
* TABLE 1
*****************
*I manually fix decimals and remove Min Max titles


/*
sum cm_pctgarea_illegal if inmainreg_firstv2==1 & country=="Col" & cm_area_sqkm!=0 
local tempm=string(r(mean), "%9.0fc")+"\%"
file open newfile using "$latexpaper/pctg_cmarea.tex", write replace
file write newfile "`tempm'"
file close newfile


sum cm_pctgarea_illegal_CA if inmainreg_firstv2==1 & country=="Col" & cm_area_sqkm!=0 

local tempm=string(r(mean), "%9.0fc")+"\%"
file open newfile using "$latexpaper/pctg_cmarea_CA.tex", write replace
file write newfile "`tempm'"
file close newfile

	
sum unodc_propmined_illegal if inmainreg_firstv2==1 & country=="Col" & cm_area_sqkm!=0 
local tempm=string(r(mean), "%9.0fc")+"\%"
file open newfile using "$latexpaper/pctg_UNODCarea.tex", write replace
file write newfile "`tempm'"
file close newfile


sum titulosarea_km2 if inmainreg_firstv2==1 & country=="Col" & cm_area_sqkm!=0 



*Sum stats Colombia
*Sum stats ML training
qui eststo clear
qui estpost tabstat cm_censed  cm_pctgarea_illegal  cm_pctgmines_cielo_abierto cm_pctgarea_illegal_CA unodc_propmined_illegal unodc_propmined_illegal_censed   if inmainreg_firstv2==1 & country=="Col"   , statistics(mean p50 sd min max N) columns(statistics) 
qui esttab using "$latexslides/sumstats_muniA.tex", cells("mean(fmt(%9.2fc)  label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1fc)  label(Max)) count(fmt(%9.0gc)  label(Obs.))") nomtitle nonumber label  replace noobs  booktabs 
qui esttab using "$latexpaper/sumstats_muniA.tex", cells("mean(fmt(%9.2fc) label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1fc)  label(Max)) count(fmt(%9.0gc)  label(Obs.))") nomtitle nonumber label  replace noobs  booktabs fragment nodep nonum noli nomtitles

*Sum stats reform 
qui eststo clear
qui estpost tabstat  pctg_budget_roy_change pctg_budget_roy_change_ifloss pctg_budget_roy_change_ifwin if inmainreg_firstv2==1 & country=="Col"   , statistics(mean p50 sd min max N) columns(statistics) 
qui esttab using "$latexslides/sumstats_muniB.tex", cells("mean(fmt(%9.2fc)  label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1fc)  label(Max)) count(fmt(%9.0gc)  label(Obs.))") nomtitle nonumber label  replace noobs  booktabs 
qui esttab using "$latexpaper/sumstats_muniB.tex", cells("mean(fmt(%9.2fc) label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1fc)  label(Max)) count(fmt(%9.0gc)  label(Obs.))") nomtitle nonumber label  replace noobs  booktabs fragment nodep nonum noli nomtitles


*Sum stats socio-eco 
qui eststo clear
qui estpost tabstat pobl_tot areamuni_sqkm titulosarea_km2  if inmainreg_firstv2==1 & country=="Col"   , statistics(mean p50 sd min max N) columns(statistics) 
qui esttab using "$latexslides/sumstats_muni.tex", cells("mean(fmt(%9.2fc)  label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1fc)  label(Max)) count(fmt(%9.0gc)  label(Obs.))") nomtitle nonumber label  replace noobs  booktabs 
qui esttab using "$latexpaper/sumstats_muni.tex", cells("mean(fmt(%9.2fc) label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1fc)  label(Max)) count(fmt(%9.0gc)  label(Obs.))") nomtitle nonumber label  replace noobs  booktabs fragment nodep nonum noli nomtitles

*Add Stats for PERU
qui eststo clear
qui estpost tabstat pobl_tot areamuni_sqkm titulosarea_km2 if inmainreg_firstv2==1 & country=="Peru"  , statistics(mean p50 sd min max N) columns(statistics)
qui esttab using "$latexslides/sumstats_muni_peru.tex", cells("mean(fmt(%9.2fc)  label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1fc)  label(Max)) count(fmt(%9.0gc)  label(Obs.)) ") nomtitle nonumber label  replace noobs  booktabs 
qui esttab using "$latexpaper/sumstats_muni_peru.tex", cells("mean(fmt(%9.2fc) label(Mean)) p50(fmt(%9.2fc)  label(Median)) sd(fmt(%9.2fc)  label(Std. Dev.)) min(fmt(%9.1fc)  label(Min)) max(fmt(%9.1gc)  label(Max)) count(fmt(%9.0gc)  label(Obs.)) ") nomtitle nonumber label  replace noobs  booktabs fragment
*/

********************
* TABLE 2
*****************

*Confusion Matrix from R


********************
* TABLE 3
*****************

****************************
*DD Col-Peru
*****************************


preserve
mat ddmat=J(6,3,0)

reg propmined_illegal ind_after ind_col after_x_col

matrix A=e(b)
matrix B=e(V)

mat ddmat[5,3] =round(A[1,3],0.01)
mat ddmat[6,3] =round(sqrt(B[3,3]),0.01)

keep if e(sample)==1

sum propmined_illegal if ind_after==0 & ind_col==0 
mat ddmat[1,1] =round(r(mean),0.01)
mat ddmat[2,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_col==0
mat ddmat[3,1] =round(r(mean),0.01)
mat ddmat[4,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==0 & ind_col==1 
mat ddmat[1,2] =round(r(mean),0.01)
mat ddmat[2,2] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_col==1 &  e(sample)==1
mat ddmat[3,2] =round(r(mean),0.01)
mat ddmat[4,2] = round(r(sd),0.01)

reg propmined_illegal ind_after if ind_col==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,1] =round(A[1,1],0.01)
mat ddmat[6,1] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_after if ind_col==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,2] =round(A[1,1],0.01)
mat ddmat[6,2] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_col if ind_after==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[1,3] =round(A[1,1],0.01)
mat ddmat[2,3] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_col if ind_after==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[3,3] =round(A[1,1],0.01)
mat ddmat[4,3] =round(sqrt(B[1,1]),0.01)

svmat ddmat
gen c=_n if ddmat3!=.
gen odd=mod(c,2)

gen t1=abs((ddmat3[_n]/ddmat3[_n+1])*odd)
gen start_col="***" if t1 >= 2.576 & t1!=. 
replace start_col="**" if t1 < 2.576 & t1 >=2.326 & t1!=.
replace start_col="*" if t1 < 2.326 & t1 >=1.96 & t1!=.
drop t1

gen t_col1=abs((ddmat1[5]/ddmat1[6])*odd) if c==5
gen start_col1="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col1="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col1="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen t_col1=abs((ddmat2[5]/ddmat2[6])*odd) if c==5
gen start_col2="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col2="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col2="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen t_col1=abs((ddmat2[5]/ddmat2[6])*odd) if c==5
gen start_col3="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col3="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col3="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen brace_left="(" if odd!=.
gen brace_right=")" if odd!=.

egen concat_se1=concat(brace_left ddmat1 brace_right) if odd==0 
egen concat_se2=concat(brace_left ddmat2 brace_right) if odd==0 
egen concat_se3=concat(brace_left ddmat3 brace_right) if odd==0 
drop brace_left brace_right

gen ddmat1_odd=ddmat1 if odd==1
egen concat_col1=concat(ddmat1_odd start_col1)
replace concat_col1=concat_se1 if concat_col1=="."

gen ddmat2_odd=ddmat2 if odd==1
egen concat_col2=concat(ddmat2_odd start_col2)
replace concat_col2=concat_se2 if concat_col2=="."

gen ddmat3_odd=ddmat3 if odd==1
egen concat_col3=concat(ddmat3_odd start_col)
replace concat_col3=concat_se3 if concat_col3=="."

keep ind_after propmined_illegal ind_col after_x_col concat_col1 concat_col2 concat_col3

foreach i in 1 2 3 {
foreach j in 1 2 3 4 5 6 {
local col`i'`j'=concat_col`i'[`j'] 
}
}
*


qui local temp=round(`col23')
file open newfile using "$latexpaper\propmined_illegal.tex", write replace
file write newfile "`temp'"
file close newfile


file open DiD using "$latexpaper/sumstats_ddperu.tex", write replace


file write DiD "\begin{tabular}{lccc}" _n
file write DiD "\midrule" _n
file write DiD " \% of mined area mined illegally          & Peru 			  & Colombia 			 & Difference \\ " _n
file write DiD "\toprule" _n
file write DiD "Before the reform    & `col11'   & `col21'     & `col31'         \\" _n
file write DiD "           & `col12'   & `col22'	 & `col32'        \\" _n
file write DiD "After the reform   & `col13'   & `col23'	 & `col33'         \\" _n
file write DiD "          & `col14'   & `col24'	 & `col34'        \\" _n
file write DiD "Difference & `col15'   & `col25'	 & `col35'         \\" _n
file write DiD "           & `col16'   & `col26'     & `col36'        \\ \hline" _n
file write DiD "\bottomrule" _n
file write DiD "\end{tabular}" _n


file close DiD

restore



****************************
*DD Col-Peru long term
*****************************


preserve
mat ddmat=J(6,3,0)

reg propmined_illegal ind_after ind_col after_x_col if ano==2010 | ano==2014

matrix A=e(b)
matrix B=e(V)

mat ddmat[5,3] =round(A[1,3],0.01)
mat ddmat[6,3] =round(sqrt(B[3,3]),0.01)

keep if e(sample)==1

sum propmined_illegal if ind_after==0 & ind_col==0 
mat ddmat[1,1] =round(r(mean),0.01)
mat ddmat[2,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_col==0
mat ddmat[3,1] =round(r(mean),0.01)
mat ddmat[4,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==0 & ind_col==1 
mat ddmat[1,2] =round(r(mean),0.01)
mat ddmat[2,2] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_col==1 &  e(sample)==1
mat ddmat[3,2] =round(r(mean),0.01)
mat ddmat[4,2] = round(r(sd),0.01)

reg propmined_illegal ind_after if ind_col==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,1] =round(A[1,1],0.01)
mat ddmat[6,1] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_after if ind_col==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,2] =round(A[1,1],0.01)
mat ddmat[6,2] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_col if ind_after==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[1,3] =round(A[1,1],0.01)
mat ddmat[2,3] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_col if ind_after==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[3,3] =round(A[1,1],0.01)
mat ddmat[4,3] =round(sqrt(B[1,1]),0.01)

svmat ddmat
gen c=_n if ddmat3!=.
gen odd=mod(c,2)

gen t1=abs((ddmat3[_n]/ddmat3[_n+1])*odd)
gen start_col="***" if t1 >= 2.576 & t1!=. 
replace start_col="**" if t1 < 2.576 & t1 >=2.326 & t1!=.
replace start_col="*" if t1 < 2.326 & t1 >=1.96 & t1!=.
drop t1

gen t_col1=abs((ddmat1[5]/ddmat1[6])*odd) if c==5
gen start_col1="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col1="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col1="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen t_col1=abs((ddmat2[5]/ddmat2[6])*odd) if c==5
gen start_col2="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col2="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col2="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen t_col1=abs((ddmat2[5]/ddmat2[6])*odd) if c==5
gen start_col3="***" if t_col1 >= 2.576 & t_col1!=. 
replace start_col3="**" if t_col1 < 2.576 & t_col1 >=2.326 & t_col1!=.
replace start_col3="*" if t_col1 < 2.326 & t_col1 >=1.96 & t_col1!=.
drop t_col1

gen brace_left="(" if odd!=.
gen brace_right=")" if odd!=.

egen concat_se1=concat(brace_left ddmat1 brace_right) if odd==0 
egen concat_se2=concat(brace_left ddmat2 brace_right) if odd==0 
egen concat_se3=concat(brace_left ddmat3 brace_right) if odd==0 
drop brace_left brace_right

gen ddmat1_odd=ddmat1 if odd==1
egen concat_col1=concat(ddmat1_odd start_col1)
replace concat_col1=concat_se1 if concat_col1=="."

gen ddmat2_odd=ddmat2 if odd==1
egen concat_col2=concat(ddmat2_odd start_col2)
replace concat_col2=concat_se2 if concat_col2=="."

gen ddmat3_odd=ddmat3 if odd==1
egen concat_col3=concat(ddmat3_odd start_col)
replace concat_col3=concat_se3 if concat_col3=="."

keep ind_after propmined_illegal ind_col after_x_col concat_col1 concat_col2 concat_col3

foreach i in 1 2 3 {
foreach j in 1 2 3 4 5 6 {
local col`i'`j'=concat_col`i'[`j'] 
}
}
*


qui local temp=round(`col23')
file open newfile using "$latexpaper\propmined_illegal_long.tex", write replace
file write newfile "`temp'"
file close newfile


file open DiD using "$latexpaper/sumstats_ddperu_long.tex", write replace


file write DiD "\begin{tabular}{lccc}" _n
file write DiD "\midrule" _n
file write DiD " \% of mined area mined illegally          & Peru 			  & Colombia 			 & Difference \\ " _n
file write DiD "\toprule" _n
file write DiD "2010 (before the reform)    & `col11'   & `col21'     & `col31'         \\" _n
file write DiD "           & `col12'   & `col22'	 & `col32'        \\" _n
file write DiD "2014 (after the reform)   & `col13'   & `col23'	 & `col33'         \\" _n
file write DiD "          & `col14'   & `col24'	 & `col34'        \\" _n
file write DiD "Difference & `col15'   & `col25'	 & `col35'         \\" _n
file write DiD "           & `col16'   & `col26'     & `col36'        \\ \hline" _n
file write DiD "\bottomrule" _n
file write DiD "\end{tabular}" _n


file close DiD

restore


********************
* TABLE 5
*****************

*************************************
**Quantity reported production
**********************************


qui eststo clear

** By product
qui eststo m1prod: xi: reghdfe prod_parea_carbon after_x_col     , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)


qui eststo m2prod: xi: reghdfe prod_parea_oro after_x_col    , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui eststo m3prod: xi: reghdfe prod_parea_plata after_x_col    , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui eststo m4prod: xi: reghdfe prod_parea_platino after_x_col   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

 estout m1prod m2prod m3prod   using "$latexslides/prod_norm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) prefoot(\midrule ) stats(N N_Clust_Colombia N_Clust_Peru ymean r2 r2_within, fmt( %9.2gc %9.2gc a2 a2 a2) labels ("N. of obs." "Colombian municipalities" "Peruvian municipalities" "Mean of dep. var." "\$R^2\$" "Within \$R^2\$")) replace



 estout m1prod m2prod m3prod   using "$latexpaper/prod_norm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) prefoot(\midrule ) stats(N N_Clust_Colombia N_Clust_Peru ymean r2 r2_within, fmt( %9.2gc %9.2gc a2 a2 a2) labels ("N. of obs." "Colombian municipalities" "Peruvian municipalities" "Mean of dep. var." "\$R^2\$" "Within \$R^2\$")) replace


********************
* TABLE A8 A9 FigureA6
*****************

*************************************
*** Heterogeneous effect of main regression


qui eststo m1hetNobs: xi: reghdfe propmined_illegal after_x_col     if after_x_ind_leaf2!=. , absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_ind_leaf2 "After X Col X Weak Institutions"
replace after_x_ind_leaf2=1 if ind_after==1 & after_x_ind_leaf2==1
qui eststo m1hetins: xi: reghdfe propmined_illegal after_x_col   after_x_ind_leaf2  if after_x_ind_leaf2!=. , absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'


label var after_x_ag "After X Col X Armed Groups"
replace after_x_ag=1 if ind_after==1 & ind_armedgroup_b2012==1
qui eststo m1hetag: xi: reghdfe propmined_illegal after_x_col after_x_ag  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'


label var after_x_jds "After X Col X Judiciary Strength"
replace after_x_jds=1 if ind_after==1 & justice_percent_d==1
qui eststo m1judstrg: xi: reghdfe propmined_illegal after_x_col after_x_jds  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_poor "After X Col X Poor"
gen poor=(after_x_poor==1)
bys codmpio: egen poor_max=max(poor)
replace after_x_poor=1 if ind_after==1 & poor_max==1
qui eststo m1hetnbi: xi: reghdfe propmined_illegal after_x_col after_x_poor  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_loser "After X Col X Loser"
replace after_x_loser=1 if ind_after==1 & ind_loser==1

qui eststo m1hetloser: xi: reghdfe propmined_illegal after_x_col after_x_loser  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_pctg_loss "After X Col X \% Budget Loss"
replace after_x_pctg_loss=pctg_loss*ind_after if ind_after==1 & ind_col==1
qui eststo m1hetpbudc: xi: reghdfe propmined_illegal after_x_col after_x_pctg_loss  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_pctgloss_if "After X Col X \% Budget Loss if Loss"
label var after_x_pctgwin_if "After X Col X \% Budget Win if Won"
replace after_x_pctgloss_if=pctg_loss*ind_after*ind_loser if ind_after==1 & ind_loser==1 & ind_col==1
replace after_x_pctgwin_if=-pctg_loss*ind_after*(1-ind_loser) if ind_after==1 & ind_loser==0 & ind_col==1

qui eststo m1hetpbudwl: xi: reghdfe propmined_illegal after_x_col after_x_pctgloss_if after_x_pctgwin_if  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_censed "After X Col X Censed"
replace after_x_censed=1 if ind_after==1 & cm_censed==1
qui eststo m1hetcensed: xi: reghdfe propmined_illegal after_x_col   after_x_censed  if after_x_ind_leaf2!=. , absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_oil "After X Col X Oil"
qui eststo m1hetoil: xi: reghdfe propmined_illegal after_x_col after_x_oil  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_pop "After X Col X High population"
qui eststo m1hetpop: xi: reghdfe propmined_illegal after_x_col after_x_pop  if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

label var after_x_area "After X Col X Large area"
qui eststo m1hetarea: xi: reghdfe propmined_illegal after_x_col after_x_area if after_x_ind_leaf2!=., absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

estout m1hetNobs m1hetloser m1hetpbudc m1hetpbudwl m1hetcensed m1hetins  m1hetag    using "$latexpaper/fminedhet1_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_Clust_Colombia N_Clust_Peru ymean r2 r2_within , fmt( %9.2gc %9.2gc %9.2gc a2 a2) labels ( "N. of obs." "Colombian municipalities" "Peruvian municipalities" "Mean of dep. var." "\$R^2\$" "Within \$R^2\$" )) replace


cor ind_leaf2 ind_armedgroup_b2012


estout m1hetNobs  m1hetoil m1hetpop m1hetarea  m1hetnbi  using "$latexpaper/fminedhet2_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_Clust_Colombia N_Clust_Peru ymean r2 r2_within , fmt( %9.2gc %9.2gc %9.2gc a2 a2) labels ( "N. of obs." "Colombian municipalities" "Peruvian municipalities" "Mean of dep. var." "\$R^2\$" "\$R^2\$" "Within \$R^2\$" )) replace

estout m1hetNobs m1hetins m1hetag  using "$latexslides/fminedhetins_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_clust meany r2 , fmt( %9.2gc %9.2gc %9.2gc a2 ) labels ( "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

estout m1hetNobs m1hetins  m1hetag m1hetloser  using "$latexslides/fminedhet1_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( %9.2gc %9.2gc %9.2gc a2 ) labels ( "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

estout m1hetNobs m1hetoil m1hetpop m1hetarea  m1hetnbi   using "$latexslides/fminedhet2_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( %9.2gc %9.2gc %9.2gc a2 ) labels ( "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


******* Plot of heterogeneous effects
coefplot (m1hetins \ m1hetag \ m1hetloser \ m1judstrg \ m1hetoil \ m1hetpop \ m1hetarea     \ m1hetnbi), ///
 keep(after_x_ind_leaf2  after_x_jds after_x_ag after_x_poor after_x_loser after_x_gold after_x_oil after_x_pop after_x_area) xline(0)

graph export "$latexslides/hetplot.pdf", replace
graph export "$latexpaper/hetplot.pdf", replace

******* Plot of heterogeneous effects short
coefplot (m1hetins \ m1hetag \ m1hetloser \ m1judstrg), ///
 keep(after_x_ind_leaf2  after_x_jds after_x_ag  after_x_loser) xline(0)

graph export "$latexslides/hetplot_short.pdf", replace
graph export "$latexpaper/hetplot_short.pdf", replace


/*
**National parks
keep codmpio areaprilegalSP_sqkm areaprilegal_sqkm areamuni_sqkm areamuniSP_sqkm ind_after ano
gen areaprilegalP_sqkm= areaprilegal_sqkm- areaprilegalSP_sqkm
gen areamuniP_sqkm= areamuni_sqkm- areamuniSP_sqkm
replace areaprilegalP_sqkm=0 if areaprilegalP_sqkm<0

gen area1=areaprilegalSP_sqkm/areamuniSP_sqkm
gen area0=areaprilegalP_sqkm/areamuniP_sqkm
keep if areamuniP_sqkm!=0
/*
rename areaprilegalSP_sqkm area1
rename areaprilegalP_sqkm area0
*/
reshape long area, i(codmpio ano) j(outP)
gen afterXoutP= ind_after* outP
xi: reghdfe area afterXoutP i.ano  , absorb(codmpio) vce(cluster codmpio)


fvset base 2010 ano
char ano[omit] 2010


qui reghdfe area c.outP##b2010.ano   , absorb(codmpio) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels  keep(*#c.outP 2010b.ano#co.outP) ci vertical yline(0) xline(2011) rename(2007.ano#c.outP=2007 ///
2004.ano#c.outP=2004 ///
2005.ano#c.outP=2005 ///
2006.ano#c.outP=2006 ///
2008.ano#c.outP=2008 ///
2009.ano#c.outP=2009 ///
2010b.ano#co.outP=2010 ///
2011.ano#c.outP=2011 ///
2012.ano#c.outP=2012 ///
2013.ano#c.outP=2013 ///
2014.ano#c.outP=2014)
*/
************
* Summary statistics
*******
graph dot (sum) titulosarea_sqkm if country=="Col", over(ano) vertical ytitle("Total area (sqkm) mining titles by year")
graph export "$latexslides/Titles_area.pdf", as(pdf) replace
graph export "$latexpaper/Titles_area.pdf", as(pdf) replace


/*MR: Remove pctg_budget_roy_change as per Santi's request*/
*pctg_budget_roy_change 



qui sum cm_pctgmines_illegal [fw= cm_nmines] if ano==2010
qui local temp=round(r(mean))
file open newfile using "$latexpaper/pctg_cmim.tex", write replace
file write newfile "`temp'\%"
file close newfile

eststo clear
qui xi: my_ptest pctg_budget_roy_change ind_metalespreciosos_ever ind_Hidrocarburos_ever ///
     cm_pctgmines_cielo_abierto ///
   ind_armedgroup_b2012 pobl_tot areamuni_sqkm  if ano==2014 & areamuni_sqkm!=., by(ind_loser) 

esttab using "$latexslides/summary_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

esttab using "$latexpaper/summary_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

eststo clear
qui xi: my_ptest propmined_illegal_bef propmined_illegal_aft pctg_budget_roy_change ///
 ind_armedgroup_b2012_munil propmined_illegal_bef_ag propmined_illegal_aft_ag, by(ind_loser) 

esttab using "$latexslides/sumstats_regvarmined.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

esttab using "$latexpaper/sumstats_regvarmined.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 


*************************************
**Summary statistics by type
**********************************
**********************************



qui sum codmpio if ano==2007 & areamuni_sqkm!=. & cm_censed==1
qui local mainresult=r(N)
file open newfile using "$latexslides/Ncensused.tex", write replace
file write newfile "`mainresult'"
file close newfile

**Analysis of tpr and fpr by winner losser
/*
preserve
eststo clear
qui gen w_tr_tpr=tr_mine 
qui gen w_tr_tprtitulo=tr_minetitulo 
qui gen w_tr_tprnotitulo=tr_minenotitulo 
qui gen w_tr_fpr=tr_nomine 
qui gen w_tr_fprtitulo=tr_nominetitulo
qui gen w_tr_fprnotitulo=tr_nominenotitulo
qui xi: my_ptestw tr_tpr tr_tprtitulo tr_tprnotitulo tr_fpr tr_fprtitulo tr_fprnotitulo , by(ind_loser) 

esttab using "$latexslides/sumstats_tprfpr.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 
restore
*/


** Analysis of tpr and fpr with legality in the columns
***
preserve
keep if tr_mine!=. | tr_nomine!=.
keep codmpio ind_loser tr_*
*Rename variables for the re-shape
rename tr_tprtitulo tr_tprtipo1
rename tr_tprnotitulo tr_tprtipo0
rename tr_fprtitulo tr_fprtipo1
rename tr_fprnotitulo tr_fprtipo0

rename tr_minetitulo tr_minetipo1
rename tr_minenotitulo tr_minetipo0
rename tr_nominetitulo tr_nominetipo1
rename tr_nominenotitulo tr_nominetipo0

**Reshape for a separate row for the legal and the illegal part of  amuni

reshape long tr_minetipo tr_nominetipo tr_tprtipo tr_fprtipo, i(codmpio) j(ind_legality)

qui gen tr_tprwinner=tr_tprtipo if ind_loser==0
qui gen tr_tprloser=tr_tprtipo if ind_loser==1
qui gen tr_fprwinner=tr_fprtipo if ind_loser==0
qui gen tr_fprloser=tr_fprtipo if ind_loser==1
eststo clear
qui gen w_tr_tpr=tr_mine 
qui gen w_tr_tprwinner=tr_minetipo if ind_loser==0
qui gen w_tr_tprloser=tr_minetipo if ind_loser==1
qui gen w_tr_fpr=tr_nomine 
qui gen w_tr_fprwinner=tr_nominetipo if ind_loser==0
qui gen w_tr_fprloser=tr_nominetipo if ind_loser==1
label var tr_tprwinner "TPR Winner"
label var tr_tprloser "TPR Loser"
label var tr_fprwinner "FPR Winner"
label var tr_fprloser "FPR Loser"
qui xi: my_ptestw tr_tprwinner tr_tprloser tr_fprwinner tr_fprloser , by(ind_legality) 

esttab using "$latexslides/sumstats_tprfpr_bylegal.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Illegal" "Legal" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 
restore

*****
* Visual diff-diff
*******
preserve
collapse (mean) propmined_illegal , by(ano country)
label var propmined_illegal "Proportion of mined area mined illegaly"
twoway (line propmined_illegal ano if country=="Col", sort lw(thick))(line propmined_illegal ano if country=="Peru", sort lw(thick) lp(dash) ), title("Proportion of mined area mined illegaly by year")scale(1.2)    xline(2010) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predillegal_propmined_peru.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propmined_peru.pdf", as(pdf) replace
restore

preserve
fvset base 2010 ano
char ano[omit] 2010
qui reghdfe propmined_illegal c.ind_loser##b2010.ano price_index_u  , absorb(codmpio) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(*#c.ind_loser 2010b.ano#co.ind_loser) ci vertical yline(0) xline(2011) rename(2007.ano#c.ind_loser=2007 ///
2004.ano#c.ind_loser=2004 ///
2005.ano#c.ind_loser=2005 ///
2006.ano#c.ind_loser=2006 ///
2008.ano#c.ind_loser=2008 ///
2009.ano#c.ind_loser=2009 ///
2010b.ano#co.ind_loser=2010 ///
2011.ano#c.ind_loser=2011 ///
2012.ano#c.ind_loser=2012 ///
2013.ano#c.ind_loser=2013 ///
2014.ano#c.ind_loser=2014)
graph export "$latexslides/partrends_wl.pdf", replace
graph export "$latexpaper/partrends_wl.pdf", replace
restore

preserve
keep if ind_muni_allyears==1
collapse (mean) propmined_illegal , by(ano country)
label var propmined_illegal "Proportion of mined area mined illegally"
twoway (line propmined_illegal ano if country=="Col", sort lw(thick))(line propmined_illegal ano if country=="Peru", sort lw(thick) lp(dash) ), scale(1.2)    xline(2010) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/Predillegal_propmined_peru_munially.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propmined_peru_munially.pdf", as(pdf) replace
restore

**Titles
preserve
collapse (sum) titulosarea_sqkm , by(ano country)
label var titulosarea_sqkm "Area titled"
twoway (line titulosarea_sqkm ano if country=="Col", sort lw(thick))(line titulosarea_sqkm ano if country=="Peru", sort lw(thick) lp(dash) ), title("Area legal titles by year")scale(1.2)    xline(2010) legend(label(1 "Colombia") label(2 "Peru"))
graph export "$latexslides/plotAreaTitles.pdf", as(pdf) replace
graph export "$latexpaper/plotAreaTitles.pdf", as(pdf) replace
restore

**Winner-losser
preserve
collapse (mean) prop_illegal, by(ano ind_loser)

label var prop_illegal "Fraction of area mined illegal"
twoway (line prop_illegal ano if ind_loser==0, sort lw(thick))(line prop_illegal ano if ind_loser==1, sort lw(thick) ), title("Evolution of fraction of area mined illegally")scale(1.2)    xline(2010) legend(label(1 "Winners") label(2 "Losers"))
graph export "$latexslides/diff_in_diff_wl.pdf", replace
graph export "$latexpaper/diff_in_diff_wl.pdf", replace
restore



** Col winner vs Col losers vs Peru
/*
preserve
collapse (mean) propmined_illegal [fweight=round(areamuni_sqkm)], by(ano country ind_loser)
label var propmined_illegal "Proportion of mined area mined illegaly"
twoway (line propmined_illegal ano if country=="Col" & ind_loser==0, sort lw(thick)) ///
     (line propmined_illegal ano if country=="Col" & ind_loser==1, sort lw(thick)) ///
	 (line propmined_illegal ano if country=="Peru", sort lw(thick) ), ///
	 title("Proportion of mined area mined illegaly by year")scale(1.2)    xline(2011.5) legend(label(1 "Colombia-Winner") label(2 "Colombia-Loser") label(3 "Peru"))
graph export "$latexslides/Predillegal_propmined_peru.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propmined_peru.pdf", as(pdf) replace
restore
*/
***************
* Parallel trends
*********

preserve
collapse (mean) propmined_illegal, by(ano ind_col)
label var propmined_illegal "% of mined area mined illegaly"
twoway (line propmined_illegal ano if ind_col==1, sort lw(thick)) ///
     (line propmined_illegal ano if ind_col==0, sort lw(thick)), ///
	  xline(2010) legend(label(1 "Colombia") label(2 "Peru")) legend(on)
graph export "$latexslides/partrends_colperu_raw.pdf", replace
graph export "$latexpaper/partrends_colperu_raw.pdf", replace
restore


preserve
fvset base 2010 ano
char ano[omit] 2010
label var ind_col "Colombia"
eststo clear
eststo parallel_1: reghdfe propmined_illegal c.ind_col##b2010.ano   , absorb(codmpio) vce(cluster codmpio)

estout parallel_1 using "$latexpaper/partrends_colperu_table.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( *.ano#c.ind_col ) prefoot(\midrule ) stats(N N_clust r2 , fmt(%9.2gc %9.2gc a2 ) labels ("N. of obs." "Municipalities" "\$R^2\$" )) replace

tab ano, gen(I)
forvalues i = 1(1)11{
gen int_`i' = ind_col * I`i'
}
gen base = 0

global dynamic int_1 int_2 int_3 int_4 int_5 int_6 base int_8 int_9 int_10 int_11

reghdfe propmined_illegal $dynamic   , absorb(codmpio ano) vce(cluster codmpio)

coefplot, graphregion(color(white)) baselevels omitted  keep($dynamic) xtitle("Years") ytitle("Coefficient") ci vertical yline(0) xline(7) rename( int_1=2004 ///
int_2=2005 ///
int_3=2006 ///
int_4=2007 ///
int_5=2008 ///
int_6=2009 ///
base=2010 ///
int_8=2011 ///
int_9=2012 ///
int_10=2013 ///
int_11=2014)



graph export "$latexslides/partrends_colperu.pdf", replace
graph export "$latexpaper/partrends_colperu.pdf", replace

restore


*** minin titles

preserve

collapse (mean) titulosarea_sqkm, by(ano ind_col)
label var titulosarea_sqkm "Area mining permits (km2)"
twoway (line titulosarea_sqkm ano if ind_col==1, sort lw(thick)) ///
     (line titulosarea_sqkm ano if ind_col==0, sort lw(thick)), ///
	  xline(2010) legend(label(1 "Colombia") label(2 "Peru")) legend(on)
graph export "$latexslides/partrends_colperu_raw_titulosarea_sqkm.pdf", replace
graph export "$latexpaper/partrends_colperu_raw_titulosarea_sqkm.pdf", replace
restore

preserve
fvset base 2010 ano
char ano[omit] 2010
label var ind_col "Colombia"
eststo clear
eststo parallel_1: reghdfe titulosarea_sqkm c.ind_col##b2010.ano   , absorb(codmpio) vce(cluster codmpio)

estout parallel_1 using "$latexpaper/partrends_colperu_table_titulosarea_sqkm.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( *.ano#c.ind_col ) prefoot(\midrule ) stats(N N_clust r2 , fmt(%9.2gc %9.2gc a2 ) labels ("N. of obs." "Municipalities" "\$R^2\$" )) replace

tab ano, gen(I)
forvalues i = 1(1)11{
gen int_`i' = ind_col * I`i'
}
gen base = 0

global dynamic int_1 int_2 int_3 int_4 int_5 int_6 base int_8 int_9 int_10 int_11

reghdfe titulosarea_sqkm $dynamic   , absorb(codmpio ano) vce(cluster codmpio)

coefplot, graphregion(color(white)) baselevels omitted  keep($dynamic) xtitle("Years") ytitle("Coefficient") ci vertical yline(0) xline(7) rename( int_1=2004 ///
int_2=2005 ///
int_3=2006 ///
int_4=2007 ///
int_5=2008 ///
int_6=2009 ///
base=2010 ///
int_8=2011 ///
int_9=2012 ///
int_10=2013 ///
int_11=2014)



graph export "$latexslides/partrends_colperu_titulosarea_sqkm.pdf", replace
graph export "$latexpaper/partrends_colperu_titulosarea_sqkm.pdf", replace

restore

**Concurrent mining titles

preserve

collapse (mean) titulosConcarea_sqkm, by(ano ind_col)
label var titulosConcarea_sqkm "Area concurrent mining permits (km2)"
twoway (line titulosConcarea_sqkm ano if ind_col==1, sort lw(thick)) ///
     (line titulosConcarea_sqkm ano if ind_col==0, sort lw(thick)), ///
	  xline(2010) legend(label(1 "Colombia") label(2 "Peru")) legend(on)
graph export "$latexslides/partrends_colperu_raw_titulosConcarea_sqkm.pdf", replace
graph export "$latexpaper/partrends_colperu_raw_titulosConcarea_sqkm.pdf", replace
restore

preserve
fvset base 2010 ano
char ano[omit] 2010
label var ind_col "Colombia"
eststo clear
eststo parallel_1: reghdfe titulosConcarea_sqkm c.ind_col##b2010.ano   , absorb(codmpio) vce(cluster codmpio)

estout parallel_1 using "$latexpaper/partrends_colperu_table_titulosConcarea_sqkm.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( *.ano#c.ind_col ) prefoot(\midrule ) stats(N N_clust r2 , fmt(%9.2gc %9.2gc a2 ) labels ("N. of obs." "Municipalities" "\$R^2\$" )) replace

tab ano, gen(I)
forvalues i = 1(1)11{
gen int_`i' = ind_col * I`i'
}
gen base = 0

global dynamic int_1 int_2 int_3 int_4 int_5 int_6 base int_8 int_9 int_10 int_11

reghdfe titulosConcarea_sqkm $dynamic   , absorb(codmpio ano) vce(cluster codmpio)

coefplot, graphregion(color(white)) baselevels omitted  keep($dynamic) xtitle("Years") ytitle("Coefficient") ci vertical yline(0) xline(7) rename( int_1=2004 ///
int_2=2005 ///
int_3=2006 ///
int_4=2007 ///
int_5=2008 ///
int_6=2009 ///
base=2010 ///
int_8=2011 ///
int_9=2012 ///
int_10=2013 ///
int_11=2014)



graph export "$latexslides/partrends_colperu_titulosConcarea_sqkm.pdf", replace
graph export "$latexpaper/partrends_colperu_titulosConcarea_sqkm.pdf", replace

restore


**Corresponding table


preserve
eststo clear
eststo parallel_1: reghdfe propmined_illegal c.ind_col#ano    , absorb(codmpio ano) vce(cluster codmpio )
estadd ysumm

label var ind_col "Colombia"
label var ano "Year"
estout parallel_1 using "$latexpaper/partrends_colperu_table_2.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( *c.ind_col ) prefoot(\midrule ) stats(N N_clust r2 , fmt(%9.2gc %9.2gc a2 ) labels ("N. of obs." "Municipalities" "\$R^2\$" )) replace


restore

preserve
replace propmined_illegal_tityear=propmined_illegal if propmined_illegal_tityear==.
tab ano, gen(I)
forvalues i = 1(1)11{
gen int_`i' = ind_col * I`i'
}
gen base = 0

global dynamic int_1 int_2 int_3 int_4 int_5 int_6 base int_8 int_9 int_10 int_11

reghdfe propmined_illegal_tityear $dynamic   , absorb(codmpio ano) vce(cluster codmpio)

coefplot, graphregion(color(white)) baselevels omitted   keep($dynamic) xtitle("Years") ytitle("Coefficient") ci vertical yline(0) xline(7) rename( int_1=2004 ///
int_2=2005 ///
int_3=2006 ///
int_4=2007 ///
int_5=2008 ///
int_6=2009 ///
base=2010 ///
int_8=2011 ///
int_9=2012 ///
int_10=2013 ///
int_11=2014)

graph export "$latexslides/partrends_tityear.pdf", replace
graph export "$latexpaper/partrends_tityear.pdf", replace
restore


preserve
eststo parallel_2: reghdfe newpropmined_illegal  c.ind_col#c.ano if ano<2011   , absorb(codmpio ano) vce(cluster codmpio )
estadd ysumm
eststo parallel_3: reghdfe titulosarea_s~m c.ind_col#c.ano if ano<2011   , absorb(codmpio ano) vce(cluster codmpio )
estadd ysumm

restore
 



**** Census balance

eststo clear
qui xi: my_ptest pctg_loss ind_metalespreciosos_ever ind_Hidrocarburos_ever ///
        ind_armedgroup_b2012 pobl_tot areamuni_sqkm  if ano==2014 & areamuni_sqkm!=., by(cm_censed) 

esttab using "$latexslides/censed_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Censused" "Not Censused" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

esttab using "$latexpaper/censed_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Censused" "Not Censused" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 


**** Balance weak institutions

eststo clear
qui xi: my_ptest propmined_illegal_bef nbi_2011 pctg_loss ind_metalespreciosos_ever ind_Hidrocarburos_ever ///
        ind_armedgroup_b2012 pobl_tot areamuni_sqkm  if  areamuni_sqkm!=., by(ind_leaf2) 

esttab using "$latexslides/weakins_balance.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Strong" "Weak" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

*/
********************************************************
* Main results slide fraction MINED different ID strategies
*********************************************************
eststo clear
reghdfe propmined_illegal after_x_col   , absorb(codmpio ano) vce(cluster codmpio)
matrix tempm=e(b)

local mainafterresult=tempm[1,1]
qui local mainafterresult=round(`mainafterresult'*100)/100
file open newfile using "$latexpaper/mainafterresult.tex", write replace
file write newfile "`mainafterresult'"
file close newfile
file open newfile using "$latexslides/mainafterresult.tex", write replace
file write newfile "`mainafterresult'"
file close newfile



qui sum codmpio if e(sample) & ano==2014 & ind_loser==1 & country=="Col"
qui local mainresult=r(N)
file open newfile using "$latexslides/Nlosers.tex", write replace
file write newfile "`mainresult'"
file close newfile
file open newfile using "$latexpaper/Nlosers.tex", write replace
file write newfile "`mainresult'"
file close newfile


eststo clear
qui eststo m1: xi: reghdfe propmined_illegal_dt after_x_col price_index_u  if country=="Col", absorb(codmpio) vce(cluster codmpio)
estadd ysumm
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"
estadd local dptrends "No"
matrix tempm=e(b)



*****************


qui eststo m2: xi: reghdfe propmined_illegal_dt after_x_col after_x_pctg_loss price_index_u  if country=="Col", absorb(codmpio) vce(cluster codmpio)
estadd ysumm
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"
estadd local weights "No"
matrix tempm=e(b)


local mainpctlosresult=tempm[1,2]
qui local mainpctlosresult=round(`mainpctlosresult'*100)/10


file open newfile using "$latexpaper/mainpctlosresult.tex", write replace
file write newfile "`mainpctlosresult'"
file close newfile

qui eststo m3: xi: reghdfe propmined_illegal_dt after_x_col after_x_peru after_x_pctg_loss, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"
estadd local weights "No"

qui eststo m3nwl: xi: reghdfe propmined_illegal_dt after_x_col after_x_peru  , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"
estadd local weights "No"
estadd local dptrends "No"
matrix tempm=e(b)



replace after_x_ind_leaf2=0 if country=="Peru"


qui eststo m4: xi: reghdfe propmined_illegal after_x_col  after_x_pctg_loss  , absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

********************
* TABLE 4
*****************

*************************************
**Main results
**********************************


***Column 1
qui eststo m4nwl: xi: reghdfe propmined_illegal after_x_col , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"
matrix tempm=e(b)




local peruddafterresult=tempm[1,1]
qui local peruddafterresult=round(`peruddafterresult'*100)/100

file open newfile using "$latexpaper/peruddafterresult.tex", write replace
file write newfile "`peruddafterresult'"
file close newfile

file open newfile using "$latexslides/peruddafterresult.tex", write replace
file write newfile "`peruddafterresult'"
file close newfile

distinct codmpio  if country=="Col" & e(sample)==1
qui local mainresult= string(r(ndistinct), "%9.2gc") 
file open newfile using "$latexslides/Nmunis.tex", write replace
file write newfile "`mainresult'"
file close newfile
file open newfile using "$latexpaper/Nmunis.tex", write replace
file write newfile "`mainresult'"
file close newfile

distinct codmpio  if country=="Peru" & e(sample)==1
qui local mainresult=string(r(ndistinct), "%9.2gc") 
file open newfile using "$latexslides/Nmunis_peru.tex", write replace
file write newfile "`mainresult'"
file close newfile
file open newfile using "$latexpaper/Nmunis_peru.tex", write replace
file write newfile "`mainresult'"
file close newfile

***Column 2

qui eststo n3: xi: reghdfe newpropmined_illegal after_x_col if inmainreg==1, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit: xi: reghdfe titulosConcarea_sqkm after_x_col if inmainreg==1, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"




qui estout m4nwl n3 m3tit  using "$latexpaper/fminedstocknew_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) prefoot(\midrule ) stats(  N N_Clust_Colombia N_Clust_Peru  ymean r2 r2_within , fmt(  %9.2gc %9.2gc %9.2gc a2 a2) labels ( "N. of obs." "Colombian municipalities" "Peruvian municipalities" "Mean of dep. var." "\$R^2\$" "Within \$R^2\$" )) replace

qui estout m4nwl n3 m3tit  using "$latexpaper/fminedstocknew_reg_lean.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N , fmt(  %9.2gc ) labels ( "N. of obs."  )) replace


*************************************
**Main results - lag treatment
**********************************

gen after_x_col_lagged=(ind_col==1 & ano>=2012)

***Column 1
qui eststo m4nwl_lag : xi: reghdfe propmined_illegal after_x_col_lagged , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

***Column 2

qui eststo n3_lag : xi: reghdfe newpropmined_illegal after_x_col_lagged, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit_lag : xi: reghdfe titulosConcarea_sqkm after_x_col_lagged, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

label var after_x_col_lagged "After x Colombia"

estout m4nwl_lag  n3_lag  m3tit_lag  using "$latexpaper/fminedstocknew_reg_lagged.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col_lagged  ) stats(  N , fmt(  %9.2gc 2 ) labels ( "N. of obs." )) replace




*************************************
**Main results - without 2011
**********************************


***Column 1
qui eststo m4nwl_n2011 : xi: reghdfe propmined_illegal after_x_col if ano!=2011, absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

***Column 2

qui eststo n3_n2011 : xi: reghdfe newpropmined_illegal after_x_col if ano!=2011, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit_n2011 : xi: reghdfe titulosConcarea_sqkm after_x_col if ano!=2011, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"



estout m4nwl_n2011  n3_n2011  m3tit_n2011  using "$latexpaper/fminedstocknew_reg_n2011.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N  , fmt(  %9.2gc) labels ( "N. of obs." )) replace


*************************************
**Main results - 2007-2014
**********************************


***Column 1
qui eststo m4nwl_2007_2014 : xi: reghdfe propmined_illegal after_x_col if ano>=2007 & ano<=2014, absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

***Column 2

qui eststo n3_2007_2014 : xi: reghdfe newpropmined_illegal after_x_col  if ano>=2007 & ano<=2014, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit_2007_2014 : xi: reghdfe titulosConcarea_sqkm after_x_col  if ano>=2007 & ano<=2014, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"



estout m4nwl_2007_2014  n3_2007_2014  m3tit_2007_2014  using "$latexpaper/fminedstocknew_reg_2007_2014.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N  , fmt(  %9.2gc) labels ( "N. of obs." )) replace



*************************************
**Main results - 2007-2010+ 2014
**********************************


***Column 1
qui eststo m4nwl_long : xi: reghdfe propmined_illegal after_x_col if ano<=2010 | ano==2014, absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

***Column 2

qui eststo n3_long : xi: reghdfe newpropmined_illegal after_x_col  if ano<=2010 | ano==2014, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit_long : xi: reghdfe titulosConcarea_sqkm after_x_col  if ano<=2010 | ano==2014, absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"



estout m4nwl_long  n3_long  m3tit_long  using "$latexpaper/fminedstocknew_reg_long.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N  , fmt(  %9.2gc) labels ( "N. of obs." )) replace


*************************************
**Main results - muni trends
**********************************


***Column 1
qui eststo m4nwl_muni : xi: reghdfe propmined_illegal after_x_col , absorb(codmpio#c.ano codmpio ano) vce(cluster codmpio)
qui estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

***Column 2

qui eststo n3_muni : xi: reghdfe newpropmined_illegal after_x_col  , absorb(codmpio#c.ano codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE" 

***Column 3

qui eststo m3tit_muni : xi: reghdfe titulosConcarea_sqkm after_x_col , absorb(codmpio#c.ano codmpio ano) vce(cluster codmpio)
estadd ysumm
distinct codmpio  if country=="Col" & e(sample)==1
qui estadd local N_Clust_Colombia= r(ndistinct)
distinct codmpio  if country=="Peru" & e(sample)==1
qui estadd local N_Clust_Peru= r(ndistinct)

qui sum titulosConcarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"



estout m4nwl_muni  n3_muni  m3tit_muni  using "$latexpaper/fminedstocknew_reg_muni_trends.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N  , fmt(  %9.2gc) labels ( "N. of obs." )) replace

*************************************
**Main results - Just numerator
**********************************


gen lg_areaprilegal_sqkm=log(areaprilegal_sqkm+1)
gen IHS_areaprilegal_sqkm=asinh(areaprilegal_sqkm)
eststo m_area : reghdfe areaprilegal_sqkm after_x_col if inmainreg==1 & areaprilegal_sqkm<=715.1274 , absorb(codmpio ano) vce(cluster codmpio)
eststo lg_area :reghdfe lg_areaprilegal_sqkm after_x_col if inmainreg==1 & areaprilegal_sqkm<=715.1274, absorb(codmpio ano) vce(cluster codmpio)
eststo ihs_area :reghdfe IHS_areaprilegal_sqkm after_x_col if inmainreg==1 & areaprilegal_sqkm<=715.1274, absorb(codmpio ano) vce(cluster codmpio)

estout m_area  lg_area  ihs_area  using "$latexpaper/area_ilegal_col_peru_robust_numerator.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N  , fmt(  %9.2gc) labels ( "N. of obs." )) replace


gen lg_areaprmined_sqkm=log(areaprmined_sqkm+1)
gen IHS_areaprmined_sqkm=asinh(areaprmined_sqkm)
eststo m_area : reghdfe areaprmined_sqkm after_x_col if inmainreg==1 & areaprmined_sqkm<=810.1233, absorb(codmpio ano) vce(cluster codmpio)
eststo lg_area :reghdfe lg_areaprmined_sqkm after_x_col if inmainreg==1 & areaprmined_sqkm<=810.1233, absorb(codmpio ano) vce(cluster codmpio)
eststo ihs_area :reghdfe IHS_areaprmined_sqkm after_x_col if inmainreg==1 & areaprmined_sqkm<=810.1233, absorb(codmpio ano) vce(cluster codmpio)

estout m_area  lg_area  ihs_area  using "$latexpaper/area_ilegal_col_peru_robust_denominator.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) stats(  N  , fmt(  %9.2gc) labels ( "N. of obs." )) replace


***************************
***************************
***************************
***************************



qui eststo m4nwl_nofarc: xi: reghdfe propmined_illegal after_x_col  if FARC!=1  , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"

********* Legal titles


qui eststo m1tit: xi: reghdfe titulosarea_sqkm after_x_col price_index_u  , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
qui sum titulosarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"


qui eststo m2tit: xi: reghdfe titulosarea_sqkm after_x_col after_x_peru  , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
qui sum titulosarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local timefetrend "Trend"




qui eststo m3tit_nofarc: xi: reghdfe titulosarea_sqkm after_x_col   if FARC!=1 , absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
qui sum titulosarea_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefe "Yes"
estadd local ltrend "No"
estadd local timefetrend "TimeFE"


** CLICK 6
** New mined area
qui eststo n1: xi: reghdfe newpropmined_illegal_dt after_x_col price_index_u  , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "Trend"

qui eststo n2: xi: reghdfe newpropmined_illegal_dt after_x_col after_x_peru , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "Trend"




qui eststo n3_nofarc: xi: reghdfe newpropmined_illegal after_x_col  if FARC!=1 , absorb(codmpio ano) vce(cluster codmpio)
estadd ysumm
qui sum newpropmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local timefetrend "TimeFE"    

qui estout n1 n2 n3  using "$latexslides/newarea_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats( timefetrend  N N_clust meany r2 , fmt(%fmt %9.2gc %9.2gc  %9.2gc a2  ) labels ("Time FE-Trend"  "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace




qui estout m1tit m2tit m3tit using "$latexslides/titulosarea_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats( timefetrend N N_clust meany r2 , fmt( %fmt %9.2gc %9.2gc %9.2gc a2 ) labels ("Time FE-Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


qui estout m4nwl_nofarc n3_nofarc m3tit_nofarc  using "$latexpaper/fminedstocknew_reg_nofarc.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) prefoot(\midrule ) stats(  N N_clust meany r2 , fmt(  %9.2gc %9.2gc %9.2gc a2 ) labels ( "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace



qui estout m1 m2 m3 m4 using "$latexslides/fmined_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_pctg_loss after_x_peru ) prefoot(\midrule ) stats( timefetrend N N_clust meany r2 , fmt( %fmt %9.2gc %9.2gc a2 a2 ) labels ("Time FE-Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/fmined_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_pctg_loss after_x_peru price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust meany r2 , fmt( %fmt %fmt %9.2gc %9.2gc a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace




** CLICK 1
** Calculate bounds of ommited variable bias
** See Altonji et al(2005) or Oster(2013)
/*

*New version 2019
bs r(beta), rep(100): psacalc beta after_x_col, model( reghdfe propmined_illegal after_x_col  i.ano  , absorb(codmpio) vce(cluster codmpio))

qui reg propmined_illegal_dt after_x_col after_x_pctg_loss  i.codmpio
matrix tempm=e(b)
local ols_after=tempm[1,1]
local ols_afteraffi=tempm[1,2]

*Altonji indicator after
psacalc after_x_col set
qui local absdif=abs(`r(output)'-`ols_after')
qui local altonjilb_after=round((`ols_after'-`absdif')*100)/100
file open newfile using "$latexslides/altonjiminedlb_aftercol.tex", write replace
file write newfile "`altonjilb_after'"
file close newfile
file open newfile using "$latexpaper/altonjiminedlb_aftercol.tex", write replace
file write newfile "`altonjilb_after'"
file close newfile

qui local altonjiub_after=round((`ols_after'+`absdif')*100)/100
file open newfile using "$latexslides/altonjiminedub_aftercol.tex", write replace
file write newfile "`altonjiub_after'"
file close newfile
file open newfile using "$latexpaper/altonjiminedub_aftercol.tex", write replace
file write newfile "`altonjiub_after'"
file close newfile

*Altonji indicator afterXLoser

psacalc after_x_pctg_loss set

qui local absdif=abs(`r(output)'-`ols_afteraffi')
qui local altonjilb_afteraffi=round((`ols_afteraffi'-`absdif')*100)/100
file open newfile using "$latexslides/altonjiminedlb_afterploss.tex", write replace
file write newfile "`altonjilb_afteraffi'"
file close newfile
file open newfile using "$latexpaper/altonjiminedlb_afterploss.tex", write replace
file write newfile "`altonjilb_afteraffi'"
file close newfile

qui local altonjiub_afteraffi=round((`ols_afteraffi'+`absdif')*100)/100
file open newfile using "$latexslides/altonjiminedub_afterploss.tex", write replace
file write newfile "`altonjiub_afteraffi'"
file close newfile
file open newfile using "$latexpaper/altonjiminedub_afterploss.tex", write replace
file write newfile "`altonjiub_afteraffi'"
file close newfile
*/

** CLICK 2
**See Belloni et al for explanation
/*
preserve
do "$dir_do/32_doubleLassoPeru"
restore
*/


** CLICK 3
** Robustness to different weight


qui gen w_analizedpctgarea=round( 100*analizedarea_sqkm/ areamuni_sqkm )

qui eststo m5: xi: reghdfe propmined_illegal_dt after_x_col price_index_u  [fw=w_analizedpctgarea]  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local weights "Yes"

qui eststo m6: xi: reghdfe propmined_illegal_dt after_x_col after_x_peru  [fw=w_analizedpctgarea], absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local weights "Yes"

qui estout m1 m5 m3nwl m6 using "$latexslides/weights_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats(weights   N N_clust ymean r2 , fmt(%fmt %9.2gc %9.2gc a2 a2 ) labels ("Weights"  "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m1 m5 m3nwl m6 using "$latexpaper/weights_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats(weights   N N_clust ymean r2 , fmt(%fmt %9.2gc %9.2gc a2 a2 ) labels ("Weights"  "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

** CLICK 4
** Robustness to state trends
qui foreach x of varlist trend_depto* {
replace `x'=0 if `x'==.
}

qui eststo m7: xi: reghdfe propmined_illegal_dtst after_x_col price_index_u   , absorb(codmpio) vce(cluster codmpio)
qui sum propmined_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'
estadd local dptrends "Yes"

qui eststo m8: xi: reghdfe propmined_illegal_dtst after_x_col after_x_peru  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local dptrends "Yes"

qui estout m1 m7 m3nwl m8 using "$latexslides/fmined_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats(dptrends   N N_clust ymean r2 , fmt(%fmt %9.2gc %9.2gc a2 a2 ) labels ("State trends"  "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m1 m7 m3nwl m8 using "$latexpaper/fmined_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats(dptrends   N N_clust ymean r2 , fmt(%fmt %9.2gc %9.2gc a2 a2 ) labels ("State trends"  "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


*** CLICK 
** Legalizations


qui eststo n1: xi: reghdfe mined_lglzation_dt after_x_col price_index_u   , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "Trend"

qui eststo n2: xi: reghdfe mined_lglzation_dt after_x_col after_x_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "Trend"


qui eststo n3: xi: reghdfe mined_lglzation after_x_col   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "TimeFE"   


qui estout n1 n2 n3  using "$latexslides/legalization_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru ) prefoot(\midrule ) stats( timefetrend  N N_clust ymean r2 , fmt(%fmt %9.2gc %9.2gc a2 a2 ) labels ("Time FE-Trend"  "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout n1 n2 n3  using "$latexpaper/legalization_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru) prefoot(\midrule ) stats( timefetrend  N N_clust ymean r2 , fmt(%fmt %9.2gc %9.2gc a2 a2 ) labels ("Time FE-Trend"  "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


** Other cutoff 
qui eststo m10: xi: reghdfe c12_propmined_illegal_dt after_x_col price_index_u  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "Trend"

qui eststo m11: xi: reghdfe c12_propmined_illegal_dt after_x_col after_x_peru , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "Trend"

qui eststo m12: xi: reghdfe c12_propmined_illegal after_x_col , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local timefetrend "TimeFE"

qui estout m10 m11 m12 using "$latexslides/fminedc12_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  after_x_peru ) prefoot(\midrule ) stats( timefetrend N N_clust ymean r2 , fmt( %fmt %9.2gc %9.2gc a2 a2 ) labels ("Time FE-Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


**CLICK 7
** RD Peru
gen after_x_colFar=after_x_col*(dist_border_km>=1000)
label var after_x_colFar "After x Col x Far"

qui eststo m4_500: xi: reghdfe propmined_illegal after_x_col   if dist_border_km<500, absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4_1000: xi: reghdfe propmined_illegal after_x_col after_x_colFar   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4_g1000: xi: reghdfe propmined_illegal after_x_col   if dist_border_km>=1000, absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo madj: xi: reghdfe propadjmined_illegal after_x_col    , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo mw: xi: reghdfe propmined_illegal after_x_col   [aw=w_analizedpctgarea], absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

qui estout m4nwl m4_1000 m4_500 using "$latexslides/peruRD_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col ) prefoot(\midrule ) stats(   N N_clust ymean r2 , fmt(%9.2gc %9.2gc a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m4nwl m4_1000 m4_g1000  m12 madj mw using "$latexpaper/robust_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col ) prefoot(\midrule ) stats(   N N_clust ymean r2 , fmt(%9.2gc %9.2gc a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


*** Title closure
*** 
qui eststo m1_tityear: xi: reghdfe propmined_illegal_tityear_dt after_x_col price_index_u  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m1_tityear using "$latexslides/tityear_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col ) prefoot(\midrule ) stats(   N N_clust ymean r2 , fmt(%9.2gc %9.2gc a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace



** CLICK 5
******
* Adjusted measures of mining based on fpr and tpr
****

qui eststo madj1: xi: reghdfe propadjmined_illegal_dt after_x_col price_index_u  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo madj2: xi: reghdfe propadjmined_illegal_dt after_x_col after_x_peru  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo madj3: xi: reghdfe propadjmined_illegal after_x_col   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm


qui estout madj1 madj2 madj3 using "$latexslides/adj_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru) prefoot(\midrule ) stats(  N N_clust ymean r2 , fmt( %9.2gc %9.2gc a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout madj1 madj2 madj3 using "$latexpaper/adj_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_peru) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( %9.2gc %9.2gc a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


**** Probability mined
qui eststo m1_prob: xi: reghdfe propmined_illegal_prob after_x_col  , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
qui sum propmined_illegal_prob if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'




*National Parks

qui eststo m1SP: xi: reghdfe propminedSP_illegal after_x_col  , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
qui sum propminedSP_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'



qui estout m1SP m1_prob     using "$latexpaper/otherm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) prefoot(\midrule ) stats(  N N_clust meany r2 , fmt( %9.2gc %9.2gc %9.2fc a2  ) labels ("N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m1 m1SP m4nwl using "$latexslides/fminednwl_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col   ) prefoot(\midrule ) stats( timefetrend N N_clust meany r2 , fmt( %fmt %9.2gc %9.2gc a2 a2 ) labels ("Time FE-Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace



qui estout m4nwl m1SP m1_prob m4_1000   m12 madj mw    using "$latexpaper/robust_reg_unificada.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col after_x_colFar ) prefoot(\midrule ) stats(   N N_clust ymean r2 , fmt(%9.2gc %9.2gc a2 a2 ) labels ( "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

*********
*** Click 8
*******

qui eststo om1: xi: reghdfe prop_illegal after_x_col , absorb(codmpio ano) vce(cluster codmpio)
qui sum prop_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

qui eststo om2: xi: reghdfe areaprilegal_sqkm after_x_col   , absorb(codmpio ano) vce(cluster codmpio)
qui sum areaprilegal_sqkm if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

qui eststo om3: xi: reghdfe log_illegal after_x_col   , absorb(codmpio ano) vce(cluster codmpio)
qui sum log_illegal if e(sample)
local auchi=round(`r(mean)',0.01)
qui estadd local meany `auchi'

qui estout om1 om2  using "$latexslides/otherm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col ) prefoot(\midrule ) stats(  N N_clust ymean r2 , fmt( %9.2gc %9.2gc %9.2fc a2) labels ("N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


*** Poverty RD
qui eststo clear
qui eststo m1: xi: reghdfe propmined_illegal_dt ind_after  after_x_pctg_loss price_index_u   , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: reghdfe propmined_illegal_dt ind_after  after_x_pctg_loss price_index_u  if nbi_2011>25 & nbi_2011<35 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: reghdfe prop_illegal ind_after after_x_pctg_loss price_index_u price_index_m trend   , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: xi: reghdfe prop_illegal ind_after after_x_pctg_loss price_index_u price_index_m trend  if nbi_2011>25 & nbi_2011<35 , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm



qui estout m1 m2 m3 m4 using "$latexslides/povertyRD_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( %9.2gc %9.2gc a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/povertyRD_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss ) prefoot(\midrule ) stats(  N N_clust ymean r2 , fmt(  %9.2gc %9.2gc a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

** For non-parametric estimation split in half winner and losers
local nlossgroups=4

qui forval i=1/`nlossgroups' {
gen D_lossG`i'=0
}
qui sum pctg_loss if pctg_loss<0, d
local lbloss=`r(p50)'
qui sum pctg_loss if pctg_loss>=0, d
local ubloss=`r(p50)'

replace D_lossG1=1 if pctg_loss<`lbloss'
replace D_lossG2=1 if pctg_loss>=`lbloss' & pctg_loss<0
replace D_lossG3=1 if pctg_loss<`ubloss' & pctg_loss>=0
replace D_lossG`nlossgroups'=1 if pctg_loss>=`ubloss' & pctg_loss!=.

forval i=1/`nlossgroups' {
gen after_x_lossG`i'=ind_after*D_lossG`i'
}

label var after_x_lossG2 "After X Bottom half winner"
label var after_x_lossG3 "After X Bottom half loser"
label var after_x_lossG4 "After X Top half loser"


qui eststo m5: xi: reghdfe propmined_illegal_dt ind_after  after_x_lossG2-after_x_lossG4 price_index_u , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"
qui eststo m6: xi: reghdfe prop_illegal ind_after  after_x_lossG2-after_x_lossG4 price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local timefe "No"
estadd local ltrend "Yes"

qui estout m1 m5 m3 m6 using "$latexslides/nonparametric_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss after_x_lossG* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace
qui estout m1 m2 m5 m3 m4 m6 using "$latexpaper/nonparametric_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss after_x_lossG* price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace





***Production without normalizing
qui eststo clear
qui eststo m1prod: xi: reghdfe prod_carbon after_x_col     , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm


qui eststo m2prod: xi: reghdfe prod_oro after_x_col    , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3prod: xi: reghdfe prod_plata after_x_col    , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4prod: xi: reghdfe prod_platino after_x_col   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm

qui estout m1prod m2prod m3prod m4prod  using "$latexslides/prod_raw_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) prefoot(\midrule ) stats(N N_clust ymean r2 , fmt( %9.2gc %9.2gc a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m1prod m2prod m3prod m4prod  using "$latexpaper/prod_raw_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_col  ) prefoot(\midrule ) stats(N N_clust ymean r2 , fmt( %9.2gc %9.2gc a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

*With dept trends

qui eststo clear

qui eststo m1: xi: reghdfe prop_illegal ind_after price_index_u  trend_depto* , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m2: xi: reghdfe prop_illegal ind_after after_x_loser price_index_u  trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m3: xi: reghdfe propmined_illegal ind_after  price_index_u trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m4: xi: reghdfe propmined_illegal ind_after after_x_loser price_index_u  trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"
qui estout m1 m2 m3 m4 using "$latexslides/main_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("State Year FE" "State Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/main_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("State Year FE" "State Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

*Homicides armed groups
eststo clear
qui eststo m4: xi: reghdfe tasa_homag after_x_pctg_loss price_index_u   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"


qui eststo m5: xi: reghdfe tasa_homag after_x_pctg_loss price_index_u  if ind_armedgroup_b2012==0 , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui eststo m6: xi: reghdfe tasa_homag after_x_pctg_loss price_index_u  if ind_armedgroup_b2012==1 , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui estout m4 m5 m6 using "$latexpaper/tasa_homag_creg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_pctg_loss price_index_u ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt(%fmt %9.2gc %9.2gc a2 a2 ) labels ("Time FE" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

e

/*
Size of illegal mines
xi: reghdfe kstar_illegal after_x_col price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
graph dot (mean) kstar_illegal if country=="Col", over(ano) vertical ytitle("Total area (sqkm) mining titles by year")
*/


/*
**Define gap like Fisman and Wei
gen gap=log( areaprmined_sqkm)-log( areaprmined_sqkm- areaprilegal_sqkm)
replace gap=log( areaprmined_sqkm) if areaprmined_sqkm== areaprilegal_sqkm
xi: reghdfe gap after_x_col   i.ano  , absorb(codmpio) vce(cluster codmpio)
*/

****************************
******* Updated above here
*******************************


qui sum lumpy_share if ano==2014, d
qui local temp=round(r(mean))
file open newfile using "$latexpaper/lumpy_mean.tex", write replace
file write newfile "`temp'"
file close newfile

qui local temp=round(r(p25))
file open newfile using "$latexpaper/lumpy_p25.tex", write replace
file write newfile "`temp'"
file close newfile

qui local temp=round(r(p50))
file open newfile using "$latexpaper/lumpy_p50.tex", write replace
file write newfile "`temp'"
file close newfile
file open newfile using "$latexslides/lumpy_p50.tex", write replace
file write newfile "`temp'"
file close newfile

qui local temp=round(r(p75))
file open newfile using "$latexpaper/lumpy_p75.tex", write replace
file write newfile "`temp'"
file close newfile

*Exclude Orinoquia/Amazonia
*qui drop if coddepto==18 | coddepto==50 | coddepto>80
**Replicate the graph from the theor framework

twoway (lfitci changeil_befaft  pctg_budget_roy_change if ano==2014 & changeil_befaft<1 & pctg_budget_roy_change>-25 & pctg_budget_roy_change<15, lw(thick) clc(orange) fi(inten10)) /// 
(scatter changeil_befaft  pctg_budget_roy_change if ano==2014 & changeil_befaft<1 & pctg_budget_roy_change>-25 & pctg_budget_roy_change<15, msymbol(circle_hollow) mc(dkgreen)) ///
 , scale(1.1)   yscale(r(0 1)) ytitle("Change in percentage of area mined illegally")
graph export "$latexslides/emp_theo_pred.pdf", replace
graph export "$latexpaper/emp_theo_pred.pdf", replace

twoway (lfitci changeil_befaft  pctg_budget_roy_change if ano==2014 & changeil_befaft<1 & pctg_budget_roy_change>-25 & pctg_budget_roy_change<15, lw(thick) clc(orange) fi(inten10)) /// 
(scatter changeil_befaft  pctg_budget_roy_change if ano==2014 & changeil_befaft<1 & pctg_budget_roy_change>-25 & pctg_budget_roy_change<15, msymbol(circle_hollow) mc(dkgreen)) ///
 , scale(1.1)  yscale(r(0 1)) ytitle("Change in percentage of area mined illegally")
graph export "$latexslides/emp_theo_pred_mined.pdf", replace
graph export "$latexpaper/emp_theo_pred_mined.pdf", replace

*************************************
** THE REASON I DO THIS REGRESSIONS FIRST IS THAT THEY ARE "FINAL"
** IE THEY DONT DEPEND ON THE ILLEGAL MINING PREDICTIONS
**********************************

*************************************
**Fiscal effort
**********************************

qui eststo clear


qui eststo m1: xi: reghdfe pctg_tribut ind_after  after_x_loser trend   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m2: xi: reghdfe pctg_tribut after_x_loser   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m3: xi: reghdfe norm_tribut ind_after  after_x_loser trend   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m4: xi: reghdfe norm_tribut after_x_loser  , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui estout m1 m2 m3 m4 using "$latexslides/tribut_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after  after_x_loser  ) prefoot(\midrule ) stats(timefe ltrend N N_clust ymean r2 , fmt(%fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/tribut_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after  after_x_loser  ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


*************************************
**Effects on Infant Mortality
**********************************

qui eststo clear

qui eststo m1: xi: reghdfe tmi ind_after  after_x_loser trend  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m2: xi: reghdfe tmi after_x_loser    , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m3: xi: reghdfe tmi after_x_pctg_loss    , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m4: xi: reghdfe tmi after_x_pctgloss_if after_x_pctgwin_if   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui estout m1 m2 m3 m4  using "$latexslides/tmi_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after  after_x_loser after_x_pctg_loss after_x_pctgloss_if after_x_pctgwin_if) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4  using "$latexpaper/tmi_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after  after_x_loser after_x_pctg_loss after_x_pctgloss_if after_x_pctgwin_if) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace





graph dot (sum) analizedarea_sqkm, over(ano) vertical ytitle("Total area (sqkm) analyzed by year")
graph export "$latexslides/Analized_area.pdf", as(pdf) replace
graph export "$latexpaper/Analized_area.pdf", as(pdf) replace

graph dot (sum) areaprmined_sqkm , over(ano) vertical ytitle("Total area (sqkm) predicted as mined by year")
graph export "$latexslides/Predmined_area.pdf", as(pdf) replace
graph export "$latexpaper/Predmined_area.pdf", as(pdf) replace

graph dot (mean) prop_mined [fweight=round(areamuni_sqkm)] , over(ano) vertical ytitle("Fraction of analyzed area predicted as mined") 
graph export "$latexslides/Predmined_proparea.pdf", as(pdf) replace
graph export "$latexpaper/Predmined_proparea.pdf", as(pdf) replace


sort codmpio ano
gen newtitulosarea_sqkm= titulosarea_sqkm- titulosarea_sqkm[_n-1] if codmpio==codmpio[_n-1] & ano==(ano[_n-1]+1)
graph dot (sum) newtitulosarea_sqkm if country=="Col" , over(ano) vertical ytitle("New area (sqkm) mining titles by year")
graph export "$latexslides/newTitles_area.pdf", as(pdf) replace
graph export "$latexpaper/newTitles_area.pdf", as(pdf) replace

graph dot (sum) areaprilegal_sqkm , over(ano) vertical ytitle("Total area (sqkm) predicted illegal by year")
graph export "$latexslides/Predillegal_area.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_area.pdf", as(pdf) replace

graph dot (mean) prop_illegal [fweight=round(areamuni_sqkm)] , over(ano) vertical ytitle("Proportion of area mined illegaly")
graph export "$latexslides/Predillegal_proparea.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_proparea.pdf", as(pdf) replace

graph dot (mean) propmined_illegal [fweight=round(areamuni_sqkm)] , over(ano) vertical ytitle("Proportion of mined area mined illegaly")
graph export "$latexslides/Predillegal_propmined.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propmined.pdf", as(pdf) replace

*Adjusted predictions with TPR FPR formula
graph dot (mean) propadj_illegal [fweight=round(areamuni_sqkm)] , over(ano) vertical ytitle("Proportion of area mined illegaly")
graph export "$latexslides/Predillegal_propadjarea.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propadjarea.pdf", as(pdf) replace

graph dot (mean) propadjmined_illegal [fweight=round(areamuni_sqkm)] , over(ano) vertical ytitle("Proportion of mined area mined illegaly")
graph export "$latexslides/Predillegal_propadjmined.pdf", as(pdf) replace
graph export "$latexpaper/Predillegal_propadjmined.pdf", as(pdf) replace

******************************
*************************************
**Summary statistics
**********************************
**********************************

qui sum ind_armedgroup_b2012 if ano==2014 & prop_illegal!=.
qui local temp=round(r(mean)*100)
file open newfile using "$latexpaper/pctgag.tex", write replace
file write newfile "`temp'"
file close newfile

qui sum cm_pctgmines_illegal if ano==2014 & prop_illegal!=.
qui local temp=round(r(mean)*100)
file open newfile using "$latexpaper/pctg_cmim_muni.tex", write replace
file write newfile "`temp'"
file close newfile



qui eststo clear
qui estpost tabstat price_index_u areaprmined_sqkm areaprilegal_sqkm prop_illegal propmined_illegal  if propmined_illegal!=., statistics(mean p50 sd min max count) columns(statistics)
qui esttab using "$latexslides/sumstats_muniyear.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs 
qui esttab using "$latexpaper/sumstats_muniyear.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs 

qui sum prop_illegal if propmined_illegal!=.
qui local temp=round(r(mean))
file open newfile using "$latexpaper\prop_illegal.tex", write replace
file write newfile "`temp'"
file close newfile







** Sum stats of regression vars
eststo clear
qui xi: my_ptest prop_illegal_bef prop_illegal_aft pctg_budget_roy_change ///
 ind_armedgroup_b2012_munil prop_illegal_bef_ag prop_illegal_aft_ag, by(ind_loser) 

esttab using "$latexslides/sumstats_regvar.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 

eststo clear
qui xi: my_ptest prop_illegal_bef prop_illegal_aft prop_illegal_bef_ag prop_illegal_aft_ag, by(ind_loser) 
esttab using "$latexpaper/sumstats_regvar.tex", label replace ///
booktabs nomtitle noobs nonumbers  nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("All" "Winners" "Losers" "Difference")  ///
cells("mu_0(fmt(a2)) mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2) star pvalue(d_p))" "se_0(par) se_1(par) se_2(par) se_3(par)") 






*************************************
**Basic diff-diff VISUAL
**********************************


preserve


qui eststo clear
qui eststo t1: reghdfe prop_illegal c.ano#c.ind_loser ind_loser ano  price_index_u price_index_m  , vce(cluster codmpio)
qui estadd ysumm
estadd local munife "No"
estadd local timefe "No"

qui eststo t2: xi: reghdfe prop_illegal c.ano#c.ind_loser   price_index_u price_index_m  , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
qui estout t1 t2 using "$latexslides/trends.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( c.ano#c.ind_loser ind_loser ano)  stats(munife timefe N N_clust ymean r2 , fmt(%fmt %fmt a2 a2 a2 a2 ) labels ("Municipality FE" "Time FE" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace
restore



preserve
keep if ind_muni_allyears==1
collapse (mean) prop_illegal, by(ano ind_loser)

label var prop_illegal "Fraction of area mined illegal"
twoway (line prop_illegal ano if ind_loser==0, sort lw(thick))(line prop_illegal ano if ind_loser==1, sort lw(thick) ), title("Evolution of fraction of area mined illegally")scale(1.2)    xline(2011.5) legend(label(1 "Winners") label(2 "Losers"))
graph export "$latexslides/diff_in_diff_basico_munially.pdf", replace
graph export "$latexpaper/diff_in_diff_basico_munially.pdf", replace
restore

preserve
keep if ind_armedgroup_b2012==0
collapse (mean) prop_illegal, by(ano ind_loser)

label var prop_illegal "Fraction of area mined illegal"
twoway (line prop_illegal ano if ind_loser==1, sort) (line prop_illegal ano if ind_loser==0, sort), xline(2011.5) legend(label(1 "Losers") label(2 "Winners"))
graph export "$latexslides/diff_in_diff_nag.pdf", replace
graph export "$latexpaper/diff_in_diff_nag.pdf", replace
restore

preserve
keep if ind_armedgroup_b2012==1
collapse (mean) prop_illegal, by(ano ind_loser)

label var prop_illegal "Fraction of area mined illegal"
twoway (line prop_illegal ano if ind_loser==1, sort) (line prop_illegal ano if ind_loser==0, sort), xline(2011.5) legend(label(1 "Losers") label(2 "Winners"))
graph export "$latexslides/diff_in_diff_ag.pdf", replace
graph export "$latexpaper/diff_in_diff_ag.pdf", replace
restore

preserve
collapse (mean) tmi, by(ano ind_loser)

label var tmi "Infant mortality rate"
twoway (line tmi ano if ind_loser==1, sort) (line tmi ano if ind_loser==0, sort), xline(2011.5) legend(label(1 "Losers") label(2 "Winners"))
graph export "$latexslides/diff_in_diff_tmi.pdf", replace
graph export "$latexpaper/diff_in_diff_tmi.pdf", replace
restore
/*
preserve
collapse (median) prop_illegal, by(ano ind_loser)
twoway (line prop_illegal ano if ind_loser==1, sort) (line prop_illegal ano if ind_loser==0, sort), legend(label(1 "Affected") label(2 "Not Affected"))
graph export "$latexslides/diff_in_diff_basico_median.pdf", replace
restore
*/






*************************************
**Main regression
**********************************

qui eststo clear

qui eststo m1: reg prop_illegal ind_after ind_loser after_x_loser price_index_u price_index_m trend , vce(cluster codmpio)
qui estadd ysumm
estadd local munife "No"
estadd local timefe "No"


qui eststo m2: xi: reghdfe prop_illegal ind_after  after_x_loser price_index_u   trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"
estadd local sampleused "All"
matrix tempm=e(b)
qui sum prop_illegal if e(sample)



local mainresult=tempm[1,1]
local mainloserresult=tempm[1,2]
qui local mainresrelmean=round(100*`mainresult'/`r(mean)')
qui local mainloserresrelmean=round(100*`mainloserresult'/`r(mean)')
qui local mainmean=round(`r(mean)'*100)/100
qui local mainresult=round(`mainresult'*100)/100
qui local mainloserresult=round(`mainloserresult'*100)/100

file open newfile using "$latexslides/mainmean.tex", write replace
file write newfile "`mainmean'"
file close newfile
file open newfile using "$latexpaper/mainmean.tex", write replace
file write newfile "`mainmean'"
file close newfile
file open newfile using "$latexslides/mainresult.tex", write replace
file write newfile "`mainresult'"
file close newfile
file open newfile using "$latexpaper/mainresult.tex", write replace
file write newfile "`mainresult'"
file close newfile
file open newfile using "$latexslides/mainloserresult.tex", write replace
file write newfile "`mainloserresult'"
file close newfile
file open newfile using "$latexpaper/mainloserresult.tex", write replace
file write newfile "`mainloserresult'"
file close newfile
file open newfile using "$latexslides/mainloserresrelmean.tex", write replace
file write newfile "`mainloserresrelmean'"
file close newfile
file open newfile using "$latexpaper/mainloserresrelmean.tex", write replace
file write newfile "`mainloserresrelmean'"
file close newfile
file open newfile using "$latexslides/mainresrelmean.tex", write replace
file write newfile "`mainresrelmean'"
file close newfile
file open newfile using "$latexpaper/mainresrelmean.tex", write replace
file write newfile "`mainresrelmean'"
file close newfile

qui eststo m3: xi: reghdfe prop_illegal after_x_loser price_index_u price_index_m  , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m4: reg propmined_illegal ind_after ind_loser after_x_loser price_index_u price_index_m  , vce(cluster codmpio)
qui estadd ysumm
estadd local munife "No"
estadd local timefe "No"

qui eststo m5: xi: reghdfe propmined_illegal ind_after  after_x_loser price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local weights "No"
estadd local ltrend "Yes"

qui eststo m5a: xi: reghdfe propmined_illegal c.ind_after##c.ind_loser price_index_u price_index_m trend , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local weights "No"
estadd local ltrend "Yes"

qui eststo m6: xi: reghdfe propmined_illegal after_x_loser price_index_u price_index_m  , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"




/*
qui tab depto, gen(dummy_dep)
forval i=1/29{
qui gen trend_dep`i'=trend*dummy_dep`i'
}
*/


**Heterogeneous results causal forest

qui eststo m2C: xi: reghdfe prop_illegal ind_after after_x_loser   price_index_u   trend if causfor_sample=="C" , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local sampleused "C"

qui eststo mCF: xi: reghdfe prop_illegal ind_after after_x_ind_leaf2 after_x_loser after_x_loser_x_ind_leaf2  price_index_u   trend if causfor_sample=="C" , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local sampleused "C"

qui estout m2  m2C mCF using "$latexslides/CFleaf_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_ind_leaf2 after_x_loser after_x_loser_x_ind_leaf2 ) prefoot(\midrule ) stats(sampleused N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Sample" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m2  m2C mCF using "$latexpaper/CFleaf_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_ind_leaf2 after_x_loser after_x_loser_x_ind_leaf2 ) prefoot(\midrule ) stats(sampleused N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Sample" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

**By importance of mining before reform

qui eststo mint1: xi: reghdfe prop_illegal ind_after price_index_u price_index_m trend, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo mint2: xi: reghdfe prop_illegal ind_after after_x_importance price_index_u price_index_m trend, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo mint3: xi: reghdfe prop_illegal after_x_importance price_index_u price_index_m  , absorb(codmpio anod) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"
qui estout mint1 mint2 mint3 using "$latexslides/importance_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_importance  ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout mint1 mint2 mint3 using "$latexpaper/importance_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_importance  ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


*************************************
**Separated by municipalities in conflict
**********************************

*New single regression
gen ind_after2=ind_after
gen ind_loser2=ind_loser

label var ind_after2 "After"
label var ind_loser2 "Loser"


qui eststo m7a: xi: reghdfe prop_illegal c.ind_after##c.ind_loser price_index_u price_index_m  trend  if ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m7b: xi: reghdfe prop_illegal c.ind_after##c.ind_loser price_index_u price_index_m  trend  if ind_armedgroup_b2012==0, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m7c: xi: reghdfe prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.ind_armedgroup_b2012 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m8a: xi: reghdfe propmined_illegal c.ind_after##c.ind_loser price_index_u price_index_m trend  if ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m8b: xi: reghdfe propmined_illegal c.ind_after##c.ind_loser price_index_u price_index_m trend  if ind_armedgroup_b2012==0, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m8c: xi: reghdfe propmined_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m trend)##c.ind_armedgroup_b2012  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"



qui estout m2 m7a m7b m7c using "$latexslides/violence_reg_prop.tex" , rename(c.ind_after2#c.ind_loser2#c.ind_armedgroup_b2012 c.ind_after#c.ind_loser c.ind_after2#c.ind_armedgroup_b2012 c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m2 m7a m7b m7c using "$latexpaper/violence_reg_prop.tex" , rename(c.ind_after2#c.ind_loser2#c.ind_armedgroup_b2012 c.ind_after#c.ind_loser c.ind_after2#c.ind_armedgroup_b2012 c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


qui estout m5 m8a m8b m8c using "$latexslides/violence_reg_propmined.tex" , rename(c.ind_after2#c.ind_loser2#c.ind_armedgroup_b2012 c.ind_after#c.ind_loser c.ind_after2#c.ind_armedgroup_b2012 c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m5 m8a m8b m8c using "$latexpaper/violence_reg_propmined.tex" , rename(c.ind_after2#c.ind_loser2#c.ind_armedgroup_b2012 c.ind_after#c.ind_loser c.ind_after2#c.ind_armedgroup_b2012 c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

** Left win guerrillas (Farc,Eln) Right wing (AUC, Bacrim)

gen Left=0
gen Right=0
replace Left=1 if ind_rightwag_b2012!=1 & ind_leftwag_b2012==1
replace Right=1 if ind_rightwag_b2012==1 & ind_leftwag_b2012!=1
replace Left=. if ind_rightwag_b2012==1 & ind_leftwag_b2012==1
replace Right=. if ind_rightwag_b2012==1 & ind_leftwag_b2012==1

qui eststo m9a: xi: reghdfe prop_illegal c.ind_after##c.ind_loser  price_index_u price_index_m trend  if Left==1 & ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m9b: xi: reghdfe prop_illegal c.ind_after##c.ind_loser  price_index_u price_index_m trend  if Left==0 & ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m9c: xi: reghdfe prop_illegal c.(c.ind_after2##c.ind_loser2  price_index_u price_index_m trend)##c.Left if ind_armedgroup_b2012==1  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"


qui eststo m10a: xi: reghdfe propmined_illegal c.ind_after##c.ind_loser  price_index_u price_index_m trend  if Left==1 & ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m10b: xi: reghdfe propmined_illegal c.ind_after##c.ind_loser  price_index_u price_index_m trend  if Left==0 & ind_armedgroup_b2012==1, absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m10c: xi: reghdfe propmined_illegal c.(c.ind_after2##c.ind_loser2  price_index_u price_index_m trend)##c.Left if ind_armedgroup_b2012==1  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui estout m2 m9a m9b m9c using "$latexslides/violencerwlw_reg_prop.tex" , rename(c.ind_after2#c.ind_loser2#c.Left c.ind_after#c.ind_loser c.ind_after2#c.Left c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule )  stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m2 m9a m9b m9c using "$latexpaper/violencerwlw_reg_prop.tex" , rename(c.ind_after2#c.ind_loser2#c.Left c.ind_after#c.ind_loser c.ind_after2#c.Left c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule )  stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


qui estout m5 m10a m10b m10c using "$latexslides/violencerwlw_reg_propmined.tex" , rename(c.ind_after2#c.ind_loser2#c.Left c.ind_after#c.ind_loser c.ind_after2#c.Left c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser)  prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m5 m10a m10b m10c using "$latexpaper/violencerwlw_reg_propmined.tex" , rename(c.ind_after2#c.ind_loser2#c.Left c.ind_after#c.ind_loser c.ind_after2#c.Left c.ind_after) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after c.ind_after#c.ind_loser) prefoot(\midrule )  stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


*** Institutional strength


qui eststo het1: xi: reghdfe prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.notarias_pc_2005 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo het2: xi:reghdfe prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.pehosclinc_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo het3: xi: reghdfe prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.peofdereci_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo het4: xi: reghdfe prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.petotalins_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
qui estadd local munife "Yes"
qui estadd local timefe "No"
qui estadd local ltrend "Yes"

qui estout het1 het2 het3 het4 using "$latexslides/inst_strength.tex" , rename(c.ind_after2#c.ind_loser2#c.notarias_pc_2005 Triple c.ind_after2#c.notarias_pc_2005 Doble ///
c.ind_after2#c.ind_loser2#c.pehosclinc_pc_2002 Triple c.ind_after2#c.pehosclinc_pc_2002 Doble ///
c.ind_after2#c.ind_loser2#c.peofdereci_pc_2002 Triple c.ind_after2#c.peofdereci_pc_2002 Doble ///
c.ind_after2#c.ind_loser2#c.petotalins_pc_2002 Triple c.ind_after2#c.petotalins_pc_2002 Doble ) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
varlabels(Triple "After $\times$ Loser $\times$ Institutional" Doble "After $\times$ Institutional")  ///
keep( ind_after2 c.ind_after2#c.ind_loser2 Doble Triple) prefoot(\midrule )  stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


*** Institutional strength 2
foreach var in notarias_pc_2005 pehosclinc_pc_2002 peofdereci_pc_2002 petotalins_pc_2002{
qui sum `var',d
qui gen D_`var'=(`var'>r(p50)) if !missing(`var')
}

qui eststo het1: xi: reghdfe prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.D_notarias_pc_2005 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
qui estadd local munife "Yes"
qui estadd local timefe "No"
qui estadd local ltrend "Yes"

qui eststo het2: xi:reghdfe prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.D_pehosclinc_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo het3: xi: reghdfe prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.D_peofdereci_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo het4: xi: reghdfe prop_illegal c.(c.ind_after2##c.ind_loser2 price_index_u price_index_m  trend)##c.D_petotalins_pc_2002 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

estout het1 het2 het3 het4 using "$latexslides/inst_strength2.tex" , rename(c.ind_after2#c.ind_loser2#c.D_notarias_pc_2005 Triple c.ind_after2#c.D_notarias_pc_2005 Doble ///
c.ind_after2#c.ind_loser2#c.D_pehosclinc_pc_2002 Triple c.ind_after2#c.D_pehosclinc_pc_2002 Doble ///
c.ind_after2#c.ind_loser2#c.D_peofdereci_pc_2002 Triple c.ind_after2#c.D_peofdereci_pc_2002 Doble ///
c.ind_after2#c.ind_loser2#c.D_petotalins_pc_2002 Triple c.ind_after2#c.D_petotalins_pc_2002 Doble ) ///
style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
varlabels(Triple "After $\times$ Loser $\times$ Institutional" Doble "After $\times$ Institutional")  ///
keep( ind_after2 c.ind_after2#c.ind_loser2 Doble Triple) prefoot(\midrule )  stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


*************************************
**Separated by satellite prone (open pit vs underground 
**********************************

qui eststo m9: xi: reghdfe prop_illegal ind_after after_x_loser price_index_u trend if  satelite_prone==1 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m10: xi: reghdfe prop_illegal ind_after after_x_loser price_index_u trend if   satelite_prone==0 , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui estout m2 m9 m10 using "$latexslides/satprone_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m2 m9 m10 using "$latexpaper/satprone_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_loser ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ( "Time FE" "Linear Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


************************
** Continuous measure
********************


*With dept trends

qui eststo clear

qui eststo m1: xi: reghdfe prop_illegal ind_after after_x_pctg_loss price_index_u  trend_depto* , absorb(codmpio)  vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m2: xi: reghdfe prop_illegal after_x_pctg_loss price_index_u  , absorb(codmpio anodepto) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"

qui eststo m3: xi: reghdfe propmined_illegal ind_after  after_x_pctg_loss price_index_u trend_depto*  , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "No"
estadd local ltrend "Yes"

qui eststo m4: xi: reghdfe propmined_illegal after_x_pctg_loss price_index_u   , absorb(codmpio anodepto) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local ltrend "No"
qui estout m1 m2 m3 m4 using "$latexslides/continuous_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("State Year FE" "State Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace
qui estout m1 m2 m3 m4 using "$latexpaper/continuous_reg_deptr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( ind_after after_x_pctg_loss price_index_u ) prefoot(\midrule ) stats( timefe ltrend N N_clust ymean r2 , fmt( %fmt %fmt a2 a2 a2 a2 ) labels ("State Year FE" "State Trend" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace


*************************************
**Homicides armed groups as dependent variable
**********************************

eststo clear
qui eststo m4: xi: reghdfe homi_totalag after_x_loser price_index_u   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"


qui eststo m5: xi: reghdfe homi_totalag after_x_loser price_index_u   if ind_armedgroup_b2012==0 , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui eststo m6: xi: reghdfe homi_totalag after_x_loser price_index_u  if ind_armedgroup_b2012==1 , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui estout m4 m5 m6 using "$latexslides/homicidesag_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_loser price_index_u ) prefoot(\midrule ) stats(timefe N N_clust ymean r2 , fmt( %fmt a2 a2 a2 a2 ) labels ( "Time FE" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m4 m5 m6 using "$latexpaper/homicidesag_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_loser price_index_u ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ( "Time FE" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

*Using pct loss instead of dummy
eststo clear
qui eststo m4: xi: reghdfe homi_totalag after_x_pctg_loss price_index_u   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"


qui eststo m5: xi: reghdfe homi_totalag after_x_pctg_loss price_index_u   if ind_armedgroup_b2012==0 , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui eststo m6: xi: reghdfe homi_totalag after_x_pctg_loss price_index_u  if ind_armedgroup_b2012==1 , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui estout m4 m5 m6 using "$latexpaper/homicidesag_creg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_pctg_loss price_index_u ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Time FE" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

*************************************
**Homicides RATE armed groups as dependent variable
**********************************
eststo clear
qui eststo m4: xi: reghdfe tasa_homag after_x_loser price_index_u   , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"


qui eststo m5: xi: reghdfe tasa_homag after_x_loser price_index_u  if ind_armedgroup_b2012==0 , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui eststo m6: xi: reghdfe tasa_homag after_x_loser price_index_u  if ind_armedgroup_b2012==1 , absorb(codmpio ano) vce(cluster codmpio)
qui estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"

qui estout m4 m5 m6 using "$latexslides/tasa_homag_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_loser ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Time FE" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

qui estout m4 m5 m6 using "$latexpaper/tasa_homag_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x_loser price_index_u ) prefoot(\midrule ) stats(timefe N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Time FE" "N. of obs." "Municipalities" "Mean of dep. var." "\$R^2\$" )) replace

*/



** Calculate bounds of ommited variable bias
** See Altonji et al(2005) or Oster(2013)
qui reg prop_illegal ind_after after_x_loser price_index_u price_index_m trend i.codmpio
matrix tempm=e(b)
local ols_after=tempm[1,1]
local ols_afteraffi=tempm[1,2]

*Altonji indicator after
psacalc ind_after set
qui local absdif=abs(`r(output)'-`ols_after')
qui local altonjilb_after=round((`ols_after'-`absdif')*100)/100
file open newfile using "$latexslides/altonjilb_after.tex", write replace
file write newfile "`altonjilb_after'"
file close newfile
file open newfile using "$latexpaper/altonjilb_after.tex", write replace
file write newfile "`altonjilb_after'"
file close newfile

qui local altonjiub_after=round((`ols_after'+`absdif')*100)/100
file open newfile using "$latexslides/altonjiub_after.tex", write replace
file write newfile "`altonjiub_after'"
file close newfile
file open newfile using "$latexpaper/altonjiub_after.tex", write replace
file write newfile "`altonjiub_after'"
file close newfile

*Altonji indicator afterXLoser
psacalc after_x_loser set

qui local absdif=abs(`r(output)'-`ols_afteraffi')
qui local altonjilb_afteraffi=round((`ols_afteraffi'-`absdif')*100)/100
file open newfile using "$latexslides/altonjilb_afteraffi.tex", write replace
file write newfile "`altonjilb_afteraffi'"
file close newfile
file open newfile using "$latexpaper/altonjilb_afteraffi.tex", write replace
file write newfile "`altonjilb_afteraffi'"
file close newfile

qui local altonjiub_afteraffi=round((`ols_afteraffi'+`absdif')*100)/100
file open newfile using "$latexslides/altonjiub_afteraffi.tex", write replace
file write newfile "`altonjiub_afteraffi'"
file close newfile
file open newfile using "$latexpaper/altonjiub_afteraffi.tex", write replace
file write newfile "`altonjiub_afteraffi'"
file close newfile
*/

***** Individual regressions by municipality

/*
qui gen beta_aft_muni=.
qui gen std_aft_muni=.
egen muni_id = group(codmpio)

qui su muni_id, meanonly
local nmunis=`r(max)'
	forvalues i=1/`nmunis' {
				di "done with `i'"
				
					qui reg prop_illegal ind_after price_index_u  trend  if muni_id==`i' 

					qui replace beta_aft_muni=_b[ind_after] if muni_id==`i' 
					qui mat auch=e(V)
					qui replace std_aft_muni=(auch[3,3])^0.5 if muni_id==`i' 
					 
				 }
				
preserve
	
collapse  ind_armedgroup_b2012 ind_minerpot ind_Hidrocarburos_ever ind_metalespreciosos_ever ind_loser pctg_budget_roy_change beta_aft_muni std_aft_muni, by(codmpio municipio depto)
*/



** Calculate bounds of ommited variable bias to optimal controls
** It still not clear if we should apply both tests given that Lasso assumes w ehave everything

/*
qui merge 1:1 codmpio ano using "$base_out/Temporary/Lasso_vars.dta"
drop _merge
qui reg propmined_illegal ind_after after_x_loser ${yvSel} i.codmpio
matrix tempm=e(b)
local mainresult=tempm[1,1]
local afteraffiresult=tempm[1,2]
qui local altonjilb_after=round(`mainresult'*100)/100
file open newfile using "$latexslides/altonjilassolb_after.tex", write replace
file write newfile "`altonjilb_after'"
file close newfile
psacalc ind_after set
qui local altonjiub_after=round(`r(output)'*100)/100
file open newfile using "$latexslides/altonjilassoub_after.tex", write replace
file write newfile "`altonjiub_after'"
file close newfile

qui local altonjilb_afteraffi=round(`afteraffiresult'*100)/100
file open newfile using "$latexslides/altonjilassolb_afteraffi.tex", write replace
file write newfile "`altonjilb_afteraffi'"
file close newfile
psacalc after_x_loser set
qui local altonjiub_afteraffi=round(`r(output)'*100)/100
file open newfile using "$latexslides/altonjilassoub_afteraffi.tex", write replace
file write newfile "`altonjiub_afteraffi'"
file close newfile
*/
