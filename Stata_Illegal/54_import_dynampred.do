forval i=2005/2005 {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/`i'_dynampred.csv", varnames(1) clear

qui foreach x of varlist dyn_* {
destring `x', force replace
}
qui gen legalm_pastm=900*dyn_a/10^6
label var legalm_pastm "Area legally mined that was mined in the past" 
drop if codane==.
saveold "$base_out/Temporary/dynampred`i'.dta", replace

} 
