*************
** Create Stata Data Set for new years 2015-2017

* 1. Illegal mining predictions from R



set more off
******************
** 1. Import illegal mining predictions from R
**********

global prmodel_tpr=0.3245
global prmodel_fpr=0.0029


forval i=2015/2016 {


<<<<<<< HEAD
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/muniResults_tit2014Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
=======
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/muniResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
>>>>>>> 7b746cd2c51150ac0cd1a56dc446445756a0730a


*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.
saveold "$base_out/Temporary/results`i'.dta", replace

} 


use "$base_out/Temporary/results2015.dta", clear
forval i=2016/2016 {
append using "$base_out/Temporary/results`i'.dta"
} 

gen analizedarea_sqkm=analizedarea/10^6
label var analizedarea_sqkm "Area (sqkm) of pixels analized in prediction"


gen areamuni_sqkm=municipioarea/10^6
label var areamuni_sqkm "Area of municipality (km2)"


gen areaprmined_sqkm=areamines/10^6
label var areaprmined_sqkm "Area (sqkm) predicted as mined"

**The adjustment is based on the analysis of the econometric error
gen areaprmined_adj_sqkm=max(0,(areaprmined_sqkm-analizedarea_sqkm*$prmodel_fpr )/($prmodel_tpr - $prmodel_fpr ))
label var areaprmined_sqkm "Area (sqkm) predicted as mined adjusted"

gen titulosarea_sqkm=titulosarea/10^6
label var titulosarea_sqkm "Area (sqkm) of mining titles"

gen titulosareaprnomined_sqkm=areatitulosnomine/10^6
label var titulosareaprnomined_sqkm "Area (sqkm) of mining titles predicted as NOT mined"

gen areaprilegal_sqkm=areaillegalmines/10^6
label var areaprilegal_sqkm "Area (sqkm) predicted as illegaly mined"

gen areaprilegal_adj_sqkm=areaprmined_adj_sqkm*areaprilegal_sqkm/areaprmined_sqkm
label var areaprmined_sqkm "Area (sqkm) predicted as illegaly mined adjusted"

gen titulosarea_ha=titulosarea_sqkm*100


rename codane codmpio
rename year ano

*linea 3359 original file
gen propmined_illegal= 100*areaprilegal_sqkm/ areaprmined_sqkm
label var propmined_illegal "Share of municipality mined area mined illegaly"

gen ind_after=1 
gen ind_col=1
gen after_x_col=1
gen country="Col"

*keep codmpio ano ind_after ind_col after_x_col propmined_illegal
drop v1

saveold "$base_out/Temporary/panel_prillegal1516t14.dta", replace

*******
***** Peru predicitions
*****

qui forval i=2015/2016 {
qui import delimited "$base_out/Predicciones/Rf10nolossy/Peru/muniResults_Rf10nolossy_`i'.csv", varnames(1) clear

*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.
saveold "$base_out/Temporary/results`i'Peru.dta", replace

} 

use "$base_out/Temporary/results2015Peru.dta", clear
qui forval i=2016/2016 {
append using "$base_out/Temporary/results`i'Peru.dta"
} 

gen analizedarea_sqkm=analizedarea/10^6
label var analizedarea_sqkm "Area (sqkm) of pixels analized in prediction"

gen areamuni_sqkm=municipioarea/10^6
label var areamuni_sqkm "Area (sqkm) of municipality in raster"

gen areaprmined_sqkm=areamines/10^6
label var areaprmined_sqkm "Area (sqkm) predicted as mined"

gen titulosarea_sqkm=titulosarea/10^6
label var titulosarea_sqkm "Area (sqkm) of mining titles"

gen titulosareaprnomined_sqkm=areatitulosnomine/10^6
label var titulosareaprnomined_sqkm "Area (sqkm) of mining titles predicted as NOT mined"

gen areaprilegal_sqkm=areaillegalmines/10^6
label var areaprilegal_sqkm "Area (sqkm) predicted as illegaly mined"

**The adjustment is based on the analysis of the econometric error
/*
gen areaprmined_adj_sqkm=max(0,(areaprmined_sqkm-analizedarea_sqkm*$prmodel_fpr )/($prmodel_tpr - $prmodel_fpr ))
label var areaprmined_adj_sqkm "Area (sqkm) predicted as mined adjusted"

gen areaprilegal_adj_sqkm=areaprmined_adj_sqkm*areaprilegal_sqkm/areaprmined_sqkm
label var areaprmined_sqkm "Area (sqkm) predicted as illegaly mined adjusted"
*/

keep codane year *_sqkm

rename codane codmpio
rename year ano


qui gen trend = (ano - 2004)/10 
qui gen prop_mined=100*areaprmined_sqkm/analizedarea_sqkm
label var prop_mined "Percentage of analyzed area predicted as mined"

gen prop_illegal= 100*areaprilegal_sqkm/ analizedarea_sqkm
label var prop_illegal "Share of municipality area mined illegaly"

gen propmined_illegal= 100*areaprilegal_sqkm/ areaprmined_sqkm
label var propmined_illegal "Share of municipality mined area mined illegaly"

/*

gen propadj_illegal= 100*areaprilegal_adj_sqkm/ analizedarea_sqkm
label var propadj_illegal "Share of municipality area mined illegaly adjusted"

gen propadjmined_illegal= 100*areaprilegal_adj_sqkm/ areaprmined_adj_sqkm
label var propadjmined_illegal "Share of municipality mined area mined illegaly adjusted"
*/

gen ind_loser=0 
label var ind_loser "Loser"

gen ind_after=1 if ano>2011 & ano!=.
replace ind_after=0 if ano<2012
label var ind_after "After"

qui gen after_x_loser=ind_loser*ind_after
label var after_x_loser "After x Loser"

qui gen prop_illegal_bef= prop_illegal if ind_after==0

gen titulosarea_ha=titulosarea_sqkm*100


gen country="Peru"
saveold "$base_out/Temporary/panel_prillegal_Peru1516.dta", replace





*****************************
*Mineral
***************************

qui forval i=2015/2016 {

qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/muniMi/muniMiResults_tit2014Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear

*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace

foreach x in gold platinum copper coal columbite iron magnesium nickel phosphate potassium uranium {
qui destring area`x'mines , force replace
qui destring areaillegal`x'mines, force replace
qui destring analized`x'area , force replace
qui destring area`x'muni, force replace
}


drop if codane==.
saveold "$base_out/Temporary/resultsMi`i'.dta", replace

} 

qui forval i=2015/2015 {
append using "$base_out/Temporary/resultsMi`i'.dta"
} 

local i=1
gen areaillegalwithpotmines=0
gen areawithpotmines=0
qui foreach x in gold platinum copper coal columbite iron magnesium nickel phosphate potassium uranium  {
gen propmined_illegal_mi`i'=100*areaillegal`x'mines/ area`x'mines

qui gen w_analizedpctgarea_mi`i'=round( 100*analized`x'area/ area`x'muni )
qui gen analizedareasqkm_mi`i'=analized`x'area/10^6
qui gen areaminedsqkm_mi`i'=area`x'mines/10^6

***Analyzed area by mineral
**The adjustment is based on the analysis of the econometric error
global prmodel_tpr=0.3245
global prmodel_fpr=0.0029
gen areaprminedMi_adj_`i'=max(0,(area`x'mines-analized`x'area*$prmodel_fpr )/($prmodel_tpr - $prmodel_fpr ))

gen areaprilegalMi_adj_`i'=areaprminedMi_adj_`i'*areaillegal`x'mines/area`x'mines


gen propadjminedMi_illegal`i'= 100*areaprilegalMi_adj_`i'/ areaprminedMi_adj_`i'



*I dont do this with rowtotal() because it will also include the total, and if I substract I get rounding errors
replace areaillegalwithpotmines=areaillegalwithpotmines+areaillegal`x'mines
replace areawithpotmines=areawithpotmines+area`x'mines
local i=`i'+1


}


*gen propmined_illegal_mi12=100*(areaillegalmines-areaillegalwithpotmines)/ (areamines-areawithpotmines)
*replace propmined_illegal_mi12=1 if propmined_illegal_mi12>1 & propmined_illegal_mi12!=.
keep codane year  propmined* propadjminedMi_illegal* w_analizedpctgarea_mi* analizedareasqkm_mi* areaminedsqkm_mi*

reshape long propmined_illegal_mi propadjminedMi_illegal w_analizedpctgarea_mi analizedareasqkm_mi areaminedsqkm_mi, i(codane year) j(mineral)
label var propmined_illegal_mi "Share of mineral-municipality mined area mined illegaly"


rename codane codmpio
rename year ano

gen afterxgold=(ano>2011)*(mineral==1)
label var afterxgold "After $\times$ Gold"

gen ind_after=(ano>2011)

gen afterxplatinum=(ano>2011)*(mineral==2)
label var afterxplatinum "After $\times$ Platinum"

gen regalia_mine=5
replace regalia_mine=6 if mineral==1
replace regalia_mine=3 if mineral==7
replace regalia_mine=12 if mineral==8
replace regalia_mine=10 if mineral==11
replace regalia_mine=1 if mineral==12
gen afterxregalia=(ano>2011)*regalia_mine
label var afterxregalia "After $\times \alpha$"

saveold "$base_out/Temporary/panel_prillegalMi1516t14.dta", replace
