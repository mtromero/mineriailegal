qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/results2004.csv", varnames(1) clear
qui destring analizedarea, force replace
gen areamuni_sqkm=municipioarea/10^6
label var areamuni_sqkm "Area (km2) of municipality in raster"
rename codane codmpio
keep codmpio areamuni_sqkm
saveold "$base_out/Temporary/puta_area.dta", replace

use "$base_out/panel_final.dta", replace
drop areamuni_sqkm
merge m:1 codmpio using "$base_out/Temporary/puta_area.dta"
