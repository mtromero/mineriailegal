

mat ddmat=J(6,3,0)

reg propmined_illegal ind_after ind_col after_x_col

matrix A=e(b)

matrix B=e(V)

mat ddmat[5,3] =round(A[1,3],0.01)

mat ddmat[6,3] =round(sqrt(B[3,3]),0.01)

keep if e(sample)==1


sum propmined_illegal if ind_after==0 & ind_col==0 
mat ddmat[1,1] =round(r(mean),0.01)
mat ddmat[2,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_col==0
mat ddmat[3,1] =round(r(mean),0.01)
mat ddmat[4,1] = round(r(sd),0.01)

sum propmined_illegal if ind_after==0 & ind_col==1 
mat ddmat[1,2] =round(r(mean),0.01)
mat ddmat[2,2] = round(r(sd),0.01)

sum propmined_illegal if ind_after==1 & ind_col==1 &  e(sample)==1
mat ddmat[3,2] =round(r(mean),0.01)
mat ddmat[4,2] = round(r(sd),0.01)

reg propmined_illegal ind_after if ind_col==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,1] =round(A[1,1],0.01)
mat ddmat[6,1] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_after if ind_col==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[5,2] =round(A[1,1],0.01)
mat ddmat[6,2] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_col if ind_after==0
matrix A=e(b)
matrix B=e(V)
mat ddmat[1,3] =round(A[1,1],0.01)
mat ddmat[2,3] =round(sqrt(B[1,1]),0.01)

reg propmined_illegal ind_col if ind_after==1
matrix A=e(b)
matrix B=e(V)
mat ddmat[3,3] =round(A[1,1],0.01)
mat ddmat[4,3] =round(sqrt(B[1,1]),0.01)

estout matrix(ddmat) using "$latexslides/sumstats_ddperu.tex", label replace ///
 starlevels(* 0.10 ** 0.05 *** 0.01)  collabels( "Peru" "Colombia" "Difference")  ///
cells(r1(fmt(a2)) r3(fmt(a2)) r5(fmt(a2) star) r2(par) r4(par) r6(par)) 

