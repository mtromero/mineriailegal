*************
** Create Stata Data Set
* Many sources of data are combined in order to create panel_final
* 1. Illegal mining predictions from R
* 2. Royalties and production ANH, SIMCO, SGR
* 3. Some stats from mining census 2010
* 4. Prices for minerals
* 5. Municipality characteristics from CEDE Panel


set more off
******************
** 1. Import illegal mining predictions from R
**********

global prmodel_tpr=0.3245
global prmodel_fpr=0.0029


forval i=2004/2014 {

*qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/results`i'.csv", varnames(1) clear
*qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/TPAIS_proj/muniResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
*qui import delimited "$base_out/Predicciones/Rf10ml3osm/SindisapCol/muniResults_Rf10ml3osm_`i'.csv", varnames(1) clear

if `i'==2012 | `i'==2013 {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/TPAIS_proj/muniResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
qui destring codane, force replace
qui destring year, force replace
qui destring titulosarea, force replace
rename titulosarea titulosConcarea
keep codane year titulosConcarea


drop if codane==.
saveold "$base_out/Temporary/titulosConc`i'.dta", replace

qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/TPAIS_proj/muniResults_tit2014Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
} 
else {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/TPAIS_proj/muniResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
}

*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.


if `i'==2012 | `i'==2013 {
merge 1:1 codane year using "$base_out/Temporary/titulosConc`i'.dta"
drop _merge
}
saveold "$base_out/Temporary/results`i'.dta", replace
} 



forval i=2004/2013 {
append using "$base_out/Temporary/results`i'.dta"
} 

gen analizedarea_sqkm=analizedarea/10^6
label var analizedarea_sqkm "Area (sqkm) of pixels analized in prediction"


gen areamuni_sqkm=municipioarea/10^6
label var areamuni_sqkm "Area of municipality (km2)"

preserve
rename codane codmpio
collapse areamuni_sqkm, by(codmpio)
saveold "$base_out/Temporary/area_muni.dta", replace
restore

gen areaprmined_sqkm=areamines/10^6
label var areaprmined_sqkm "Area (sqkm) predicted as mined"

**The adjustment is based on the analysis of the econometric error
gen areaprmined_adj_sqkm=max(0,(areaprmined_sqkm-analizedarea_sqkm*$prmodel_fpr )/($prmodel_tpr - $prmodel_fpr ))
label var areaprmined_sqkm "Area (sqkm) predicted as mined adjusted"

gen titulosarea_sqkm=titulosarea/10^6
label var titulosarea_sqkm "Area (sqkm) of mining titles"

replace titulosConcarea=titulosarea if year<2012 | year==2014
gen titulosConcarea_sqkm=titulosConcarea/10^6
label var titulosConcarea_sqkm "Area (sqkm) of concurrent mining titles"

gen titulosareaprnomined_sqkm=areatitulosnomine/10^6
label var titulosareaprnomined_sqkm "Area (sqkm) of mining titles predicted as NOT mined"

gen areaprilegal_sqkm=areaillegalmines/10^6
label var areaprilegal_sqkm "Area (sqkm) predicted as illegaly mined"

gen areaprilegal_adj_sqkm=areaprmined_adj_sqkm*areaprilegal_sqkm/areaprmined_sqkm
label var areaprmined_sqkm "Area (sqkm) predicted as illegaly mined adjusted"

keep codane year *_sqkm

rename codane codmpio
rename year ano


saveold "$base_out/Temporary/panel_prillegal.dta", replace



*** Concern that the closure of the ventanilla minera explain the results
* For the main results above we used 2012 and 2013 predictions with titles in 2014
** This is the raw measure using the titles of each year

forval i=2004/2014 {

qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/TPAIS_proj/muniResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear


*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.
saveold "$base_out/Temporary/results_tityear_`i'.dta", replace

} 



qui forval i=2004/2013 {
append using "$base_out/Temporary/results_tityear_`i'.dta"
 }
qui gen propmined_illegal_tityear=100*(areaillegalmines/10^6)/(areamines/10^6)

keep codane year propmined_illegal_tityear

rename codane codmpio
rename year ano


saveold "$base_out/Temporary/panel_prillegal_tityear.dta", replace
*/
*************************************
***** Probability mined instead of dummy mined vs non-mined
************************************
/*
qui forval i=2004/2014 {
if `i'==2012 | `i'==2013 {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/Probability/muniPrResults_tit2014Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
} 
else {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/Probability/muniPrResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
}


*These variables are in number pixels and contains same info as area
qui drop sumprmines sumprillegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring areasumprmines, force replace
qui destring areasumprillegalmines , force replace

drop if codane==.
saveold "$base_out/Temporary/results_prob2014_`i'.dta", replace

} 



qui forval i=2004/2013 {
append using "$base_out/Temporary/results_prob2014_`i'.dta"
 }
qui gen propmined_illegal_prob=100*(areasumprillegalmines/10^6)/(areasumprmines/10^6)

keep codane year propmined_illegal_prob

rename codane codmpio
rename year ano

saveold "$base_out/Temporary/panel_prillegal_prob.dta", replace

*****************************
***** Mineral
/*
qui forval i=2004/2014 {

qui import delimited "$base_out/muniMiPr/muniMiPrResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear

qui destring codane, force replace
qui destring year, force replace
 local m=1
 
 foreach x in gold platinum  coal  {
  
 qui destring sumprarea`x'mines, force replace
qui destring sumprareaillegal`x'mines , force replace
qui gen propminedMi_illegal_prob`m'=100*(sumprareaillegal`x'mines/10^6)/(sumprarea`x'mines/10^6)
local m=`m'+1

}
*Coal's code is 4 not 3
rename propminedMi_illegal_prob3 propminedMi_illegal_prob4

keep codane year  propmined*
drop if codane==.
saveold "$base_out/Temporary/results_probMi_`i'.dta", replace

} 



qui forval i=2004/2013 {
append using "$base_out/Temporary/results_probMi_`i'.dta"
 }
 



reshape long propminedMi_illegal_prob, i(codane year) j(mineral)
label var propminedMi_illegal_prob "Share of mineral-municipality mined area mined illegaly"

keep codane year mineral propmined*
rename codane codmpio
rename year ano


saveold "$base_out/Temporary/panel_prMiillegal_prob.dta", replace

***********************************************
***********Illegal mining outside National Parks
****************************************

forval i=2004/2014 {
if `i'==2012 | `i'==2013 {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/muniSinParqResults_tit2014Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
} 
else {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/muniSinParqResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear

}
*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.
saveold "$base_out/Temporary/resultsSinParq`i'.dta", replace

} 

forval i=2004/2013 {
append using "$base_out/Temporary/resultsSinParq`i'.dta"
} 

gen analizedareaSP_sqkm=analizedarea/10^6
label var analizedareaSP_sqkm "Area (sqkm) of pixels analized in prediction outside parks"

gen areamuniSP_sqkm=municipioarea/10^6
label var areamuniSP_sqkm "Area of municipality outside parks (km2)"

gen areaprminedSP_sqkm=areamines/10^6
label var areaprminedSP_sqkm "Area (sqkm) predicted as mined outside parks"


gen titulosareaSP_sqkm=titulosarea/10^6
label var titulosareaSP_sqkm "Area (sqkm) of mining titles outside parks"

gen titulosareaprnominedSP_sqkm=areatitulosnomine/10^6
label var titulosareaprnominedSP_sqkm "Area (sqkm) of mining titles predicted as NOT mined outside parks"

gen areaprilegalSP_sqkm=areaillegalmines/10^6
label var areaprilegalSP_sqkm "Area (sqkm) predicted as illegaly mined outside parks"



keep codane year *_sqkm

rename codane codmpio
rename year ano


saveold "$base_out/Temporary/panel_prillegalSP.dta", replace


**********By mineral

 qui forval i=2004/2014 {

qui import delimited "$base_out/MuniMineral/muniMiSinParqResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear


*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.

foreach x in gold platinum copper coal columbite iron magnesium nickel phosphate potassium uranium {
qui destring area`x'mines , force replace
qui destring areaillegal`x'mines, force replace
}
saveold "$base_out/Temporary/resultsMiSinParq`i'.dta", replace

} 

qui forval i=2004/2013 {
append using "$base_out/Temporary/resultsMiSinParq`i'.dta"
} 

local i=1
gen areaillegalwithpotmines=0
gen areawithpotmines=0
qui foreach x in gold platinum copper coal columbite iron magnesium nickel phosphate potassium uranium {
gen propminedSP_illegal_mi`i'=100*areaillegal`x'mines/ area`x'mines

*I dont do this with rowtotal() because it will also include the total, and if I substract I get rounding errors
replace areaillegalwithpotmines=areaillegalwithpotmines+areaillegal`x'mines
replace areawithpotmines=areawithpotmines+area`x'mines


rename areaillegal`x'mines areaillegalSPmines`i'
rename area`x'mines areaSPmines`i'
local i=`i'+1
}


keep codane year  propmined* areaillegalSPmines* areaSPmines*

reshape long propminedSP_illegal_mi areaillegalSPmines areaSPmines, i(codane year) j(mineral)
label var propminedSP_illegal_mi "Share of mineral-municipality mined area mined illegaly"

replace areaSPmines=areaSPmines/10^6
replace areaillegalSPmines=areaillegalSPmines/10^6
rename areaSPmines areaminedSPsqkm_mi
rename areaillegalSPmines areaillegalminedSPsqkm_mi

rename codane codmpio
rename year ano


saveold "$base_out/Temporary/panel_prillegalSPMi.dta", replace

***********************************************
***********By mineral
****************************************

*Title Mineral predictions

qui forval i=2004/2014 {
qui import delimited "$base_out/MuniMineral/muniMiTiResults_`i'.csv", varnames(1) clear

qui foreach x of varlist codane year tit* municipioarea {
destring `x', force replace
}

drop if codane==.
saveold "$base_out/Temporary/titulosMi`i'.dta", replace
} 

qui forval i=2004/2013 {
append using "$base_out/Temporary/titulosMi`i'.dta"
}

/*
qui gen titulosareaMi_ha1=(tit1area)/10^4
qui gen titulosareaMi_ha2=(tit2area)/10^4
qui gen titulosareaMi_ha4=(tit4area)/10^4 
*/
*How to count double mineral titles
qui gen titulosareaMi_ha1=(tit1area+tit12area/2+tit14area/2+tit124area/3)/10^4
qui gen titulosareaMi_ha2=(tit2area+tit12area/2+tit24area/2+tit124area/3)/10^4
qui gen titulosareaMi_ha4=(tit4area+tit24area/2+tit14area/2+tit124area/3)/10^4 

qui gen frtitulosareaMi1=100*(tit1area+tit12area/2+tit14area/2+tit124area/3)/municipioarea
qui gen frtitulosareaMi2=100*(tit2area+tit12area/2+tit24area/2+tit124area/3)/municipioarea
qui gen frtitulosareaMi4=100*(tit4area+tit24area/2+tit14area/2+tit124area/3)/municipioarea


keep codane year titulosareaMi_ha* frtitulosareaMi*

reshape long titulosareaMi_ha frtitulosareaMi, i(codane year) j(mineral)
label var titulosareaMi_ha "Area mineral mining titles (ha)"

rename codane codmpio
rename year ano


saveold "$base_out/Temporary/titulosMi.dta", replace

*Import dynamic predictions new area mined

qui forval i=2005/2014 {
qui import delimited "$base_out/MuniMineral/sandwichNA/muniMidynResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
*qui import delimited "$base_out/MuniMineral/muniMidynResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear

qui foreach x of varlist codane year dyn* {
destring `x', force replace
}

drop if codane==.
saveold "$base_out/Temporary/dynampredMi`i'.dta", replace
} 

qui forval i=2005/2013 {
append using "$base_out/Temporary/dynampredMi`i'.dta"
}


 local m=1
 
 foreach x in gold platinum  coal  {
  
  qui gen legalm_pastNm`x'=dyn`x'_b/10^6
qui gen illegalm_pastNm`x'=dyn`x'_e/10^6
gen newpropminedMi_illegal`m'= 100*illegalm_pastNm`x'/ (illegalm_pastNm`x'+legalm_pastNm`x')

local m=`m'+1

}

*Coal's code is 4 not 3
rename newpropminedMi_illegal3 newpropminedMi_illegal4

keep codane year newprop*

reshape long newpropminedMi_illegal, i(codane year) j(mineral)
label var newpropminedMi_illegal "Share of new mined area mined illegaly"

rename codane codmpio
rename year ano


saveold "$base_out/Temporary/dynampred_Mi.dta", replace

************************
** Alternative cutoffs
*I repeat 78 for easyness on robustness, but 78 is the optimal threshold, therefore the main result
*******************

qui forval cut=12(11)89 {
forval i=2004/2014 {

qui import delimited "$base_out/MuniMineral/Cutoff`cut'/muniMiResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear

*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace

foreach x in gold platinum copper coal columbite iron magnesium nickel phosphate potassium uranium {
qui destring area`x'mines , force replace
qui destring areaillegal`x'mines, force replace
}
drop if codane==.
saveold "$base_out/Temporary/resultsc`cut'_Mi`i'.dta", replace

} 

forval i=2004/2013 {
append using "$base_out/Temporary/resultsc`cut'_Mi`i'.dta"
} 

local i=1
gen areaillegalwithpotmines=0
gen areawithpotmines=0
foreach x in gold platinum copper coal columbite iron magnesium nickel phosphate potassium uranium {
gen c`cut'_propmined_illegal_mi`i'=100*areaillegal`x'mines/ area`x'mines

local i=`i'+1
}



keep codane year  c`cut'_propmined*

reshape long c`cut'_propmined_illegal_mi, i(codane year) j(mineral)
label var c`cut'_propmined_illegal_mi "Share of mineral-municipality mined area mined illegaly ML cut `cut'"



rename codane codmpio
rename year ano

saveold "$base_out/Temporary/c`cut'pred_Mi.dta", replace
}

***********************
***********************
** Merge all Mi DataSets
***********
***********************


qui forval i=2004/2014 {

qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/muniMi/muniMiResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear

*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace

foreach x in gold platinum copper coal columbite iron magnesium nickel phosphate potassium uranium {
qui destring area`x'mines , force replace
qui destring areaillegal`x'mines, force replace
qui destring analized`x'area , force replace
qui destring area`x'muni, force replace
}


drop if codane==.
saveold "$base_out/Temporary/resultsMi`i'.dta", replace

} 

qui forval i=2004/2013 {
append using "$base_out/Temporary/resultsMi`i'.dta"
} 

local i=1
gen areaillegalwithpotmines=0
gen areawithpotmines=0
qui foreach x in gold platinum copper coal columbite iron magnesium nickel phosphate potassium uranium  {
gen propmined_illegal_mi`i'=100*areaillegal`x'mines/ area`x'mines

qui gen w_analizedpctgarea_mi`i'=round( 100*analized`x'area/ area`x'muni )
qui gen analizedareasqkm_mi`i'=analized`x'area/10^6
qui gen areaminedsqkm_mi`i'=area`x'mines/10^6

***Analyzed area by mineral
**The adjustment is based on the analysis of the econometric error
global prmodel_tpr=0.3245
global prmodel_fpr=0.0029
gen areaprminedMi_adj_`i'=max(0,(area`x'mines-analized`x'area*$prmodel_fpr )/($prmodel_tpr - $prmodel_fpr ))

gen areaprilegalMi_adj_`i'=areaprminedMi_adj_`i'*areaillegal`x'mines/area`x'mines


gen propadjminedMi_illegal`i'= 100*areaprilegalMi_adj_`i'/ areaprminedMi_adj_`i'



*I dont do this with rowtotal() because it will also include the total, and if I substract I get rounding errors
replace areaillegalwithpotmines=areaillegalwithpotmines+areaillegal`x'mines
replace areawithpotmines=areawithpotmines+area`x'mines
local i=`i'+1


}


*gen propmined_illegal_mi12=100*(areaillegalmines-areaillegalwithpotmines)/ (areamines-areawithpotmines)
*replace propmined_illegal_mi12=1 if propmined_illegal_mi12>1 & propmined_illegal_mi12!=.
keep codane year  propmined* propadjminedMi_illegal* w_analizedpctgarea_mi* analizedareasqkm_mi* areaminedsqkm_mi*

reshape long propmined_illegal_mi propadjminedMi_illegal w_analizedpctgarea_mi analizedareasqkm_mi areaminedsqkm_mi, i(codane year) j(mineral)
label var propmined_illegal_mi "Share of mineral-municipality mined area mined illegaly"



rename codane codmpio
rename year ano

***Merge other data

merge 1:1 codmpio ano mineral using "$base_out/Temporary/panel_prillegalSPMi.dta"
drop _merge


merge 1:1 codmpio ano mineral using "$base_out/Temporary/panel_prMiillegal_prob.dta"
drop _merge

merge 1:1 codmpio ano mineral using "$base_out/Temporary/dynampred_Mi.dta"
drop _merge

qui forval cut=12(11)89 {
 merge 1:1 codmpio ano mineral using "$base_out/Temporary/c`cut'pred_Mi.dta"
drop _merge
}

 merge 1:1 codmpio ano mineral using "$base_out/Temporary/titulosMi.dta"
drop _merge


gen afterxgold=(ano>=2011)*(mineral==1)
label var afterxgold "After $\times$ Gold"

gen ind_after=(ano>=2011)

gen afterxplatinum=(ano>=2011)*(mineral==2)
label var afterxplatinum "After $\times$ Platinum"

merge m:1 mineral ano using "$base_out\Temporary\precios_mineral.dta"
drop _merge

gen regalia_mine=5
replace regalia_mine=6 if mineral==1
replace regalia_mine=3 if mineral==7
replace regalia_mine=12 if mineral==8
replace regalia_mine=10 if mineral==11
replace regalia_mine=1 if mineral==12
gen afterxregalia=(ano>=2011)*regalia_mine
label var afterxregalia "After $\times \alpha$"

label var precio "Price"
label var frtitulosareaMi "Share area titled"

merge m:1 codmpio using "$base_out/Temporary/area_muni.dta"
drop _merge

gen propmuni_illegal_mi=propmined_illegal_mi*areaminedsqkm_mi/ areamuni_sqkm

saveold "$base_out/Temporary/panel_prillegalMi.dta", replace

*/

/*
*** Size of illegal mines

import delimited "$base_out\Predicciones\Rf10onlymlOSM_ecopot_sindisap\area_count_disagg\2014Rf10onlymlOSMecopotarea_count_ntit.csv", clear
destring codane2, force replace
keep departamen municipio codane2
save "$base_out\Temporary\Codigos_Dane_areacount_ntit.dta", replace

qui forval i=2004/2014 {
import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/area_count_disagg/`i'Rf10onlymlOSMecopot_mine_disagg.csv", clear
merge m:1 departamen municipio using "$base_out\Temporary\Codigos_Dane_areacount_ntit.dta"
rename codane2 codmpio

*** Note need to think right way of declaring kstar

gen ano=`i'
save "$base_out\Temporary\count_disagg_`i'.dta", replace
}


qui forval i=2004/2013 {
append using "$base_out\Temporary\count_disagg_`i'.dta"
}

gen illegal=0
replace illegal=1 if legal=="FALSE"
replace area=area/10^6

/*
preserve

qui forval i=2004/2014 {
twoway (hist area if ano==`i' & legal=="TRUE" & area<1, ysc(r(0 2000)) freq start(0) width(0.01) color(green)) ///
(hist area if ano==`i' & legal=="FALSE" & area<1, ysc(r(0 2000)) freq start(0) width(0.01)), ///
 legend(order(1 "Legal" 2 "Illegal" ))
 graph export "$latexslides/histsize_`i'.pdf", as(pdf) replace
 }
 restore
*/

keep if illegal==1
collapse (max) area , by(codmpio ano)
rename area kstar_illegal
save "$base_out\Temporary\kstar_illegal.dta", replace

*Import TP, FP by municipality

qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/performance_by_codane_Rf10mlecoinputed.csv", clear 
drop v1
qui foreach x in * {
destring `x', force replace
}
qui gen minenotitulo=mine-minetitulo
qui gen nominenotitulo=nomine-nominetitulo
qui gen tprnotitulo=(tp-tptitulo)/(mine-minetitulo)
qui gen fprnotitulo=(fp-fptitulo)/(nomine-nominetitulo)
label var tpr "True positive rate (TPR)"
label var tprtitulo "TPR legal pixels"
label var tprnotitulo "TPR illegal pixels"
label var fpr "False positive rate (FPR)"
label var fprtitulo "FPR legal pixels"
label var fprnotitulo "FPR illegal pixels"

qui foreach x of varlist tpr* fpr*  {
replace `x'=`x'*100
}
qui foreach x in * {
rename `x' tr_`x'
}
rename tr_codane codmpio
qui gen ano=2010
saveold "$base_out/Temporary/training_tpfp.dta", replace

*Import dynamic predictions new area mined

qui forval i=2005/2014 {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/`i'_final_summary.csv", varnames(1) clear
rename codane2 codmpio

qui foreach x of varlist codmpio dyn_* {
destring `x', force replace
}

qui gen legalm_pastm=dyn_a/10^6
label var legalm_pastm "Area legally mined that was mined in the past (km2)" 

qui gen legalm_pastNm=dyn_b/10^6
label var legalm_pastNm "Area legally mined that was not mined in the past (km2)" 

qui gen legalm_pastilm=dyn_c/10^6
label var legalm_pastilm "Area legally mined that was illegally mined in the past (km2)" 
  
qui gen illegalm_pastm=dyn_d/10^6
label var illegalm_pastm "Area illegally mined that was mined in the past (km2)" 

qui gen illegalm_pastNm=dyn_e/10^6
label var illegalm_pastNm "Area illegally mined that was not mined in the past (km2)" 

qui gen illegalm_pastlm=dyn_f/10^6
label var illegalm_pastlm "Area illegally mined that was legally mined in the past (km2)" 

qui gen analyzed_area_dyn=dyn_not_na/10^6
label var analyzed_area_dyn "Analized area both current and previous year (km2)" 


keep codmpio legalm_pastm-analyzed_area_dyn
drop if codmpio==.
gen ano=`i'
saveold "$base_out/Temporary/dynampred`i'.dta", replace

} 

qui forval i=2005/2013 {
append using "$base_out/Temporary/dynampred`i'.dta"
}

saveold "$base_out/Temporary/dynampred.dta", replace

*Import dynamic predictions new area mined

qui forval i=2005/2014 {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/Col_tit2014/`i'_dynampred_endy_summary.csv", varnames(1) clear
rename codane2 codmpio

qui foreach x of varlist codmpio dyn_* {
destring `x', force replace
}

qui gen legalm_pastm=dyn_a/10^6
label var legalm_pastm "Area legally mined that was mined in the past (km2)" 

qui gen legalm_pastNm=dyn_b/10^6
label var legalm_pastNm "Area legally mined that was not mined in the past (km2)" 

qui gen legalm_pastilm=dyn_c/10^6
label var legalm_pastilm "Area legally mined that was illegally mined in the past (km2)" 

qui gen illegalm_pastm=dyn_d/10^6
label var illegalm_pastm "Area illegally mined that was mined in the past (km2)" 

qui gen illegalm_pastNm=dyn_e/10^6
label var illegalm_pastNm "Area illegally mined that was not mined in the past (km2)" 

qui gen illegalm_pastlm=dyn_f/10^6
label var illegalm_pastlm "Area illegally mined that was legally mined in the past (km2)" 

qui gen analyzed_area_dyn=dyn_not_na/10^6
label var analyzed_area_dyn "Analized area both current and previous year (km2)" 


keep codmpio legalm_pastm-analyzed_area_dyn

drop if codmpio==.
gen ano=`i'
saveold "$base_out/Temporary/dynampred`i'_tit2014.dta", replace

} 

qui forval i=2005/2013 {
append using "$base_out/Temporary/dynampred`i'_tit2014.dta"
}

qui gen newprop_illegal_endy= 100*illegalm_pastNm/ analyzed_area_dyn
label var newprop_illegal_endy "New area mined illegaly (2014 titles) as share of municipality area "
gen newpropmined_illegal_endy= 100*illegalm_pastNm/ (illegalm_pastNm+legalm_pastNm)
label var newpropmined_illegal_endy "Share of new municipality mined area mined illegaly (2014 titles)"

keep codmpio ano newprop*

saveold "$base_out/Temporary/dynampred_tit2014.dta", replace


** Distance to th border

qui import delimited "$base_out/border_distances_colombia.csv", clear
qui rename codane2 codmpio
destring codmpio, force replace
drop if codmpio==.
qui gen dist_border_km=borderdistanceclosestm/1000
qui gen distcentroid_border_km=borderdistancecentroidm/1000
qui label var dist_border_km "Distance to the Colombia-Peru border"
qui label var distcentroid_border_km "Distance centroid to the Colombia-Peru border"
keep codmpio dist_border_km distcentroid_border_km
saveold "$base_out/Temporary/border_distances_colombia.dta", replace

qui import delimited "$base_out/border_distances_peru.csv", clear
qui rename codane codmpio
destring codmpio, force replace
drop if codmpio==.
qui gen dist_border_km=borderdistanceclosestm/1000
qui gen distcentroid_border_km=borderdistancecentroidm/1000
qui label var dist_border_km "Distance to the Colombia-Peru border"
qui label var distcentroid_border_km "Distance centroid to the Colombia-Peru border"
keep codmpio dist_border_km distcentroid_border_km
saveold "$base_out/Temporary/border_distances_peru.dta", replace



********* Colombian predictions with non optimal cutoff

forval i=2004/2014 {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/Col_sindisap_cutoff12/muniResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear

*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.
saveold "$base_out/Temporary/results`i'_c12.dta", replace

} 

forval i=2004/2013 {
append using "$base_out/Temporary/results`i'_c12.dta"
} 

gen analizedarea_sqkm=analizedarea/10^6
label var analizedarea_sqkm "Area (sqkm) of pixels analized in prediction"

gen areamuni_sqkm=municipioarea/10^6
label var areamuni_sqkm "Area (km2) of municipality in raster"

gen areaprmined_sqkm=areamines/10^6
label var areaprmined_sqkm "Area (sqkm) predicted as mined"

**The adjustment is based on the analysis of the econometric error
gen areaprmined_adj_sqkm=max(0,(areaprmined_sqkm-analizedarea_sqkm*0.2 )/(0.8 - 0.2 ))
label var areaprmined_sqkm "Area (sqkm) predicted as mined adjusted"

gen titulosarea_sqkm=titulosarea/10^6
label var titulosarea_sqkm "Area (sqkm) of mining titles"

gen titulosareaprnomined_sqkm=areatitulosnomine/10^6
label var titulosareaprnomined_sqkm "Area (sqkm) of mining titles predicted as NOT mined"

gen areaprilegal_sqkm=areaillegalmines/10^6
label var areaprilegal_sqkm "Area (sqkm) predicted as illegaly mined"

gen areaprilegal_adj_sqkm=areaprmined_adj_sqkm*areaprilegal_sqkm/areaprmined_sqkm
label var areaprmined_sqkm "Area (sqkm) predicted as illegaly mined adjusted"

keep codane year *_sqkm

rename codane codmpio
rename year ano

foreach x of varlist *_sqkm {
rename `x' c12_`x'
}

saveold "$base_out/Temporary/panel_prillegal_c12.dta", replace


********* Peruvian predictions with non optimal cutoff

forval i=2004/2014 {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/Peru_sindisap_cutoff12/muniResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear

*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.
saveold "$base_out/Temporary/results`i'_c12_Peru.dta", replace

} 

forval i=2004/2013 {
append using "$base_out/Temporary/results`i'_c12_Peru.dta"
} 

gen analizedarea_sqkm=analizedarea/10^6
label var analizedarea_sqkm "Area (sqkm) of pixels analized in prediction"

gen areamuni_sqkm=municipioarea/10^6
label var areamuni_sqkm "Area (km2) of municipality in raster"

gen areaprmined_sqkm=areamines/10^6
label var areaprmined_sqkm "Area (sqkm) predicted as mined"

**The adjustment is based on the analysis of the econometric error
gen areaprmined_adj_sqkm=max(0,(areaprmined_sqkm-analizedarea_sqkm*0.2 )/(0.8 - 0.2 ))
label var areaprmined_sqkm "Area (sqkm) predicted as mined adjusted"

gen titulosarea_sqkm=titulosarea/10^6
label var titulosarea_sqkm "Area (sqkm) of mining titles"

gen titulosareaprnomined_sqkm=areatitulosnomine/10^6
label var titulosareaprnomined_sqkm "Area (sqkm) of mining titles predicted as NOT mined"

gen areaprilegal_sqkm=areaillegalmines/10^6
label var areaprilegal_sqkm "Area (sqkm) predicted as illegaly mined"

gen areaprilegal_adj_sqkm=areaprmined_adj_sqkm*areaprilegal_sqkm/areaprmined_sqkm
label var areaprmined_sqkm "Area (sqkm) predicted as illegaly mined adjusted"

keep codane year *_sqkm

rename codane codmpio
rename year ano

foreach x of varlist *_sqkm {
rename `x' c12_`x'
}

saveold "$base_out/Temporary/panel_prillegal_c12_Peru.dta", replace

*Import dynamic predictions NEW area mined PERU
qui import delimited "$base_in/Peru/PER_adm3.csv", varnames(1) clear
gen codmpio= id_3+10^5
gen rowidshape=_n
rename name_1 depto
rename name_2 provincia_peru
rename name_3 municipio
keep depto provincia_peru municipio codmpio rowidshape
saveold "$base_out/Temporary/Peru_rowid_codmpio.dta", replace

qui forval i=2005/2014 {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/Peru/`i'_dynampred_summary.csv", varnames(1) clear
rename v1 rowidshape


qui gen legalm_pastm=dyn_a/10^6
label var legalm_pastm "Area legally mined that was mined in the past (km2)" 

qui gen legalm_pastNm=dyn_b/10^6
label var legalm_pastNm "Area legally mined that was not mined in the past (km2)" 

qui gen legalm_pastilm=dyn_c/10^6
label var legalm_pastilm "Area legally mined that was illegally mined in the past (km2)" 

qui gen illegalm_pastm=dyn_d/10^6
label var illegalm_pastm "Area illegally mined that was mined in the past (km2)" 

qui gen illegalm_pastNm=dyn_e/10^6
label var illegalm_pastNm "Area illegally mined that was not mined in the past (km2)" 

qui gen illegalm_pastlm=dyn_f/10^6
label var illegalm_pastlm "Area illegally mined that was legally mined in the past (km2)" 

qui gen analyzed_area_dyn=dyn_not_na/10^6
label var analyzed_area_dyn "Analized area both current and previous year (km2)" 


keep rowidshape legalm_pastm-analyzed_area_dyn
drop if rowidshape==.
gen ano=`i'
saveold "$base_out/Temporary/dynampred`i'_peru.dta", replace

} 

qui forval i=2005/2013 {
append using "$base_out/Temporary/dynampred`i'_peru.dta"
}

merge m:1 rowidshape using "$base_out/Temporary/Peru_rowid_codmpio.dta"
drop if _merge!=3
drop _merge

saveold "$base_out/Temporary/dynampred_peru.dta", replace
*/
***********************************************
***********Illegal mining outside National Parks
****************************************

forval i=2004/2014 {

qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/Peru/muniSinParqResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear


*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.
saveold "$base_out/Temporary/resultsSinParqPeru`i'.dta", replace

} 

forval i=2004/2013 {
append using "$base_out/Temporary/resultsSinParqPeru`i'.dta"
} 

gen analizedareaSP_sqkm=analizedarea/10^6
label var analizedareaSP_sqkm "Area (sqkm) of pixels analized in prediction outside parks"

gen areamuniSP_sqkm=municipioarea/10^6
label var areamuniSP_sqkm "Area of municipality outside parks (km2)"

gen areaprminedSP_sqkm=areamines/10^6
label var areaprminedSP_sqkm "Area (sqkm) predicted as mined outside parks"


gen titulosareaSP_sqkm=titulosarea/10^6
label var titulosareaSP_sqkm "Area (sqkm) of mining titles outside parks"

gen titulosareaprnominedSP_sqkm=areatitulosnomine/10^6
label var titulosareaprnominedSP_sqkm "Area (sqkm) of mining titles predicted as NOT mined outside parks"

gen areaprilegalSP_sqkm=areaillegalmines/10^6
label var areaprilegalSP_sqkm "Area (sqkm) predicted as illegaly mined outside parks"



keep codane year *_sqkm

rename codane codmpio
rename year ano


saveold "$base_out/Temporary/panel_prillegalSP_peru.dta", replace

********************
***** Probability mined instead of dummy mined vs non-mined

qui forval i=2004/2014 {

qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/Peru/muniPrResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear



*These variables are in number pixels and contains same info as area
qui drop sumprmines sumprillegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring areasumprmines, force replace
qui destring areasumprillegalmines , force replace

drop if codane==.
saveold "$base_out/Temporary/results_probperu_`i'.dta", replace

} 



qui forval i=2004/2013 {
append using "$base_out/Temporary/results_probperu_`i'.dta"
 }
qui gen propmined_illegal_prob=100*(areasumprillegalmines/10^6)/(areasumprmines/10^6)

keep codane year propmined_illegal_prob

rename codane codmpio
rename year ano


saveold "$base_out/Temporary/panel_prillegal_prob_peru.dta", replace

*******
***** Peru predicitions
*****

qui forval i=2004/2014 {
qui import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/Peru/muniResults_Rf10onlymlOSMecopot_`i'.csv", varnames(1) clear
*qui import delimited "$base_out/Predicciones/Rf10ml3osm/SindisapPeru/muniResults_Rf10ml3osm_`i'.csv", varnames(1) clear
*These variables are in number pixels and contains same info as area
qui drop mines illegalmines
qui destring codane, force replace
qui destring year, force replace
qui destring analizedarea, force replace
qui destring areamines, force replace
qui destring areaillegalmines , force replace
qui destring municipioarea, force replace
qui destring titulosarea, force replace
qui destring areatitulosnomine , force replace
drop if codane==.
saveold "$base_out/Temporary/results`i'Peru.dta", replace

} 

qui forval i=2004/2013 {
append using "$base_out/Temporary/results`i'Peru.dta"
} 

gen analizedarea_sqkm=analizedarea/10^6
label var analizedarea_sqkm "Area (sqkm) of pixels analized in prediction"

gen areamuni_sqkm=municipioarea/10^6
label var areamuni_sqkm "Area (sqkm) of municipality in raster"

gen areaprmined_sqkm=areamines/10^6
label var areaprmined_sqkm "Area (sqkm) predicted as mined"

gen titulosarea_sqkm=titulosarea/10^6
label var titulosarea_sqkm "Area (sqkm) of mining titles"

gen titulosareaprnomined_sqkm=areatitulosnomine/10^6
label var titulosareaprnomined_sqkm "Area (sqkm) of mining titles predicted as NOT mined"

gen areaprilegal_sqkm=areaillegalmines/10^6
label var areaprilegal_sqkm "Area (sqkm) predicted as illegaly mined"

**The adjustment is based on the analysis of the econometric error
gen areaprmined_adj_sqkm=max(0,(areaprmined_sqkm-analizedarea_sqkm*$prmodel_fpr )/($prmodel_tpr - $prmodel_fpr ))
label var areaprmined_adj_sqkm "Area (sqkm) predicted as mined adjusted"

gen areaprilegal_adj_sqkm=areaprmined_adj_sqkm*areaprilegal_sqkm/areaprmined_sqkm
label var areaprmined_sqkm "Area (sqkm) predicted as illegaly mined adjusted"

keep codane year *_sqkm

rename codane codmpio
rename year ano


qui gen trend = (ano - 2004)/10 
qui gen prop_mined=100*areaprmined_sqkm/analizedarea_sqkm
label var prop_mined "Percentage of analyzed area predicted as mined"

gen prop_illegal= 100*areaprilegal_sqkm/ analizedarea_sqkm
label var prop_illegal "Share of municipality area mined illegaly"

gen propmined_illegal= 100*areaprilegal_sqkm/ areaprmined_sqkm
label var propmined_illegal "Share of municipality mined area mined illegaly"

gen propadj_illegal= 100*areaprilegal_adj_sqkm/ analizedarea_sqkm
label var propadj_illegal "Share of municipality area mined illegaly adjusted"

gen propadjmined_illegal= 100*areaprilegal_adj_sqkm/ areaprmined_adj_sqkm
label var propadjmined_illegal "Share of municipality mined area mined illegaly adjusted"

gen ind_loser=0 
label var ind_loser "Loser"

gen ind_after=1 if ano>2011 & ano!=.
replace ind_after=0 if ano<2012
label var ind_after "After"

qui gen after_x_loser=ind_loser*ind_after
label var after_x_loser "After x Loser"

qui gen prop_illegal_bef= prop_illegal if ind_after==0
qui label var prop_illegal_bef "\% of municipality area mined illegally, 2004-2011"
gen prop_illegal_aft= prop_illegal if ind_after==1
qui label var prop_illegal_aft "\% of municipality area mined illegally, 2012-2014"
qui gen propmined_illegal_bef= propmined_illegal if ind_after==0
qui label var propmined_illegal_bef "\% of mined area mined illegally, 2004-2011"
gen propmined_illegal_aft= propmined_illegal if ind_after==1
qui label var propmined_illegal_aft "\% of mined area mined illegally, 2012-2014"



merge m:1 codmpio using "$base_out/Temporary/border_distances_peru.dta"
drop _merge 

merge 1:1 codmpio ano using "$base_out/Temporary/panel_prillegal_c12_Peru.dta"
drop _merge

merge 1:1 codmpio ano using "$base_out/Temporary/dynampred_peru.dta"
drop _merge

merge 1:1 codmpio ano using "$base_out/Temporary/panel_prillegalSP_peru.dta"
drop _merge

merge 1:1 codmpio ano using "$base_out/Temporary/panel_prillegal_prob_peru.dta"
drop _merge

merge 1:1 codmpio ano using "$base_out/Temporary/panelCovarsPeru.dta"
drop _merge

gen c12_prop_illegal= 100*c12_areaprilegal_sqkm/ c12_analizedarea_sqkm
label var c12_prop_illegal "Share of municipality area mined illegaly (NOC)"

gen c12_propmined_illegal= 100*c12_areaprilegal_sqkm/ c12_areaprmined_sqkm
label var c12_propmined_illegal "Share of municipality mined area mined illegaly (NOC)"

qui gen newprop_illegal= 100*illegalm_pastNm/ analyzed_area_dyn
label var newprop_illegal "New area mined illegaly as share of municipality area "
gen newpropmined_illegal= 100*illegalm_pastNm/ (illegalm_pastNm+legalm_pastNm)
label var newpropmined_illegal "Share of new municipality mined area mined illegaly"

gen propminedSP_illegal= 100*areaprilegalSP_sqkm/ areaprminedSP_sqkm
label var propminedSP_illegal "Share of municipality mined area mined illegaly outside parks"

gen country="Peru"
saveold "$base_out/Temporary/panel_prillegal_Peru.dta", replace



*/

******* Predictions of model trained in unodc data

forval i=2004/2014 {

	if `i'==2012 | `i'==2013 {
		qui import delimited "$base_out/Predicciones/Rf10unodc/muniResults_tit2014Rf10unodc_`i'.csv", varnames(1) clear
	} 
	else {
		qui import delimited "$base_out/Predicciones/Rf10unodc/muniResults_Rf10unodc_`i'.csv", varnames(1) clear
	}

	*These variables are in number pixels and contains same info as area
	qui drop mines illegalmines
	qui destring codane, force replace
	qui destring year, force replace
	qui destring analizedarea, force replace
	qui destring areamines, force replace
	qui destring areaillegalmines , force replace
	qui destring municipioarea, force replace
	qui destring titulosarea, force replace
	qui destring areatitulosnomine , force replace
	drop if codane==.
	saveold "$base_out/Temporary/unodc_results`i'.dta", replace

} 

forval i=2004/2013 {
	append using "$base_out/Temporary/unodc_results`i'.dta"
} 

gen unodc_analizedarea_sqkm=analizedarea/10^6
label var unodc_analizedarea_sqkm "Area (sqkm) of pixels analized in prediction"


gen unodc_areaprmined_sqkm=areamines/10^6
label var unodc_areaprmined_sqkm "Area (sqkm) predicted as mined"

**The adjustment is based on the analysis of the econometric error
gen unodc_areaprmined_adj_sqkm=max(0,(unodc_areaprmined_sqkm-unodc_analizedarea_sqkm*0.21)/(0.76-0.21 ))
label var unodc_areaprmined_adj_sqkm "Area (sqkm) predicted as mined adjusted"


gen unodc_titulosareaprnomined_sqkm=areatitulosnomine/10^6
label var unodc_titulosareaprnomined_sqkm "Area (sqkm) of mining titles predicted as NOT mined UNODC"

gen unodc_areaprilegal_sqkm=areaillegalmines/10^6
label var unodc_areaprilegal_sqkm "Area (sqkm) predicted as illegaly mined UNODC"

gen unodc_areaprilegal_adj_sqkm=unodc_areaprmined_adj_sqkm*unodc_areaprilegal_sqkm/unodc_areaprmined_sqkm
label var unodc_areaprmined_sqkm "Area (sqkm) predicted as illegaly mined UNODC adjusted"


gen unodc_propmined_illegal=unodc_areaprilegal_sqkm/unodc_areaprmined_sqkm

keep codane year *_sqkm unodc_propmined_illegal

rename codane codmpio
rename year ano


saveold "$base_out/Temporary/panel_prillegal_unodc.dta", replace

/*

**********
* 2. Royalties by type of mineral ANH, SIMCO 2004-2012 , SGR
* This used to be code 1. Create royalties by mineral
*
*************
/*
use "$mipath/RawData/PanelCEDE/PANEL CARAC. GENERALES.dta", clear
collapse ano, by(municipio depto codmpio coddepto)
forval i=1/5{
qui replace municipio=subinstr(municipio,"(`i')","",.)
}
qui drop ano
qui replace municipio=subinstr(municipio,"(CD)","",.)
qui replace municipio=subinstr(municipio," ","",.)
qui saveold "$mipath/CreatedData/Codigos_DANE.dta", replace

qui replace municipio=subinstr(municipio,"Ã¡","a",.)
qui replace municipio=subinstr(municipio,"Ã©","e",.)
qui replace municipio=subinstr(municipio,"Ã­","i",.)
qui replace municipio=subinstr(municipio,"Ã³","o",.)
qui replace municipio=subinstr(municipio,"Ãº","u",.)
qui replace depto=subinstr(depto,"Ã¡","a",.)
qui replace depto=subinstr(depto,"Ã©","e",.)
qui replace depto=subinstr(depto,"Ã­","i",.)
qui replace depto=subinstr(depto,"Ã³","o",.)
qui replace depto=subinstr(depto,"Ãº","u",.)
saveold "$mipath/CreatedData/Temporary/Codigos_DANE_sintilde.dta", replace

forval iyear=2004/2011{
qui import excel "$mipath/RawData/ANH/HISTORICO DE REGALÃAS PAGADAS `iyear'.xlsx", sheet("page 1") firstrow clear

if `iyear'==2011{
qui keep BENEFICIARIO AE
qui rename AE TOTALPAGADO`iyear'
}
if `iyear'<=2010 & `iyear'>=2009 {
qui keep BENEFICIARIO AC
qui rename AC TOTALPAGADO`iyear'
}
if `iyear'==2008{
qui keep BENEFICIARIO TOTALPAGADO`iyear'
}
qui saveold "$mipath/CreatedData/Temporary/ANH_`iyear'_p1.dta", replace
local Npage=6
if `iyear'>2009{
local Npage=4
}
forval i=2/`Npage'{
qui import excel "$mipath/RawData/ANH/HISTORICO DE REGALÃAS PAGADAS `iyear'.xlsx", sheet("page `i'") firstrow clear
if (`i'>=4 & `iyear'>=2009) | (`i'>=3 & `iyear'>2009){
qui replace TOTALPAGADO`iyear'=AC if TOTALPAGADO`iyear'==""
}
if `iyear'==2004 & `i'>=2 {
qui rename TOTALPAGADO TOTALPAGADO`iyear'
qui replace TOTALPAGADO`iyear'=S if TOTALPAGADO`iyear'==""
}
qui keep BENEFICIARIO TOTALPAGADO`iyear'
qui saveold "$mipath/CreatedData/Temporary/ANH_`iyear'_p`i'.dta", replace
}

clear
forval i=1/`Npage'{
qui append using "$mipath/CreatedData/Temporary/ANH_`iyear'_p`i'.dta"
}

qui drop if TOTALPAGADO`iyear'=="-"
qui drop if TOTALPAGADO`iyear'==""
qui drop if BENEFICIARIO=="BENEFICIARIO"



qui replace BENEFICIARIO=subinstr(BENEFICIARIO,"PTO.","PUERTO",.)
qui replace BENEFICIARIO=subinstr(BENEFICIARIO,"Ã","E",.)
qui replace BENEFICIARIO=subinstr(BENEFICIARIO,"Ã","a",.)
qui replace BENEFICIARIO=subinstr(BENEFICIARIO,"Ã","i",.)
qui replace BENEFICIARIO=subinstr(BENEFICIARIO,"Ã","o",.)
qui replace TOTALPAGADO`iyear'=subinstr(TOTALPAGADO`iyear',".","",.)
qui rename BENEFICIARIO municipio
qui rename TOTALPAGADO`iyear' regalias
qui destring regalias, force replace
qui drop if municipio==""
qui sum regalias
qui local nobs=r(N)
qui local cdep=municipio[1]
gen depto=""
forval i=1/`nobs'{
qui replace depto="`cdep'" in `i'
if municipio[`i']=="TOTALES" & `i'!=`nobs'{
qui local cdep=municipio[`i'+1]
}
}

qui replace depto="Vichada" if depto=="DEPARTAMENTO VICHADA"
qui replace depto="Cordoba" if depto=="SAN ANTERO"
qui replace depto="Sucre" if depto=="COVEÃAS"

qui drop if municipio=="TOTALES"
qui replace municipio=proper(municipio)
qui replace municipio=subinstr(municipio," ","",.)
qui replace municipio=subinstr(municipio,"Ã","Ã±",.)
qui replace depto=proper(depto)
qui replace depto=subinstr(depto," ","",.)
qui replace depto=subinstr(depto,"Ã","Ã±",.)
qui sort municipio
if `iyear'==2011{
qui replace municipio="Sahagun" if municipio=="SahagunPuebloNuevo" & regalias==101067005
qui replace municipio="PuebloNuevo" if municipio=="SahagunPuebloNuevo"
qui replace regalias=regalias+regalias[_n+1] if municipio=="Sahagun" & municipio[_n+1]=="Sahagun"
qui replace regalias=regalias+regalias[_n+1] if municipio=="PuebloNuevo" & municipio[_n+1]=="PuebloNuevo"
qui drop if municipio=="Sahagun" & municipio[_n-1]=="Sahagun"
qui drop if municipio=="PuebloNuevo" & municipio[_n-1]=="PuebloNuevo"
}
if `iyear'==2010{
qui replace municipio="SanBernardodelViento" if municipio=="SanBernardo" 
}
qui replace municipio="FuentedeOro" if municipio=="FuenteDeOro"
qui replace municipio="PuertoCaicedo" if municipio=="PuertoCaycedo"
qui replace municipio="SanLuisdeGaceno" if municipio=="SanLuisDeGaceno"
qui replace municipio="PazdeAriporo" if municipio=="PazDeAriporo"
qui replace municipio="Cubarral" if municipio=="Cuburral"
qui replace municipio="Vistahermosa" if municipio=="VistaHermosa"
qui replace municipio="MoÃ±itos" if municipio=="MoÃ±Itos"
qui replace municipio="ValledelGuamuez" if municipio=="Guamues"
qui replace municipio="RiodeOro" if municipio=="RioDeOro"
qui replace municipio="BarrancadeUpia" if municipio=="BarrancaDeUpia"
qui replace municipio="SanCarlosdeGuaroa" if municipio=="SanCarlosGuaroa"
qui replace municipio="SanJosedeUre" if municipio=="SanJoseDeUre"
qui replace municipio="SanAndresSotavento" if municipio=="SanAndresDeSotavento"
qui replace municipio="SanJuandeArama" if municipio=="SanJuanDeArama"
qui replace municipio="SabanadeTorres" if municipio=="SabanaTorres"
qui replace municipio="SanLuisdePalenque" if municipio=="SanLuisDePalenque"
qui replace municipio="Tibu" if municipio=="SabanaDeTibu"
qui replace municipio="CastillalaNueva" if municipio=="CastillaLaNueva"
qui replace municipio="PuertoSalgar" if municipio=="Puertosalgar"
qui replace municipio="Tierralta" if municipio=="TierraAlta"
qui replace municipio="Uribe" if municipio=="LaUribe"
qui replace municipio="SanVicentedeChucuri" if municipio=="SanVicente" & depto=="Santander"
qui replace municipio="CoveÃ±as" if municipio=="CoveÃ±As"
qui replace depto="La Guajira" if depto=="Guajira"
qui replace depto="Norte de Santander" if depto=="NorteDeSantander"
qui replace depto="NariÃ±o" if depto=="NariÃ±O"
qui replace municipio="ElCarmendeChucuri" if municipio=="ElCarmen" & depto=="Santander"
qui replace municipio="Buenavista" if municipio=="Buenavista(S)"
qui replace municipio="SanJuandeBetulia" if municipio=="SanJuanBetulia"
qui replace municipio="SantiagodeTolu" if municipio=="SantiagoDeTolu"
qui replace municipio="SanLuisdeSince" if municipio=="Since"
qui replace depto="Sucre" if depto=="SantiagoDeTolu"
qui replace depto="Bolivar" if municipio=="Cartagena"
qui replace depto="NariÃ±o" if municipio=="Tumaco"
qui replace depto="Archipielago de San Andres" if municipio=="SanAndresIslas"
qui replace municipio="SanAndres" if municipio=="SanAndresIslas"
qui replace depto="Santander" if municipio=="PuertoWilches-Santander"
qui replace municipio="PuertoWilches" if municipio=="PuertoWilches-Santander"
qui replace depto="Magdalena" if municipio=="SitioNuevo"
qui replace municipio="Sitionuevo" if municipio=="SitioNuevo"
qui replace depto="Antioquia" if municipio=="Yondo-Antioquia"
qui replace municipio="Yondo" if municipio=="Yondo-Antioquia"
qui replace depto="Bolivar" if municipio=="SanPablo-Bolivar"
qui replace municipio="SanPablo" if municipio=="SanPablo-Bolivar"
qui replace depto="Bolivar" if municipio=="Cantagallo-Bolivar"
qui replace municipio="Cantagallo" if municipio=="Cantagallo-Bolivar"
qui replace municipio="SanBernardodelViento" if municipio=="SanBernardo" & depto=="Cordoba"
qui replace depto="Atlantico" if municipio=="Barranquilla"
qui replace municipio="SanAndresdeTumaco" if municipio=="Tumaco"
qui replace depto="Santander" if municipio=="Barrancabermeja"
qui replace municipio="CienagadeOro" if municipio=="CienagaDeOro"
qui replace depto="Valle del Cauca" if municipio=="Buenaventura"
qui replace municipio="ElCarmendeChucuri" if municipio=="Elcarmen" & depto=="Santander"
qui replace depto="Antioquia" if municipio=="Turbo"
qui replace municipio="SanAndresSotavento" if municipio=="SanAndresDeStoavento"
qui replace depto="Antioquia" if municipio=="Bagre"
qui replace municipio="ElBagre" if municipio=="Bagre"
qui replace municipio="Simacota" if municipio=="Simatoca"
qui collapse (sum) regalias, by( municipio depto)
qui merge 1:1 municipio depto using "$mipath/CreatedData/Temporary/Codigos_DANE_sintilde.dta"
qui drop if _merge==2

qui replace codmpio=20000 if municipio=="Cesar"
qui replace codmpio=73000 if municipio=="Tolima"
qui replace codmpio=54000 if municipio=="NorteDeSantander"
qui replace codmpio=52000 if municipio=="NariÃ±O"
qui replace codmpio=41000 if municipio=="Huila"
qui replace codmpio=85000 if municipio=="Casanare"
qui replace codmpio=25000 if municipio=="Cundinamarca"
qui replace codmpio=68000 if municipio=="Santander"
qui replace codmpio=44000 if municipio=="Guajira"
qui replace codmpio=99000 if municipio=="DepartamentoVichada"
qui replace codmpio=86000 if municipio=="Putumayo"
qui replace codmpio=50000 if municipio=="Meta"
qui replace codmpio=5000 if municipio=="Antioquia"
qui replace codmpio=19000 if municipio=="Cauca"
qui replace codmpio=13000 if municipio=="Bolivar" & depto=="Bolivar"
qui replace codmpio=23000 if municipio=="Cordoba" & depto=="Cordoba"
qui replace codmpio=70000 if municipio=="DepartamentoDeSucre"
if `iyear'==2011{
qui replace codmpio=81000 if municipio=="Arauca" & regalias>279*10^9
qui replace codmpio=13000 if municipio=="Bolivar" & regalias>42*10^9
qui drop if codmpio==19100
qui replace codmpio=68101 if municipio=="Bolivar" & regalias<28*10^6
qui replace codmpio=20770 if municipio=="SanMartin" & regalias>12*10^9
qui replace codmpio=50689 if municipio=="SanMartin" & regalias<4*10^9
}
if `iyear'==2010{
qui drop if codmpio==76606 | codmpio==5615 | codmpio==68176 | codmpio==20443 |codmpio==18592
qui replace codmpio=81000 if municipio=="Arauca" & regalias>214*10^9
}

qui keep codmpio regalias
qui drop if codmpio==.
qui gen ano=`iyear'
qui gen mineral="Hidrocarburos"
qui saveold "$mipath/CreatedData/Temporary/ANH_`iyear'.dta", replace

}
**Need to do this for each year ANH
clear
forval iyear=2004/2011{
append using "$mipath/CreatedData/Temporary/ANH_`iyear'.dta"
}
saveold "$mipath/CreatedData/Temporary/Hidrocarburos2004_2012.dta", replace

*The original file Regalias_caliza2004_2012.xls was not suitable for Stata, I copy and transposed by hand
import excel "$mipath/RawData/SIMCO/Regalias_caliza2004_2012_forStata.xlsx", sheet("Sheet1") firstrow clear
drop A B C F
rename D depto
rename E municipio
replace municipio=subinstr(municipio,"Ã","Ã©",.)
replace municipio=subinstr(municipio,"Ã","Ã¡",.)

replace municipio=substr(municipio, 1, 1)+ lower(substr(municipio, 2, .))

rename G y2005
rename H y2006
rename I y2007
rename J y2008
rename K y2009
rename L y2010
rename M y2011
rename N y2012
merge 1:m municipio using "$mipath/CreatedData/Codigos_DANE.dta"
drop if _merge==2
replace codmpio=5000 if municipio=="Antioquia"
replace codmpio=68000 if municipio=="Santander"
replace codmpio=76000 if municipio=="Valle del cauca"
drop if municipio=="Varios"
reshape long y, i(codmpio) j(ano)
rename y regalias
replace regalias=subinstr(regalias,".","",.)
destring regalias, force replace
keep codmpio ano regalias
gen mineral="Caliza"
saveold "$mipath/CreatedData/Temporary/Caliza2004_2012.dta", replace

clear
generate codmpio = .
set obs 1
replace codmpio = 76109 in 1
set obs 2
replace codmpio = 76000 in 2
generate var2 = 1316352 in 1
replace var2 = 1645441 in 2
rename var2 regalias
gen ano=2005
gen mineral="Manganeso"
saveold "$mipath/CreatedData/Temporary/Manganeso2004_2012.dta", replace

*The original file Regalias_niquel1990_2012.xls was not suitable for Stata, I copy and transposed by hand
import excel "$mipath/RawData/SIMCO/Regalias_niquel1990_2012_forStata.xlsx", sheet("Sheet1") firstrow clear
drop A B C 
rename D depto
rename E municipio
replace municipio=subinstr(municipio,"Ã","Ã©",.)
replace municipio=subinstr(municipio,"Ã","Ã¡",.)
replace municipio=subinstr(municipio,"Ã","Ã­",.)
replace municipio=subinstr(municipio,"Ã","Ã³",.)

replace municipio=substr(municipio, 1, 1)+ lower(substr(municipio, 2, .))
replace municipio=proper(municipio)
replace municipio=subinstr(municipio," ","",.)
rename F y1990
rename G y1991
rename H y1992
rename I y1993
rename J y1994
rename K y1995
rename L y1996
rename M y1997
rename N y1998
rename O y1999
rename P y2000
rename Q y2001
rename R y2002
rename S y2003
rename T y2004
rename U y2005
rename V y2006
rename W y2007
rename X y2008
rename Y y2009
rename Z y2010
rename AA y2011
rename AB y2012

replace municipio="MontelÃ­bano" if municipio=="MontelÃ­Bano"

merge 1:m municipio using "$mipath/CreatedData/Codigos_DANE.dta"
drop if _merge==2
replace codmpio=23000 if municipio=="CÃ³Rdoba"
drop if municipio=="Bellavista" & codmpio!=23079
drop if municipio=="Varios"
reshape long y, i(codmpio) j(ano)
rename y regalias
replace regalias=subinstr(regalias,".","",.)
destring regalias, force replace
keep codmpio ano regalias
gen mineral="Niquel"
saveold "$mipath/CreatedData/Temporary/Niquel2004_2012.dta", replace

*The original file Regalias_sal2004_2012.xls was not suitable for Stata, I copy and transposed by hand
import excel "$mipath/RawData/SIMCO/Regalias_sal2004_2012_forStata.xlsx", sheet("Sheet1") firstrow clear
drop A B C F G H
rename D depto
rename E municipio
replace municipio=subinstr(municipio,"Ã","Ã©",.)
replace municipio=subinstr(municipio,"Ã","Ã¡",.)
replace municipio=subinstr(municipio,"Ã","Ã­",.)
replace municipio=subinstr(municipio,"Ã","Ã³",.)
replace municipio=substr(municipio, 1, 1)+ lower(substr(municipio, 2, .))
replace municipio=proper(municipio)
replace municipio=subinstr(municipio," ","",.)

rename I y2007
rename J y2008
rename K y2009
rename L y2010
rename M y2011
rename N y2012
replace municipio="NemocÃ³n" if municipio=="NemocÃ³N"
merge 1:m municipio using "$mipath/CreatedData/Codigos_DANE.dta"
drop if _merge==2
replace codmpio=13000 if municipio=="BolÃ­Var"
replace codmpio=25000 if municipio=="Cundinamarca"
replace codmpio=50000 if municipio=="Meta"
drop if municipio=="Varios"
drop if municipio=="Restrepo" & codmpio!=50606
reshape long y, i(codmpio) j(ano)
rename y regalias
replace regalias=subinstr(regalias,".","",.)
destring regalias, force replace
keep codmpio ano regalias
gen mineral="Sal"
saveold "$mipath/CreatedData/Temporary/Sal2004_2012.dta", replace


*The original file Regalias_yeso2004_2012.xls was not suitable for Stata, I copy and transposed by hand
import excel "$mipath/RawData/SIMCO/Regalias_yeso2004_2012_forStata.xlsx", sheet("Sheet1") firstrow clear
drop A B C F G H
rename D depto
rename E municipio
replace municipio=subinstr(municipio,"Ã","Ã©",.)
replace municipio=subinstr(municipio,"Ã","Ã¡",.)
replace municipio=subinstr(municipio,"Ã","Ã­",.)
replace municipio=subinstr(municipio,"Ã","Ã³",.)
replace municipio=substr(municipio, 1, 1)+ lower(substr(municipio, 2, .))
replace municipio=proper(municipio)
replace municipio=subinstr(municipio," ","",.)

rename I y2007
rename J y2008
rename K y2009
rename L y2010
rename M y2011
rename N y2012
replace municipio="ValledeSanJuan" if municipio=="ValleDeSanJuan"
merge 1:m municipio using "$mipath/CreatedData/Codigos_DANE.dta"
drop if _merge==2
replace codmpio=15000 if municipio=="BoyacÃ¡"
replace codmpio=73000 if municipio=="Tolima"
replace codmpio=68000 if municipio=="Santander"
drop if municipio=="Varios"
drop if municipio=="Villanueva" & codmpio!=68872
reshape long y, i(codmpio) j(ano)
rename y regalias
replace regalias=subinstr(regalias,".","",.)
destring regalias, force replace
keep codmpio ano regalias
gen mineral="Yeso"
saveold "$mipath/CreatedData/Temporary/Yeso2004_2012.dta", replace
*/

local minerals1 "azufre carbon hierro metalespreciosos piedraspreciosos"
foreach imineral of local minerals1 {
import delimited "$mipath/CreatedData/Regalias2004_2012//`imineral'20042012.csv", clear
rename `imineral'mpio regalias
keep codmpio ano regalias
gen mineral="`imineral'"
destring regalias, force replace
saveold "$mipath/CreatedData/Temporary//`imineral'2004_2012.dta", replace
}

**
**
**
*Finally put together the file of each mineral
clear
local minerals "azufre carbon hierro metalespreciosos piedraspreciosos Hidrocarburos Caliza Manganeso Niquel Sal Yeso"
foreach imineral of local minerals {

append using "$mipath/CreatedData/Temporary//`imineral'2004_2012.dta"
}

saveold "$mipath/CreatedData/Regalis_mineral_muni.dta", replace

**Munis that produced certain mineral
collapse (sum) regalias, by(codmpio mineral)
local minerals "azufre carbon hierro metalespreciosos piedraspreciosos Hidrocarburos Caliza Manganeso Niquel Sal Yeso"
foreach imineral of local minerals {
qui gen ind_`imineral'_ever=0
replace ind_`imineral'_ever=1 if regalias!=. & regalias!=0 & strpos(mineral,"`imineral'")
}

collapse (max) ind*, by(codmpio)

label var ind_azufre_ever "Royalties from sulfur"
label var ind_carbon_ever "Royalties from coal"
label var ind_hierro_ever "Royalties from iron"
label var ind_metalespreciosos_ever "Royalties from precious metals"
label var ind_piedraspreciosos_ever "Royalties from gems"
label var ind_Hidrocarburos_ever "Royalties from oil-gas"
label var ind_Caliza_ever "Royalties from limestone"
label var ind_Manganeso_ever "Royalties from Manganesum"
label var ind_Niquel_ever "Royalties from Nickel"
label var ind_Sal_ever "Royalties from salt"
label var ind_Yeso_ever "Royalties from plaster"

saveold "$mipath/CreatedData/Temporary/mineral_ever_2004_2012.dta", replace

***************************
*****************************

*I need to create the share of royalties for each muni in the base year
*Because otherwise the share changes with the price of minerals and with production

import delimited "$base_out/munimineralpotentialpedacitos.csv", varnames(1) clear 
rename codane2 codmpio
rename type mineral
replace mineral="carbon" if mineral=="Coal"
replace mineral="hierro" if mineral=="Iron"
replace mineral="Niquel" if mineral=="Nickel"
replace mineral="Manganeso" if mineral=="Magnesium"
replace mineral="Oro" if mineral=="Gold"
replace mineral="Cobre" if mineral=="Copper"
replace mineral="Platino" if mineral=="Platinum"
collapse (sum) area, by(codmpio mineral)
bys codmpio: egen double TotalAreaMinerPot=total(area)
gen Share=area/TotalAreaMinerPot

preserve
**Define open pit minerals
gen Shareop=Share if mineral!="carbon"
collapse (sum) Shareop, by(codmpio)
gen satelite_minerpot=1 if  Shareop>0.5 & Shareop!=.
replace satelite_minerpot=0 if  Shareop<=0.5 & Shareop!=.
keep codmpio satelite_minerpot

saveold "$mipath/CreatedData/Temporary/muni_detect_satelite.dta", replace
restore

preserve
collapse (sum) area, by(codmpio)
** The reason for not creating a continuous measure is that
*pedacitos is double counting areas with two minerals underground
qui gen ind_minerpot=0
qui replace ind_minerpot=1 if area>0 & area!=.
keep codmpio ind_minerpot
saveold "$base_out/Temporary/ind_minerpot.dta", replace
restore
keep codmpio mineral Share 
gen ano=.

saveold "$base_out/Temporary/Share_mineral_muni.dta", replace
forval i=2004/2015 {
qui replace ano=`i'
qui saveold "$base_out/Temporary/Share_mineral_muni`i'.dta", replace
}

forval i=2004/2014 {
qui append using "$base_out/Temporary/Share_mineral_muni`i'.dta"
}

saveold "$base_out/Temporary/Share_mineral_muni_forindex_SinPetroleo.dta", replace

** This is the old way using royalties
use "$mipath/CreatedData/Regalis_mineral_muni.dta", clear
drop if ano>=2011 /*keep only years pre-reform */



collapse (sum) regalias,by(codmpio mineral)
bys codmpio: egen double TotalRegalias=total(regalias)
gen Share=regalias/TotalRegalias
keep codmpio mineral Share
gen ano=.

saveold "$base_out/Temporary/Share_regalias_muni.dta", replace
forval i=2004/2015 {
qui replace ano=`i'
qui saveold "$base_out/Temporary/Share_regalias_muni`i'.dta", replace
}

forval i=2004/2014 {
qui append using "$base_out/Temporary/Share_regalias_muni`i'.dta"
}

saveold "$base_out/Temporary/Share_regalias_muni_forindex.dta", replace

*****************
** Import production files
*******

**First lets do some cleaning
import delim "$base_in/DIVIPOLA_20130930", clear
drop if tipo!="CM" & tipo!="CD"
drop codigodepartamento codigocentropoblado nombrecentropoblado tipo
rename codigomunicipio codmpio
rename nombredepartamento  departamento
rename nombremunicipio municipio
*Arreglo las variables de departamento y municipio
qui foreach x of varlist departamento municipio {
	local unos "Ã¡ Ã© Ã­ Ã³ Ãº Ã± Ã Ã Ã Ã Ã Ã Ã¼ Ã"
	local doss "a e i o u n a e i o u n u u"
	local n=wordcount("`unos'")
forvalue i = 1/`n' {   // Loop to replace accents from Spanish
	local a : word `i' of `unos'
	local b : word `i' of `doss'
	replace `x'=subinstr(`x',"`a'","`b'",.)
}
}
qui replace departamento=subinstr(departamento,"Â ","",.)
qui replace municipio=subinstr(municipio,"Â ","",.)
qui replace departamento=itrim(departamento)
qui replace municipio=itrim(municipio)
qui replace departamento=ltrim(departamento)
qui replace municipio=ltrim(municipio)
qui replace departamento=rtrim(departamento)
replace municipio=rtrim(municipio)
replace departamento=lower(departamento)
replace municipio=lower(municipio)

/*algunos cambios a manos */
replace municipio="san pedro" if municipio=="san pedro de uraba" & departamento=="antioquia"
replace municipio="san vicente" if municipio=="san vicente ferrer" & departamento=="antioquia"
replace municipio="santafe de antioquia" if municipio=="santa fe de antioquia" & departamento=="antioquia"
replace municipio="san pedro" if municipio=="san pedro de uraba" & departamento=="antioquia"
replace municipio="lopez" if municipio=="lopez de micay" & departamento=="cauca"
replace municipio="armero" if municipio=="armero guayabal" & departamento=="tolima"

saveold "$base_out/Divipola_Clean.dta", replace
clear


***GOLD ******
************

import excel "$base_in/SIMCO/ProduccionOro_Anual.xlsx", sheet("ProduccionOro_Anual") clear
drop in 1/4 /*Drop first 4 lines */
egen TempMissing=rowmiss(A-O)
drop if TempMissing==15
drop in l
drop TempMissing
egen TempMissing=rowmiss(B-O)
 
*Arregla el problema de la primera observacion despues de los departamentos.
foreach x of varlist B-O {
	replace `x'=`x'[_n-1] if TempMissing==14
	replace `x'=subinstr(`x',".","",.)
	replace `x'=subinstr(`x',",",".",.)
	destring `x', force replace
}

*Creo la variable con el nombre de los departamentos
gen departamento=A if TempMissing[_n+1]==14
replace departamento=departamento[_n-1] if departamento==""
drop if TempMissing[_n+1]==14
rename A municipio
order departamento municipio

*Arreglo las variables de departamento y municipio
foreach x of varlist departamento municipio {
	local unos "Ã¡ Ã© Ã­ Ã³ Ãº Ã± Ã Ã Ã Ã Ã Ã Ã¼ Ã"
	local doss "a e i o u n a e i o u n u u"
	local n=wordcount("`unos'")
forvalue i = 1/`n' {   // Loop to replace accents from Spanish
	local a : word `i' of `unos'
	local b : word `i' of `doss'
	replace `x'=subinstr(`x',"`a'","`b'",.)
}
}
replace departamento=subinstr(departamento,"Â ","",.)
replace municipio=subinstr(municipio,"Â ","",.)
replace departamento=itrim(departamento)
replace municipio=itrim(municipio)
replace departamento=ltrim(departamento)
replace municipio=ltrim(municipio)
replace departamento=rtrim(departamento)
replace municipio=rtrim(municipio)
replace departamento=lower(departamento)
replace municipio=lower(municipio)
drop TempMissing

rename B y2001
rename C y2002
rename D y2003
rename E y2004
rename F y2005
rename G y2006
rename H y2007
rename I y2008
rename J y2009
rename K y2010
rename L y2011
rename M y2012
rename N y2013
rename O y2014
**Merge not workinggggg
merge m:1 departamento municipio using "$base_out/Divipola_Clean.dta"
drop if _merge==2
drop _merge

reshape long y, i(codmpio municipio departamento) j(ano)
rename y prod_oro
replace prod_oro=0 if prod_oro==.
saveold "$base_out/ProduccionOro_Anual.dta", replace



***COAL ******
************

import excel "$base_in/SIMCO/ProduccionCarbon_Anual.xlsx", sheet("ProduccionCarbon_Anual") clear
drop in 1/4 /*Drop first 4 lines */
egen TempMissing=rowmiss(A-K)
drop if TempMissing==11
drop in l
drop TempMissing
egen TempMissing=rowmiss(B-K)
 
*Arregla el problema de la primera observacion despues de los departamentos.
foreach x of varlist B-K {
	replace `x'=`x'[_n-1] if TempMissing==10
	replace `x'=subinstr(`x',".","",.)
	replace `x'=subinstr(`x',",",".",.)
	destring `x', force replace
}

*Creo la variable con el nombre de los departamentos
gen departamento=A if TempMissing[_n+1]==10
replace departamento=departamento[_n-1] if departamento==""
drop if TempMissing[_n+1]==10
rename A municipio
order departamento municipio

*Arreglo las variables de departamento y municipio
foreach x of varlist departamento municipio {
	local unos "Ã¡ Ã© Ã­ Ã³ Ãº Ã± Ã Ã Ã Ã Ã Ã Ã¼ Ã"
	local doss "a e i o u n a e i o u n u u"
	local n=wordcount("`unos'")
forvalue i = 1/`n' {   // Loop to replace accents from Spanish
	local a : word `i' of `unos'
	local b : word `i' of `doss'
	replace `x'=subinstr(`x',"`a'","`b'",.)
}
}
replace departamento=subinstr(departamento,"Â ","",.)
replace municipio=subinstr(municipio,"Â ","",.)
replace departamento=itrim(departamento)
replace municipio=itrim(municipio)
replace departamento=ltrim(departamento)
replace municipio=ltrim(municipio)
replace departamento=rtrim(departamento)
replace municipio=rtrim(municipio)
replace departamento=lower(departamento)
replace municipio=lower(municipio)
drop TempMissing

rename B y2006
rename C y2007
rename D y2008
rename E y2009
rename F y2010
rename G y2011
rename H y2012
rename I y2013
rename J y2014
rename K y2015
**Merge not workinggggg
merge m:1 departamento municipio using "$base_out/Divipola_Clean.dta"
drop if _merge==2
drop _merge

reshape long y, i(codmpio municipio departamento) j(ano)
rename y prod_carbon
replace prod_carbon=0 if prod_carbon==.
saveold "$base_out/ProduccionCarbon_Anual.dta", replace


***SILVER ******
************

import excel "$base_in/SIMCO/ProduccionPlata_Anual.xlsx", sheet("ProduccionPlata_Anual") clear
drop in 1/4 /*Drop first 4 lines */
egen TempMissing=rowmiss(A-M)
drop if TempMissing==13
drop in l
drop TempMissing
egen TempMissing=rowmiss(B-M)
 
*Arregla el problema de la primera observacion despues de los departamentos.
foreach x of varlist B-M {
	qui replace `x'=`x'[_n-1] if TempMissing==12
	qui replace `x'=subinstr(`x',".","",.)
	qui replace `x'=subinstr(`x',",",".",.)
	qui destring `x', force replace
}

*Creo la variable con el nombre de los departamentos
gen departamento=A if TempMissing[_n+1]==12
replace departamento=departamento[_n-1] if departamento==""
drop if TempMissing[_n+1]==12
rename A municipio
order departamento municipio

*Arreglo las variables de departamento y municipio
foreach x of varlist departamento municipio {
	local unos "Ã¡ Ã© Ã­ Ã³ Ãº Ã± Ã Ã Ã Ã Ã Ã Ã¼ Ã"
	local doss "a e i o u n a e i o u n u u"
	local n=wordcount("`unos'")
forvalue i = 1/`n' {   // Loop to replace accents from Spanish
	local a : word `i' of `unos'
	local b : word `i' of `doss'
	qui replace `x'=subinstr(`x',"`a'","`b'",.)
}
}
replace departamento=subinstr(departamento,"Â ","",.)
replace municipio=subinstr(municipio,"Â ","",.)
replace departamento=itrim(departamento)
replace municipio=itrim(municipio)
replace departamento=ltrim(departamento)
replace municipio=ltrim(municipio)
replace departamento=rtrim(departamento)
replace municipio=rtrim(municipio)
replace departamento=lower(departamento)
replace municipio=lower(municipio)
drop TempMissing

rename B y2004
rename C y2005
rename D y2006
rename E y2007
rename F y2008
rename G y2009
rename H y2010
rename I y2011
rename J y2012
rename K y2013
rename L y2014
rename M y2015
**Merge not workinggggg
merge m:1 departamento municipio using "$base_out/Divipola_Clean.dta"
drop if _merge==2
drop _merge

qui reshape long y, i(codmpio municipio departamento) j(ano)
rename y prod_plata
replace prod_plata=0 if prod_plata==.
saveold "$base_out/ProduccionPlata_Anual.dta", replace


***SAL ******
************

import excel "$base_in/SIMCO/ProduccionSal_Anual.xlsx", sheet("ProduccionSal_Anual") clear
drop in 1/4 /*Drop first 4 lines */
egen TempMissing=rowmiss(A-M)
drop if TempMissing==13
drop in l
drop TempMissing
egen TempMissing=rowmiss(B-M)
 
*Arregla el problema de la primera observacion despues de los departamentos.
foreach x of varlist B-M {
	qui replace `x'=`x'[_n-1] if TempMissing==12
	qui replace `x'=subinstr(`x',".","",.)
	qui replace `x'=subinstr(`x',",",".",.)
	qui destring `x', force replace
}

*Creo la variable con el nombre de los departamentos
gen departamento=A if TempMissing[_n+1]==12
replace departamento=departamento[_n-1] if departamento==""
drop if TempMissing[_n+1]==12
rename A municipio
order departamento municipio

*Arreglo las variables de departamento y municipio
foreach x of varlist departamento municipio {
	local unos "Ã¡ Ã© Ã­ Ã³ Ãº Ã± Ã Ã Ã Ã Ã Ã Ã¼ Ã"
	local doss "a e i o u n a e i o u n u u"
	local n=wordcount("`unos'")
forvalue i = 1/`n' {   // Loop to replace accents from Spanish
	local a : word `i' of `unos'
	local b : word `i' of `doss'
	replace `x'=subinstr(`x',"`a'","`b'",.)
}
}
replace departamento=subinstr(departamento,"Â ","",.)
replace municipio=subinstr(municipio,"Â ","",.)
replace departamento=itrim(departamento)
replace municipio=itrim(municipio)
replace departamento=ltrim(departamento)
replace municipio=ltrim(municipio)
replace departamento=rtrim(departamento)
replace municipio=rtrim(municipio)
replace departamento=lower(departamento)
replace municipio=lower(municipio)
drop TempMissing

rename B y2004
rename C y2005
rename D y2006
rename E y2007
rename F y2008
rename G y2009
rename H y2010
rename I y2011
rename J y2012
rename K y2013
rename L y2014
rename M y2015
**Merge not workinggggg
merge m:1 departamento municipio using "$base_out/Divipola_Clean.dta"
drop if _merge==2
drop _merge

reshape long y, i(codmpio municipio departamento) j(ano)
rename y prod_sal
replace prod_sal=0 if prod_sal==.
saveold "$base_out/ProduccionSal_Anual.dta", replace




***PLATINO ******
************

import excel "$base_in/SIMCO/ProduccionPlatino_Anual.xlsx", sheet("ProduccionPlatino_Anual") clear
drop in 1/4 /*Drop first 4 lines */
egen TempMissing=rowmiss(A-M)
drop if TempMissing==13
drop in l
drop in l
drop TempMissing
egen TempMissing=rowmiss(B-M)
 
*Arregla el problema de la primera observacion despues de los departamentos.
qui foreach x of varlist B-M {
	replace `x'=`x'[_n-1] if TempMissing==12
	replace `x'=subinstr(`x',".","",.)
	replace `x'=subinstr(`x',",",".",.)
	destring `x', force replace
}

*Creo la variable con el nombre de los departamentos
gen departamento=A if TempMissing[_n+1]==12
replace departamento=departamento[_n-1] if departamento==""
drop if TempMissing[_n+1]==12
rename A municipio
order departamento municipio

*Arreglo las variables de departamento y municipio
foreach x of varlist departamento municipio {
	local unos "Ã¡ Ã© Ã­ Ã³ Ãº Ã± Ã Ã Ã Ã Ã Ã Ã¼ Ã"
	local doss "a e i o u n a e i o u n u u"
	local n=wordcount("`unos'")
forvalue i = 1/`n' {   // Loop to replace accents from Spanish
	local a : word `i' of `unos'
	local b : word `i' of `doss'
	replace `x'=subinstr(`x',"`a'","`b'",.)
}
}
replace departamento=subinstr(departamento,"Â ","",.)
replace municipio=subinstr(municipio,"Â ","",.)
replace departamento=itrim(departamento)
replace municipio=itrim(municipio)
replace departamento=ltrim(departamento)
replace municipio=ltrim(municipio)
replace departamento=rtrim(departamento)
replace municipio=rtrim(municipio)
replace departamento=lower(departamento)
replace municipio=lower(municipio)
drop TempMissing

rename B y2014
rename C y2015
rename D y2004
rename E y2005
rename F y2006
rename G y2007
rename H y2008
rename I y2009
rename J y2010
rename K y2011
rename L y2012
rename M y2013

**Merge not workinggggg
merge m:1 departamento municipio using "$base_out/Divipola_Clean.dta"
drop if _merge==2
drop _merge

reshape long y, i(codmpio municipio departamento) j(ano)
rename y prod_platino
replace prod_platino=0 if prod_platino==.
saveold "$base_out/ProduccionPlatino_Anual.dta", replace


*****
* PETROLEO Y GAS
**************
/*
local ff "gas petroleo"
foreach imineral of local ff  {
forval iano=2004/2014{
if "`imineral'"=="gas" {

if `iano'==2004 {
import excel "$base_in\ACIPET\Gas `iano'.xlsx", sheet("GAS`iano'") cellrange(A3:D166) firstrow clear
}
if `iano'==2005 {
import excel "$base_in\ACIPET\Gas `iano'.xlsx", sheet("GAS`iano'") cellrange(A3:D176) firstrow clear
}
if `iano'==2006{
import excel "$base_in\ACIPET\gas `iano'.xlsx", sheet("GAS `iano'") cellrange(A3:D195) firstrow clear
rename Municipio MUNICIPIO
}
if `iano'==2007 {
import excel "$base_in\ACIPET\GAS `iano'.xls", sheet("GAS `iano'") cellrange(A3:D208) firstrow clear
rename Municipio MUNICIPIO
}
if `iano'==2008 {
import excel "$base_in\ACIPET\Gas `iano'.xlsx", sheet("GAS") cellrange(A3:D223) firstrow clear
}
if `iano'==2009 {
import excel "$base_in\ACIPET\gas `iano'.xlsx", sheet("GAS 2009") cellrange(A3:D226) firstrow clear
rename TOTALGAS2009 TOTAL
}
if `iano'==2010 {
import excel "$base_in\ACIPET\Gas `iano'.xlsx", sheet("GAS `iano'") cellrange(A3:D339) firstrow clear
rename SUMATOTALPRODUCCION2010 TOTAL
}
if `iano'==2011 {
import excel "$base_in\ACIPET\Gas `iano'.xlsx", sheet("GAS `iano'") cellrange(A3:D359) firstrow clear
rename SUMATOTALPRODUCCION2011 TOTAL
}
if `iano'==2012 {
import excel "$base_in\ACIPET\Gas `iano'.xlsx", sheet("GAS `iano'") cellrange(A4:D385) firstrow clear
rename TOTALPRODUCCION2012 TOTAL
}
if `iano'==2013 {
import excel "$base_in\ACIPET\Petroleo y gas `iano'.xlsx", sheet("PETÓLEO `iano'") cellrange(A5:X898) firstrow clear
rename TOTALPRODUCCION2013 TOTAL
keep if FLUIDO=="GAS "
}
if `iano'==2014 {
import excel "$base_in\ACIPET\Oil and Gas `iano'.xlsx", sheet("BD") cellrange(A5:S979) firstrow clear
rename TOTALPRODUCCION2014 TOTAL
keep if FLUIDO=="GAS "
}
}
if "`imineral'"=="petroleo" {

if `iano'==2004 {
import excel "$base_in\ACIPET\Oil `iano'.xlsx", sheet("OIL `iano'") cellrange(A3:D201) firstrow clear
}
if `iano'==2005 {
import excel "$base_in\ACIPET\Oil `iano'.xlsx", sheet("CAMP`iano'") cellrange(A3:D215) firstrow clear
}
if `iano'==2006{
import excel "$base_in\ACIPET\Oil `iano'.xlsx", sheet("CAMP`iano'") cellrange(A3:D243) firstrow clear
}
if `iano'==2007 {
import excel "$base_in\ACIPET\Oil `iano'.xlsx", sheet("OIL `iano'") cellrange(A3:D268) firstrow clear
}
if `iano'==2008 {
import excel "$base_in\ACIPET\Oil `iano'.xlsx", sheet("OIL `iano'") cellrange(A3:D280) firstrow clear
}
if `iano'==2009 {
import excel "$base_in\ACIPET\Oil `iano'.xlsx", sheet("Hoja1") cellrange(A4:D288) firstrow clear
rename TOTALPETRLEO2009 TOTAL
}
if `iano'==2010 {
import excel "$base_in\ACIPET\Oil `iano'.xlsx", sheet("OIL `iano'") cellrange(A2:D346) firstrow clear
rename TOTALPRODUCCION2010 TOTAL
}
if `iano'==2011 {
import excel "$base_in\ACIPET\Oil `iano'.xlsx", sheet("OIL `iano'") cellrange(A2:D396) firstrow clear
rename TOTALPRODUCCION2011 TOTAL
}
if `iano'==2012 {
import excel "$base_in\ACIPET\Oil `iano'.xlsx", sheet("OIL `iano'") cellrange(A3:D459) firstrow clear
rename TOTALPRODUCCION2012 TOTAL
}
if `iano'==2013 {
import excel "$base_in\ACIPET\Petroleo y gas `iano'.xlsx", sheet("PETÓLEO `iano'") cellrange(A5:X898) firstrow clear
rename TOTALPRODUCCION2013 TOTAL
keep if FLUIDO=="PETROLEO "
}
if `iano'==2014 {
import excel "$base_in\ACIPET\Oil and Gas `iano'.xlsx", sheet("BD") cellrange(A5:S979) firstrow clear
rename TOTALPRODUCCION2014 TOTAL
keep if FLUIDO=="PETROLEO "
}
}

rename DEPARTAMENTO departamento
rename MUNICIPIO municipio
rename TOTAL prod_`imineral'
collapse (sum) prod_`imineral', by(municipio departamento)
qui replace departamento="boyaca" if municipio=="Topaga-Corrales"
***All this tejemaneje because some pozos in two munis
qui replace municipio=subinstr(municipio,"/","- ",.)
qui gen guionpos=strpos(municipio,"-")
qui gen muni1=substr(municipio,1,guionpos-2) if guionpos!=0
qui replace muni1="Aguachica" if strpos(muni1,"Aguachic")
replace muni1="Palermo" if strpos(muni1,"Palerm")
qui replace muni1="topaga" if strpos(muni1,"Topag")
qui gen muni2=substr(municipio,guionpos+2,.) if guionpos!=0
qui replace muni2="Neiva" if strpos(muni2,"eiva")
qui replace muni2="Corrales" if strpos(muni2,"orrales")
qui count if guionpos>0
local dupli=r(N)
local orin=_N
local newn=`orin'+2*`dupli'
sort guionpos
set obs `newn'
forval i=1/`dupli' {
local pos1=`orin'+`i'
local pos2=`orin'+`i'+`dupli'
qui replace municipio=muni1[`orin'+1-`i'] in `pos1'
qui replace departamento=departamento[`orin'+1-`i'] in `pos1'
qui replace prod_`imineral'=(prod_`imineral'[`orin'+1-`i'])/2 in `pos1'
qui replace municipio=muni2[`orin'+1-`i'] in `pos2'
qui replace departamento=departamento[`orin'+1-`i'] in `pos2'
qui replace prod_`imineral'=(prod_`imineral'[`orin'+1-`i'])/2 in `pos2'
}
drop if guionpos>0 & guionpos!=.

collapse (sum) prod_`imineral', by(municipio departamento)
qui replace municipio="San Vicente de Chucuri" if strpos(municipio,"San Vivente de Chucuri")
*Arreglo las variables de departamento y municipio
replace departamento=lower(departamento)
replace municipio=lower(municipio)
qui replace municipio=subinstr(municipio,"á","a",.)
qui replace municipio=subinstr(municipio,"é","e",.)
qui replace municipio=subinstr(municipio,"í","i",.)
qui replace municipio=subinstr(municipio,"ó","o",.)
qui replace municipio=subinstr(municipio,"ò","o",.)
qui replace municipio=subinstr(municipio,"ú","u",.)
qui replace municipio=subinstr(municipio,"ñ","n",.)
qui replace departamento=subinstr(departamento,"á","a",.)
qui replace departamento=subinstr(departamento,"é","e",.)
qui replace departamento=subinstr(departamento,"í","i",.)
qui replace departamento=subinstr(departamento,"ó","o",.)
qui replace departamento=subinstr(departamento,"ú","u",.)
qui replace departamento=subinstr(departamento,"ñ","n",.)
qui replace departamento="casanare" if municipio=="mani"
qui replace departamento="casanare" if municipio=="trinidad"
qui replace departamento="casanare" if municipio=="pore"
qui replace departamento="casanare" if municipio=="aguazul"
qui replace departamento="casanare" if municipio=="villanueva" & departamento=="meta"
qui replace municipio=subinstr(municipio,"pto","puerto",.)
qui replace departamento="santander" if municipio=="puerto wilches"
qui replace municipio="el zulia" if municipio=="rio zulia"
qui replace municipio="valle del guamuez" if municipio=="valle de guamuez"
qui replace departamento="antioquia" if municipio=="puerto nare"
qui replace departamento="cauca" if municipio=="piamonte"
qui replace municipio="talaigua nuevo" if municipio=="talaiga nuevo"
qui replace municipio="cucuta" if municipio=="san jose de cucuta"
qui replace municipio="villavieja" if municipio=="viilavieja"
qui replace municipio="el carmen de chucuri" if municipio=="el carmen del chucuri"
qui replace municipio="yopal" if municipio=="el yopal"
qui replace municipio="vistahermosa" if municipio=="vista hermosa"
qui replace municipio="puerto gaitan" if municipio=="puerto giatan"
qui replace municipio="espinal" if municipio=="espina"
qui replace departamento="meta" if municipio=="puerto gaitan"
qui replace departamento="putumayo" if municipio=="mocoa"
qui replace municipio="villanueva" if municipio=="villa nueva"
qui replace municipio="flandes" if municipio=="landes"
qui replace municipio="villagarzon" if municipio=="villa garzon "
qui replace municipio="villagarzon" if municipio=="villa garzon"
qui replace municipio="palmito" if municipio=="palmitos"
qui replace municipio="cabuyaro" if municipio=="cubayaro"
qui replace municipio="mani" if municipio=="manio"
qui replace departamento="meta" if municipio=="cabuyaro"
qui replace departamento="cesar" if municipio=="aguachica"
qui replace municipio="rionegro" if municipio=="rio negro"
qui replace municipio="riohacha" if municipio=="rioacha"
qui replace departamento="la guajira" if municipio=="riohacha"
qui replace departamento="la guajira" if departamento=="guajira"
qui replace municipio="aguazul" if municipio=="guazul"
qui replace municipio="san pedro" if municipio=="san pedro "
collapse (sum) prod_`imineral', by(municipio departamento)

merge 1:1 departamento municipio using "$base_out/Divipola_Clean.dta"
drop if _merge==2
drop _merge
drop if codmpio==.
qui gen ano=`iano'
qui saveold "$base_out/Temporary/Produccion_`imineral'`iano'.dta", replace

}
}

*/
clear 
local ff "gas petroleo"
foreach imineral of local ff  {
forval iano=2004/2014{
append using "$base_out/Temporary/Produccion_`imineral'`iano'.dta"
}
}
replace prod_gas=prod_gas/1000 if ano<2010
collapse (sum) prod_petroleo prod_gas, by(codmpio ano)
saveold "$base_out/Temporary/ProduccionGasPetroleo_Anual.dta", replace
**********
*
* This used to be code 2. Create royalties CEDE SGR
*
*************

local deptos "Antioquia  Atlantico BogotaDC Bolivar Boyaca Caldas Caqueta Cauca Cesar Cordoba Cundinamarca Choco Huila  LaGuajira  Magdalena  Meta  Narino  NorteDeSantander Quindio   Risaralda  Santander  Sucre Tolima Valle Arauca  Casanare Putumayo  SanAndres   Amazonas  Guainia   Guaviare  Vaupes  Vichada"

foreach iyear in 2012 2013 2014 {
foreach idepto of local deptos {                         
qui import excel "$mipath/RawData/SGR/SGR_`idepto'`iyear'.xls", sheet("Datos Reporte SGR") cellrange(B17) firstrow clear
qui gen ano=`iyear'
qui saveold "$mipath/CreatedData/Temporary/SGR_`idepto'`iyear'", replace
}
}


clear
*Note we aren't using SGR 2012 because good suspicion that is already included in CEDE(DNP) panel
foreach iyear in 2013 2014 {
foreach idepto of local deptos                     {                         
qui append using "$mipath/CreatedData/Temporary/SGR_`idepto'`iyear'"
}
}
rename Codigo codmpio
qui destring codmpio, force replace
rename Total transf_resources

saveold "$mipath/CreatedData/SGR_All_2013_4", replace
keep codmpio ano transf_resources
drop if mod(codmpio,1000)==0
saveold "$mipath/CreatedData/Temporary/SGR_formerge", replace

** Import inflation to have transfers in real terms

import excel "$mipath/RawData/inflacion_1954_2015.xlsx", sheet("ipc_total-ano") cellrange(A8:F746) firstrow clear
rename AnoMes ano
rename Indice indice
keep ano indice
destring ano, force replace
replace indice=subinstr(indice,",",".",.)
destring indice, force replace
replace ano=ano-7
drop if mod(ano,100)!=0
replace ano=ano/100
drop if ano<1993 | ano>2014
sum indice if ano==2012
local scale=r(mean)
replace indice=indice/`scale'
saveold "$mipath/CreatedData/Temporary/inflation_formerge", replace

use "$mipath/RawData/PanelCEDE/PANEL_BUEN_GOBIERNO.dta", clear
qui gen transf_resources=y_cap_regalias

keep codmpio ano transf_resources 
gen source=1
*This part should be erased when CEDE updates 2014
keep if ano<2013
append using "$mipath/CreatedData/Temporary/SGR_formerge"
sort codmpio ano source
replace transf_resources=transf_resources/10^6 if ano>2012
merge m:1 ano using "$mipath/CreatedData/Temporary/inflation_formerge"
drop _merge
gen royalties=transf_resources/indice


/*
gen transf_scale=100 if ano==2012
forval i=1993/2011{
qui replace transf_scale=transf_resources/transf_resources[_n+(2012-`i')] if codmpio==codmpio[_n+(2012-`i')] & ano[_n+(2012-`i')]==2012 & source[_n+(2012-`i')]==1
*replace transf_scale=transf_resources/transf_resources[_n+(2013-`i')] if codmpio==codmpio[_n+(2013-`i')] & ano[_n+(2013-`i')]==2012 & source[_n+(2013-`i')]==1
}

forval i=2013/2014{
qui replace transf_scale=transf_resources/transf_resources[_n+(2012-`i')] if codmpio==codmpio[_n+(2012-`i')] & ano[_n+(2012-`i')]==2012 & source[_n+(2012-`i')]==.
*replace transf_scale=transf_resources/transf_resources[_n+(2011-`i')] if codmpio==codmpio[_n+(2011-`i')] & ano[_n+(2011-`i')]==2012 & source[_n+(2011-`i')]==.
}
*/
saveold "$base_out/Royalties1993_2014", replace

**********
*
* This used to be part of code 2.GraphEvol
* It creates a definition of mining municipality based on receiving royalties on 2011
*************

use "$base_out/Regalis_mineral_muni.dta", clear
drop if ano!=2011
collapse (sum) regalias, by(codmpio)
gen MuniMinero=(regalias>0)
drop regalias
merge 1:1 codmpio using "$base_out/Codigos_DANE.dta",
keep codmpio MuniMinero
replace MuniMinero=0 if MuniMinero==.
saveold "$base_out/MuniMinero.dta", replace

**********
* 3. Extract some data from mining census
* This used to be part of code 03_modify_census

*************

use "$base_in/CensoMinero/cm_bd_final.dta", clear
gen illegal=tipo_mina-1
sum illegal

*some mines have area 0, this is an error. This replaces it with missing values
replace ag_ue_06=. if ag_ue_06==0

gen paga_regalias=. 
replace paga_regalias=0 if aad_02==2
replace paga_regalias=1 if aad_02==1

gen titulo=. 
replace titulo=0 if aad_04==10
replace titulo=1 if aad_04<10

gen aut_ambiental=.
replace aut_ambiental=0 if ao_foyf_16_04==1
replace aut_ambiental=1 if ao_foyf_16_04==2

gen mineral=at_pyr_01
replace mineral=. if at_pyr_01==999
label define mineral 1 "Coal" 2 "Gems" 3 "Metals" 4 "Non-metals"
label values mineral mineral
gen type_metal=.

forval i=1/3{
	replace type_metal=`i' if at_pyr_03_01_0`i'==1
}

replace type_metal=7 if at_pyr_03_01_07==1
forval i=10/12{
	replace type_metal=`i' if at_pyr_03_01_`i'==1
}

label define type_metal 7 "Gold" 10 "Silver" 11 "Platinum" 
label values type_metal type_metal

gen type_nometal=.
forval i=1/7{
	replace type_nometal=`i' if at_pyr_04_01_0`i'==1
}

replace type_nometal=9 if at_pyr_04_01_09==1
forval i=10/23{
	replace type_nometal=`i' if at_pyr_04_01_`i'==1
}

label define type_nometal 1 "Clay" 2 "Sand" 9 "Cement limestone" 14 "Gravel" 16 "Rocks" 20 "Salt" 21 "Magnesium silicate"
label values type_nometal type_nometal

gen type_gem=.
forval i=1/2{
	replace type_gem=`i' if at_pyr_05_01_0`i'==1
}

gen cielo_abierto=ag_ue_07-1
gen type_mineral=100 if mineral==1
replace type_mineral=200+type_gem if mineral==2
replace type_mineral=300+type_metal if mineral==3
replace type_mineral=400+type_nometal if mineral==4
label define type_mineral 100 "Coal" 202 "Esmeralds" 307 "Gold" 310 "Silver" 311 "Platinum" 401 "Clay" 402 "Sand" 409 "Cement limestone" 414 "Gravel" 416 "Rocks" 420 "Salt" 421 "Magnesium silicate"
label values type_mineral type_mineral

qui gen cielo_abierto_carbon=cielo_abierto if mineral==1
qui gen cielo_abierto_piedprec=cielo_abierto if mineral==2
qui gen cielo_abierto_metprec=cielo_abierto if mineral==3
qui gen cielo_abierto_Caliza=cielo_abierto if type_mineral==409
qui gen cielo_abierto_Sal=cielo_abierto if type_mineral==420
qui gen cielo_abierto_Manganeso=cielo_abierto if type_mineral==421

gen area_sqkm=ag_ue_06/100
foreach xvar in illegal paga_regalias cielo_abierto cielo_abierto_carbon cielo_abierto_piedprec cielo_abierto_metprec cielo_abierto_Caliza cielo_abierto_Sal cielo_abierto_Manganeso {
	qui gen `xvar'_xarea=`xvar'*area_sqkm
}
rename cod_mpio codmpio

gen CA_illegal=area_sqkm if cielo_abierto==1 & illegal==1
gen CA=area_sqkm if cielo_abierto==1

preserve
// Create mineral_new with new codes for minerals + no labeled as other 
tempvar decode_type_min
decode type_mineral, g(`decode_type_min')
gen mineral_new=type_mineral if `decode_type_min'!=""
replace mineral_new=999 if type_mineral!=. & `decode_type_min'==""

egen count_mineral_new = count(mineral_new), by(mineral_new)
egen group = group(count_mineral_new mineral_new)
replace group = -group

la def e_group -12 "Coal" -6 "Esmeralds" -13 "Gold" -7 "Silver" -2 "Platinum" -11 "Clay" -10 "Sand" ///
	-8 "Cement limestone" -9 "Gravel" -3 "Rocks" -4 "Salt" -1 "Magnesium silicate" -5 "Other"
la val group e_group

eststo: estpost tabstat mineral_new, s(N) by(group) 

matrix colupct = e(count) // create matrix with sums
scalar c = colsof(colupct) // create scalar with number of rows of matrix
matrix colupct = 100*colupct/colupct[1,c] // transorm matrix values to percentages

estadd matrix colupct = colupct // add matrix 'colupct' to stored estimates

eststo clear // clear previously stored estimates
							 
estout using "${latexpaper}/Censo_typeMineral.tex", style(tex) cells("count(fmt(0) lab(Total)) colupct(fmt(2) lab(Percentage))") mlabels(none) collabels(none) ///
	replace eqlabels(none)  varlabels(`e(labels)') label
	
	restore

collapse (count) cm_nmines=id_plat (sum) CA_illegal CA  cm_area_sqkm=area_sqkm (mean) illegal paga_regalias cielo_abierto cielo_abierto_carbon cielo_abierto_piedprec cielo_abierto_metprec cielo_abierto_Caliza cielo_abierto_Sal cielo_abierto_Manganeso ///
         (mean) *_xarea cm_avgareamine=area_sqkm (sd) cm_stdareamine=area_sqkm , by(codmpio) 
		 
		 
foreach xvar in illegal paga_regalias cielo_abierto {
	qui rename `xvar' cm_pctgmines_`xvar'
	qui replace `xvar'_xarea=`xvar'_xarea*cm_nmines/cm_area_sqkm
	qui rename `xvar'_xarea cm_pctgarea_`xvar'
}

gen cm_pctgarea_illegal_CA=CA_illegal/CA


foreach xvar in carbon piedprec metprec Caliza Sal Manganeso {
	qui rename cielo_abierto_`xvar' cm_pctgmines_ca_`xvar'
	qui replace cielo_abierto_`xvar'_xarea=cielo_abierto_`xvar'_xarea*cm_nmines/cm_area_sqkm
	qui rename cielo_abierto_`xvar'_xarea cm_pctgarea_ca_`xvar'
}

gen cm_censed=1
label var cm_censed "Municipality included in mining census 2010"
		 
saveold "$base_out/Temporary/CensoMinero_bymuni.dta", replace



**********
*
* This used to be part of code 08_prices
* 
*************

import delim using "$base_in/DCOILWTICO.csv", clear
rename date year
rename value Hidrocarburos
sum Hidrocarburos if year==2004
gen precio_Hidrocarburos=Hidrocarburos/r(mean)
drop Hidrocarburos
saveold "$base_out/Temporary/precios_wti", replace

import excel "$base_in/SIMCO/Precios_carbon1994_2012.xls", sheet("Precios_carbon1994_2012") cellrange(A4:E22) clear
rename A year
destring year, force replace
rename C precio_carbon
keep year precio_carbon
replace precio_carbon=subinstr(precio_carbon,".","",.)
replace precio_carbon=subinstr(precio_carbon,",",".",.)
destring precio_carbon, force replace
/*http://www.infomine.com/ChartsAndData/ChartBuilder.aspx?z=f&gf=145246.USD.st&dr=max&cd=1 */
local new = _N + 1
set obs `new'
replace precio_carbon=57 in l
replace year=2013 in l
local new = _N + 1
set obs `new'
replace precio_carbon=47.5 in l
replace year=2014 in l
local new = _N + 1
set obs `new'
replace precio_carbon=42.5 in l
replace year=2015 in l
label var precio_carbon "Price (USD per ton)"
label var year "Year"

twoway line precio_carbon year  if year>2003 & year<2015, lw(thick) scale(1.1) scheme(s1color) ///
xline(2011) title("Evolution of coal's price")
graph export "$latexslides/coal_price.pdf", replace
graph export "$latexpaper/coal_price.pdf", replace


sum precio_carbon if year==2004
replace precio_carbon=precio_carbon/r(mean)




saveold "$base_out/Temporary/precios_carbon", replace

import delim "$base_in/BanRep/PreciosMetales_Banrep.csv", clear
gen date=date(fecha, "DMY")
gen year=year(date)
gen month=month(date)
label var oro "Price (COP per gram)"
label var plata "Price (COP per gram)"
label var platino "Price (COP per gram)"
label var date "Year"
twoway line oro date if year>2003 & year<2015, lw(thick) scale(1.1) scheme(s1color) ///
xline(18993) xlabel(16071 "2004" 17898 "2009" 19724 "2014") title("Evolution of gold's price")
graph export "$latexslides/gold_price.pdf", replace
graph export "$latexpaper/gold_price.pdf", replace
twoway line plata date if year>2003 & year<2015, lw(thick) scale(1.1) scheme(s1color) ///
xline(18993) xlabel(16071 "2004" 17898 "2009" 19724 "2014") title("Evolution of silver's price")
graph export "$latexslides/silver_price.pdf", replace
graph export "$latexpaper/silver_price.pdf", replace
twoway line platino date if year>2003 & year<2015, lw(thick) scale(1.1) scheme(s1color) ///
xline(18993) xlabel(16071 "2004" 17898 "2009" 19724 "2014") title("Evolution of platinum's price")
graph export "$latexslides/platino_price.pdf", replace
graph export "$latexpaper/platino_price.pdf", replace

collapse (mean) platino plata oro, by(year)
rename platino precio_platino
rename plata precio_plata
rename oro precio_oro
drop if year<2004
foreach x in platino plata oro{
qui sum precio_`x' if year==2004
qui replace precio_`x'=precio_`x'/r(mean)
}

qui keep year precio*
qui saveold "$base_out/Temporary/precios_metales", replace
qui merge 1:1 year using "$base_out/Temporary/precios_carbon"
qui drop _merge
qui merge 1:1 year using "$base_out/Temporary/precios_wti"
qui drop _merge
qui reshape long precio@, i(year) j(mineral) string

qui replace mineral="carbon" if mineral=="_carbon" 
qui replace mineral="Oro" if mineral=="_oro"
qui replace mineral="metalespreciosos" if mineral=="_plata"
qui replace mineral="Platino" if mineral=="_platino"
qui replace mineral="hierro" if mineral=="_HIERRO" 
qui replace mineral="Niquel" if mineral=="_NIQUEL" 
qui replace mineral="Hidrocarburos" if mineral=="_Hidrocarburos" 
qui collapse (mean) precio, by(year mineral)

*Reshape a columns year mineral price
*Merge con regalias x muni y generar indice precios x muni, o con Censo
rename year ano
drop if ano<2004
merge 1:m mineral ano using  "$base_out/Temporary/Share_mineral_muni_forindex_SinPetroleo"
drop if mineral=="Niquel" | mineral=="hierro" | mineral=="Hidrocarburos" | mineral=="_Hidrocarburos" | mineral=="Caliza" | mineral=="Manganeso" | mineral=="Sal" | mineral=="Yeso" | mineral=="_ALUMINIO" | mineral=="_COBRE"  | mineral=="_ESTANO" | mineral=="_ESTAO" | mineral=="_PLOMO" | mineral=="_ZINC" |  mineral=="azufre" |  mineral=="piedraspreciosos" 

gen Index=Share*precio*100
collapse (mean) Index, by(ano codmpio)
rename Index price_index
saveold "$base_out/Temporary/indexPrecios", replace
 
******
** The part below calculates the price index using fossil fuels
** so it can be used in the quantity regressions. In contrast the price_index above
** is for area mined so no fossil fuels.

qui use "$base_out/Temporary/precios_metales", clear
qui merge 1:1 year using "$base_out/Temporary/precios_carbon"
qui drop _merge
qui merge 1:1 year using "$base_out/Temporary/precios_wti"
qui drop _merge
rename year ano
drop if ano<2004

saveold "$base_out/Temporary/Precios_wff", replace

qui reshape long precio@, i(ano) j(mineral) string

qui replace mineral="carbon" if mineral=="_carbon" 
qui replace mineral="Oro" if mineral=="_oro"
qui replace mineral="metalespreciosos" if mineral=="_plata"
qui replace mineral="Platino" if mineral=="_platino"
qui replace mineral="hierro" if mineral=="_HIERRO" 
qui replace mineral="Niquel" if mineral=="_NIQUEL" 
qui replace mineral="Hidrocarburos" if mineral=="_Hidrocarburos" 
qui collapse (mean) precio, by(ano mineral)


merge 1:m mineral ano using  "$base_out/Temporary/Share_regalias_muni_forindex"
drop if mineral=="Niquel" | mineral=="hierro" |  mineral=="Caliza" | mineral=="Manganeso" | mineral=="Sal" | mineral=="Yeso" | mineral=="_ALUMINIO" | mineral=="_COBRE"  | mineral=="_ESTANO" | mineral=="_ESTAO" | mineral=="_PLOMO" | mineral=="_ZINC" |  mineral=="azufre" |  mineral=="piedraspreciosos" 

gen Index=Share*precio*100
collapse (mean) Index, by(ano codmpio)
rename Index price_index_wff
saveold "$base_out/Temporary/indexPrecios_wff", replace

use "$base_out\Temporary\precios_metales.dta", clear
rename precio_oro precio1
rename precio_platino precio2
drop precio_plata
reshape long precio, i( year) j(mineral)
append using "$base_out\Temporary\precios_carbon.dta"
keep if year>2003
rename year ano

replace mineral=4  if precio==. & precio_carbon!=.
replace precio=precio_carbon if precio==.

keep if ano<2015
keep ano mineral precio
save "$base_out\Temporary\precios_mineral.dta", replace

**********
*
* INDEX DE PRODUCCION
* 
*************

*******add production ****

qui use "$mipath/CreatedData/Temporary/cede_gral_forpanel.dta", clear
keep codmpio ano
merge 1:1 codmpio ano using "$base_out/ProduccionOro_Anual.dta"
drop if _merge==2
drop _merge municipio departamento 
merge 1:1 codmpio ano using "$base_out/ProduccionCarbon_Anual.dta"
drop if _merge==2
drop _merge municipio departamento 
merge 1:1 codmpio ano using "$base_out/ProduccionPlata_Anual.dta"
drop if _merge==2
drop _merge municipio departamento 
merge 1:1 codmpio ano using "$base_out/ProduccionSal_Anual.dta"
drop if _merge==2
drop _merge municipio departamento 
merge 1:1 codmpio ano using "$base_out/ProduccionPlatino_Anual.dta"
drop if _merge==2
drop _merge municipio departamento 
merge 1:1 codmpio ano using "$base_out/Temporary/ProduccionGasPetroleo_Anual.dta"
drop if _merge==2
drop _merge 
drop if ano<2004

foreach x in _carbon _oro _plata _platino _sal _petroleo _gas{
qui bys codmpio: egen maxprod`x'=max(prod`x') if !missing(prod`x') 
qui replace prod`x'=. if prod`x'==0 & (maxprod`x'==0 | maxprod`x'==.)
qui drop maxprod`x'
}

saveold "$base_out/Temporary/panel_prod_raw", replace
/*
replace prod_oro=0 if prod_oro==. & ano>=2001 & ano<=2014
replace prod_carbon=0 if prod_carbon==. & ano>=2006 & ano<=2015
replace prod_plata=0 if prod_plata==. & ano>=2004 & ano<=2015
replace prod_sal=0 if prod_sal==. & ano>=2003 & ano<=2014
replace prod_petroleo=0 if prod_petroleo==. & ano>=2004 & ano<=2014
replace prod_gas=0 if prod_gas==. & ano>=2004 & ano<=2014
replace prod_platino=0 if prod_platino==. & ano>=2004 & ano<=2015
*/
reshape long prod, i(ano codmpio) j (mineral) string
saveold "$base_out/Temporary/panel_prod", replace
qui gen prod_norm=.

foreach x in _carbon _oro _plata _platino _sal _petroleo _gas{
qui bys codmpio: egen minano=min(ano) if !missing(prod) & mineral=="`x'" & prod>0
gen prod_temp=prod if ano==minano & !missing(prod) & mineral=="`x'"
qui bys codmpio: egen prod_first=mean(prod_temp) if !missing(prod) & mineral=="`x'"
qui replace prod_norm=prod/prod_first  if mineral=="`x'"
drop minano prod_temp prod_first
}

**Crazy values because production fluctuations so tope code

qui egen p90prod_norm=pctile(prod_norm), by(mineral ano) p(90)
qui replace prod_norm=p90prod_norm if prod_norm!=. & prod_norm>p90prod_norm

*** Save the raw and normalized production by mineral
preserve 
keep ano codmpio mineral prod_norm
reshape wide prod, i(codmpio ano) j(mineral) string
saveold "$base_out/Temporary/panel_prod_norm", replace
restore


**For the index I need to group minerals
qui replace mineral="carbon" if mineral=="_carbon" 
qui replace mineral="metalespreciosos" if mineral=="_oro"
qui replace mineral="metalespreciosos" if mineral=="_plata"
qui replace mineral="metalespreciosos" if mineral=="_platino"
qui replace mineral="sal" if mineral=="_sal"
qui replace mineral="Hidrocarburos" if mineral=="_petroleo"
qui replace mineral="Hidrocarburos" if mineral=="_gas"


qui collapse (mean) prod_norm, by(ano mineral codmpio)

drop if ano<2004
merge 1:1 codmpio mineral ano using  "$base_out/Temporary/Share_regalias_muni_forindex"
drop if mineral=="Niquel" | mineral=="hierro" | mineral=="Caliza" | mineral=="Manganeso" | mineral=="Sal" | mineral=="Yeso" | mineral=="_ALUMINIO" | mineral=="_COBRE"  | mineral=="_ESTANO" | mineral=="_ESTAO" | mineral=="_PLOMO" | mineral=="_ZINC" |  mineral=="azufre" |  mineral=="piedraspreciosos" 

gen Index=Share*prod_norm*100
collapse (mean) Index, by(ano codmpio)
rename Index prod_index
saveold "$base_out/Temporary/indexProd", replace



**********
*
* This used to be part of code 06_Create_panel_forreg
* 
*************




use "$mipath/RawData/PanelCEDE/PANEL_CARACTERISTICAS_GENERALES.dta", clear
keep codmpio municipio depto ano pobl_tot indrural TMI nacimientos notarias pehosclinc peofdereci petotalins nbi ipm_accsalud_p
rename TMI tmi
local by=2005
sort codmpio ano
foreach x in notarias pehosclinc peofdereci petotalins {
qui gen `x'_pc=`x'/pobl_tot
*We do this to extract last year with info
qui gen `x'_temp=`x' if ano==`by'
qui gen `x'_pc_temp=`x'_pc if ano==`by'
qui by codmpio: egen `x'_`by'=max(`x'_temp)
qui by codmpio: egen `x'_pc_`by'=max(`x'_pc_temp)
qui drop `x'_pc_temp
local by=2002
}
saveold "$mipath/CreatedData/Temporary/cede_gral_forpanel.dta", replace

**Datos violencia CEDE
qui use "$mipath/RawData/PanelCEDE/PANEL_CONFLICTO_Y_VIOLENCIA.dta", clear
qui drop if ano<2004
qui keep codmpio ano homi_AUC homi_BACRIM homi_ELN homi_FARC FARC ELN AUC
sort codmpio ano

** Defining AG presence before reform with all the years
gen ind_hom_armedgroup_b2012=0
gen ind_hom_leftwag_b2012=0
gen ind_hom_rightwag_b2012=0
foreach x in homi_AUC homi_BACRIM homi_ELN homi_FARC {
qui replace ind_hom_armedgroup_b2012=1 if `x'!=. & `x'>0 & ano<2012
}
by codmpio: egen ind_armedgroup_b2012=max(ind_hom_armedgroup_b2012)
foreach x in homi_AUC homi_BACRIM  {
qui replace ind_hom_rightwag_b2012=1 if `x'!=. & `x'>0 & ano<2012
}
by codmpio: egen ind_rightwag_b2012=max(ind_hom_rightwag_b2012)

foreach x in homi_ELN homi_FARC {
qui replace ind_hom_leftwag_b2012=1 if `x'!=. & `x'>0 & ano<2012
}
by codmpio: egen ind_leftwag_b2012=max(ind_hom_leftwag_b2012)
qui drop ind_hom_armedgroup_b2012 ind_hom_leftwag_b2012 ind_hom_rightwag_b2012

** Defining AG presence before reform only with 2011
gen ind_hom_armedgroup_2011=0
foreach x in homi_AUC homi_BACRIM homi_ELN homi_FARC {
qui replace ind_hom_armedgroup_2011=1 if `x'!=. & `x'>0 & ano==2011
}
by codmpio: egen ind_armedgroup_2011=max(ind_hom_armedgroup_2011)
qui drop ind_hom_armedgroup_2011

** Defining AG presence before reform with CEDE dummies (not only homicides)
gen ind_cede_armedgroup_2008=0
foreach x in AUC ELN FARC {
qui replace ind_cede_armedgroup_2008=1 if `x'!=. & `x'>0 & ano==2008
}
by codmpio: egen ind_armedgroup_cede_2008=max(ind_cede_armedgroup_2008)
qui drop ind_cede_armedgroup_2008


qui egen homi_totalag=rowtotal(homi_AUC homi_ELN homi_BACRIM homi_FARC)

qui saveold "$mipath/CreatedData/Temporary/cede_violencia_forpanel.dta", replace

**Datos fiscales CEDE
qui use "$mipath/RawData/PanelCEDE/PANEL_BUEN_GOBIERNO.dta", clear
drop if ano<2004
qui keep codmpio ano y_total y_corr_tribut y_cap_regalias ing_propios

qui gen pctg_tribut=100*y_corr_tribut/y_total
label var pctg_tribut "Pctg of muni income from own taxes"
qui gen y_corr_tribut_2004=y_corr_tribut if ano==2004
qui egen y_corr_tribut_base=mean(y_corr_tribut_2004), by(codmpio)
qui gen norm_tribut=100*y_corr_tribut/y_corr_tribut_base
qui drop y_corr_tribut_2004 y_corr_tribut_base



merge m:1 ano using "$mipath/CreatedData/Temporary/inflation_formerge"
drop if _merge!=3
drop _merge
foreach x in y_total y_corr_tribut y_cap_regalias ing_propios {
replace `x'=`x'/indice
}

qui saveold "$mipath/CreatedData/Temporary/cede_fiscal_forpanel.dta", replace


*********************************************************************
********************************************************************
*Estimated royalties based on production pre-reform and formula
* This is to alleviate endogeneity concerns of winner/losers
*********************************************************************
********************************************************************


use "$base_out/REGALIAS_muni_pre.dta", clear
qui gen regalias_est_pre= regalias_petroleo_muni+ regalias_oro_muni+ regalias_plata_muni+ regalias_platino_muni+ regalias_carbon_muni+ regalias_puerto_muni
qui keep codmpio regalias_est_pre
saveold "$base_out/Temporary/REGALIAS_muni_pre.dta", replace

use "$base_out/REGALIAS_muni_post.dta", clear
qui gen regalias_est_post= regalias_petroleo_muni+ regalias_oro_muni+ regalias_plata_muni+ regalias_platino_muni+ regalias_carbon_muni
qui keep codmpio regalias_est_post
qui merge 1:1 codmpio using "$base_out/Temporary/REGALIAS_muni_pre.dta"
drop _merge

qui gen delta_roy_est= regalias_est_post- regalias_est_pre
qui gen ind_loser_est=1 if delta_roy_est<0
saveold "$base_out/Temporary/REGALIAS_est_loser", replace
*/
/*
****
** Assess the lumpiness of royalties proyects
****
qui import excel "$base_in\SGR\Proyectos_15082016.xlsx", sheet("TOTAL PROYECTOS") cellrange(A10:AA10102) firstrow clear

qui rename CÓDIGODANEENTEEJECUTOR codmpio
qui keep if mod(codmpio,1000)!=0

qui gen lumpy=0

qui replace lumpy=1 if SUBSECTOR=="Agricultura - Distritos De Riego"
qui replace lumpy=1 if SUBSECTOR=="Agricultura - Proyectos de Desarrollo Rural"
**One could argue that can do sewage for less houses
qui replace lumpy=1 if SUBSECTOR=="AGUA POTABLE Y SANEAMIENTO BASICO - Acueducto alcantarillado y plantas de tratamiento"
qui replace lumpy=1 if SUBSECTOR=="AGUA POTABLE Y SANEAMIENTO BASICO - Residuos solidos"
** The one below sounds weird, but is building contention walls, red cross building, fire stations
qui replace lumpy=1 if SUBSECTOR=="Medio Ambiente y Riesgo Â - AtenciÃ³n de desastres"
** Need to discriminate deeper Medio Ambiente y Riesgo Â - Control de la contaminaciÃ³n y manejo de residuos
** There are 120 stoves but also landfill
qui replace lumpy=1 if SUBSECTOR=="Medio Ambiente y Riesgo Â - GestiÃ³n integral de las aguas nacionales"
qui replace lumpy=1 if SUBSECTOR=="Comercio, Industria y Turismo - Fondo del Turismo"
** Below coliseum
qui replace lumpy=1 if SUBSECTOR=="CULTURA, DEPORTE Y RECREACION"
qui replace lumpy=1 if SUBSECTOR=="Deporte Â - Infraestructura deportiva"
qui replace lumpy=1 if SUBSECTOR=="Desarrollo Social - Infraestructura social y comunitaria"
qui replace lumpy=1 if SUBSECTOR=="Minas y EnergÃ­a - EnergÃ­a ElÃ©ctrica - GeneraciÃ³n Zonas No Interconectadas"
qui replace lumpy=1 if SUBSECTOR=="Minas y EnergÃ­a - EnergÃ­a ElÃ©ctrica - TransmisiÃ³n (> 220 KV)"
qui replace lumpy=1 if SECTOR=="TRANSPORTE"




qui replace lumpy=0 if strpos(NOMBREDELPROYECTO,"ESTUDIO")
qui replace lumpy=1 if strpos(NOMBREDELPROYECTO,"CONSTRUCCIÃN")
* ADECUACIÃN  is also Lumpy?

*I do this because lot of CONTRUCCION 40 ESTUFAS, 120 CASAS
qui replace lumpy=0 if SUBSECTOR=="VIVIENDA - Vivienda rural"
qui replace lumpy=0 if SUBSECTOR=="VIVIENDA - Vivienda urbana"

qui gen cost= AA/10^6
qui gen lumpy_cost=cost*(lumpy==1)
qui collapse (sum) cost lumpy_cost, by(codmpio)
qui gen lumpy_share=100* lumpy_cost/ cost

qui keep codmpio lumpy_share
qui save "$base_out/Temporary/lumpy_share.dta", replace
*/

use "C:\Users\Santi\Dropbox\Research\NetworksMigration\CreatedData/Temporary/data_forreg.dta", clear
collapse (sum) ORO_GRAMOS  , by(codmpio CEDULAONITVENDEDOR year month)
collapse (sum) ORO_GRAMOS (count) Nminers=CEDULAONITVENDEDOR , by(codmpio year month)
keep if year<2012
collapse (sum) ORO_GRAMOS Nminers , by(codmpio)
save "$base_out/Temporary/gold_miners.dta", replace

*********************************************************************
********************************************************************

*********************************************************************
********************************************************************
**Merge everything
***************************************************************************
************************************************************************

*********************************************************************
********************************************************************

qui use "$mipath/CreatedData/Temporary/cede_gral_forpanel.dta", clear

merge 1:1 codmpio ano using "$mipath/CreatedData/Temporary/cede_fiscal_forpanel.dta"
drop _merge

merge 1:1 codmpio ano using "$mipath/CreatedData/Temporary/cede_violencia_forpanel.dta"
drop _merge

merge 1:1 codmpio ano using "$mipath/CreatedData/Temporary/price_index_pqCOL_likePeru.dta"
drop _merge

merge 1:1 codmpio ano using "$base_out/Temporary/indexPrecios"
drop if _merge==2
drop _merge
gen price_index_u=price_index
forval i=2004/2014{
qui sum price_index if ano==`i'
replace price_index_u=r(mean) if price_index_u==. & ano==`i'
}
gen price_index_m=0
replace price_index_m=1 if price_index==.
label var price_index "Raw mineral price index"
label var price_index_u "Mineral price index"
label var price_index_m "Dummy missing mineral price index"

merge 1:1 codmpio ano using "$base_out/Temporary/indexPrecios_wff"
drop if _merge==2
drop _merge
gen price_index_wff_u=price_index_wff
forval i=2004/2014{
qui sum price_index_wff if ano==`i'
replace price_index_wff_u=r(mean) if price_index_wff_u==. & ano==`i'
}
gen price_index_wff_m=0
replace price_index_wff_m=1 if price_index_wff==.
label var price_index_wff "Raw natural resource price index"
label var price_index_wff_u "Natural resource price index"
label var price_index_wff_m "Dummy missing natural resource price index"

merge m:1 ano using "$base_out/Temporary/Precios_wff"
drop if _merge==2
drop _merge

merge 1:1 codmpio ano using "$base_out/Temporary/indexProd"
drop if _merge==2
drop _merge
gen prod_index_u=prod_index
forval i=2004/2014{
qui sum prod_index if ano==`i'
replace prod_index_u=r(mean) if prod_index_u==. & ano==`i'
}
gen prod_index_m=0
replace prod_index_m=1 if prod_index==.
label var prod_index "Raw mineral prod index"
label var prod_index_u "Mineral prod index"
label var prod_index_m "Dummy missing mineral prod index"

merge 1:1 codmpio ano using "$base_out/Temporary/panel_prod_raw"
drop if _merge==2
drop _merge

merge 1:1 codmpio ano using "$base_out/Temporary/panel_prod_norm"
drop if _merge==2
drop _merge

merge 1:1 codmpio ano using "$mipath/CreatedData/Royalties1993_2014.dta"
drop if _merge==2
drop _merge




drop indice source transf_resources*

merge m:1 codmpio using "$base_out/Temporary/lumpy_share.dta"
drop if _merge==2
drop _merge

merge m:1 codmpio using "$base_out/Temporary/border_distances_colombia.dta"
drop if _merge==2
drop _merge

merge m:1 codmpio using "$base_out/Temporary/ind_minerpot.dta"
drop if _merge==2
drop _merge

merge m:1 codmpio using "$mipath/CreatedData/Temporary/muni_detect_satelite.dta"
drop if _merge==2
drop _merge

merge m:1 codmpio using "$mipath/CreatedData/MuniMinero.dta"
drop if _merge==2
drop _merge

merge m:1 codmpio using "$mipath/CreatedData/Temporary/gold_miners.dta"
drop if _merge==2
drop _merge

sort codmpio ano
gen delta_royalties=royalties-royalties[_n-1] if codmpio==codmpio[_n-1] & ano==(ano[_n-1]+1)
drop if ano<2004
drop if ano==.
drop if mod(codmpio,1000)==0 /*just check... since  dropped by _merge==2 above */
drop if mod(codmpio,1000)==999 /*just check... since  dropped by _merge==2 above */
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
gen royalties_pc= royalties/ pobl_tot
qui gen pct_roy= 100*royalties/ y_total
label var royalties "Royalties in millions of COP2012"
label var tmi "Mortality rate per 1,000 newborns"
label var royalties_pc "Royalties per capita"
label var pobl_tot "Population"
label var nacimientos "Births"
label var nbi "Poverty index" 
qui gen nbi_temp=nbi if ano==2011
qui bysort codmpio: egen nbi_2011=max(nbi_temp)
qui drop nbi_temp
label var nbi_2011 "Poverty index 2011 measure" 
label var y_total "Municipality income"
label var pct_roy "Royalties as pctg of muni income"
label var ing_propios "Local taxes as pctg of muni income"
label var ano "Year"
saveold "$mipath/CreatedData/panel_forreg.dta", replace

qui gen roy_bef=royalties if ano==2010 | ano==2011
qui gen roy_aft=royalties if ano==2013 | ano==2014
qui gen y_bef=y_total if ano==2010 | ano==2011
collapse tmi pobl_tot nbi y_total MuniMinero roy_bef roy_aft y_bef, by(codmpio)
gen aff=2*( roy_aft- roy_bef)/( roy_aft+ roy_bef)
gen pctg_budget_roy_change=100*( roy_aft- roy_bef)/y_bef
*Why did we have this line? Is it people that had 0 mining royalties before?
*drop if MuniMinero==1 & aff==2
keep codmpio aff pctg_budget_roy_change
label var pctg_budget_roy_change "Change in royalties as percentage of budget"
saveold "$mipath/CreatedData/Temporary/Muni_affected_reform.dta", replace

*I was doing merge with MuniMinero this to get rid of Munis with royalties in SIMCO but 0 in DNP
*merge 1:m codmpio MuniMinero using "$mipath/CreatedData/panel_forreg.dta"
merge 1:m codmpio using "$mipath/CreatedData/panel_forreg.dta"

drop _merge
saveold "$mipath/CreatedData/panel_forreg.dta", replace


qui sum royalties_pc, detail
qui local topcut=round(r(p95)*100)/100
qui replace royalties_pc=`topcut' if royalties_pc>`topcut'


qui merge 1:1 codmpio ano using "$base_out/Temporary/panel_prillegal.dta"
drop _merge

qui merge 1:1 codmpio ano using "$base_out/Temporary/panel_prillegalSP.dta"
drop _merge

qui merge 1:1 codmpio ano using "$base_out/Temporary/panel_prillegal_unodc.dta"
drop _merge

qui merge 1:1 codmpio ano using "$base_out/Temporary/panel_prillegal_tityear.dta"
drop _merge

qui merge 1:1 codmpio ano using "$base_out/Temporary/panel_prillegal_prob.dta"
drop _merge

qui merge 1:1 codmpio ano using "$base_out/Temporary/kstar_illegal.dta"
drop _merge


qui merge 1:1 codmpio ano using "$base_out/Temporary/panel_prillegal_c12.dta"
drop _merge

qui merge 1:1 codmpio ano using "$base_out/Temporary/dynampred.dta"
drop _merge

qui merge 1:1 codmpio ano using "$base_out/Temporary/dynampred_tit2014.dta"
drop _merge

qui merge 1:1 codmpio ano using "$base_out/Temporary/training_tpfp.dta"
drop if _merge==2
drop _merge

merge m:1 codmpio using "$base_out/Temporary/CensoMinero_bymuni.dta"
drop _merge
replace cm_censed=0 if cm_censed==.

 merge m:1 codmpio using "$mipath/CreatedData/Temporary/mineral_ever_2004_2012.dta"
 drop if _merge==2
drop _merge
local minerals "azufre carbon hierro piedraspreciosos Hidrocarburos Caliza Manganeso Niquel Sal Yeso"
foreach imineral of local minerals {
replace ind_`imineral'_ever=0 if ind_`imineral'_ever==.
}
*For gold, silver and platinum w ehave production data, not need to rely on royalties
replace ind_metalespreciosos_ever=(Nminers!=.)
label var ind_metalespreciosos_ever "Produced precious metals"

merge m:1 codmpio using "$base_out/Temporary/REGALIAS_est_loser"
 drop if _merge==2
drop _merge

**Add this line to exclude big cities
bys codmpio: egen maxpob=max(pobl_tot)
drop if maxpob>400000 & !missing(maxpob)
***Add this line to exclude municipalities where TMI fluctuates just because sample size
*drop if nacimientos<30
*drop if tmi==.

gen ind_loser=1 if pctg_budget_roy_change<0 
replace ind_loser=0 if ind_loser==.
label var ind_loser "Loser"

gen pctg_budget_roy_change_ifloss=pctg_budget_roy_change if pctg_budget_roy_change<0
label var pctg_budget_roy_change_ifloss "Change in royalties \% budget if loser"
gen pctg_budget_roy_change_ifwin=pctg_budget_roy_change if pctg_budget_roy_change>=0
label var pctg_budget_roy_change_ifwin "Change in royalties \% budget if winner"

gen ind_after=1 if ano>2011 & ano!=.
replace ind_after=0 if ano<2012
label var ind_after "After"

qui gen after_x_loser=ind_loser*ind_after
label var after_x_loser "After x Loser"

qui gen pctg_loss=-pctg_budget_roy_change
label var pctg_loss "\% Loss" 
qui gen after_x_pctg_loss=pctg_loss*ind_after
label var after_x_pctg_loss "After x \% Budget Loss"
qui gen after_x_pctgloss_if=pctg_loss*ind_after*ind_loser
label var after_x_pctgloss_if "After x \% Budget Loss if Loss"
qui gen after_x_pctgwin_if=-pctg_loss*ind_after*(1-ind_loser)
label var after_x_pctgwin_if "After x \% Budget Win if Won"

*Calculate mining importance before reform
qui gen y_total_bef_i=y_total if ano<2011
qui gen y_cap_regalias_bef_i=y_cap_regalias if ano<2011
qui bys codmpio: egen double y_total_bef=total(y_total_bef_i)
qui bys codmpio: egen double y_cap_regalias_bef=total(y_cap_regalias_bef_i)
qui gen mining_importance=y_cap_regalias_bef/y_total_bef
label var mining_importance "\% of budget from royalties before reform"

qui gen after_x_importance=mining_importance*ind_after
label var after_x_importance "After x \% budget from royalties before"

qui gen prop_mined=100*areaprmined_sqkm/analizedarea_sqkm
label var prop_mined "Percentage of analyzed area predicted as mined"

gen prop_illegal= 100*areaprilegal_sqkm/ analizedarea_sqkm
label var prop_illegal "Share of municipality area mined illegaly"

gen propmined_illegal= 100*areaprilegal_sqkm/ areaprmined_sqkm
label var propmined_illegal "Share of municipality mined area mined illegaly"

gen propminedSP_illegal= 100*areaprilegalSP_sqkm/ areaprminedSP_sqkm
label var propminedSP_illegal "Share of municipality mined area mined illegaly outside parks"


gen propadj_illegal= 100*areaprilegal_adj_sqkm/ analizedarea_sqkm
label var propadj_illegal "Share of municipality area mined illegaly adjusted"

gen propadjmined_illegal= 100*areaprilegal_adj_sqkm/ areaprmined_adj_sqkm
label var propadjmined_illegal "Share of municipality mined area mined illegaly adjusted"

gen c12_prop_illegal= 100*c12_areaprilegal_sqkm/ c12_analizedarea_sqkm
label var c12_prop_illegal "Share of municipality area mined illegaly (NOC)"

gen c12_propmined_illegal= 100*c12_areaprilegal_sqkm/ c12_areaprmined_sqkm
label var c12_propmined_illegal "Share of municipality mined area mined illegaly (NOC)"

gen c12_propadj_illegal= 100*c12_areaprilegal_adj_sqkm/ c12_analizedarea_sqkm
label var propadj_illegal "Share of municipality area mined illegaly adjusted (NOC)"

gen c12_propadjmined_illegal= 100*c12_areaprilegal_adj_sqkm/ c12_areaprmined_adj_sqkm
label var c12_propadjmined_illegal "Share of municipality mined area mined illegaly adjusted (NOC)"

**For sum stats
qui gen prop_illegal_bef= prop_illegal if ind_after==0
qui label var prop_illegal_bef "\% of area illegal before"
gen prop_illegal_aft= prop_illegal if ind_after==1
qui label var prop_illegal_aft "\% of area illegal after"
gen pctg_budget_roy_change_munil=pctg_budget_roy_change if ano==2014
label var pctg_budget_roy_change_munil "Change in royalties as percentage of budget"
gen ind_armedgroup_b2012_munil= ind_armedgroup_b2012 if ano==2014
label var ind_armedgroup_b2012_munil "Armed group presence before reform"

qui gen prop_illegal_ag=prop_illegal if ind_armedgroup_b2012==1 
label var prop_illegal_ag "\% of area illegal if armed groups"
qui gen prop_illegal_bef_ag= prop_illegal_ag if ind_after==0
qui label var prop_illegal_bef_ag "\% of area illegal before AG"
gen prop_illegal_aft_ag= prop_illegal_ag if ind_after==1
qui label var prop_illegal_aft_ag "\% of area illegal after AG"

qui gen propmined_illegal_bef= propmined_illegal if ind_after==0
qui label var propmined_illegal_bef "\% of mined area illegal before"
gen propmined_illegal_aft= propmined_illegal if ind_after==1
qui label var propmined_illegal_aft "\% of mined area illegal after"
qui gen propmined_illegal_ag=propmined_illegal if ind_armedgroup_b2012==1 
label var propmined_illegal_ag "\% of mined area illegal if armed groups"
qui gen propmined_illegal_bef_ag= propmined_illegal_ag if ind_after==0
qui label var propmined_illegal_bef_ag "\% of mined area illegal before AG"
gen propmined_illegal_aft_ag= propmined_illegal_ag if ind_after==1
qui label var propmined_illegal_aft_ag "\% of mined area illegal after AG"

***Caution the calculations below assume monotonic increasing
**If pixels swithc froim illegal to legal or other might be problematic
sort codmpio ano

/*
** The reason for the division is that maybe mining went up becaus of more analyzed area
qui gen newarea_prmined_sqkm=areaprmined_sqkm/(analizedarea_sqkm/ areamuni_sqkm)-areaprmined_sqkm[_n-1]/(analizedarea_sqkm[_n-1]/ areamuni_sqkm[_n-1]) if codmpio==codmpio[_n-1] & ano==(ano[_n-1]+1)
qui gen newarea_prilegal_sqkm=areaprilegal_sqkm/(analizedarea_sqkm/ areamuni_sqkm)-areaprilegal_sqkm[_n-1]/(analizedarea_sqkm[_n-1]/ areamuni_sqkm[_n-1]) if codmpio==codmpio[_n-1] & ano==(ano[_n-1]+1)

**Thereason for dividing by areamuni instead of analyzed area is that, when re-scaling
** by analyzed area to generate the variable now the unit is total muni area
qui gen newprop_illegal= 100*newarea_prilegal_sqkm/ areamuni_sqkm
label var newprop_illegal "New area mined illegaly as share of municipality area "
gen newpropmined_illegal= 100*newarea_prilegal_sqkm/ newarea_prmined_sqkm
label var newpropmined_illegal "Share of municipality mined area mined illegaly"
*/

qui gen newprop_illegal= 100*illegalm_pastNm/ analyzed_area_dyn
label var newprop_illegal "New area mined illegaly as share of municipality area "
gen newpropmined_illegal= 100*illegalm_pastNm/ (illegalm_pastNm+legalm_pastNm)
label var newpropmined_illegal "Share of new municipality mined area mined illegaly"

qui gen cm_pctgmines_illegal_ag=cm_pctgmines_illegal if ind_armedgroup_b2012==1
qui gen cm_pctgarea_illegal_ag=cm_pctgarea_illegal if ind_armedgroup_b2012==1
label var MuniMinero "Mining municipality"
label var ind_armedgroup_b2012 "Armed group presence before reform"
label var price_index "Price Index (2005=100)"
label var cm_pctgmines_illegal "\% illegal mines (Census)"
label var cm_pctgmines_illegal_ag "\% illegal mines if armed group present"
label var cm_pctgarea_illegal "\% illegal area (Census)"
label var cm_pctgarea_illegal_ag "\% illegal area if armed group present"
label var cm_pctgmines_cielo_abierto "\% open pit mines (Census)"

qui gen tasa_homag= 10^6*homi_totalag/ pobl_tot

qui gen satelite_prone=.

*Those open pit, according to Census, we should be able to observe
local minerals "carbon metprec piedprec Caliza Manganeso Sal"
foreach imineral of local minerals {
qui replace satelite_prone=1 if cm_pctgmines_ca_`imineral'!=. & cm_pctgmines_ca_`imineral'>0
}

*Those that are not in Census, but have underground mineral usually open pit we should observe 

qui replace satelite_prone=1 if satelite_prone==. & satelite_minerpot==1 


**Now lets mark those underground or that we can't observe
local minerals "carbon metprec piedprec Caliza Manganeso Sal"
foreach imineral of local minerals {
qui replace satelite_prone=0 if satelite_prone==. & cm_pctgmines_ca_`imineral'==0
}


qui replace satelite_prone=0 if satelite_prone==. & satelite_minerpot==0 



*Drop San Andres and Providence, Islands not analyzed in satellite data
drop if codmpio==88001 | codmpio==88564
*En los corregimintos del Amazonas no hay datos regalias, pero si los tenemos separados en
*el shapefile de municipios  qusamos para el panel de ilegal
drop if pctg_budget_roy_change==.

*Keep only municipalities with mining potential
keep if ind_minerpot==1 | cm_censed==1
* Normalized trend variable 
qui gen trend = (ano - 2004)/10 



* Gen variable for department year fixed effects
qui gen anodepto=ano*100+coddepto

*Department trend
qui gen propmined_illegal_dtst=.
qui tab coddepto, gen(idepto)
forval i=1/26{
qui gen trend_depto`i'=trend*idepto`i'
qui xi: areg propmined_illegal trend if ano<2011 & idepto`i'==1, absorb(codmpio) vce(cluster codmpio)
qui replace propmined_illegal_dtst=propmined_illegal-_b[trend]*trend if idepto`i'==1
}

*For AG tripple diff
qui gen after_x_ag=ind_after*ind_armedgroup_b2012
qui label var after_x_ag "After x Armed Groups"
qui gen after_x_loser_x_ag=after_x_loser*ind_armedgroup_b2012
qui label var after_x_loser_x_ag "After x Loser x Armed Groups"
qui gen price_index_u_x_ag=price_index_u*ind_armedgroup_b2012
qui gen trend_x_ag=trend*ind_armedgroup_b2012




** Lets make sure all regressions have the same observations as our main regression
qui reg prop_illegal ind_after ind_loser after_x_loser price_index_u price_index_m  , vce(cluster codmpio)
keep if e(sample)



*Average change mining before-after reform
*Note: I have to substract the trend so that the regression in differences if
*comparable to the diff in diff specification. I divide by 10 because I defined 
* the trend variable normalized between 0 and 1 (`i'-2004)/10

*National trend (although it doesn't change forest because is a constant for all munis)
qui xi: areg prop_illegal  trend , absorb(codmpio)  vce(cluster codmpio)
matrix tempm=e(b)
qui gen trendcoef=tempm[1,1] 

qui xi: areg propmined_illegal  trend  , absorb(codmpio)  vce(cluster codmpio)
matrix tempm=e(b)
qui gen trendminedcoef=tempm[1,1] 
/*
*If trends by department
qui gen trendcoef=.
qui xi: areg prop_illegal  trend_depto*  , absorb(codmpio)  vce(cluster codmpio)
matrix tempm=e(b)
qui forval i=1/29 {
replace trendcoef=tempm[1,`i'] if idepto`i'==1
}
*/
sort codmpio ano
gen changeil_befaft= (prop_illegal + prop_illegal[_n-1]+prop_illegal[_n-2]- prop_illegal[_n-3]-prop_illegal[_n-4]-prop_illegal[_n-5])/3 if ano==2014 & ano[_n-5]==2009 & codmpio==codmpio[_n-5]
label var changeil_befaft "Change in illegal mining"
gen changeminedil_befaft= (propmined_illegal + propmined_illegal[_n-1]+propmined_illegal[_n-2]- propmined_illegal[_n-3]-propmined_illegal[_n-4]-propmined_illegal[_n-5])/3 if ano==2014 & ano[_n-5]==2009 & codmpio==codmpio[_n-5]
label var changeminedil_befaft "Change in shared mined illegal"
gen changeil_befaft_dtr= (prop_illegal + prop_illegal[_n-1]+prop_illegal[_n-2]- prop_illegal[_n-3]-prop_illegal[_n-4]-prop_illegal[_n-5]-9*trendcoef/10)/3 if ano==2014 & ano[_n-5]==2009 & codmpio==codmpio[_n-5]
gen changeminedil_befaft_dtr= (propmined_illegal + propmined_illegal[_n-1]+propmined_illegal[_n-2]- propmined_illegal[_n-3]-propmined_illegal[_n-4]-propmined_illegal[_n-5]-9*trendcoef/10)/3 if ano==2014 & ano[_n-5]==2009 & codmpio==codmpio[_n-5]
*The line above only works if all 2009-2014 not missing, I know 2009, 2012, 2014 all munis
* If only 2013 missing
replace changeil_befaft_dtr= (prop_illegal + prop_illegal[_n-1]- prop_illegal[_n-2]-prop_illegal[_n-4]-6*trendcoef/10)/3 if ano==2014 & ano[_n-1]==2012 & ano[_n-4]==2009 & codmpio==codmpio[_n-4]
replace changeminedil_befaft_dtr= (propmined_illegal + propmined_illegal[_n-1]- propmined_illegal[_n-2]-propmined_illegal[_n-4]-6*trendminedcoef/10)/3 if ano==2014 & ano[_n-1]==2012 & ano[_n-4]==2009 & codmpio==codmpio[_n-4]
* If only 2011 missing
replace changeil_befaft_dtr= (prop_illegal + prop_illegal[_n-2]- prop_illegal[_n-3]-prop_illegal[_n-4]-7*trendcoef/10)/3 if ano==2014 & ano[_n-1]==2013 & ano[_n-3]==2010 & ano[_n-4]==2009 & codmpio==codmpio[_n-4]
replace changeminedil_befaft_dtr= (propmined_illegal + propmined_illegal[_n-2]- propmined_illegal[_n-3]-propmined_illegal[_n-4]-7*trendminedcoef/10)/3 if ano==2014 & ano[_n-1]==2013 & ano[_n-3]==2010 & ano[_n-4]==2009 & codmpio==codmpio[_n-4]
* If only 2010 missing
replace changeil_befaft_dtr= (prop_illegal + prop_illegal[_n-2]- prop_illegal[_n-3]-prop_illegal[_n-4]-6*trendcoef/10)/3 if ano==2014 & ano[_n-3]==2011  & ano[_n-4]==2009 & codmpio==codmpio[_n-4]
replace changeminedil_befaft_dtr= (propmined_illegal + propmined_illegal[_n-2]- propmined_illegal[_n-3]-propmined_illegal[_n-4]-6*trendminedcoef/10)/3 if ano==2014 & ano[_n-3]==2011  & ano[_n-4]==2009 & codmpio==codmpio[_n-4]


label var changeil_befaft_dtr "Change in illegal mining detrended"
label var changeminedil_befaft_dtr "Change in share mined illegal detrended"

saveold "$base_out/Temporary/panel_quasifinal.dta", replace
sort codmpio ano
gen tmi_2013=tmi[_n-1] if ano==2014 & ano[_n-1]==2013
**** Produce dataset for heteregoneous treatment effect analysis casual forest R
keep if ano==2014
keep changeil_befaft changeil_befaft_dtr changeminedil_befaft changeminedil_befaft_dtr ind_loser tmi_2013 royalties_pc codmpio ind_armedgroup_b2012 ///
   ind_metalespreciosos_ever ind_leftwag_b2012 ind_rightwag_b2012 indrural nbi_2011 ///
   notarias_pc_2005 mining_importance pehosclinc_pc_2002 peofdereci_pc_2002 petotalins_pc_2002 ///
   pobl_tot
   drop if changeil_befaft==.
export delimited "$base_out/input_causfor.csv", replace





****************************************************************************************************
****************************************************************************************************
************************** ADD Leopoldo/Juan F. Jud_Strength****************************************
****************************************************************************************************
****************************************************************************************************
/*
use "$base_in/Judicial_FP.dta", clear
collapse (mean)  justice_percent justice_percent9599, by(M_code)
rename M_code codmpio
merge 1:m codmpio using "$base_out/panel_final.dta"
drop if _merge==1
drop _merge

*Something really odd... check codmpio


qui gen after_x_jds=ind_after*justice_percent9599
qui label var after_x_jds "After x Judiciary Strength (95-99)"
save "$base_out/panel_final.dta",replace
