preserve

*La intente hacer con c.ind_oro##b2010.ano pero me dropeaba 2006 y 2012 :(
forval i=2004/2014 {
if `i'~=2010 {
gen ind_gold`i'=(mineral==1)*(ano==`i')

}
}
gen base=0


qui reghdfe c67_propmined_illegal_mi ind_gold2004-ind_gold2009 base ind_gold2011-ind_gold2014 precio , absorb(codmpio#ano codmpio#mineral) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels scheme(s1color) keep(ind_gold2004-ind_gold2009 base ind_gold2011-ind_gold2014) ci vertical yline(0) xline(2011) rename( ///
ind_gold2004=2004 ///
ind_gold2005=2005 ///
ind_gold2006=2006 ///
ind_gold2007=2007 ///
ind_gold2008=2008 ///
ind_gold2009=2009 ///
base=2010         ///
ind_gold2011=2011 ///
ind_gold2012=2012 ///
ind_gold2013=2013 ///
ind_gold2014=2014)
graph export "$latexslides/partrends_munimi_c67.pdf", replace
graph export "$latexpaper/partrends_munimi_c67.pdf", replace
restore
