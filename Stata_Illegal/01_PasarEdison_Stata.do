global base_in "C:\Users\Santi\Dropbox\MineriaIlegal\RawData"
global base_out "C:\Users\Santi\Dropbox\MineriaIlegal\CreatedData"


use "$base_out\Temporary\Codigos_DANE_MAYUSC.dta", clear
qui foreach x of varlist depto municipio {
  
	replace `x'=subinstr(`x'," ","",.)
	}
	replace municipio="LAMONTANITA" if codmpio==18410
	replace municipio="CANASGORDAS" if codmpio==5138
	replace municipio="BRICENO" if codmpio==5107
		replace municipio="MAGUI" if codmpio==52427
		replace municipio="PENOL" if codmpio==5541
		replace municipio="NARINO" if codmpio==5483
		replace municipio="ELPENON" if codmpio==13268
		replace municipio="SALDANA" if codmpio==73671
		replace municipio="SANJOSEDELAMONTANA" if codmpio==5658
		   replace municipio="MONITOS" if codmpio== 23500
		replace municipio="NARINO" if codmpio==25483 | codmpio==52480
		replace municipio="UTICA" if codmpio==25851
		replace municipio="ELPINON" if codmpio==47258
		 replace municipio="PIJINODELCARMEN" if codmpio==47545
		 replace municipio="CHACHAGUI" if codmpio==52240
		 replace municipio="ELPENOL" if codmpio==52254
		 replace municipio="OCANA" if codmpio==54498
		 replace municipio="COVENAS" if codmpio==70221
		 replace municipio="PUERTOCARRENO" if codmpio==99001
	sort depto municipio
save "$base_out\Temporary\CodigosDANEMAYUSC.dta", replace
*/

/*
matrix A = (3 , 43944 \ 3,4162 \ 3 , 11958 \ 3, 41434 \  3 , 9310 \ 3 , 65423 \ 3, 63981 \  3, 35364 \ 3, 9036 \ 3, 56685 ///
          \ 3, 52168 \ 3 , 35705 \ 3, 23041 \ 3, 963  \ 3, 2542   \ 3, 738     \ 3 , 446  \ 3 , 941 \ 3, 36 \ 3 , 64359 ///
           \ 3 , 17541 \ 3 , 7 \ 3, 43853 \ 3, 54617 \ 3, 39702   \ 2 ,1078 \ 3, 4113 \ 3, 18648 \ 3 , 33995 \ 3, 65515 ///
           \ 3, 1488 \ 3, 7 \ 3,11 \ 3, 5335 \ 3, 1547 \ 3, 6064 \  3, 483 \ 3, 1345 \ 3, 4611 \ 3,1373 ///
          \ 3, 1580 \ 3, 12 )
local mylist Anex "GyD varios" Coominercol GyD GyD2 Gutierrez Gutierrez2 Gutierrez3 Mepre MH   ///
"ntr metals zf" FRZF S&J Mineros BusinessGVG Golden_Idumat XQuadra Orafa GyS Ramirez  /// *Ramirez es 20
Ramirez2 PlateriaR Ciigsa Goldex "Goldex (2)" "ci ntr metals" FunBio Banr DC Esc ///
Macedo Quibdo Goldgem MyD Noropel Platex PGM RexM RexG Minex ///
 InvOrex DyU
local sheetcount=1
qui foreach sheetname of local mylist {
 if (`sheetcount'>35) {
  local lb=A[`sheetcount',1]
  local ub=A[`sheetcount',2]
  if (`sheetcount'~=22 & `sheetcount'~=26 & `sheetcount'~=30 & `sheetcount'<31) {
 import excel "$base_in\BASE DE DATOS MINERIA EN COLOMBIA (ANM)\Punto 2 Produccion oro 2004 a 2014\MUNICIPIOS REPORTADOS COMO PRODUCTORES_LIQUIDACION_REGALIAS 2005 a May 2015.xls", ///
 sheet("`sheetname'") cellrange(A`lb':O`ub') firstrow clear
 } 
 else {
  import excel "$base_in\BASE DE DATOS MINERIA EN COLOMBIA (ANM)\Punto 2 Produccion oro 2004 a 2014\MUNICIPIOS REPORTADOS COMO PRODUCTORES_LIQUIDACION_REGALIAS 2005 a May 2015.xls", ///
 sheet("`sheetname'") cellrange(A`lb':N`ub') firstrow clear
 }
 
 if (`sheetcount'==6 | `sheetcount'==12 | `sheetcount'==41) {
 destring CODIGOAPLICACIÓNPORCENTAJED, force replace
 }
 
  local vartype: type FECHACOMPRA
if substr("`vartype'",1,3)=="str" {
 gen date2 = date( FECHACOMPRA , "DM20Y")
format date2 %tddd-Mon-YY
drop FECHACOMPRA
rename date2 FECHACOMPRA
}
 
 if (`sheetcount'==14 | `sheetcount'==22) {
 rename NIT CEDULAONITVENDEDOR
 }
 local vartype: type CEDULAONITVENDEDOR
if substr("`vartype'",1,3)=="str" {
replace CEDULAONITVENDEDOR=subinstr(CEDULAONITVENDEDOR,",","",.)
replace CEDULAONITVENDEDOR=subinstr(CEDULAONITVENDEDOR,".","",.)
gen guionpos=strpos(CEDULAONITVENDEDOR,"-")
replace guionpos=length(CEDULAONITVENDEDOR)+1 if guionpos==0
gen CEDULAONITPGUION=substr(CEDULAONITVENDEDOR,guionpos+1,.) if guionpos~=(length(CEDULAONITVENDEDOR)+1)
replace CEDULAONITVENDEDOR=substr(CEDULAONITVENDEDOR,1,guionpos-1)
drop guionpos
destring CEDULAONITVENDEDOR, force replace
destring CEDULAONITPGUION, force replace
}
  
  
  if (`sheetcount'==14 | `sheetcount'==22) {
  rename NoTitulo TITULOMINERO
  rename Sintitulo SINTITULOMINERO
  rename ORO ORO_GRAMOS
  rename PLATA PLATA_GRAMOS
  rename PLATINO PLATINO_GRAMOS
  }
  else {
    
  rename GRAMOSPUROS ORO_GRAMOS
    if (`sheetcount'==4 | `sheetcount'==6 | `sheetcount'==20 | `sheetcount'==21 | `sheetcount'==22 | `sheetcount'==24 | `sheetcount'>=26 ) {
	rename E SINTITULOMINERO
  rename I PLATA_GRAMOS
  rename J PLATINO_GRAMOS
  } 
  else {
   rename F SINTITULOMINERO
  rename J PLATA_GRAMOS
  rename K PLATINO_GRAMOS
  if (`sheetcount'~=5 & `sheetcount'~=23) {
  drop PER*
  }
  else {
  drop A
  }
  }
  }
  
  
  foreach x in ORO PLATA PLATINO {
  local vartype: type `x'_GRAMOS
if substr("`vartype'",1,3)=="str" {
destring `x', force replace
}
 } 
 
   foreach x in ORO PLATA PLATINO {
  local vartype: type REGALIA`x'
if substr("`vartype'",1,3)=="str" {
destring REGALIA`x', force replace
}
 } 
 
 local vartype: type SINTITULOMINERO
if substr("`vartype'",1,3)~="str" {
gen temp=string(SINTITULOMINERO)
drop SINTITULOMINERO
rename temp SINTITULOMINERO
}

 local vartype: type TITULOMINERO
if substr("`vartype'",1,3)~="str" {
gen temp=string(TITULOMINERO)
drop TITULOMINERO
rename temp TITULOMINERO
}

 gen comercializadora="`sheetname'"
 save "$base_out\Temporary\compraventa`sheetcount'.dta", replace
 }
 local sheetcount=`sheetcount'+1
}
*/
use "$base_out\Temporary\compraventa1.dta", clear
qui forval isheet =2/42 {
append using "$base_out\Temporary\compraventa`isheet'.dta"
}

**Fix muni names to merge codane

rename MUNICIPIODEPROCEDENCIA municipio
rename DEPARTAMENTODEPROCEDENCIA depto

qui foreach x of varlist depto municipio {
	local unos "á é í ó ú ñ ò Ã¡ Ã© Ã­ Ã³ Ãº Ã± Ã Ã Ã Ã Ã Ã Ã¼ Ã"
	local doss "a e i o u n o a e i o u n a e i o u n u u"
	local n=wordcount("`unos'")
forvalue i = 1/`n' {   // Loop to replace accents from Spanish
	local a : word `i' of `unos'
	local b : word `i' of `doss'
	replace `x'=subinstr(`x',"`a'","`b'",.)
}
}


qui foreach x of varlist depto municipio {
    replace `x'=upper(`x')
	replace `x'=subinstr(`x'," ","",.)
	replace `x'=subinstr(`x',".","",.)
	local unos "Á É Í Ó Ñ ¥ À Ì Ò Ü Û š"
	local doss "A E I O N N A I O U U U"
	local n=wordcount("`unos'")
forvalue i = 1/`n' {   // Loop to replace accents from Spanish
	local a : word `i' of `unos'
	local b : word `i' of `doss'
	replace `x'=subinstr(`x',"`a'","`b'",.)
}
}

replace depto="CHOCO" if depto=="CHO" | depto=="CHIOCO"
replace depto="BOLIVAR" if depto=="BOL" | depto=="BOLIBAR" | depto=="SURDEBOLIVAR"
replace depto="ANTIOQUIA" if depto=="ANT" | depto=="ANTIOQUA" | depto=="ANTIOQUIA}" | depto=="ANTIIQUIA" | depto=="ATIOQUIA" | depto=="ANTIQUIA" | municipio=="MACEO" 
replace depto="NARINO" if depto=="NARIÂ¥O" | depto=="NARI¥O" | depto=="NAR" | depto=="NARIÂNO" | municipio=="PIZARRO"
replace depto="BOGOTA,D.C." if depto=="BOGOTADC"
replace depto="VALLEDELCAUCA" if depto=="VALLE" | depto=="VALLEDELCAUCADELCAUCA" | depto=="VALLEDELCAUDA" | municipio=="BUENAVENTURA"
replace depto="CALDAS" if depto=="CAL" | municipio=="RISARALDA"
replace depto="CAUCA" if depto=="CAU" | municipio=="GUAPI" | municipio=="LOPEZDEMICAY" | municipio=="POPAYAN" | municipio=="PUERTOTEJADA" | municipio=="SANTANDERDEQUILICHAO"
replace depto="CAQUETA" if depto=="CAQUETA(AMAZONAS)"
replace depto="CORDOBA" if depto=="CORDABA" | depto=="CORDODA" | depto=="CORDODBA" | depto=="CORODBA" | depto=="CORODOBA"  | depto=="COR"
replace depto="CUNDINAMARCA" if depto=="CUN"
replace depto="GUAINIA" if depto=="GUA"
replace depto="GUAVIARE" if depto=="GVI"
replace depto="TOLIMA" if depto=="TOL"
replace depto="VAUPES" if depto=="VAUPEZ"
replace depto="NORTEDESANTANDER" if depto=="NORTESANTANDER" | depto=="NTEDESANTANDER"
drop if depto=="ABR"
drop if municipio==""
drop if municipio=="MARMATO" & depto=="SEGOVIA"
drop if municipio=="ZARAGOZA-BAGRE"
drop if municipio=="ANTIOQUIA"

replace depto="ANTIOQUIA" if municipio=="VEGACHI" | municipio=="NECHI" | municipio=="ZARAGOZA" | municipio=="CAUCASIA" | municipio=="CACERES" | municipio=="ELBAGRE" /// 
                            | municipio=="REMEDIOS" | municipio=="SEGOVIA" | municipio=="TARAZA"
replace depto="ANTIOQUIA" if municipio=="SABANALARGA" & depto==""					
replace depto="BOLIVAR" if municipio=="TIQUISIO" | municipio=="SIMITI" | municipio=="SANTAROSADELSUR"
replace depto="CHOCO" if municipio=="UNIONPANAMERICANA" | municipio=="TADO" | municipio=="ISTMINA"  | municipio=="CONDOTO" | municipio=="QUIBDO"
replace depto="RISARALDA" if municipio=="QUINCHIA"
replace depto="META" if municipio=="PUERTORICO"
replace depto="CORDOBA" if municipio=="AYAPEL" | municipio=="PUERTOLIBERTADOR" | municipio=="SANJOSEDEURE"
replace depto="PUTUMAYO" if municipio=="PUERTOLEGIZAMON"
replace depto="QUINDIO" if municipio=="RISARALDA"
replace depto="SANTANDER" if municipio=="SANMIGUEL"
replace municipio="LEGUIZAMO" if municipio=="PUERTOLEGIZAMON"
replace municipio="PUERTOARICA" if municipio=="ARICA"
*ANTIGUO NOMBRE DE YONDO
replace municipio="YONDO" if municipio=="CASABE"
replace municipio="AMALFI" if municipio=="AMALFI}"
replace municipio="MARINILLA" if municipio=="MARILILLA"
replace municipio="CANASGORDAS" if municipio=="CANASGORD" | municipio=="GANASGORDAS" | municipio=="CANASGPRDAS"
replace municipio="ELBAGRE" if municipio=="BAGRE"
replace municipio="ALBAN" if municipio=="ALBAN(SANJOSE)"
replace municipio="ALTOBAUDO" if municipio=="ALTOBAUDO(PIEDEP" | municipio=="ALTOVAUDO"
replace municipio="ALTOSDELROSARIO" if municipio=="ALTOSDEL"
replace municipio="ANORI" if municipio=="ANORY"
replace municipio="LAAPARTADA" if municipio=="APARTADA"
replace municipio="ARGELIA" if municipio=="ARGELIA-C" | municipio=="ARGELIA-V"
replace municipio="ATRATO" if municipio=="ATRATO(YUTO)" | municipio=="ATRATO-YUTO" | municipio=="ATRATOYUTO" | municipio=="ATRATOCHOCO"
replace municipio="ANSERMA" if municipio=="ANCERMA"
replace municipio="ELBANCO" if municipio=="BANCO"
replace municipio="BARBACOAS" if municipio=="BARBACOA"
replace municipio="BARRANCABERMEJA" if municipio=="BARRANCAB"
replace municipio="BARRANCODELOBA" if municipio=="BARRANCADELOBA"
replace municipio="BARRANQUILLA" if municipio=="BARRANQUI"
replace municipio="BELENDEUMBRIA" if municipio=="BELEM" | municipio=="BELENDE" | municipio=="BELENDEUNGRIA"
replace municipio="VETAS" if municipio=="BETAS"
replace municipio="BOGOTA,D.C." if municipio=="BOGOTA" | municipio=="BOGOTADC"
replace depto="BOGOTA,D.C." if municipio=="BOGOTA,D.C."
replace municipio="BOLIVAR" if municipio=="BOLIVAR-C"
replace municipio="BUENAVENTURA" if municipio=="BUENAVENT" | municipio=="BUENAVENTUTA"
replace municipio="BUENOSAIRES" if municipio=="BUENOSAIRESCAUCA" | municipio=="BUENOSAI"
replace municipio="BUGALAGRANDE" if municipio=="BUGA"
replace municipio="CAJAMARCA" if municipio=="CAJAMARACA"
replace municipio="CALIFORNIA" if municipio=="CALIFORNI"
replace municipio="CALIMA" if municipio=="CALIMA(DARIEN)" | municipio=="CALIMA-DARIEN" | municipio=="CALIMADARIEN"
replace municipio="ELCANTONDELSANPABLO" if municipio=="CANTANDESANPABLO" | municipio=="CANTON" | municipio=="CANTONDASANPABLO" | municipio=="CARTONDESANPABLO" ///
                                        | municipio=="CANTONDE" | municipio=="CANTONDELSANPABLO" | municipio=="CANTONDESANPABLO" ///
										| municipio=="CANTONSANPABLO" | municipio=="CANTONDESANPABLO(MANAGRU)" | municipio=="CANTONDESANPLABLO" | municipio=="ELCANTONDESANPABLO" ///
										  | municipio=="MANAGRU" | municipio=="MANAGRU-CANTONDESANPABLO"  | municipio=="MANAGRUCANTONDESANPABLO" | municipio=="MANARUCANTONDESANPABLO"
replace municipio="CARMENDELDARIEN" if municipio=="CARMENDE" | municipio=="CARMENDELATRATO"
replace municipio="CAROLINA" if municipio=="CAROLINADELPRINCIPE"
replace municipio="CASABIANCA" if municipio=="CASABLANCA"
replace municipio="CACERES" if municipio=="CASERES"
replace municipio="CAUCASIA" if municipio=="CAUCACIA" | municipio=="CUCASIA"
replace municipio="CERTEGUI" if municipio=="CERETEGUI" | municipio=="CERTERGUI" | municipio=="CERTGUI" | municipio=="CERTIGUI"
replace municipio="ELCERRITO" if municipio=="CERRITO"
replace municipio="SIPI" if municipio=="CIPI"
replace municipio="CISNEROS" if municipio=="CISNERO"
replace municipio="CUMBITARA" if municipio=="COMBITARA" | municipio=="CONVITARA"
replace municipio="CONDOTO" if municipio=="COONDOTO"
replace municipio="COPACABANA" if municipio=="COPACABAN"
replace municipio="CURILLO" if municipio=="CURRILLO"
replace municipio="RIOIRO" if municipio=="DELRIOIRO"
replace municipio="LOSANDES" if municipio=="LOSANDES(SOTOMAYOR"
replace municipio="OLAYAHERRERA" if municipio=="OLAYAHERRERA(BOCAS"
replace municipio="PASTO" if municipio=="PASTO(SANJUANDEP"
replace municipio="QUIBDO" if municipio=="QUIBDO(SANFRANCISC"
replace municipio="ROBERTOPAYAN" if municipio=="ROBERTOPAYAN(SANJ"
replace municipio="SANTABARBARA" if municipio=="SANTABARBARA(ISCUA"
replace municipio="SANTACRUZ" if municipio=="SANTACRUZ(GUACHAVE"
replace municipio="DONMATIAS" if municipio=="DONMATIA"
replace municipio="LADORADA" if municipio=="DORADA"
replace municipio="ELDOVIO" if municipio=="DOVIO"
replace municipio="ATRATO" if municipio=="ELATRATO"
replace municipio="PATIA" if municipio=="ELBORDOPATIA" | municipio=="ELBORDO"
replace municipio="ELCARMENDEATRATO" if municipio=="ELCARMEN"
replace municipio="JARDIN" if municipio=="ELJARDIN"
replace municipio="RETIRO" if municipio=="ELRETIRO"
replace municipio="PUERTOBERRIO" if municipio=="ELVAPORPUERTOBERRIO" | municipio=="PURTOBERRIO"
replace municipio="ENVIGADO" if municipio=="ENVIGADA"
replace municipio="LAESTRELLA" if municipio=="ESTRELLA"
replace municipio="FALAN" if municipio=="FALAM"
replace municipio="FILADELFIA" if municipio=="FILADELDIA"
replace municipio="ROSAS" if municipio=="FONDAS"
replace municipio="VILLAGARZON" if municipio=="GARZON" & depto=="PUTUMAYO"
replace municipio="GUAPI" if municipio=="GUAPICAUCA"
replace municipio="UNGUIA" if municipio=="HUNGUIA"
replace municipio="RIOIRO" if municipio=="IRO"
replace municipio="SANTABARBARA" if municipio=="ISCUANDE" | municipio=="ISCUANDE(SANTABARBARA)" | municipio=="ISCUANDENARINO"
replace municipio="ISTMINA" if municipio=="ITSMINA" | municipio=="ISTIMA" | municipio=="ISMINA" | municipio=="MUNICIPIODEISTMINA"
replace municipio="LAPINTADA" if municipio=="LAPINTAD"
replace municipio="VICTORIA" if municipio=="LAVICTORIA"
replace municipio="LEGUIZAMO" if municipio=="LATAGUA"
replace municipio="LOPEZ" if municipio=="LOPEZDEMICAY" | municipio=="LOPEZ(MICAY)" 
replace municipio="LOSANDES" if municipio=="LOSANDES(CMSOTOMAYOR)" | municipio=="LOSANDESSOTOMAYOR" | municipio=="SOTOMAYOR"
replace municipio="MEDIOATRATO" if municipio=="MEDIOATRATO" | municipio=="MEDIOARATO" | municipio=="MEDIOATR" | municipio=="MEDIOATRTO"  | municipio=="MEDATRATO"
replace municipio="MEDIOBAUDO" if municipio=="MEDIOBAU" | municipio=="MEDIOBAUD="
replace municipio="MEDIOSANJUAN" if municipio=="MEDIOSANJUAN(ANDAGOLLA)" | municipio=="MEDIOSANJUAN(ANDAGOYA)"
replace municipio="MARMATO" if municipio=="MAMATO"
replace municipio="MANIZALES" if municipio=="MANIZALEZ"
replace municipio="SANTANDERDEQUILICHAO" if municipio=="MONDOMO"
replace municipio="MONTECRISTO" if municipio=="MONTERISTO"
replace municipio="NOVITA" if municipio=="MOVITA"
replace municipio="CANASGORDAS" if municipio=="MUNICIPIODECANASGORDAS"
replace municipio="LLORO" if municipio=="MUNICIPIODELLORO"
replace municipio="RIOQUITO" if municipio=="PAIMADO-RIOQUITO" | municipio=="RIO-QUITO" | municipio=="PAIMADORIOQUITO" | municipio=="PAIMADO" ///
                               | municipio=="RIOQUITO(PAIMADO)" | municipio=="RIOQUITO(PAIMADOESCABECERA)" | municipio=="RIOQUITOPAIMADO"
replace municipio="PATIA" if municipio=="PATIA(ELBORDO)" | municipio=="PATIA-C" | municipio=="PATIA-ELBORDO"
replace municipio="NOROSI" if municipio=="NORISI"
replace municipio="LAPINTADA" if municipio=="PINTADA"
replace municipio="TIQUISIO" if municipio=="PIQUISIO"
replace municipio="FRANCISCOPIZARRO" if municipio=="PIZARRO"
replace municipio="PLANETARICA" if municipio=="PLANETAR"
replace municipio="PUERTOLIBERTADOR" if municipio=="PLIBERTADOR"
replace municipio="INIRIDA" if municipio=="PTOINIRIDA" | municipio=="PTOIRINIDA"
replace municipio="LEGUIZAMO" if municipio=="PTOLEGUIZAMO" | municipio=="PUERTOLE" | municipio=="PUERTOLEGUISAMO" | municipio=="PUERTOLEGUIZAMO"  | municipio=="PUERTOLEGUIZAMO(LATAGUA)"
replace municipio="" if municipio=="" | municipio==""
replace municipio="SANTODOMINGO" if municipio=="PORCE"
replace municipio="PUERTOGUZMAN" if municipio=="PTOGUZMAN"
replace municipio="PUERTOBERRIO" if municipio=="PUERTOBE"
replace municipio="PUERTONARE" if municipio=="PUERTONARE(LASIERRA)"
replace municipio="PUERTORICO" if municipio=="PUERTORICOSURDEBOLIVAR"
replace municipio="QUIBDO" if municipio=="QUIBD0"
replace municipio="REMEDIOS" if municipio=="REMDIOS" | municipio=="REMERDIOS" | municipio=="RENMEDIOS"
replace municipio="ROBERTOPAYAN" if municipio=="ROBEROPAYAN" | municipio=="ROBERTO-PAYAN" | municipio=="ROBERTOP" 
replace municipio="" if municipio=="" | municipio==""
replace municipio="ELENCANTO" if municipio=="REMANSO"
replace municipio="RIONEGRO" if municipio=="RIONEGRO(ANT)"
replace municipio="SANJOSEDELGUAVIARE" if municipio=="SANJOSE" & depto=="GUAVIARE"
replace municipio="SANJOSEDELPALMAR" if (municipio=="SANJOSE" & depto=="CHOCO") | municipio=="SNJOSEDELPALMAR"
replace municipio="SANMARTINDELOBA" if municipio=="SANMARTI" | municipio=="SMARTINLOBA"
replace municipio="SANPABLO" if municipio=="SANPABLOSURDEBOLIVAR"
replace municipio="SANTAROSA" if municipio=="SANROSADELSUR" | municipio=="STAROSASURBOLIVAR" | municipio=="STAROSADELSUR" | municipio=="STAROSA" | municipio=="SANROSASURBOLIVAR"
replace municipio="SANTAISABEL" if municipio=="SANTA-ISABEL"
replace municipio="CALI" if municipio=="SANTIAGODECALI"
replace municipio="SANTODOMINGO" if municipio=="SANTODOMING0"
replace municipio="SEVILLA" if municipio=="SEVILLA-V"
replace municipio="SIBUNDOY" if municipio=="SIBUNDAY"
replace municipio="SANTACRUZ" if municipio=="SANTACRUZ(GUACHAVESCM)" | municipio=="SANTACRUZGUACHAVEZ"
replace municipio="SANTANDERDEQUILICHAO" if municipio=="SANTANDERDEQUILICAHO" | municipio=="SANTANDERDEQ" | municipio=="SANTANDERDEQUILICH" ///
                                           |  municipio=="SANTANDERDEQUILICHAO" | municipio=="SANTANDERDEQUILIQUICHAO" | municipio=="SANTANDERQUILICHAO" | municipio=="SANTANDERDE"
replace municipio="SIMITI" if municipio=="SIMIT" | municipio=="SIMTI"
replace municipio="LASIERRA" if municipio=="SIERRA"
replace municipio="SIPI" if municipio=="SIP"
replace municipio="TARAIRA" if municipio=="TORAIRA"
replace municipio="TARAZA" if municipio=="TRAZA"
replace municipio="SANANDRESDETUMACO" if municipio=="TUMACO"
replace municipio="UNIONPANAMERICANA" if strpos(municipio,"UNION") & depto=="CHOCO"
replace municipio="ZARAGOZA" if municipio=="ZARAGOSA" | municipio=="SARAGOZA"
replace municipio="SEGOVIA" if municipio=="SEGOVIA**" | municipio=="SEGOVIA-ANTIOQUIA"
replace municipio="TADO" if municipio=="TAIMADO" | municipio=="TAMAIDO" | municipio=="TAOD"
replace municipio="SANJOSEDEURE" if municipio=="URE" 
replace municipio="VALLEDESANJUAN" if municipio=="VALLESANJUAN"
replace municipio="YALI" if municipio=="YALY"
replace municipio="LLORO" if municipio=="YORO"
replace municipio="ATRATO" if municipio=="YUTO"
replace municipio="TIMBIQUI" if municipio=="TIMBIQI" | municipio=="TIMBIUI" | municipio=="TIMBIQUE"
replace municipio="VILLAMARIA" if municipio=="VALLAMARIA" | municipio=="VILLAMARIACALDAS"
replace municipio="VETAS" if municipio=="VE" | municipio=="VETA"
replace municipio="LALLANADA" if municipio=="LALLANAD"
replace municipio="QUIBDO" if municipio=="QUIBO"
replace municipio="SANTAROSA" if municipio=="SANTAROSA-C"
replace municipio="SANTABARBARA" if municipio=="SANABRIA-MPIOSTABARBARA"
drop if municipio=="CAUCA" & depto=="CAUCA"
replace municipio="SANTANDERDEQUILICHAO" if municipio=="SANTANDER"
replace municipio="ELCANTONDELSANPABLO" if municipio=="ELCANTONDELS"
replace municipio="SANCARLOS" if municipio=="SANCARLO"
replace municipio="SANRAFAEL" if municipio=="SANRAFAE"
replace depto="BOGOTA,D.C." if municipio=="SANTAFEDEBOGOTA"
replace municipio="BOGOTA,D.C." if municipio=="SANTAFEDEBOGOTA"
replace municipio="SOLANO" if municipio=="SOLANO(ARARACUARA)"
replace municipio="SUAREZ" if municipio=="SUAREZCAUCA"
replace municipio="SANTABARBARA" if municipio=="SANTABAR"
replace municipio="SUAREZ" if municipio=="SUARES"
replace municipio="MARMATO" if municipio=="MARMATO-CALDAS"
replace municipio="SANTABARBARA" if municipio=="SANTABARBARA-ISCUANDE"
replace municipio="SANTABARBARA" if municipio=="SANTABAR" | municipio=="SANTABARBARADEISCUANDE" | municipio=="SANTABARBARADEIZCUANDE"
replace municipio="SANTABARBARA" if municipio=="SANABRIA" | municipio=="SANTABARBARAISCUANDE"
replace municipio="CIMITARRA" if municipio=="SIMITARRA"
replace municipio="ELCANTONDELSANPABLO" if municipio=="ELCANTONDELSANPA"
replace municipio="SANTABARBARA" if municipio=="SANABRIA-MPIOSANTAB"
replace municipio="VETAS" if municipio=="VETASSANTANDER"
replace municipio="MARMATO" if municipio=="MARMATP"
replace municipio="SANTAFEDEANTIOQUIA" if municipio=="SANTAFED"
replace municipio="SANTAROSADEOSOS" if municipio=="SANTAROSA" & depto=="ANTIOQUIA"
replace municipio="SUAREZ" if municipio=="SUREZ"
replace municipio="SANTAROSADELSUR" if municipio=="SANTAROSADELSURDEBOLIVAR" | municipio=="SANTAROSASUR" | municipio=="SANTAROSARSUR" | municipio=="SANTAROSASUIR"
replace municipio="SANTABARBARA" if municipio=="SANAURIA(SANTABARBARA)"
replace municipio="PUERTOBERRIO" if municipio=="PUERTOBERRRIO"
replace municipio="SANTAROSADELSUR" if municipio=="SANTAROSASURDEBOLIVAR"
replace municipio="INIRIDA" if municipio=="PUERTOINIRIDA"
replace municipio="PUERTONARE" if municipio=="NARE"
replace municipio="MANZANARES" if municipio=="MANZANARE"
replace municipio="ELTAMBO" if municipio=="TAMBO"
replace municipio="SANANDRESDECUERQUIA" if municipio=="SANANDRES" & depto=="ANTIOQUIA"
replace municipio="SANTAROSADEOSOS" if municipio=="SANTAROS" & depto=="ANTIOQUIA"
replace municipio="SANJERONIMO" if municipio=="SANJERON"
replace municipio="MAGUI" if municipio=="MAGUI(PAYAN)" | municipio=="MAGUI-PAYAN" | municipio=="MAGUIPAYAN"
replace municipio="" if municipio==""
replace municipio="" if municipio==""
replace municipio="" if municipio==""

merge m:1 depto municipio using "$base_out\Temporary\CodigosDANEMAYUSC.dta"
keep if _merge==3
drop _merge

gen persona=1 if CEDULAONITVENDEDOR < 8*10^8 | ( CEDULAONITVENDEDOR > 9.1*10^8 & CEDULAONITVENDEDOR < 1.7*10^9)
replace persona=0 if persona==.

drop if CEDULAONITVENDEDOR==0
save "$base_out\Temporary\compraventas_all.dta", replace
/*
preserve
keep if persona==1
collapse ORO_GRAMOS, by( CEDULAONITVENDEDOR )

keep CEDULAONITVENDEDOR
export delimited using "$base_out\gold_sellers_cedulas.csv", replace
restore
