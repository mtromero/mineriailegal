use "$base_out/panel_final.dta", clear
append using "$base_out/Temporary/panel_prillegal_Peru1516.dta"
append using "$base_out/Temporary/panel_prillegal1516t14.dta"

replace country="Col" if country==""

replace ind_col=(country=="Col")
replace after_x_col=(ind_col==1 & ano>=2011)
replace ind_after=1 if ano>=2011





preserve
collapse (mean) propmined_illegal, by(ano ind_col)
drop if ano==.
label val ano
*drop if ano>=2015
label var propmined_illegal "% of mined area mined illegaly"
twoway (line propmined_illegal ano if ind_col==1, sort lw(thick)) ///
     (line propmined_illegal ano if ind_col==0, sort lw(thick)), ///
	  xline(2010) legend(label(1 "Colombia") label(2 "Peru")) legend(on) xlabel(2004(2)2016) 
graph export "$latexslides/partrends_colperu_raw_1517.pdf", replace
graph export "$latexpaper/partrends_colperu_raw_1517.pdf", replace
restore


preserve
fvset base 2010 ano
char ano[omit] 2010
label var ind_col "Colombia"
eststo clear
eststo parallel_1: reghdfe propmined_illegal c.ind_col##b2010.ano   , absorb(codmpio) vce(cluster codmpio)

estout parallel_1 using "$latexpaper/partrends_colperu_table_1517.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( *.ano#c.ind_col ) prefoot(\midrule ) stats(N N_clust r2 , fmt(%9.2gc %9.2gc a2 ) labels ("N. of obs." "Municipalities" "\$R^2\$" )) replace

tab ano, gen(I)
forvalues i = 1(1)13{
gen int_`i' = ind_col * I`i'
}
gen base = 0

global dynamic int_1 int_2 int_3 int_4 int_5 int_6 base int_8 int_9 int_10 int_11 int_12 int_13 /* int_14*/

reghdfe propmined_illegal $dynamic   , absorb(codmpio ano) vce(cluster codmpio)

coefplot, graphregion(color(white)) baselevels omitted  keep($dynamic) xtitle("Years") ytitle("Coefficient") ci vertical yline(0) xline(7) rename( int_1=2004 ///
int_2=2005 ///
int_3=2006 ///
int_4=2007 ///
int_5=2008 ///
int_6=2009 ///
base=2010 ///
int_8=2011 ///
int_9=2012 ///
int_10=2013 ///
int_11=2014 ///
int_12=2015 ///
int_13=2016)



graph export "$latexslides/partrends_colperu_1517.pdf", replace
graph export "$latexpaper/partrends_colperu_1517.pdf", replace

restore
