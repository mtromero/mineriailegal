qui import excel "$base_in\SGR\Proyectos_15082016.xlsx", sheet("TOTAL PROYECTOS") cellrange(A10:AA10102) firstrow clear

qui rename CÓDIGODANEENTEEJECUTOR codmpio
qui keep if mod(codmpio,1000)!=0

qui gen lumpy=0

qui replace lumpy=1 if SUBSECTOR=="Agricultura - Distritos De Riego"
qui replace lumpy=1 if SUBSECTOR=="Agricultura - Proyectos de Desarrollo Rural"
**One could argue that can do sewage for less houses
qui replace lumpy=1 if SUBSECTOR=="AGUA POTABLE Y SANEAMIENTO BASICO - Acueducto alcantarillado y plantas de tratamiento"
qui replace lumpy=1 if SUBSECTOR=="AGUA POTABLE Y SANEAMIENTO BASICO - Residuos solidos"
** The one below sounds weird, but is building contention walls, red cross building, fire stations
qui replace lumpy=1 if SUBSECTOR=="Medio Ambiente y Riesgo  - Atención de desastres"
** Need to discriminate deeper Medio Ambiente y Riesgo  - Control de la contaminación y manejo de residuos
** There are 120 stoves but also landfill
qui replace lumpy=1 if SUBSECTOR=="Medio Ambiente y Riesgo  - Gestión integral de las aguas nacionales"
qui replace lumpy=1 if SUBSECTOR=="Comercio, Industria y Turismo - Fondo del Turismo"
** Below coliseum
qui replace lumpy=1 if SUBSECTOR=="CULTURA, DEPORTE Y RECREACION"
qui replace lumpy=1 if SUBSECTOR=="Deporte  - Infraestructura deportiva"
qui replace lumpy=1 if SUBSECTOR=="Desarrollo Social - Infraestructura social y comunitaria"
qui replace lumpy=1 if SUBSECTOR=="Minas y Energía - Energía Eléctrica - Generación Zonas No Interconectadas"
qui replace lumpy=1 if SUBSECTOR=="Minas y Energía - Energía Eléctrica - Transmisión (> 220 KV)"
qui replace lumpy=1 if SECTOR=="TRANSPORTE"




qui replace lumpy=0 if strpos(NOMBREDELPROYECTO,"ESTUDIO")
qui replace lumpy=1 if strpos(NOMBREDELPROYECTO,"CONSTRUCCIÓN")
* ADECUACIÓN  is also Lumpy?

*I do this because lot of CONTRUCCION 40 ESTUFAS, 120 CASAS
qui replace lumpy=0 if SUBSECTOR=="VIVIENDA - Vivienda rural"
qui replace lumpy=0 if SUBSECTOR=="VIVIENDA - Vivienda urbana"

qui gen cost= AA/10^6
qui gen lumpy_cost=cost*(lumpy==1)
qui collapse (sum) cost lumpy_cost, by(codmpio)
qui gen lumpy_share=100* lumpy_cost/ cost

qui keep codmpio lumpy_share
qui save "$base_out/Temporary/lumpy_share.dta"
