
*** Check what is the trimester with less clouds
import delimited C:\Users\santi\Dropbox\CloudsMauricio\Inventario2003_2014_Menos80PctNubes.csv, clear

gen pr= wrspath*1000+ wrsrow
drop if pr<3057 | pr==3060 | pr==3062 | pr==3063 | pr==4055 | pr==5063 | pr==7053 | pr==10052 | pr==10060
gen mes=substr( dateacquired,1,strpos( dateacquired,"/")-1)
gen tempdiaano=substr( dateacquired,strpos( dateacquired,"/")+1,.)
gen dia=substr( tempdiaano,1,strpos( tempdiaano,"/")-1)
gen ano=substr( tempdiaano,strpos( tempdiaano,"/")+1,.)
destring dia mes ano, force replace

forval i=1/12 {
if `i'<=10 {
egen avgcf`i'=mean(cloudcover) if mes>=`i' & mes<=(`i'+2), by(pr) 
} 
if `i'==11 {
egen avgcf`i'=mean(cloudcover) if (mes>=`i' & mes<=(`i'+1)) | mes==1, by(pr) 
}
if `i'==12 {
egen avgcf`i'=mean(cloudcover) if (mes>=1 & mes<=2) | mes==12, by(pr) 
}

}

preserve
collapse avgcf1-avgcf12, by(pr)
egen minccpr=rowmin(avgcf*)
gen check=minccpr- avgcf12
restore


**I am excluding the PR we know caused problems when cleaning clouds 9 months ago
import delimited C:\Users\santi\Dropbox\MineriaIlegal\DataNubes\copy\Escenas2003_2016_Dec_Jan_Feb.csv, clear 
gen pr= wrspath*1000+ wrsrow
drop if pr<3057 | pr==3060 | pr==3062 | pr==3063 | pr==4055 | pr==5063 | pr==6052  | pr==7053 | pr==7062 | pr==8061| pr==10052 | pr==10060 | pr==11058
drop pr
export delimited using "C:\Users\santi\Dropbox\MineriaIlegal\DataNubes\copy\Escenas2003_2016_Dec_Jan_Feb_filt.csv", replace
