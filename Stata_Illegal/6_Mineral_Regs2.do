use "C:\Users\santiago.saavedrap\Dropbox\MineriaIlegal\CreatedData\Temporary\hist_prices_long2.dta", clear
rename year ano
rename variable mineral
rename value precio
replace mineral=12 if mineral==13
collapse precio, by(mineral ano)

keep if ano<2015
gen regalia_mine=0.05
replace regalia_mine=0.06 if mineral==1
replace regalia_mine=0.03 if mineral==7
replace regalia_mine=0.12 if mineral==8
replace regalia_mine=0.10 if mineral==11
replace regalia_mine=0.01 if mineral==12
save "C:\Users\santiago.saavedrap\Dropbox\MineriaIlegal\CreatedData\Temporary\precios_mineral2.dta", replace

use "$base_out/Temporary/panel_prillegalMi.dta", clear
gen afterxgold=(ano>2011)*(mineral==1)

gen munitime= codmpio*10000+ ano
gen minetime=codmpio*10000+ mineral
merge m:1 mineral ano using "C:\Users\santiago.saavedrap\Dropbox\MineriaIlegal\CreatedData\Temporary\precios_mineral2.dta"

gen afterxregalia=(ano>2011)*regalia_mine
gen munimine= codmpio*10+ mineral
reghdfe propmined_illegal_mi afterxregalia precio , absorb(munitime mineral) vce(cluster codmpio)
