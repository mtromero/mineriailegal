import delimited "$base_out\Predicciones\Rf10onlymlOSM_ecopot_sindisap\area_count_disagg\2014Rf10onlymlOSMecopotarea_count_ntit.csv", clear
destring codane2, force replace
keep departamen municipio codane2
save "$base_out\Temporary\Codigos_Dane_areacount_ntit.dta", replace

qui forval i=2004/2014 {
import delimited "$base_out/Predicciones/Rf10onlymlOSM_ecopot_sindisap/area_count_disagg/`i'Rf10onlymlOSMecopot_mine_disagg.csv", clear
merge m:1 departamen municipio using "$base_out\Temporary\Codigos_Dane_areacount_ntit.dta"
rename codane2 codmpio
keep if legal=="FALSE"
collapse (max) area , by(codmpio)
gen ano=`i'
save "$base_out\Temporary\kmax_`i'.dta", replace
}


qui forval i=2004/2013 {
append using "$base_out\Temporary\kmax_`i'.dta"
}

rename area kstar_illegal

save "$base_out\Temporary\kstar_illegal.dta", replace
